﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("PAIS")]
    public class PaisGS:MasterClass
    {

        #region Atributos

        
        private int _Codigo;
        
        private string _Nome;
        private string _Sigla;
        
        private bool _Ativo;

        private DateTime _DataRegistro;
        private DateTime _DataDesativacao;


        #endregion

        #region Propriedades

        

        [CampoNaTabela("CD_PAIS", DbType.Int32,true)]
        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        

        [CampoNaTabela("TX_NOME", DbType.String)]
        public String Nome
        {
            get { return _Nome; }
            set { _Nome = value; }
        }

        [CampoNaTabela("TX_PAIS", DbType.String)]
        public String Sigla
        {
            get { return _Sigla; }
            set { _Sigla = value; }
        }


        [CampoNaTabela("BL_ATIVO", DbType.Boolean)]
        public Boolean Ativo
        {
            get { return _Ativo; }
            set { _Ativo = value; }
        }


        [CampoNaTabela("DT_REGISTRO", DbType.DateTime)]
        public DateTime DataRegistro
        {
            get { return _DataRegistro; }
            set { _DataRegistro = value; }
        }

        [CampoNaTabela("DT_DESATIVACAO", DbType.DateTime)]
        public DateTime DataDesativacao
        {
            get { return _DataDesativacao; }
            set { _DataDesativacao = value; }
        }


        

        #endregion

        #region Métodos

        public static PaisGS Obtem(string pNome)
        {
            PaisGS item = new PaisGS();
            List<PaisGS> itens = new List<PaisGS>();

            try
            {
                //Avaliar o nome como passado
                foreach (PaisGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " UPPER(TX_NOME)  = '" + pNome.ToUpper() + "'"))
                {
                    itens.Add(obj);
                }

                //Não encontrou País com o nome igual, procurando com nome parecido
                if (itens.Count == 0)
                {
                    foreach (PaisGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " UPPER(TX_NOME) LIKE = '%" + pNome.ToUpper() + "%'"))
                    {
                        itens.Add(obj);
                    }

                    if (itens.Count == 0)
                    {
                        throw new Exception("País não encontrado!");
                    }
                    else if (itens.Count > 1)
                    {
                        throw new Exception("Encontrado mais de 1 País com a descrição passada!");
                    }
                    else if (itens.Count == 1)
                    {
                        item = itens[0];
                    }
                }
                else if (itens.Count > 1)
                {
                    throw new Exception("Encontrado mais de 1 País com a descrição passada!");
                }
                else if (itens.Count == 1)
                {
                    item = itens[0];
                }

            }
            catch (Exception erro)
            {
                throw new Exception("[PAIS].[OBTEM] Falha ao obter o País com a descrição " + pNome + ": " + erro.Message);
            }


            return item;
        }

        #endregion


    }
}
