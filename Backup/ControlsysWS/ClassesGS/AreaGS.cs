﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("AREA")]
    public class AreaGS:MasterClass
    {

        #region Atributos

        
        private int _Codigo;

        private string _Nome;        
        private string _Numero;

        private bool _Ativo;

        private DateTime _DataRegistro;
        private DateTime _DataDesativacao;


        #endregion

        #region Propriedades

        

        [CampoNaTabela("CD_AREA", DbType.Int32,true,true)]
        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        [CampoNaTabela("TX_NOME", DbType.String)]
        public String Nome
        {
            get { return _Nome; }
            set { _Nome = value; }
        }
        

        [CampoNaTabela("TX_NUMERO", DbType.String)]
        public String Numero
        {
            get { return _Numero; }
            set { _Numero = value; }
        }

        [CampoNaTabela("BL_ATIVO", DbType.Boolean)]
        public Boolean Ativo
        {
            get { return _Ativo; }
            set { _Ativo = value; }
        }


        [CampoNaTabela("DT_REGISTRO", DbType.DateTime)]
        public DateTime DataRegistro
        {
            get { return _DataRegistro; }
            set { _DataRegistro = value; }
        }

        [CampoNaTabela("DT_DESATIVACAO", DbType.DateTime)]
        public DateTime DataDesativacao
        {
            get { return _DataDesativacao; }
            set { _DataDesativacao = value; }
        }


        

        #endregion

        #region Métodos

        public static AreaGS Obtem(String pNumero)
        {
            bool Achou = false;
            AreaGS item = new AreaGS();

            foreach (AreaGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " TX_NUMERO = '" + pNumero + "'"))
            {
                Achou = true;
                item = obj;
                break;
            }

            if (Achou)
                return item;
            else
                return null;
                //throw new Exception("[AREAGS].[OBTEM] Registro de Área não encontrado (" + pNumero.ToString() + ")");
        }

        #endregion

    }
}
