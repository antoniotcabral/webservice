﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("COLABORADOR")]
    public class ColaboradorGS:MasterClass
    {

        #region Atributos

        private int _CodigoPapel;
        private string _CodigoEmpresa;

        private decimal _NumeroPis;
        private Decimal _Salario;

        private string _Mensalista;
        private string _Observacao;
        private string _ConselhoProfissional;
        private string _RegistroProfissional;

        private DateTime _DataAdmissao;

        private int _CodigoArea;

        #endregion

        #region Propriedades

        [CampoNaTabela("CD_PAPEL", DbType.Int32,true,false)]
        public Int32 CodigoPapel
        {
            get { return _CodigoPapel; }
            set { _CodigoPapel = value; }
        }

        [CampoNaTabela("CD_EMPRESA", DbType.String)]
        public String  CodigoEmpresa
        {
            get { return _CodigoEmpresa; }
            set { _CodigoEmpresa = value; }
        }

        [CampoNaTabela("NU_PIS", DbType.Decimal)]
        public Decimal NumeroPis
        {
            get { return _NumeroPis; }
            set { _NumeroPis = value; }
        }

        [CampoNaTabela("NU_SALARIO", DbType.Decimal)] 
        public Decimal Salario
        {
            get { return _Salario; }
            set { _Salario = value; }
        }

        [CampoNaTabela("TX_MENSALISTA", DbType.String)]
        public String Mensalista
        {
            get { return _Mensalista; }
            set { _Mensalista = value; }
        }


        [CampoNaTabela("TX_OBSERVACAO", DbType.String)]
        public String Observacao
        {
            get { return _Observacao; }
            set { _Observacao = value; }
        }

        [CampoNaTabela("TX_CONSELHOPROF", DbType.String)]
        public String ConselhoProfissional
        {
            get { return _ConselhoProfissional; }
            set { _ConselhoProfissional = value; }
        }

        [CampoNaTabela("TX_REGISTROPROF", DbType.String)]
        public String RegistroProfissional
        {
            get { return _ConselhoProfissional; }
            set { _ConselhoProfissional = value; }
        }


        [CampoNaTabela("DT_ADMISSAO", DbType.DateTime)]
        public DateTime DataAdmissao
        {
            get { return _DataAdmissao; }
            set { _DataAdmissao = value; }
        }

        [CampoNaTabela("CD_AREA", DbType.Int32)]
        public Int32 CodigoArea
        {
            get { return _CodigoArea; }
            set { _CodigoArea = value; }
        }

        #endregion

        #region Métdos

        public static ColaboradorGS Obtem(Int32 pCodigoPapel)
        {
            bool Achou = false;
            ColaboradorGS item = new ColaboradorGS();

            foreach (ColaboradorGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " CD_PAPEL = " + pCodigoPapel))
            {
                Achou = true;
                item = obj;
                break;
            }

            if (Achou)
                return item;
            else
                throw new Exception("[COLABORADORGS].[OBTEM] Registro de Pessoa não encontrado (" + pCodigoPapel.ToString() + ")");
        }


        #endregion



    }
}
