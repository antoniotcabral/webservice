﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("COLABORADOR_AGENDAMENTO")]
    public class ColaboradorAgendamentoGS:MasterClass
    {

        #region Atributos

        private int _CodigoPapel;        
        private int _CodigoGrupoTrabalho;
        //private int _CodigoSetor;

        private DateTime _DataAdmissao;

        private string _ConselhoProfissional;
        private string _Mensalista;
        private string _Observacao;
        private string _NumeroPis;


        private string _CodigoEmpresa;

        
        private Decimal _Salario;

        
        
        
        private string _RegistroProfissional;

        

        #endregion

        #region Propriedades

        [CampoNaTabela("CD_PAPEL_AGENDAMENTO", DbType.Int32,true,false)]
        public Int32 CodigoPapel
        {
            get { return _CodigoPapel; }
            set { _CodigoPapel = value; }
        }

        [CampoNaTabela("CD_GRUPO_TRABALHO", DbType.Int32)]
        public Int32 CodigoGrupoTrabalho
        {
            get { return _CodigoGrupoTrabalho; }
            set { _CodigoGrupoTrabalho = value; }
        }

        //[CampoNaTabela("CD_SETOR", DbType.Int32)]
        //public Int32 CodigoSetor
        //{
        //    get { return _CodigoSetor; }
        //    set { _CodigoSetor = value; }
        //}

        [CampoNaTabela("TX_CONSPROF", DbType.String)]
        public String ConselhoProfissional
        {
            get { return _ConselhoProfissional; }
            set { _ConselhoProfissional = value; }
        }

        [CampoNaTabela("DT_ADMISSAO", DbType.DateTime)]
        public DateTime DataAdmissao
        {
            get { return _DataAdmissao; }
            set { _DataAdmissao = value; }
        }

        [CampoNaTabela("TX_HORMENS", DbType.String)]
        public String Mensalista
        {
            get { return _Mensalista; }
            set { _Mensalista = value; }
        }

        [CampoNaTabela("TX_OBS", DbType.String)]
        public String Observacao
        {
            get { return _Observacao; }
            set { _Observacao = value; }
        }

        [CampoNaTabela("TX_PIS", DbType.String)]
        public String NumeroPis
        {
            get { return _NumeroPis; }
            set { _NumeroPis = value; }
        }

        [CampoNaTabela("TX_REGISPROF", DbType.String)]
        public String RegistroProfissional
        {
            get { return _RegistroProfissional; }
            set { _RegistroProfissional = value; }
        }

        [CampoNaTabela("DE_SAL", DbType.Decimal)]
        public Decimal Salario
        {
            get { return _Salario; }
            set { _Salario = value; }
        }


        [CampoNaTabela("CD_EMPRESA", DbType.String)]
        public String  CodigoEmpresa
        {
            get { return _CodigoEmpresa; }
            set { _CodigoEmpresa = value; }
        }

        #endregion

        #region Métodos

        public static ColaboradorAgendamentoGS Obtem(Int32 pCodigoPapelAgendamento)
        {
            ColaboradorAgendamentoGS item = new ColaboradorAgendamentoGS();
            Boolean Achou = false;


            //Avaliar o nome como passado
            foreach (ColaboradorAgendamentoGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " CD_PAPEL_AGENDAMENTO = " + pCodigoPapelAgendamento ))
            {
                Achou = true;
                item = obj;
                break;
            }

            if (Achou)
                return item;
            else
                throw new Exception("[COLABORADORAGENDAMENTO].[OBTEM] Registro de Colaborador Agendamento não encontrado (Código Papel Agendamento: " + pCodigoPapelAgendamento.ToString() + ")");
        }

        #endregion



    }
}
