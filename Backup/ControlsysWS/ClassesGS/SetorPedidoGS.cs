﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("SETOR_PEDIDO")]
    public class SetorPedidoGS:MasterClass
    {

        #region Atributos

        
        private int _Codigo;
        private int _CodigoSetor;
        private int _CodigoPedido;

        private bool _Ativo;

        private DateTime _DataRegistro;
        private DateTime _DataDesativacao;


        #endregion

        #region Propriedades

        

        [CampoNaTabela("CD_SETOR_PEDIDO", DbType.Int32,true,true)]
        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        [CampoNaTabela("CD_SETOR", DbType.Int32)]
        public Int32 CodigoSetor
        {
            get { return _CodigoSetor; }
            set { _CodigoSetor = value; }
        }

        [CampoNaTabela("CD_PEDIDO", DbType.Int32)]
        public Int32 CodigoPedido
        {
            get { return _CodigoPedido; }
            set { _CodigoPedido = value; }
        }


        [CampoNaTabela("BL_ATIVO", DbType.Boolean)]
        public Boolean Ativo
        {
            get { return _Ativo; }
            set { _Ativo = value; }
        }


        [CampoNaTabela("DT_REGISTRO", DbType.DateTime)]
        public DateTime DataRegistro
        {
            get { return _DataRegistro; }
            set { _DataRegistro = value; }
        }

        [CampoNaTabela("DT_DESATIVACAO", DbType.DateTime)]
        public DateTime DataDesativacao
        {
            get { return _DataDesativacao; }
            set { _DataDesativacao = value; }
        }


        

        #endregion

        #region Métodos

        public static SetorPedidoGS Obtem(int pCodigoPedido)
        {
            bool Achou = false;
            SetorPedidoGS item = new SetorPedidoGS();

            foreach (SetorPedidoGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " CD_PEDIDO = " + pCodigoPedido))
            {
                Achou = true;
                item = obj;
                break;
            }

            if (Achou)
                return item;
            else
                throw new Exception("[SETORPEDIDO].[OBTEM] Registro de Setor para o Pedido não encontrado (" + pCodigoPedido + ")");
        }

        public static bool ExisteSetorPedido(int pCodigoPedido)
        {
            bool Achou = false;
            SetorPedidoGS item = new SetorPedidoGS();

            foreach (SetorPedidoGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " CD_PEDIDO = '" + pCodigoPedido + "'"))
            {
                Achou = true;
                break;
            }

            return Achou;
        }

        #endregion



    }
}
