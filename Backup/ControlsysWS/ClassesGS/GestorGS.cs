﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("GESTOR")]
    public class GestorGS:MasterClass
    {

        #region Atributos

        
        private int _Codigo;       
        private int _CodigoPedido;
        private int _CodigoColaborador;

        private bool _Ativo;

        private DateTime _DataRegistro;
        private DateTime _DataDesativacao;


        #endregion

        #region Propriedades

        

        [CampoNaTabela("CD_GESTOR", DbType.Int32,true,true)]
        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        [CampoNaTabela("CD_PEDIDO", DbType.Int32)]
        public Int32 CodigoPedido
        {
            get { return _CodigoPedido; }
            set { _CodigoPedido = value; }
        }

        [CampoNaTabela("CD_COLABORADOR", DbType.Int32)]
        public Int32 CodigoColaborador
        {
            get { return _CodigoColaborador; }
            set { _CodigoColaborador = value; }
        }        

        [CampoNaTabela("BL_ATIVO", DbType.Boolean)]
        public Boolean Ativo
        {
            get { return _Ativo; }
            set { _Ativo = value; }
        }


        [CampoNaTabela("DT_REGISTRO", DbType.DateTime)]
        public DateTime DataRegistro
        {
            get { return _DataRegistro; }
            set { _DataRegistro = value; }
        }

        [CampoNaTabela("DT_DESATIVACAO", DbType.DateTime)]
        public DateTime DataDesativacao
        {
            get { return _DataDesativacao; }
            set { _DataDesativacao = value; }
        }

       

        

        #endregion

        #region Métodos

        public static GestorGS ObtemPadrao()
        {
            GestorGS item = new GestorGS();
            //List<CargoGS> itens = new List<CargoGS>();
            bool Achou = false;

            try
            {
                //Avaliar o nome como passado
                foreach (GestorGS obj in item.ObtemRegistrosFiltrados(item.GetType(), "  CD_PEDIDO  = 1 AND BL_ATIVO = 1"))
                {
                    item = obj;
                    Achou = true;
                    break;
                }



            }
            catch (Exception erro)
            {
                throw new Exception("[GESTOR].[OBTEMPADRAO] Falha ao obter o Gestor Padrão.");
            }

            if (Achou)
                return item;
            else
                return null;
        }

       

        #endregion


    }
}
