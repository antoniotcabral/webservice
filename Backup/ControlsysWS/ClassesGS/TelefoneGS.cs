﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("TELEFONE")]
    public class TelefoneGS:MasterClass
    {

        #region Atributos

        
        private int _Codigo;
        private string _CodigoPessoa;

        
        private string _Numero;
        private string _Tipo;
        
        private bool _Ativo;

        private DateTime _DataRegistro;
        private DateTime _DataDesativacao;


        #endregion

        #region Propriedades



        [CampoNaTabela("CD_TELEFONE", DbType.Int32, true, true)]
        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        [CampoNaTabela("CD_PESSOA", DbType.String)]
        public String CodigoPessoa
        {
            get { return _CodigoPessoa; }
            set { _CodigoPessoa = value; }
        }

        [CampoNaTabela("NU_TELEFONE", DbType.String)]
        public String Numero
        {
            get { return _Numero; }
            set { _Numero= value; }
        }

        [CampoNaTabela("TX_TIPO", DbType.String)]
        public String Tipo
        {
            get { return _Tipo; }
            set { _Tipo = value; }
        }


        [CampoNaTabela("BL_ATIVO", DbType.Boolean)]
        public Boolean Ativo
        {
            get { return _Ativo; }
            set { _Ativo = value; }
        }


        [CampoNaTabela("DT_REGISTRO", DbType.DateTime)]
        public DateTime DataRegistro
        {
            get { return _DataRegistro; }
            set { _DataRegistro = value; }
        }

        [CampoNaTabela("DT_DESATIVACAO", DbType.DateTime)]
        public DateTime DataDesativacao
        {
            get { return _DataDesativacao; }
            set { _DataDesativacao = value; }
        }


        

        #endregion

        #region Métodos

        public static TelefoneGS Obtem(string pCodigoPessoa, string pTipo)
        {
            TelefoneGS item = new TelefoneGS();
            List<TelefoneGS> itens = new List<TelefoneGS>();
            bool Achou = false;

            try
            {
                //Avaliar o nome como passado
                foreach (TelefoneGS obj in item.ObtemRegistrosFiltrados(item.GetType(), "  CD_PESSOA  = '" + pCodigoPessoa + "' AND TX_TIPO = '" + pTipo + "'"))
                {
                    item = obj;
                    Achou = true;
                    break;
                }

                

            }
            catch (Exception erro)
            {
                throw new Exception("[TELEFONE].[OBTEM] Falha ao obter o Telefone do Colaborador " + pCodigoPessoa + "(" + pTipo + "): " + erro.Message);
            }

            if (Achou)
                return item;
            else
                return null;
        }

        #endregion



    }
}
