using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("TREINAMENTO")]
    public class TreinamentoGS:MasterClass
    {

        #region Atributos

        
        private int _Codigo;
        private int _Dias;

        private string _Nome;        
        private string _Descricao;

        private bool _Ativo;

        private DateTime _DataRegistro;
        private DateTime _DataDesativacao;

        private string _CodigoSap;


        #endregion

        #region Propriedades



        [CampoNaTabela("CD_TREINAMENTO", DbType.Int32, true)]
        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        [CampoNaTabela("TX_NOME", DbType.String)]
        public String Nome
        {
            get { return _Nome; }
            set { _Nome = value; }
        }


        [CampoNaTabela("NU_dIAS", DbType.Int32)]
        public Int32 Dias
        {
            get { return _Dias; }
            set { _Dias = value; }
        }


        [CampoNaTabela("TX_DESCRICAO", DbType.String)]
        public String Descricao
        {
            get { return _Descricao; }
            set { _Descricao = value; }
        }

        [CampoNaTabela("BL_ATIVO", DbType.Boolean)]
        public Boolean Ativo
        {
            get { return _Ativo; }
            set { _Ativo = value; }
        }


        [CampoNaTabela("DT_REGISTRO", DbType.DateTime)]
        public DateTime DataRegistro
        {
            get { return _DataRegistro; }
            set { _DataRegistro = value; }
        }

        [CampoNaTabela("DT_DESATIVACAO", DbType.DateTime)]
        public DateTime DataDesativacao
        {
            get { return _DataDesativacao; }
            set { _DataDesativacao = value; }
        }

        [CampoNaTabela("CD_SAP", DbType.String)]
        public String CodigoSap
        {
            get { return _CodigoSap; }
            set { _CodigoSap = value; }
        }


        

        #endregion

        #region M�todos

        public static TreinamentoGS Obtem(String pCodigoSap)
        {
            bool Achou = false;
            TreinamentoGS item = new TreinamentoGS();

            foreach (TreinamentoGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " CD_SAP = '" + pCodigoSap + "'"))
            {
                Achou = true;
                item = obj;
                break;
            }

            if (Achou)
                return item;
            else
                throw new Exception("[TREINAMENTOGS].[OBTEM] Registro de Treinamento n�o encontrado (" + pCodigoSap + ")");
        }

        public static bool ExisteTreinamento(string pCodigoSap)
        {
            TreinamentoGS item = new TreinamentoGS();

            foreach (TreinamentoGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " CD_SAP = '" + pCodigoSap + "'"))
            {
                return true;
            }

            return false;

        }


        #endregion



    }
}
