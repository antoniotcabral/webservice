﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("PESSOA_FISICA")]
    public class PessoaFisicaGS:MasterClass
    {

        #region Atributos

        private string _Codigo;
        private int _CodigoNaturalidade;
        private int _RgUfCodigo;
        private int _CarteiraTrabalhoUfCodigo;
        private int _CodigoMunicipio;
        

        private string _Cpf;
        private string _Passaporte;
        private string _RgNumero;
        private string _RgOrgaoEmissao;
        private string _NomeMae;
        private string _NomePai;
        private string _TipoSanguineo;
        private string _Sexo;
        private string _Rne;
        private string _EstadoCivil;
        private string _Escolaridade;
        private string _CrNrNumero;
        private string _CrTipo;
        private string _Cnh;
        private string _CnhTipo;
        private string _CarteiraTrabalhoSerie;
        private string _Nacionalidade;

        private Decimal _TituloEleitorNumero;
        private Decimal _TituloEleitorZona;
        private Decimal _TituloEleitorSecao;
        private Decimal _CarteiraTrabalhoNumero;

        private DateTime _RgDataEmissao;
        private DateTime _DataNascimento;
        private DateTime _VistoData;
        private DateTime _VistoDataValidade;
        private DateTime _RneDataExpedicao;
        private DateTime _CnhValidade;
        private DateTime _CarteiraTrabalhoEmissao;

        

        



        #endregion

        #region Propriedades

        [CampoNaTabela("CD_PESSOA_FISICA", DbType.String,true,false)]
        public String Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        [CampoNaTabela("CD_NATURAL", DbType.Int32)]
        public Int32 CodigoNaturalidadeCidade
        {
            get { return _CodigoNaturalidade; }
            set { _CodigoNaturalidade = value; }
        }

        [CampoNaTabela("CD_RG_UF", DbType.Int32)]
        public Int32 RgCodigoUf
        {
            get { return _RgUfCodigo; }
            set { _RgUfCodigo = value; }
        }

        [CampoNaTabela("CD_CT_ID_UF", DbType.Int32)]
        public Int32 CarteiraTrabalhoCodigoUf
        {
            get { return _CarteiraTrabalhoUfCodigo; }
            set { _CarteiraTrabalhoUfCodigo = value; }
        }

        [CampoNaTabela("CD_MUNICIPIO", DbType.Int32)]
        public Int32 CodigoMunicipio
        {
            get { return _CodigoMunicipio; }
            set { _CodigoMunicipio = value; }
        }

        [CampoNaTabela("TX_CPF", DbType.String)]
        public String Cpf
        {
            get { return _Cpf; }
            set { _Cpf = value; }
        }


        [CampoNaTabela("TX_PASSAPORTE", DbType.String)]
        public String Passaporte
        {
            get { return _Passaporte; }
            set { _Passaporte = value; }
        }

        [CampoNaTabela("TX_RG", DbType.String)]
        public String RgNumero
        {
            get { return _RgNumero; }
            set { _RgNumero = value; }
        }

        [CampoNaTabela("TX_RG_ORGAO", DbType.String)]
        public String RgOrgao
        {
            get { return _RgOrgaoEmissao; }
            set { _RgOrgaoEmissao = value; }
        }

        [CampoNaTabela("TX_MAE", DbType.String)]
        public String NomeMae
        {
            get { return _NomeMae; }
            set { _NomeMae = value; }
        }

        [CampoNaTabela("TX_PAI", DbType.String)]
        public String NomePai
        {
            get { return _NomePai; }
            set { _NomePai = value; }
        }

        [CampoNaTabela("TX_SANGE", DbType.String)]
        public String TipoSanguineo
        {
            get { return _TipoSanguineo; }
            set { _TipoSanguineo = value; }
        }

        [CampoNaTabela("TX_SEXO", DbType.String)]
        public String Sexo
        {
            get { return _Sexo; }
            set { _Sexo = value; }
        }

        [CampoNaTabela("TX_RNE", DbType.String)]
        public String Rne
        {
            get { return _Rne; }
            set { _Rne = value; }
        }

        [CampoNaTabela("TX_ESTADOCIVIL", DbType.String)]
        public String EstadoCivil
        {
            get { return _EstadoCivil; }
            set { _EstadoCivil = value; }
        }

        [CampoNaTabela("TX_ESCOLARIDADE", DbType.String)]
        public String Escolaridade
        {
            get { return _Escolaridade; }
            set { _Escolaridade = value; }
        }

        [CampoNaTabela("NU_CR_NR", DbType.String)]
        public String CrNrNumero
        {
            get { return _CrNrNumero; }
            set { _CrNrNumero = value; }
        }

        [CampoNaTabela("NU_CR_TIPO", DbType.String)]
        public String CrTipo
        {
            get { return _CrTipo; }
            set { _CrTipo = value; }
        }

        [CampoNaTabela("TX_CNH", DbType.String)]
        public String Cnh
        {
            get { return _Cnh; }
            set { _Cnh = value; }
        }

        [CampoNaTabela("TX_CNH_TP", DbType.String)]
        public String CnhTipo
        {
            get { return _CnhTipo; }
            set { _CnhTipo = value; }
        }

        [CampoNaTabela("TX_CT_SERIE", DbType.String)]
        public String CarteiraTrabalhoSerie
        {
            get { return _CarteiraTrabalhoSerie; }
            set { _CarteiraTrabalhoSerie = value; }
        }

        [CampoNaTabela("TX_NACIONALIDADE", DbType.String)]
        public String Nacionalidade
        {
            get { return _Nacionalidade; }
            set { _Nacionalidade = value; }
        }

        [CampoNaTabela("NU_TE", DbType.Decimal)]
        public Decimal TituloEleitorNumero
        {
            get { return _TituloEleitorNumero; }
            set { _TituloEleitorNumero = value; }
        }

        [CampoNaTabela("NU_TE_ZONA", DbType.Decimal)]
        public Decimal TituloEleitorZona
        {
            get { return _TituloEleitorZona; }
            set { _TituloEleitorZona = value; }
        }

        [CampoNaTabela("NU_TE_SECAO", DbType.Decimal)]
        public Decimal TituloEleitorSecao
        {
            get { return _TituloEleitorSecao; }
            set { _TituloEleitorSecao = value; }
        }

        [CampoNaTabela("NU_CT", DbType.Decimal)]
        public Decimal CarteiraTrabalhoNumero
        {
            get { return _CarteiraTrabalhoNumero; }
            set { _CarteiraTrabalhoNumero = value; }
        }

        [CampoNaTabela("DT_RG_EMISSAO", DbType.DateTime)]
        public DateTime RgDataEmissao
        {
            get { return _RgDataEmissao; }
            set { _RgDataEmissao = value; }
        }

        [CampoNaTabela("DT_NASCIMENTO", DbType.DateTime)]
        public DateTime DataNascimento
        {
            get { return _DataNascimento; }
            set { _DataNascimento = value; }
        }

        [CampoNaTabela("DT_VISTO", DbType.DateTime)]
        public DateTime VistoData
        {
            get { return _VistoData; }
            set { _VistoData = value; }
        }

        [CampoNaTabela("DT_VALIDADE_VISTO", DbType.DateTime)]
        public DateTime VistoDataValidade
        {
            get { return _VistoDataValidade; }
            set { _VistoDataValidade = value; }
        }

        [CampoNaTabela("DT_EXPEDICAO_RNE", DbType.DateTime)]
        public DateTime RneDataExpedicao
        {
            get { return _RneDataExpedicao; }
            set { _RneDataExpedicao = value; }
        }

        [CampoNaTabela("DT_CNH_VALIDADE", DbType.DateTime)]
        public DateTime CnhValidade
        {
            get { return _CnhValidade; }
            set { _CnhValidade = value; }
        }

        [CampoNaTabela("DT_CT_DT", DbType.DateTime)]
        public DateTime CarteiraTrabalhoEmissao
        {
            get { return _CarteiraTrabalhoEmissao; }
            set { _CarteiraTrabalhoEmissao = value; }
        }




        

        #endregion

        #region Métodos

        public static PessoaFisicaGS Obtem(String pCpf)
        {
            bool Achou = false;
            PessoaFisicaGS item = new PessoaFisicaGS();

            foreach (PessoaFisicaGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " TX_CPF = '" + pCpf + "'"))
            {
                Achou = true;
                item = obj;
                break;
            }

            if (Achou)
                return item;
            else
                return null;
                //throw new Exception("[PESSOAFISICAGS].[OBTEM] Registro de Pessoa Física não encontrado (" + pCpf + ")");
        }

        #endregion

    }
}
