﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("TREINAMENTO_PESSOA")]
    public class TreinamentoPessoaGS:MasterClass
    {

        #region Atributos

        private int _Codigo;
        private string _CodigoPessoaFisica;
        private int _CodigoTreinamento;
        private int _CodigoPapel;

        private DateTime _Realizado;
        private DateTime _Validade;
        private DateTime _Registro;
        private DateTime _Desativacao;

        private bool _Ativo;

        
        #endregion

        #region Propriedades

        [CampoNaTabela("CD_TREINAMENTO_PESSOA", DbType.Int32,true,true)]
        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        [CampoNaTabela("CD_PESSOA_FISICA", DbType.String)]
        public String CodigoPessoaFisica
        {
            get { return _CodigoPessoaFisica; }
            set { _CodigoPessoaFisica = value; }
        }

        [CampoNaTabela("CD_TREINAMENTO", DbType.Int32)]
        public Int32 CodigoTreinamento
        {
            get { return _CodigoTreinamento; }
            set { _CodigoTreinamento = value; }
        }

        [CampoNaTabela("CD_PAPEL", DbType.Int32)]
        public Int32 CodigoPapel
        {
            get { return _CodigoPapel; }
            set { _CodigoPapel = value; }
        }

        
        [CampoNaTabela("DT_REALIZADO", DbType.DateTime)]
        public DateTime Realizado
        {
            get { return _Realizado; }
            set { _Realizado = value; }
        }

        [CampoNaTabela("DT_VALIDADE", DbType.DateTime)]
        public DateTime Validade
        {
            get { return _Validade; }
            set { _Validade = value; }
        }

        [CampoNaTabela("DT_REGISTRO", DbType.DateTime)]
        public DateTime Registro
        {
            get { return _Registro; }
            set { _Registro = value; }
        }

        [CampoNaTabela("DT_DESATIVACAO", DbType.DateTime)]
        public DateTime Desativacao
        {
            get { return _Desativacao; }
            set { _Desativacao = value; }
        }

        [CampoNaTabela("BL_ATIVO", DbType.Boolean)]
        public Boolean Ativo
        {
            get { return _Ativo; }
            set { _Ativo = value; }
        }        

        #endregion


        #region Métodos

        public static TreinamentoPessoaGS Obtem(Int32 pCodigo)
        {
            bool Achou = false;
            TreinamentoPessoaGS item = new TreinamentoPessoaGS();

            foreach (TreinamentoPessoaGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " CD_TREINAMENTO_PESSOA = " + pCodigo))
            {
                Achou = true;
                item = obj;
                break;
            }

            if (Achou)
                return item;
            else
                throw new Exception("[TREINAMENTOPESSOAGS].[OBTEM] Registro de Treinamento não encontrado (" + pCodigo.ToString() + ")");
        }

        public static TreinamentoPessoaGS Obtem(String pCodigoPessoa, Int32 pCodigoTreinamento  )
        {
            bool Achou = false;
            TreinamentoPessoaGS item = new TreinamentoPessoaGS();

            foreach (TreinamentoPessoaGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " CD_PESSOA_FISICA = '" + pCodigoPessoa + "' AND CD_TREINAMENTO = "+ pCodigoTreinamento))
            {
                Achou = true;
                item = obj;
                break;
            }

            if (Achou)
                return item;
            else
                throw new Exception("[TREINAMENTOPESSOAGS].[OBTEM] Registro de Treinamento não encontrado (CodigoPessoa: " + pCodigoPessoa.ToString() + ". Codigo Treinamento: " + pCodigoTreinamento.ToString() +" )");
        }

        public static bool ExisteTreinamento(Int32 pCodigo)
        {
            TreinamentoGS item = new TreinamentoGS();

            foreach (TreinamentoGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " CD_TREINAMENTO_PESSOA = " + pCodigo.ToString()))
            {
                return true;
            }

            return false;

        }

        public static bool ExisteTreinamento(String pCodigoPessoa, Int32 pCodigoTreinamento)
        {
            TreinamentoPessoaGS item = new TreinamentoPessoaGS();

            foreach (TreinamentoPessoaGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " CD_PESSOA_FISICA = '" + pCodigoPessoa + "' AND CD_TREINAMENTO = " + pCodigoTreinamento))
            {
                return true;
            }

            return false;

        }


        #endregion
        

    }
}
