﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("CIDADE")]
    public class CidadeGS:MasterClass
    {

        #region Atributos

        
        private int _Codigo;
        private int _CodigoEstado;

        
        private string _Nome;
        
        private bool _Ativo;

        private DateTime _DataRegistro;
        private DateTime _DataDesativacao;


        #endregion

        #region Propriedades

        

        [CampoNaTabela("CD_CIDADE", DbType.Int32,true,true)]
        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        [CampoNaTabela("CD_ESTADO", DbType.Int32)]
        public Int32 CodigoEstado
        {
            get { return _CodigoEstado; }
            set { _CodigoEstado = value; }
        }

        [CampoNaTabela("TX_CIDADE", DbType.String)]
        public String Nome
        {
            get { return _Nome; }
            set { _Nome = value; }
        }


        [CampoNaTabela("BL_ATIVO", DbType.Boolean)]
        public Boolean Ativo
        {
            get { return _Ativo; }
            set { _Ativo = value; }
        }


        [CampoNaTabela("DT_REGISTRO", DbType.DateTime)]
        public DateTime DataRegistro
        {
            get { return _DataRegistro; }
            set { _DataRegistro = value; }
        }

        [CampoNaTabela("DT_DESATIVACAO", DbType.DateTime)]
        public DateTime DataDesativacao
        {
            get { return _DataDesativacao; }
            set { _DataDesativacao = value; }
        }


        

        #endregion

        #region Métodos

        public static CidadeGS Obtem(string pNome, int pCodigoEstado)
        {
            CidadeGS item = new CidadeGS();
            List<CidadeGS> itens = new List<CidadeGS>();

            try
            {
                //Avaliar o nome como passado
                foreach (CidadeGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " UPPER(TX_CIDADE)  = '" + pNome.ToUpper() + "' AND CD_ESTADO = " + pCodigoEstado))
                {
                    itens.Add(obj);
                }

                //Não encontrou Estado com o nome igual, procurando com nome parecido
                if (itens.Count == 0)
                {
                    foreach (CidadeGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " UPPER(TX_CIDADE) LIKE = '%" + pNome.ToUpper() + "%' AND CD_ESTADO = " + pCodigoEstado))
                    {
                        itens.Add(obj);
                    }

                    if (itens.Count == 0)
                    {
                        //throw new Exception("Cidade não encontrada!");
                        
                        CidadeGS cidade = new CidadeGS();
                        cidade.Ativo = true;
                        cidade.CodigoEstado = pCodigoEstado;
                        cidade.DataRegistro = DateTime.Now;
                        cidade.DataDesativacao = DateTime.MinValue;
                        cidade.Nome = pNome;
                        int CodigoCidade = cidade.InserirRegistroRetornaID();

                        return CidadeGS.Obtem(pNome, pCodigoEstado);

                    }
                    else if (itens.Count > 1)
                    {
                        throw new Exception("Encontrada mais de 1 Cidade com a descrição passada!");
                    }
                    else if (itens.Count == 1)
                    {
                        item = itens[0];
                    }
                }
                else if (itens.Count > 1)
                {
                    throw new Exception("Encontrada mais de 1 Cidade com a descrição passada!");
                }
                else if (itens.Count == 1)
                {
                    item = itens[0];
                }

            }
            catch (Exception erro)
            {
                throw new Exception("[CIDADE].[OBTEM] Falha ao obter o Cidade com a descrição " + pNome + "(" + pCodigoEstado.ToString() + "): " + erro.Message);
            }


            return item;
        }

        public static CidadeGS Obtem(string pNome)
        {
            bool Achou = false;
            CidadeGS item = new CidadeGS();


            try
            {
                //Avaliar o nome como passado
                foreach (CidadeGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " UPPER(TX_CIDADE)  = '" + pNome.ToUpper() + "' AND BL_ATIVO =1 ORDER BY DT_REGISTRO DESC"))
                {
                    Achou = true;
                    item = obj;
                    break;
                }

                if (!Achou)
                {
                    throw new Exception("[CIDADE].[OBTEM] Cidade inexistente(" + pNome + ")!");
                }

            }
            catch (Exception erro)
            {
                throw new Exception("[CIDADE].[OBTEM] Falha ao obter o Município com a descrição " + pNome + " - " + erro.Message);
            }


            return item;
        }

        #endregion

    }
}
