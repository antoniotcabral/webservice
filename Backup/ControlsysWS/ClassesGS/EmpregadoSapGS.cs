﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("EMPREGADO_SAP")]
    public class EmpregadoSapGS:MasterClass
    {

        #region Atributos

        private int _Codigo;
        private int _CodigoGestorPonto;
        private int _CodigoSuperiorImediato;

        #endregion

        #region Propriedades

        [CampoNaTabela("CD_EMPREGADO_SAP", DbType.Int32,true,false)]
        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }


        [CampoNaTabela("CD_GESTOR_PONTO", DbType.Int32)]
        public Int32 CodigoGestorPonto
        {
            get { return _CodigoGestorPonto; }
            set { _CodigoGestorPonto = value; }
        }

        [CampoNaTabela("CD_SUPERIOR_IMEDIATO", DbType.Int32)]
        public Int32 CodigoSuperiorImediato
        {
            get { return _CodigoSuperiorImediato; }
            set { _CodigoSuperiorImediato = value; }
        }

        
        #endregion

        #region Métodos

        public static EmpregadoSapGS Obtem(int pCodigo)
        {
            bool Achou = false;
            EmpregadoSapGS item = new EmpregadoSapGS();

            foreach (EmpregadoSapGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " CD_EMPREGADO_SAP = " + pCodigo))
            {
                Achou = true;
                item = obj;
                break;
            }

            if(Achou)
                return item;
            else
                throw new Exception("[EMPREGADOSAP].[OBTEM] Registro de Empregado SAP não encontrado (" + pCodigo + ")");
        }

        public static Boolean Existe(int pCodigo)
        {
            bool Achou = false;
            EmpregadoSapGS item = new EmpregadoSapGS();

            foreach (EmpregadoSapGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " CD_EMPREGADO_SAP = " + pCodigo))
            {
                Achou = true;                
                break;
            }

            return Achou;
        }

        #endregion

    }
}
