﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("PAPEL_AGENDAMENTO")]
    public class PapelAgendamentoGS:MasterClass
    {

        #region Atributos

        private int _Codigo;
        private int _CodigoCargo;
        private int _CodigoGestor;
        private string _CodigoUsuario;
        private int _CodigoPessoaAgendamento;
        private int _CodigoPapel;
        private int _CodigoArea;
        private int _CodigoSap;
        private int _CodigoSetor;

        private DateTime _DataRegistro;

        #endregion

        #region Propriedades


        [CampoNaTabela("CD_PAPEL_AGENDAMENTO", DbType.Int32,true,true)]
        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        [CampoNaTabela("CD_CARGO", DbType.Int32)]
        public Int32 CodigoCargo
        {
            get { return _CodigoCargo; }
            set { _CodigoCargo = value; }
        }

        [CampoNaTabela("CD_GESTOR", DbType.Int32)]
        public Int32 CodigoGestor
        {
            get { return _CodigoGestor; }
            set { _CodigoGestor = value; }
        }

        [CampoNaTabela("CD_AREA", DbType.Int32)]
        public Int32 CodigoArea
        {
            get { return _CodigoArea; }
            set { _CodigoArea = value; }
        }

        [CampoNaTabela("CD_SETOR", DbType.Int32)]
        public Int32 CodigoSetor
        {
            get { return _CodigoSetor; }
            set { _CodigoSetor = value; }
        }

        [CampoNaTabela("CD_USUARIO", DbType.String)]
        public String CodigoUsuario
        {
            get { return _CodigoUsuario; }
            set { _CodigoUsuario = value; }
        }

        [CampoNaTabela("CD_PESSOA_AGENDAMENTO", DbType.Int32)]
        public Int32 CodigoPessoaAgendamento
        {
            get { return _CodigoPessoaAgendamento; }
            set { _CodigoPessoaAgendamento = value; }
        }


        [CampoNaTabela("DT_REGISTRO", DbType.DateTime)]
        public DateTime DataRegistro
        {
            get { return _DataRegistro; }
            set { _DataRegistro = value; }
        }

        [CampoNaTabela("CD_PAPEL", DbType.Int32)]
        public Int32 CodigoPapel
        {
            get { return _CodigoPapel; }
            set { _CodigoPapel = value; }
        }


        [CampoNaTabela("CD_EMPREGADO_SAP", DbType.Int32)]
        public Int32 CodigoSap
        {
            get { return _CodigoSap; }
            set { _CodigoSap = value; }
        }

        

        #endregion

        #region Métodos

        public static PapelAgendamentoGS Obtem(Int32 pCodigo)
        {
            PapelAgendamentoGS item = new PapelAgendamentoGS();
            Boolean Achou = false;


            //Avaliar o nome como passado
            foreach (PapelAgendamentoGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " CD_PAPEL_AGENDAMENTO = " + pCodigo + " AND CD_PAPEL IS NULL"))
            {
                Achou = true;
                item = obj;
                break;
            }

            if (Achou)
                return item;
            else
                throw new Exception("[PAPELAGENDAMENTO].[OBTEM] Registro de Papel Agendamento não encontrado (" + pCodigo.ToString() + ")");
        }

        public static PapelAgendamentoGS ObtemPorCodPessoa(Int32 pCodigoPessoaAgendamento)
        {
            PapelAgendamentoGS item = new PapelAgendamentoGS();
            Boolean Achou = false;


            //Avaliar o nome como passado
            foreach (PapelAgendamentoGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " CD_PESSOA_AGENDAMENTO = " + pCodigoPessoaAgendamento))
            {
                Achou = true;
                item = obj;
                break;
            }

            if (Achou)
                return item;
            else
                throw new Exception("[PAPELAGENDAMENTO].[OBTEMPORCODPESSOA] Registro de Papel Agendamento não encontrado (" + pCodigoPessoaAgendamento.ToString() + ")");
        }

        #endregion



    }
}
