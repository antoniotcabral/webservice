﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using ControlsysWS.ReturnLog;
using System.ServiceModel;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("INTEGRACAO")]
    public class IntegracaoGS:MasterClass
    {

        #region Atributos

        private int _Codigo;
        private DateTime _Ocorrencia;
        private string _Status;
        private string _Mensagem;
        private DateTime _DataStatus;
        private string _TipoIntegracao;

        #endregion

        #region Propriedades

        [CampoNaTabela("CD_INTEGRACAO", DbType.Int32,true,false)]
        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        [CampoNaTabela("DT_INTEGRACAO", DbType.DateTime)] 
        public DateTime Ocorrencia
        {
            get { return _Ocorrencia; }
            set { _Ocorrencia = value; }
        }

        [CampoNaTabela("TX_STATUS", DbType.String)]
        public String Status
        {
            get { return _Status; }
            set { _Status = value; }
        }


        [CampoNaTabela("TX_MENSAGEM", DbType.String)]
        public String Mensagem
        {
            get { return _Mensagem; }
            set { _Mensagem = value; }
        }


        [CampoNaTabela("DT_STATUS", DbType.DateTime)]
        public DateTime DataStatus
        {
            get { return _DataStatus; }
            set { _DataStatus = value; }
        }


        [CampoNaTabela("TX_TIPO", DbType.String)]
        public String TipoIntegracao
        {
            get { return _TipoIntegracao; }
            set { _TipoIntegracao = value; }
        }

        #endregion

        #region Métodos

        private static int ObtemProximoCodigo()
        {
            try
            {

                IntegracaoGS integra = new IntegracaoGS();

                string cmdCodigo = "SELECT MAX(CD_INTEGRACAO)+1 FROM INTEGRACAO";

                int ProxCod = integra.ExecutaComandoContarRegistros(cmdCodigo);

                return ProxCod == -1 ? 1 : ProxCod;

            }
            catch (Exception erro)
            {
                throw new Exception("[ObtemProximoCodigo]Falha ao obter o código da integração: " + erro.Message);                
            }
        }

        public static bool ExisteIntegracao(int pCodigoIntegracao)
        {
            bool Achou = false;
            IntegracaoGS item = new IntegracaoGS();

            foreach (IntegracaoGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " CD_INTEGRACAO = " + pCodigoIntegracao))
            {
                Achou = true;
                break;
            }

            return Achou;
        }

        public static Integracao ProcessaIntegracao(String pTipo, Int32 pCodigo)
        {
            try
            {
                IntegracaoGS resposta = new IntegracaoGS();

                resposta.Codigo = pCodigo;//ObtemProximoCodigo();
                resposta.DataStatus = DateTime.Now;
                resposta.Mensagem = String.Empty;
                resposta.Ocorrencia = DateTime.Now;
                resposta.Status = "INICIADA";
                resposta.TipoIntegracao = pTipo;

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[IntegracaoGS.ProcessaIntegracao] Registrando Início da Integração no Banco de Dados." + Environment.NewLine);                
                }
                
                if(IntegracaoGS.ExisteIntegracao(pCodigo))
                {
                    resposta.AtualizarRegistro();
                }
                else
                {
                    resposta.InserirRegistro();
                }

                Integracao integra = resposta.Converte();

                //if (MasterClass.LogAtivado)
                //{
                //    MasterClass.TextoLog.Append("[IntegracaoGS.ProcessaIntegracao] Registrando Início da Integração no Webservice." + Environment.NewLine);
               // }



                //if (System.Configuration.ConfigurationSettings.AppSettings["ProcessaReturnLog"] == "true")
                //{
                //    processaWSReturnLog(integra);
                //}

                return integra;


            }
            catch (Exception erro)
            {
                throw new Exception("[ProcessaIntegracao] Falha ao criar Integração. (Detalhe:" + erro.Message + ")");
            }
        }

        public Integracao Converte()
        {
            Integracao resposta = new Integracao();

            resposta.Codigo = this.Codigo;
            resposta.DataStatus = this.DataStatus;
            resposta.Mensagem = this.Mensagem;
            resposta.Ocorrencia = this.Ocorrencia;
            resposta.Status = this.Status;
            resposta.TipoIntegracao = this.TipoIntegracao;

            return resposta;

        }

        public static void processaWSReturnLog(Integracao pIntegracao)
        {

            try
            {
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[IntegracaoGS.processaWSReturnLog] Preparando chamada ao webservice." + Environment.NewLine);
                }

                #region EndPoint
               
                BasicHttpBinding ccBinding = new BasicHttpBinding("MI_OB_CSYS_ReturnLogBinding");

                //security settings
                ccBinding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
                ccBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;


                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[IntegracaoGS.processaWSReturnLog] Configurando EndPoint: " + MasterClass.webservice + Environment.NewLine);
                }

                //Endpoint address definition            
                string strEndereco = MasterClass.webservice;
                EndpointAddress ccEndpointAddress = new EndpointAddress(strEndereco);

                //if (MasterClass.LogEmArquivo)
                //   MasterClass.GravaLogErro("Utilizando webservice publicado em " + strEndereco);

                ///Initialize Service Registry Client                
                ControlsysWS.ReturnLog.MI_OB_CSYS_ReturnLogClient Cliente = new MI_OB_CSYS_ReturnLogClient();
                Cliente.Endpoint.Binding = ccBinding;
                Cliente.Endpoint.Address = ccEndpointAddress;


                ///Login settings
                //Cliente.ClientCredentials.HttpDigest.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;                                
                //Cliente.ClientCredentials.HttpDigest.ClientCredential.UserName = MasterClass.UsuarioWS;
                //Cliente.ClientCredentials.HttpDigest.ClientCredential.Password = MasterClass.SenhaWS;
                Cliente.ClientCredentials.UserName.UserName = MasterClass.UsuarioWS;
                Cliente.ClientCredentials.UserName.Password = MasterClass.SenhaWS;
                
                 #endregion

                ControlsysWS.ReturnLog.DT_CSysReturnLogReturnLog  CSysReturnLog = new DT_CSysReturnLogReturnLog();
                ControlsysWS.ReturnLog.MI_OB_CSYS_ReturnLogClient ReturnLogClient = new MI_OB_CSYS_ReturnLogClient();
                ControlsysWS.ReturnLog.DT_CSysReturnLogReturnLog CSysReturnLogReturnLog = new DT_CSysReturnLogReturnLog();
                ControlsysWS.ReturnLog.MI_OB_CSYS_ReturnLog1 CSYS_ReturnLog1 = new MI_OB_CSYS_ReturnLog1();

                String MensagemSap;

                if(pIntegracao.Mensagem.Length >255)
                {
                    int comprimento = pIntegracao.Mensagem.Length;

                    MensagemSap = pIntegracao.Mensagem.Substring(pIntegracao.Mensagem.Length - 255);
                }
                else
                {
                    MensagemSap = pIntegracao.Mensagem;
                }

                CSysReturnLogReturnLog.ID = pIntegracao.Codigo.ToString();
                CSysReturnLogReturnLog.Mensagem = MensagemSap;
                CSysReturnLogReturnLog.Status = pIntegracao.Status;
                CSysReturnLogReturnLog.TX_TIPO = pIntegracao.TipoIntegracao;

                ControlsysWS.ReturnLog.DT_CSysReturnLogReturnLog[] retorno = new DT_CSysReturnLogReturnLog[1];

                retorno[0] = CSysReturnLogReturnLog;

                if (MasterClass.LogAtivado) 
                {
                    MasterClass.TextoLog.Append("[IntegracaoGS.processaWSReturnLog] Efetuando a Chamada." + Environment.NewLine);
                }


                if (System.Configuration.ConfigurationSettings.AppSettings["ProcessaReturnLog"] == "true")
                {
                    Cliente.MI_OB_CSYS_ReturnLog(retorno);
                }

               
            }
            catch (Exception erro)
            {

                //Tratamento dos erros
                //A operação unidirecional retornou uma mensagem não nula com Ação='
                //"The one-way operation returned a non-null message with Action=''."
                //Solução encontrada: Editado o arquivo Service References\ReturnLog\Reference.cs
                //Editada a linha para constar OneWay=false
                //[System.ServiceModel.OperationContractAttribute(IsOneWay=false, Action="http://sap.com/xi/WebService/soap1.1")]

                //if (erro.Message == "A operação unidirecional retornou uma mensagem não nula com Ação=''.")
                //{
                //    return;
                //}

                //if (erro.Message == "The one-way operation returned a non-null message with Action=''.")
                //{
                //    return;
                //}

                
                
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[IntegracaoGS.processaWSReturnLog] Erro: " + erro.Message + Environment.NewLine);
                }

                //throw new Exception("Falha no processamento da requisição de ReturnLog " + erro.Message);
            }
            

        }

        #endregion

    }
}
