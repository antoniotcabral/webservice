﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("TREINAMENTO_PAPEL_AGEND")]
    public class TreinamentoPapelAgendamentoGS:MasterClass
    {

        #region Atributos

        
        private int _Codigo;
        private int _CodigoPapelAgendamento;
        private int _CodigoTreinamento;

        private DateTime _DataRealizacao;
        private DateTime _DataValidade;
        private DateTime _DataRegistro;
        private DateTime _DataDesativacao;

        private Boolean _Ativo;




        #endregion

        #region Propriedades

        

        [CampoNaTabela("CD_TREINAMENTO_PAPELAGEND", DbType.Int32,true,true)]
        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        [CampoNaTabela("CD_PAPEL_AGENDAMENTO", DbType.Int32)]
        public Int32 CodigoPapelAgendamento
        {
            get { return _CodigoPapelAgendamento; }
            set { _CodigoPapelAgendamento = value; }
        }

        [CampoNaTabela("CD_TREINAMENTO", DbType.Int32)]
        public Int32 CodigoTreinamento
        {
            get { return _CodigoTreinamento; }
            set { _CodigoTreinamento = value; }
        }


        [CampoNaTabela("DT_REALIZADO", DbType.DateTime)]
        public DateTime DataRealizacao
        {
            get { return _DataRealizacao; }
            set { _DataRealizacao = value; }
        }

        [CampoNaTabela("DT_VALIDADE", DbType.DateTime)]
        public DateTime DataValidade
        {
            get { return _DataValidade; }
            set { _DataValidade  = value; }
        }

        [CampoNaTabela("DT_REGISTRO", DbType.DateTime)]
        public DateTime DataRegistro
        {
            get { return _DataRegistro; }
            set { _DataRegistro = value; }
        }

        [CampoNaTabela("DT_DESATIVACAO", DbType.DateTime)]
        public DateTime DataDesativacao
        {
            get { return _DataDesativacao; }
            set { _DataDesativacao = value; }
        }

        [CampoNaTabela("BL_ATIVO", DbType.Boolean)]
        public Boolean Ativo
        {
            get { return _Ativo; }
            set { _Ativo = value; }
        }

        #endregion

        

    }
}
