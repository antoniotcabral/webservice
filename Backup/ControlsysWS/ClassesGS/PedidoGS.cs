﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("PEDIDO")]
    public class PedidoGS:MasterClass
    {

        #region Atributos

        
        private int _Codigo;
        private int _CodigoPedidoPai;
        private int _CodigoProjeto;

        private string _Cnae;
        private string _CodigoEmpresa;
        private string _Numero;
        private string _Nome;
        private string _Observacao;
        private string _Status;

        //private bool _Ativo;

        private DateTime _DataRegistro;
        private DateTime _DataStatus;


        #endregion

        #region Propriedades

        

        [CampoNaTabela("CD_PEDIDO", DbType.Int32,false,true)]
        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        [CampoNaTabela("CD_PEDIDO_PAI", DbType.Int32)]
        public Int32 CodigoPedidoPai
        {
            get { return _CodigoPedidoPai; }
            set { _CodigoPedidoPai = value; }
        }

        [CampoNaTabela("CD_PROJETO", DbType.Int32)]
        public Int32 CodigoProjeto
        {
            get { return _CodigoProjeto; }
            set { _CodigoProjeto = value; }
        }

        [CampoNaTabela("TX_CNAE", DbType.String)]
        public String Cnae
        {
            get { return _Cnae; }
            set { _Cnae = value; }
        }


        [CampoNaTabela("CD_EMPRESA", DbType.String)]
        public String CodigoEmpresa
        {
            get { return _CodigoEmpresa; }
            set { _CodigoEmpresa = value; }
        }

        [CampoNaTabela("NU_PEDIDO", DbType.String, true)]
        public String Numero
        {
            get { return _Numero; }
            set { _Numero = value; }
        }

        [CampoNaTabela("TX_NOME", DbType.String)]
        public String Nome
        {
            get { return _Nome; }
            set { _Nome = value; }
        }

        [CampoNaTabela("TX_OBSERVACAO", DbType.String)]
        public String Observacao
        {
            get { return _Observacao; }
            set { _Observacao = value; }
        }

        [CampoNaTabela("TX_STATUS", DbType.String)]
        public String Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        //[CampoNaTabela("BL_ATIVO", DbType.Boolean)]
        //public Boolean Ativo
        //{
        //    get { return _Ativo; }
        //    set { _Ativo = value; }
        //}


        [CampoNaTabela("DT_REGISTRO", DbType.DateTime)]
        public DateTime DataRegistro
        {
            get { return _DataRegistro; }
            set { _DataRegistro = value; }
        }

        [CampoNaTabela("DT_STATUS", DbType.DateTime)]
        public DateTime DataStatus
        {
            get { return _DataStatus; }
            set { _DataStatus = value; }
        }
               

        #endregion

        #region Métodos

        public static PedidoGS Obtem(String pNumeroPedido)
        {
            bool Achou = false;
            PedidoGS item = new PedidoGS();

            foreach (PedidoGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " NU_PEDIDO = '" + pNumeroPedido + "'"))
            {
                Achou = true;
                item = obj;
                break;
            }

            if (Achou)
                return item;
            else
                throw new Exception("[PEDIDOCOMPRA].[OBTEM] Registro de Pedido não encontrado (" + pNumeroPedido + ")");
        }

        #endregion


    }
}
