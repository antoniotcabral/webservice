﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("ASO")]
    public class AsoGS:MasterClass
    {

        #region Atributos

        
        private int _Codigo;
        private int _CodigoPapel;
        private int _TipoAtendimento;

        private string _TipoRegistro;
        private bool _Compareceu;

        private DateTime _DataValidade;
        private DateTime _DataRealizacao;


        #endregion

        #region Propriedades

        

        [CampoNaTabela("CD_ASO", DbType.Int32,true,true)]
        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        [CampoNaTabela("CD_PAPEL", DbType.Int32)]
        public Int32 CodigoPapel
        {
            get { return _CodigoPapel; }
            set { _CodigoPapel = value; }
        }

        [CampoNaTabela("CD_TIPO_ATENDIMENTO", DbType.Int32)]
        public Int32 TipoAtendimento
        {
            get { return _TipoAtendimento; }
            set { _TipoAtendimento = value; }
        }

        [CampoNaTabela("TX_TIPO_REGISTRO", DbType.String)]
        public String TipoRegistro
        {
            get { return _TipoRegistro; }
            set { _TipoRegistro = value; }
        }


        [CampoNaTabela("DT_VALIDADE", DbType.DateTime)]
        public DateTime DataValidade
        {
            get { return _DataValidade; }
            set { _DataValidade = value; }
        }

        [CampoNaTabela("DT_REALIZACAO", DbType.DateTime)]
        public DateTime DataRealizacao
        {
            get { return _DataRealizacao; }
            set { _DataRealizacao = value; }
        }

        [CampoNaTabela("BL_COMPARECEU", DbType.Boolean)]
        public Boolean Compareceu
        {
            get { return _Compareceu; }
            set { _Compareceu = value; }
        }

      


        

        #endregion

        

    }
}
