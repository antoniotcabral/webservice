﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("PRAZO")]
    public class PrazoGS:MasterClass
    {

        #region Atributos

        
        private int _Codigo;
        private int _CodigoPedido;
        private DateTime _DataInicio;
        private DateTime _DataFim;
        private DateTime _Desativacao;
        private DateTime _Registro;
        
        private bool _Aditivo;
        private bool _Ativo;
        
        private string _Observacao;

        #endregion

        #region Propriedades



        [CampoNaTabela("CD_PRAZO", DbType.Int32, true,true)]
        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }


        [CampoNaTabela("CD_PEDIDO", DbType.Int32)]
        public Int32 CodigoPedido
        {
            get { return _CodigoPedido; }
            set { _CodigoPedido = value; }
        }

        [CampoNaTabela("BL_ATIVO", DbType.Boolean)]
        public Boolean Ativo
        {
            get { return _Ativo; }
            set { _Ativo = value; }
        }

        [CampoNaTabela("BL_ADITIVO", DbType.Boolean)]
        public Boolean Aditivo
        {
            get { return _Aditivo; }
            set { _Aditivo = value; }
        }

        [CampoNaTabela("TX_OBSERVACAO", DbType.String)]
        public String Observacao
        {
            get { return _Observacao; }
            set { _Observacao = value; }
        }


        [CampoNaTabela("DT_INICIO", DbType.DateTime)]
        public DateTime DataInicio
        {
            get { return _DataInicio; }
            set { _DataInicio = value; }
        }

        [CampoNaTabela("DT_FIM", DbType.DateTime)]
        public DateTime DataFim
        {
            get { return _DataFim; }
            set { _DataFim = value; }
        }

        [CampoNaTabela("DT_DESATIVACAO", DbType.DateTime)]
        public DateTime Desativacao
        {
            get { return _Desativacao; }
            set { _Desativacao = value; }
        }

        [CampoNaTabela("DT_REGISTRO", DbType.DateTime)]
        public DateTime Registro
        {
            get { return _Registro; }
            set { _Registro = value; }
        }


        

        #endregion

        #region Métodos

        public static bool ExistePrazo(int pCodigoPedido)
        {
            bool Achou = false;     
            PrazoGS item = new PrazoGS();

            foreach (PrazoGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " CD_PEDIDO = '" + pCodigoPedido + "'"))
            {
                Achou = true;                
                break;
            }

            return Achou;
        }


        public static PrazoGS Obtem(int pCodigoPedido)
        {
            bool Achou = false;
            PrazoGS item = new PrazoGS();

            foreach (PrazoGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " CD_PEDIDO = '" + pCodigoPedido + "'"))
            {
                Achou = true;
                item = obj;
                break;
            }

            if (Achou)
                return item;
            else
                throw new Exception("[PRAZO].[OBTEM] Registro de Prazo não encontrado (Pedido: " + pCodigoPedido + ")");
        }

        #endregion


    }
}
