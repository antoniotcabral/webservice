﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("ALOCACAO_COLABORADOR")]
    public class AlocacaoColaboradorGS:MasterClass
    {

        #region Atributos

        
        private int _Codigo;
        private int _CodigoPapel;
        private int _CodigoPedido;
        private int _CodigoSetor;

        private bool _Ativo;

        private DateTime _DataRegistro;
        private DateTime _DataDesativacao;


        #endregion

        #region Propriedades



        [CampoNaTabela("CD_ALOCACAO_COLABORADOR", DbType.Int32, true, true)]
        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        [CampoNaTabela("CD_PAPEL", DbType.Int32)]
        public Int32 CodigoPapel
        {
            get { return _CodigoPapel; }
            set { _CodigoPapel = value; }
        }

        [CampoNaTabela("CD_PEDIDO", DbType.Int32)]
        public Int32 CodigoPedido
        {
            get { return _CodigoPedido; }
            set { _CodigoPedido = value; }
        }

        [CampoNaTabela("CD_SETOR", DbType.Int32)]
        public Int32 CodigoSetor
        {
            get { return _CodigoSetor; }
            set { _CodigoSetor = value; }
        }

        
        [CampoNaTabela("BL_ATIVO", DbType.Boolean)]
        public Boolean Ativo
        {
            get { return _Ativo; }
            set { _Ativo = value; }
        }


        [CampoNaTabela("DT_REGISTRO", DbType.DateTime)]
        public DateTime DataRegistro
        {
            get { return _DataRegistro; }
            set { _DataRegistro = value; }
        }

        [CampoNaTabela("DT_DESATIVACAO", DbType.DateTime)]
        public DateTime DataDesativacao
        {
            get { return _DataDesativacao; }
            set { _DataDesativacao = value; }
        }       

        #endregion

        #region Métodos

        public static AlocacaoColaboradorGS Obtem(Int32 pCodigoPapel)
        {
            bool Achou = false;
            AlocacaoColaboradorGS item = new AlocacaoColaboradorGS();

            foreach (AlocacaoColaboradorGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " CD_PAPEL = " + pCodigoPapel + " AND BL_ATIVO=1"))
            {
                Achou = true;
                item = obj;
                break;
            }

            if (Achou)
                return item;
            else
                return null;
                //throw new Exception("[AREAGS].[OBTEM] Registro de Área não encontrado (" + pNumero.ToString() + ")");
        }

        #endregion

    }
}
