﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("dbo.GRUPO_TRAB_COLAB")]
    public class GrupoTrabalhoColaboradorGS:MasterClass
    {

        #region Atributos

        
        private int _Codigo;        
        private int _CodigoGrupoTrabalho;
        private int _CodigoPapel;

        private bool _Ativo;

        private DateTime _DataRegistro;
        private DateTime _DataDesativacao;


        #endregion

        #region Propriedades



        [CampoNaTabela("CD_GRUPO_TRAB_COLAB", DbType.Int32, true, true)]
        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        [CampoNaTabela("CD_PAPEL", DbType.Int32)]
        public Int32 CodigoPapel
        {
            get { return _CodigoPapel; }
            set { _CodigoPapel = value; }
        }

        [CampoNaTabela("CD_GRUPO", DbType.Int32)]
        public Int32 CodigoGrupoTrabalho
        {
            get { return _CodigoGrupoTrabalho; }
            set { _CodigoGrupoTrabalho = value; }
        }


        [CampoNaTabela("BL_ATIVO", DbType.Boolean)]
        public Boolean Ativo
        {
            get { return _Ativo; }
            set { _Ativo = value; }
        }


        [CampoNaTabela("DT_REGISTRO", DbType.DateTime)]
        public DateTime DataRegistro
        {
            get { return _DataRegistro; }
            set { _DataRegistro = value; }
        }

        [CampoNaTabela("DT_DESATIVACAO", DbType.DateTime)]
        public DateTime DataDesativacao
        {
            get { return _DataDesativacao; }
            set { _DataDesativacao = value; }
        }


        

        #endregion

        #region Métodos

        

        #endregion



    }
}
