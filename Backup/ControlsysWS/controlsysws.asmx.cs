﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using br.com.globalsys.controlsys.webservice;
using System.ComponentModel;

namespace ControlsysWS
{
    /// <summary>
    /// Webservice Controlsys
    /// </summary>
    [WebService(Namespace = "br.com.globalsys.controlsys", Description = "Controlsys webservice versão 1.1.03", Name = "Controlsys WebService")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]    
    // [System.Web.Script.Services.ScriptService]
    public class Service1 : System.Web.Services.WebService
    {

        //private static Colaborador colaboradorEmProcessamento;
        //private static Integracao integracaoEmProcessamento;
        //private static CentroCusto centroCustoEmProcessamento;
        //private static ElementoPep elementoPepEmProcessamento;
        //private static Fornecedor fornecedorEmProcessamento;
        //private static PedidoCompra pedidoCompraEmProcessamento;
        ////private static Posicao posicaoEmProcessamento;
        //private static TipoTreinamento tipoTreinamentoEmProcessamento;
        //private static Afastamento afastamentoEmProcessamento;
        //private static ASO asoEmProcessamento;
        //private static MarcacaoExame marcacaoExameEmProcessamento;
        //private static Ferias feriasEmProcessamento;
        //private static PHT phtEmProcessamento;
        private bool controleFornecedor = false;


        [WebMethod]
        public void IncluiColaborador(Colaborador pColaborador, int pIdIntegracao)
        {
            try
            {
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog = new System.Text.StringBuilder();
                    MasterClass.TextoLog.Append("[ControlsysWS] Iniciando procedimento IncluiColaborador." + Environment.NewLine);
                    MasterClass.TextoLog.Append("[ControlsysWS] IdIntegracao = " + pIdIntegracao.ToString() + Environment.NewLine);
                    //MasterClass.TextoLog.Append("[ControlsysWS] Cpf Colaborador = " + pColaborador.Cpf + Environment.NewLine);
                }

                //colaboradorEmProcessamento = pColaborador;

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Registrando Início da Integração" + Environment.NewLine);
                }

                Integracao  integracaoEmProcessamento = IntegracaoGS.ProcessaIntegracao("Colaborador", pIdIntegracao);

                BackgroundWorker processaItem = new BackgroundWorker();

                processaItem.DoWork += new DoWorkEventHandler(processaColaboradorInclui_DoWork);

                MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());

                MasterClass.TextoLog = new System.Text.StringBuilder();

                object[] Argumentos = { pColaborador, integracaoEmProcessamento };

                processaItem.RunWorkerAsync(Argumentos);                

                //return integracaoEmProcessamento.Codigo;            
            }
            catch (Exception erro)
            {
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Erro: " + erro.Message + Environment.NewLine);
                    MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());
                }

                
            }
            
        }

        void processaColaboradorInclui_DoWork(object sender, DoWorkEventArgs e)
        {
            object[] argumentos = (Object[])e.Argument;

            Colaborador c = (Colaborador)argumentos[0];
            Integracao i = (Integracao)argumentos[1];

            try
            {  

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Iniciando processamento de COLABORADOR" + Environment.NewLine);
                }

                if (!c.ValidaColaborador())
                {
                    throw MasterClass.UltimoErro;
                }

                c.InsereColaborador(i);

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Registrando Fim da Integração.ID:" + i.Codigo + " - Status: " + i.Status);
                }

                IntegracaoGS.processaWSReturnLog(i);
            }
            catch (Exception erro)
            {

                i.Status = "ERRO";
                i.Mensagem += "[ColaboradorInclui] " + erro.Message;
                i.DataStatus = DateTime.Now;

                i.Converte().AtualizarRegistro();
                IntegracaoGS.processaWSReturnLog(i);

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Erro: " + erro.Message + Environment.NewLine);
                    MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());                
                }

                
            }
        }

        [WebMethod]
        public void AtualizaColaborador(Colaborador pColaborador, int pIdIntegracao)
        {       

            try
            {
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog = new System.Text.StringBuilder();
                    MasterClass.TextoLog.Append("[ControlsysWS] Iniciando procedimento AtualizaColaborador." + Environment.NewLine);
                    MasterClass.TextoLog.Append("[ControlsysWS] IdIntegracao = " + pIdIntegracao.ToString() + Environment.NewLine);
                    //MasterClass.TextoLog.Append("[ControlsysWS] Cpf Colaborador = " + pColaborador.Cpf + Environment.NewLine);
                }

                //colaboradorEmProcessamento = pColaborador;

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Registrando Início da Integração" + Environment.NewLine);
                }

                Integracao integracaoEmProcessamento = IntegracaoGS.ProcessaIntegracao("Colaborador", pIdIntegracao);

                BackgroundWorker processaItem = new BackgroundWorker();

                processaItem.DoWork += new DoWorkEventHandler(processaColaboradorAtualiza_DoWork);

                MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());

                MasterClass.TextoLog = new System.Text.StringBuilder();

                object[] Argumentos = { pColaborador, integracaoEmProcessamento };

                processaItem.RunWorkerAsync(Argumentos);                

                

                // return integracaoEmProcessamento.Codigo;
            }
            catch (Exception erro)
            {
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Erro: " + erro.Message + Environment.NewLine);
                }
            }


        }

        void processaColaboradorAtualiza_DoWork(object sender, DoWorkEventArgs e)
        {

            object[] argumentos = (Object[])e.Argument;

            Colaborador colaboradorEmProcessamento = (Colaborador)argumentos[0];
            Integracao integracaoEmProcessamento = (Integracao)argumentos[1];

            try
            {  
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Iniciando processamento de COLABORADOR" + Environment.NewLine);
                }

                if (!colaboradorEmProcessamento.ValidaColaborador())
                {
                    throw MasterClass.UltimoErro;
                }

                colaboradorEmProcessamento.AtualizaColaborador(integracaoEmProcessamento);

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Registrando Fim da Integração.ID:" + integracaoEmProcessamento.Codigo + " - Status: " + integracaoEmProcessamento.Status);
                }

                IntegracaoGS.processaWSReturnLog(integracaoEmProcessamento);

                MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());
            }
            catch (Exception erro)
            {

                integracaoEmProcessamento.Status = "ERRO";
                integracaoEmProcessamento.Mensagem += "[ColaboradorAtualiza] " + erro.Message;
                integracaoEmProcessamento.DataStatus = DateTime.Now;

                integracaoEmProcessamento.Converte().AtualizarRegistro();
                IntegracaoGS.processaWSReturnLog(integracaoEmProcessamento);

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Erro: " + erro.Message + Environment.NewLine);
                    MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());
                }
                
            }

        }

        

        [WebMethod]
        public void IncluiCentroCusto(CentroCusto pCentroCusto, int pIdIntegracao)
        {
            try
            {
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog = new System.Text.StringBuilder();
                    MasterClass.TextoLog.Append("[ControlsysWS] Iniciando procedimento IncluiCentroCusto." + Environment.NewLine);
                    MasterClass.TextoLog.Append("[ControlsysWS] IdIntegracao = " + pIdIntegracao.ToString() + Environment.NewLine);
                    //MasterClass.TextoLog.Append("[ControlsysWS] Numero Centro Custo = " + pCentroCusto.Numero + Environment.NewLine);
                }

                CentroCusto centroCustoEmProcessamento = pCentroCusto;

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Registrando Início da Integração" + Environment.NewLine);
                }

                Integracao integracaoEmProcessamento = IntegracaoGS.ProcessaIntegracao("CentroCusto", pIdIntegracao);

                BackgroundWorker processaItem = new BackgroundWorker();

                processaItem.DoWork += new DoWorkEventHandler(processaCentroCustorInclui_DoWork);

                MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());

                MasterClass.TextoLog = new System.Text.StringBuilder();

                object[] Argumentos = { centroCustoEmProcessamento, integracaoEmProcessamento };

                processaItem.RunWorkerAsync(Argumentos);

                //return integracaoEmProcessamento.Codigo;    
            }
            catch (Exception erro)
            {
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Erro: " + erro.Message + Environment.NewLine);
                }

                MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());
            }
        }

        void processaCentroCustorInclui_DoWork(object sender, DoWorkEventArgs e)
        {
            object[] argumentos = (Object[])e.Argument;

            CentroCusto centroCustoEmProcessamento = (CentroCusto)argumentos[0];
            Integracao integracaoEmProcessamento = (Integracao)argumentos[1];

            try
            {
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Iniciando processamento de CENTRO DE CUSTO" + Environment.NewLine);
                }

                if (!centroCustoEmProcessamento.ValidaCentroCusto())
                {
                    throw MasterClass.UltimoErro;
                }
                centroCustoEmProcessamento.InsereCentroCusto(integracaoEmProcessamento);

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Registrando Fim da Integração.ID:" + integracaoEmProcessamento.Codigo + " - Status: " + integracaoEmProcessamento.Status);
                }

                IntegracaoGS.processaWSReturnLog(integracaoEmProcessamento);

                MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());

            }
            catch(Exception erro)
            {

                integracaoEmProcessamento.Status = "ERRO";
                integracaoEmProcessamento.Mensagem += "[CentroCustoInclui] " + erro.Message;
                integracaoEmProcessamento.DataStatus = DateTime.Now;

                integracaoEmProcessamento.Converte().AtualizarRegistro();
                IntegracaoGS.processaWSReturnLog(integracaoEmProcessamento);

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Erro: " + erro.Message + Environment.NewLine);
                }

                MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());
            }
        }

        [WebMethod]
        public void AtualizaCentroCusto(CentroCusto pCentroCusto, int pIdIntegracao)
        {
            try
            {
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog = new System.Text.StringBuilder();
                    MasterClass.TextoLog.Append("[ControlsysWS] Iniciando procedimento AtualizaCentroCusto." + Environment.NewLine);
                    MasterClass.TextoLog.Append("[ControlsysWS] IdIntegracao = " + pIdIntegracao.ToString() + Environment.NewLine);
                    //MasterClass.TextoLog.Append("[ControlsysWS] Numero Centro Custo = " + pCentroCusto.Numero + Environment.NewLine);
                }

                CentroCusto centroCustoEmProcessamento = pCentroCusto;
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Registrando Início da Integração" + Environment.NewLine);
                }

                Integracao integracaoEmProcessamento = IntegracaoGS.ProcessaIntegracao("CentroCusto", pIdIntegracao);

                BackgroundWorker processaItem = new BackgroundWorker();

                processaItem.DoWork += new DoWorkEventHandler(processaCentroCustoAtualiza_DoWork);

                MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());

                MasterClass.TextoLog = new System.Text.StringBuilder();

                object[] Argumentos = { centroCustoEmProcessamento, integracaoEmProcessamento };

                processaItem.RunWorkerAsync(Argumentos);
            }
            catch (Exception erro)
            {
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Erro: " + erro.Message + Environment.NewLine);
                }

                MasterClass.GravaLogErro(MasterClass.TextoLog.ToString()); 
            }

            //return integracaoEmProcessamento.Codigo;
        }

        void processaCentroCustoAtualiza_DoWork(object sender, DoWorkEventArgs e)
        {

            object[] argumentos = (Object[])e.Argument;

            CentroCusto centroCustoEmProcessamento = (CentroCusto)argumentos[0];
            Integracao integracaoEmProcessamento = (Integracao)argumentos[1];

            try
            {
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Iniciando processamento de CENTRO DE CUSTO" + Environment.NewLine);
                }

                if (!centroCustoEmProcessamento.ValidaCentroCusto())
                {
                    throw MasterClass.UltimoErro;
                }

                centroCustoEmProcessamento.AtualizaCentroCusto(integracaoEmProcessamento);

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Registrando Fim da Integração.ID:" + integracaoEmProcessamento.Codigo + " - Status: " + integracaoEmProcessamento.Status);
                }

                IntegracaoGS.processaWSReturnLog(integracaoEmProcessamento);
            }
            catch (Exception erro)
            {

                integracaoEmProcessamento.Status = "ERRO";
                integracaoEmProcessamento.Mensagem += "[CentroCustoAtualiza] " + erro.Message;
                integracaoEmProcessamento.DataStatus = DateTime.Now;

                integracaoEmProcessamento.Converte().AtualizarRegistro();
                IntegracaoGS.processaWSReturnLog(integracaoEmProcessamento);

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Erro: " + erro.Message + Environment.NewLine);
                }
                MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());
            }
        }

        

        [WebMethod]
        public void IncluiElementoPep(ElementoPep pElementoPep, int pIdIntegracao)
        {
            try
            {
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog = new System.Text.StringBuilder();
                    MasterClass.TextoLog.Append("[ControlsysWS] Iniciando procedimento IncluiElementoPep." + Environment.NewLine);
                    MasterClass.TextoLog.Append("[ControlsysWS] IdIntegracao = " + pIdIntegracao.ToString() + Environment.NewLine);
                    //MasterClass.TextoLog.Append("[ControlsysWS] Numero Elemento PEP = " + pElementoPep.Numero + Environment.NewLine);
                }

                ElementoPep elementoPepEmProcessamento = pElementoPep;

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Registrando Início da Integração" + Environment.NewLine);
                }

                Integracao integracaoEmProcessamento = IntegracaoGS.ProcessaIntegracao("ElementoPep", pIdIntegracao);

                BackgroundWorker processaItem = new BackgroundWorker();

                MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());

                MasterClass.TextoLog = new System.Text.StringBuilder();

                processaItem.DoWork += new DoWorkEventHandler(processaElementoPepInclui_DoWork);

                object[] Argumentos = { elementoPepEmProcessamento, integracaoEmProcessamento };

                processaItem.RunWorkerAsync(Argumentos);
            }
            catch (Exception erro)
            {
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Erro: " + erro.Message + Environment.NewLine);
                }

                MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());
            }

                    
        }

        void processaElementoPepInclui_DoWork(object sender, DoWorkEventArgs e)
        {

            object[] argumentos = (Object[])e.Argument;

            ElementoPep elementoPepEmProcessamento = (ElementoPep)argumentos[0];
            Integracao integracaoEmProcessamento = (Integracao)argumentos[1];

            try
            {
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Iniciando processamento de ELEMENTO PEP" + Environment.NewLine);
                }

                if (!elementoPepEmProcessamento.ValidaElementoPep())
                {
                    throw MasterClass.UltimoErro;
                }

                elementoPepEmProcessamento.InsereElementoPep(integracaoEmProcessamento);

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Registrando Fim da Integração.ID:" + integracaoEmProcessamento.Codigo + " - Status: " + integracaoEmProcessamento.Status);
                }
                IntegracaoGS.processaWSReturnLog(integracaoEmProcessamento);
            }
            catch (Exception erro)
            {

                integracaoEmProcessamento.Status = "ERRO";
                integracaoEmProcessamento.Mensagem += "[ElementoPepInclui] " + erro.Message;
                integracaoEmProcessamento.DataStatus = DateTime.Now;

                integracaoEmProcessamento.Converte().AtualizarRegistro();
                IntegracaoGS.processaWSReturnLog(integracaoEmProcessamento);

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Erro: " + erro.Message + Environment.NewLine);
                }
                MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());
            }
        }

        [WebMethod]
        public void AtualizaElementoPep(ElementoPep pElementoPep, int pIdIntegracao)
        {
            try
            {
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog = new System.Text.StringBuilder();
                    MasterClass.TextoLog.Append("[ControlsysWS] Iniciando procedimento AtualizaElementoPep." + Environment.NewLine);
                    MasterClass.TextoLog.Append("[ControlsysWS] IdIntegracao = " + pIdIntegracao.ToString() + Environment.NewLine);
                    //MasterClass.TextoLog.Append("[ControlsysWS] Numero Elemento PEP = " + pElementoPep.Numero + Environment.NewLine);
                }
                ElementoPep elementoPepEmProcessamento = pElementoPep;

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Registrando Início da Integração" + Environment.NewLine);
                }

                Integracao integracaoEmProcessamento = IntegracaoGS.ProcessaIntegracao("ElementoPep", pIdIntegracao);

                BackgroundWorker processaItem = new BackgroundWorker();

                processaItem.DoWork += new DoWorkEventHandler(processaElementoPepAtualiza_DoWork);

                MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());

                MasterClass.TextoLog = new System.Text.StringBuilder();

                object[] Argumentos = { elementoPepEmProcessamento, integracaoEmProcessamento };

                processaItem.RunWorkerAsync(Argumentos);
            }
            catch (Exception erro)
            {
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Erro: " + erro.Message + Environment.NewLine);
                }

                MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());
            }

            //return integracaoEmProcessamento.Codigo;
        }

        void processaElementoPepAtualiza_DoWork(object sender, DoWorkEventArgs e)
        {
            object[] argumentos = (Object[])e.Argument;

            ElementoPep elementoPepEmProcessamento = (ElementoPep)argumentos[0];
            Integracao integracaoEmProcessamento = (Integracao)argumentos[1];


            try
            {
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Iniciando processamento de ELEMENTO PEP" + Environment.NewLine);
                }

                if (!elementoPepEmProcessamento.ValidaElementoPep())
                {
                    throw MasterClass.UltimoErro;
                }
                elementoPepEmProcessamento.AtualizaElementoPep(integracaoEmProcessamento);

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Registrando Fim da Integração.ID:" + integracaoEmProcessamento.Codigo + " - Status: " + integracaoEmProcessamento.Status);
                }

                IntegracaoGS.processaWSReturnLog(integracaoEmProcessamento);
            }
            catch (Exception erro)
            {

                integracaoEmProcessamento.Status = "ERRO";
                integracaoEmProcessamento.Mensagem += "[ElementoPepAtualiza] " + erro.Message;
                integracaoEmProcessamento.DataStatus = DateTime.Now;

                integracaoEmProcessamento.Converte().AtualizarRegistro();
                IntegracaoGS.processaWSReturnLog(integracaoEmProcessamento);

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Erro: " + erro.Message + Environment.NewLine);
                }

                MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());
            }
        }

        

        [WebMethod]
        public void IncluiFornecedor(Fornecedor pFornecedor, int pIdIntegracao)
        {
            if (controleFornecedor)
            {
                System.Threading.Thread.Sleep(500);
                IncluiFornecedor(pFornecedor, pIdIntegracao);
            }
            else
            {
                controleFornecedor = true;

                try
                {
                    if (MasterClass.LogAtivado)
                    {
                        MasterClass.TextoLog = new System.Text.StringBuilder();
                        MasterClass.TextoLog.Append("[ControlsysWS] Iniciando procedimento IncluiFornecedor." + Environment.NewLine);
                        MasterClass.TextoLog.Append("[ControlsysWS] IdIntegracao = " + pIdIntegracao.ToString() + Environment.NewLine);
                        //MasterClass.TextoLog.Append("[ControlsysWS] Cnpj Fornecedor = " + pFornecedor.Cnpj + Environment.NewLine);
                    }



                    Fornecedor fornecedorEmProcessamento = pFornecedor;

                    if (MasterClass.LogAtivado)
                    {
                        MasterClass.TextoLog.Append("[ControlsysWS] Registrando Início da Integração" + Environment.NewLine);
                    }

                    Integracao integracaoEmProcessamento = IntegracaoGS.ProcessaIntegracao("Fornecedor", pIdIntegracao);

                    //BackgroundWorker processaItem = new BackgroundWorker();

                    //processaItem.DoWork += new DoWorkEventHandler(processaFornecedorInclui_DoWork);

                    MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());

                    MasterClass.TextoLog = new System.Text.StringBuilder();

                    object[] Argumentos = { fornecedorEmProcessamento, integracaoEmProcessamento };

                    processaFornecedor(Argumentos);

                    controleFornecedor = false;

                    //processaItem.RunWorkerAsync(Argumentos);

                    //return integracaoEmProcessamento.Codigo;
                }
                catch (Exception erro)
                {

                    controleFornecedor = false;

                    if (MasterClass.LogAtivado)
                    {
                        MasterClass.TextoLog.Append("[ControlsysWS] Erro: " + erro.Message + Environment.NewLine);
                    }

                    MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());
                }
            }
        }

        void processaFornecedor(object[] argumentos)
        {
            Fornecedor fornecedorEmProcessamento = (Fornecedor)argumentos[0];
            Integracao integracaoEmProcessamento = (Integracao)argumentos[1];

            try
            {

                //object[] argumentos = (Object[])e.Argument;


                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Iniciando processamento de FORNECEDOR" + Environment.NewLine);
                }

                //Conforme requisitado pelo Vanderson, todos os novos Fornecedores vindos da integração, vêm desativados
                fornecedorEmProcessamento.Ativo = 0;

                if (!fornecedorEmProcessamento.ValidaFornecedor())
                {
                    throw MasterClass.UltimoErro;
                }

                fornecedorEmProcessamento.InsereFornecedor(integracaoEmProcessamento);


                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Registrando Fim da Integração.ID:" + integracaoEmProcessamento.Codigo + " - Status: " + integracaoEmProcessamento.Status);
                }

                IntegracaoGS.processaWSReturnLog(integracaoEmProcessamento);
            }
            catch (Exception erro)
            {

                integracaoEmProcessamento.Status = "ERRO";
                integracaoEmProcessamento.Mensagem += "[FornecedorInclui] " + erro.Message;
                integracaoEmProcessamento.DataStatus = DateTime.Now;

                integracaoEmProcessamento.Converte().AtualizarRegistro();
                IntegracaoGS.processaWSReturnLog(integracaoEmProcessamento);

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Erro: " + erro.Message + Environment.NewLine);
                }

                MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());
            }

        }
        

        [WebMethod]
        public void AtualizaFornecedor(Fornecedor pFornecedor, int pIdIntegracao)
        {
            try
            {
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog = new System.Text.StringBuilder();
                    MasterClass.TextoLog.Append("[ControlsysWS] Iniciando procedimento IncluiColaborador." + Environment.NewLine);
                    MasterClass.TextoLog.Append("[ControlsysWS] IdIntegracao = " + pIdIntegracao.ToString() + Environment.NewLine);
                    //MasterClass.TextoLog.Append("[ControlsysWS] Cnpj Fornecedor = " + pFornecedor.Cnpj + Environment.NewLine);
                }

                Fornecedor fornecedorEmProcessamento = pFornecedor;

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Registrando Início da Integração" + Environment.NewLine);
                }

                Integracao integracaoEmProcessamento = IntegracaoGS.ProcessaIntegracao("Fornecedor", pIdIntegracao);

                BackgroundWorker processaItem = new BackgroundWorker();

                processaItem.DoWork += new DoWorkEventHandler(processaFornecedorAtualiza_DoWork);

                MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());

                MasterClass.TextoLog = new System.Text.StringBuilder();

                object[] Argumentos = { fornecedorEmProcessamento, integracaoEmProcessamento };

                processaItem.RunWorkerAsync(Argumentos);
            }
            catch (Exception erro)
            {
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Erro: " + erro.Message + Environment.NewLine);
                }

                MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());
            }

            //return integracaoEmProcessamento.Codigo;
        }

        void processaFornecedorAtualiza_DoWork(object sender, DoWorkEventArgs e)
        {
            object[] argumentos = (Object[])e.Argument;

            Fornecedor fornecedorEmProcessamento = (Fornecedor)argumentos[0];
            Integracao integracaoEmProcessamento = (Integracao)argumentos[1];

            try
            {

                
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Iniciando processamento de FORNECEDOR" + Environment.NewLine);
                }

                if (!fornecedorEmProcessamento.ValidaFornecedor())
                {
                    throw MasterClass.UltimoErro;
                }
                
                fornecedorEmProcessamento.AtualizaFornecedor(integracaoEmProcessamento);
                

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Registrando Fim da Integração.ID:" + integracaoEmProcessamento.Codigo + " - Status: " + integracaoEmProcessamento.Status);
                }

                IntegracaoGS.processaWSReturnLog(integracaoEmProcessamento);
            }
            catch (Exception erro)
            {
                integracaoEmProcessamento.Status = "ERRO";
                integracaoEmProcessamento.Mensagem += "[FornecedorAtualiza] " + erro.Message;
                integracaoEmProcessamento.DataStatus = DateTime.Now;

                integracaoEmProcessamento.Converte().AtualizarRegistro();
                IntegracaoGS.processaWSReturnLog(integracaoEmProcessamento);

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Erro: " + erro.Message + Environment.NewLine);
                }
                MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());
            }
        }

        

        [WebMethod]
        public void IncluiPedidoCompra(PedidoCompra pPedidoCompra, int pIdIntegracao)
        {
            try
            {
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog = new System.Text.StringBuilder();
                    MasterClass.TextoLog.Append("[ControlsysWS] Iniciando procedimento IncluiPedidoCompra." + Environment.NewLine);
                    MasterClass.TextoLog.Append("[ControlsysWS] IdIntegracao = " + pIdIntegracao.ToString() + Environment.NewLine);
                    //MasterClass.TextoLog.Append("[ControlsysWS] Número Pedido = " + pPedidoCompra.Numero + Environment.NewLine);
                }

                PedidoCompra pedidoCompraEmProcessamento = pPedidoCompra;

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Registrando Início da Integração" + Environment.NewLine);
                }

                Integracao integracaoEmProcessamento = IntegracaoGS.ProcessaIntegracao("PedidoCompra", pIdIntegracao);

                BackgroundWorker processaItem = new BackgroundWorker();

                processaItem.DoWork += new DoWorkEventHandler(processaPedidoCompraInclui_DoWork);

                MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());

                MasterClass.TextoLog = new System.Text.StringBuilder();

                object[] Argumentos = { pedidoCompraEmProcessamento, integracaoEmProcessamento };

                processaItem.RunWorkerAsync(Argumentos);
            }
            catch (Exception erro)
            {
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Erro: " + erro.Message + Environment.NewLine);
                }

                MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());
            }

            //return integracaoEmProcessamento.Codigo;
        }

        void processaPedidoCompraInclui_DoWork(object sender, DoWorkEventArgs e)
        {

            object[] argumentos = (Object[])e.Argument;

            PedidoCompra pedidoCompraEmProcessamento = (PedidoCompra)argumentos[0];
            Integracao integracaoEmProcessamento = (Integracao)argumentos[1];

            try
            {

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Iniciando processamento de PEDIDO DE COMPRA" + Environment.NewLine);
                }

                

                ////Validando Pedido de Compra
                //if (pedidoCompraEmProcessamento.Status != "0" &&
                //    pedidoCompraEmProcessamento.Status != "1")
                //{
                //    integracaoEmProcessamento.Status = "ERRO";
                //    integracaoEmProcessamento.Mensagem += "Valor inválido para o campo STATUS. ";
                //    integracaoEmProcessamento.DataStatus = DateTime.Now;

                //    integracaoEmProcessamento.Converte().AtualizarRegistro();
                //    IntegracaoGS.processaWSReturnLog(integracaoEmProcessamento);

                //    throw new Exception("Valor inválido para o campo STATUS no objeto PedidoCompra");
                //}
                
                pedidoCompraEmProcessamento.Status = pedidoCompraEmProcessamento.Status == "1" ? "Ativo" : "Inativo";

                //if (pedidoCompraEmProcessamento.NumeroSetor == string.Empty || pedidoCompraEmProcessamento.NumeroSetor == null)
                //{
                //    throw new Exception("Setor não informado para o Pedido de Compra " + pedidoCompraEmProcessamento.Numero);
                //}

                if (!pedidoCompraEmProcessamento.ValidaPedidoCompra())
                {
                    throw MasterClass.UltimoErro;
                }

                pedidoCompraEmProcessamento.InserePedidoCompra(integracaoEmProcessamento);

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Registrando Fim da Integração.ID:" + integracaoEmProcessamento.Codigo + " - Status: " + integracaoEmProcessamento.Status);
                }

                
                IntegracaoGS.processaWSReturnLog(integracaoEmProcessamento);
            }
            catch (Exception erro)
            {               

                integracaoEmProcessamento.Status = "ERRO";
                integracaoEmProcessamento.Mensagem += "[PedidoCompraInclui] " + erro.Message;
                integracaoEmProcessamento.DataStatus = DateTime.Now;

                integracaoEmProcessamento.Converte().AtualizarRegistro();
                IntegracaoGS.processaWSReturnLog(integracaoEmProcessamento);

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Erro: " + erro.Message + Environment.NewLine);
                }
                MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());
            }
        }

        [WebMethod]
        public void AtualizaPedidoCompra(PedidoCompra pPedidoCompra, int pIdIntegracao)
        {
            try
            {
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog = new System.Text.StringBuilder();
                    MasterClass.TextoLog.Append("[ControlsysWS] Iniciando procedimento AtualizaPedidoCompra." + Environment.NewLine);
                    MasterClass.TextoLog.Append("[ControlsysWS] IdIntegracao = " + pIdIntegracao.ToString() + Environment.NewLine);
                    //MasterClass.TextoLog.Append("[ControlsysWS] Número Pedido = " + pPedidoCompra.Numero + Environment.NewLine);
                }

                PedidoCompra pedidoCompraEmProcessamento = pPedidoCompra;

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Registrando Início da Integração" + Environment.NewLine);
                }

                Integracao integracaoEmProcessamento = IntegracaoGS.ProcessaIntegracao("PedidoCompra", pIdIntegracao);

                BackgroundWorker processaItem = new BackgroundWorker();

                processaItem.DoWork += new DoWorkEventHandler(processaPedidoCompraAtualiza_DoWork);

                MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());

                MasterClass.TextoLog = new System.Text.StringBuilder();


                object[] Argumentos = { pedidoCompraEmProcessamento, integracaoEmProcessamento };

                processaItem.RunWorkerAsync(Argumentos);
            }
            catch (Exception erro)
            {
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Erro: " + erro.Message + Environment.NewLine);
                }

                MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());
            }

            //return integracaoEmProcessamento.Codigo;
        }

        void processaPedidoCompraAtualiza_DoWork(object sender, DoWorkEventArgs e)
        {
            object[] argumentos = (Object[])e.Argument;

            PedidoCompra pedidoCompraEmProcessamento = (PedidoCompra)argumentos[0];
            Integracao integracaoEmProcessamento = (Integracao)argumentos[1];

            try
            {

                

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Iniciando processamento de PEDIDO DE COMPRA" + Environment.NewLine);
                }

                ////Validando Pedido de Compra
                //if (pedidoCompraEmProcessamento.Status != "0" &&
                //    pedidoCompraEmProcessamento.Status != "1")
                //{
                //    integracaoEmProcessamento.Status = "ERRO";
                //    integracaoEmProcessamento.Mensagem += "Valor inválido para o campo STATUS. ";
                //    integracaoEmProcessamento.DataStatus = DateTime.Now;

                //    integracaoEmProcessamento.Converte().AtualizarRegistro();
                //    IntegracaoGS.processaWSReturnLog(integracaoEmProcessamento);

                //    throw new Exception("Valor inválido para o campo STATUS no objeto PedidoCompra");
                //}

                pedidoCompraEmProcessamento.Status = pedidoCompraEmProcessamento.Status == "1" ? "Ativo" : "Inativo";

                if (!pedidoCompraEmProcessamento.ValidaPedidoCompra())
                {
                    throw MasterClass.UltimoErro;
                }

                

                //if (pedidoCompraEmProcessamento.NumeroSetor == string.Empty || pedidoCompraEmProcessamento.NumeroSetor == null)
                //{
                //    throw new Exception("Setor não informado para o Pedido de Compra " + pedidoCompraEmProcessamento.Numero);
                //}

                pedidoCompraEmProcessamento.AtualizaPedidoCompra(integracaoEmProcessamento);

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Registrando Fim da Integração.ID:" + integracaoEmProcessamento.Codigo + " - Status: " + integracaoEmProcessamento.Status);
                }

                IntegracaoGS.processaWSReturnLog(integracaoEmProcessamento);

            }
            catch (Exception erro)
            {

                integracaoEmProcessamento.Status = "ERRO";
                integracaoEmProcessamento.Mensagem += "[PedidoCompraAtualiza] " + erro.Message;
                integracaoEmProcessamento.DataStatus = DateTime.Now;

                integracaoEmProcessamento.Converte().AtualizarRegistro();
                IntegracaoGS.processaWSReturnLog(integracaoEmProcessamento);

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Erro: " + erro.Message + Environment.NewLine);
                }

                MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());
            }

        }

        /*
        [WebMethod]
        public Int32 IncluiUnidadeOrganizacional(UnidadeOrganizacional pUnidadeOrganizacional)
        {
            Random identificador = new Random();
            return identificador.Next();
        }

        [WebMethod]
        public Int32 AtualizaUnidadeOrganizacional(UnidadeOrganizacional pUnidadeOrganizacional)
        {
            Random identificador = new Random();
            return identificador.Next();
        }

        

        [WebMethod]
        public Int32 IncluiPosicao(Posicao pPosicao)
        {
            posicaoEmProcessamento = pPosicao;

            Integracao integracaoEmProcessamento = IntegracaoGS.ProcessaIntegracao("POSIÇÃO");

            BackgroundWorker processaItem = new BackgroundWorker();

            processaItem.DoWork += new DoWorkEventHandler(processaPosicaoInclui_DoWork);

            processaItem.RunWorkerAsync();

            return integracaoEmProcessamento.Codigo;
        }

        void processaPosicaoInclui_DoWork(object sender, DoWorkEventArgs e)
        {
            posicaoEmProcessamento.InserePosicao(integracaoEmProcessamento);            
        }

        [WebMethod]
        public Int32 AtualizaPosicao(Posicao pPosicao)
        {
            Random identificador = new Random();
            return identificador.Next();
        }
         */

        /*
        [WebMethod]
        public void IncluiPHT(PHT pPHT, int pIdIntegracao)
        {
            phtEmProcessamento = pPHT;

            Integracao integracaoEmProcessamento = IntegracaoGS.ProcessaIntegracao("PLANO DE HORÁRIO DE TRABALHO", pIdIntegracao);

            BackgroundWorker processaItem = new BackgroundWorker();

            processaItem.DoWork += new DoWorkEventHandler(processaPhtInclui_DoWork);

            processaItem.RunWorkerAsync();

            //return integracaoEmProcessamento.Codigo;
        }

        void processaPhtInclui_DoWork(object sender, DoWorkEventArgs e)
        {
            phtEmProcessamento.InserePht(integracaoEmProcessamento);
            IntegracaoGS.processaWSReturnLog(integracaoEmProcessamento);
        }
         


        [WebMethod]
        public void AtualizaPHT(PHT pPHT, int pIdIntegracao)
        {
            phtEmProcessamento = pPHT;

            Integracao integracaoEmProcessamento = IntegracaoGS.ProcessaIntegracao("PLANO DE HORÁRIO DE TRABALHO", pIdIntegracao);

            BackgroundWorker processaItem = new BackgroundWorker();

            processaItem.DoWork += new DoWorkEventHandler(processaPhtAtualiza_DoWork);

            processaItem.RunWorkerAsync();

            //return integracaoEmProcessamento.Codigo;
        }

        void processaPhtAtualiza_DoWork(object sender, DoWorkEventArgs e)
        {
            phtEmProcessamento.AtualizaPht(integracaoEmProcessamento);
            IntegracaoGS.processaWSReturnLog(integracaoEmProcessamento);
        }
         * */

        //[WebMethod]
        //public void IncluiTipoTreinamento(TipoTreinamento pTipoTreinamento, int pIdIntegracao)
        //{
        //    try
        //    {
        //        if (MasterClass.LogAtivado)
        //        {
        //            MasterClass.TextoLog = new System.Text.StringBuilder();
        //            MasterClass.TextoLog.Append("[ControlsysWS] Iniciando procedimento IncluiColaborador." + Environment.NewLine);
        //            MasterClass.TextoLog.Append("[ControlsysWS] IdIntegracao = " + pIdIntegracao.ToString() + Environment.NewLine);
        //            MasterClass.TextoLog.Append("[ControlsysWS] Código Tipo Treinamento = " + pTipoTreinamento.Codigo + Environment.NewLine);
        //        }
                
        //        TipoTreinamento tipoTreinamentoEmProcessamento = pTipoTreinamento;

        //        if (MasterClass.LogAtivado)
        //        {
        //            MasterClass.TextoLog.Append("[ControlsysWS] Registrando Início da Integração" + Environment.NewLine);
        //        }

        //        Integracao integracaoEmProcessamento = IntegracaoGS.ProcessaIntegracao("TipoTreinamento", pIdIntegracao);

        //        BackgroundWorker processaItem = new BackgroundWorker();

        //        processaItem.DoWork += new DoWorkEventHandler(processaTipoTreinamentoInclui_DoWork);

        //        MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());

        //        MasterClass.TextoLog = new System.Text.StringBuilder();

        //        object[] Argumentos = { tipoTreinamentoEmProcessamento, integracaoEmProcessamento };

        //        processaItem.RunWorkerAsync(Argumentos);
        //    }
        //    catch (Exception erro)
        //    {
        //        if (MasterClass.LogAtivado)
        //        {
        //            MasterClass.TextoLog.Append("[ControlsysWS] Erro: " + erro.Message + Environment.NewLine);
        //        }

        //        MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());
        //    }


        //    //return integracaoEmProcessamento.Codigo;
        //}

        //void processaTipoTreinamentoInclui_DoWork(object sender, DoWorkEventArgs e)
        //{

        //    object[] argumentos = (Object[])e.Argument;

        //    TipoTreinamento tipoTreinamentoEmProcessamento = (TipoTreinamento)argumentos[0];
        //    Integracao integracaoEmProcessamento = (Integracao)argumentos[1];


        //    try
        //    {
        //        if (MasterClass.LogAtivado)
        //        {
        //            MasterClass.TextoLog.Append("[ControlsysWS] Iniciando processamento de TIPO DE TREINAMENTO" + Environment.NewLine);
        //        }

                
        //        tipoTreinamentoEmProcessamento.InsereTipoTreinamento(integracaoEmProcessamento);

        //        if (MasterClass.LogAtivado)
        //        {
        //            MasterClass.TextoLog.Append("[ControlsysWS] Registrando Fim da Integração.ID:" + integracaoEmProcessamento.Codigo + " - Status: " + integracaoEmProcessamento.Status);
        //        }

        //        IntegracaoGS.processaWSReturnLog(integracaoEmProcessamento);
        //    }
        //    catch (Exception erro)
        //    {
        //        integracaoEmProcessamento.Status = "ERRO";
        //        integracaoEmProcessamento.Mensagem += "[TreinamentoInclui] " + erro.Message;
        //        integracaoEmProcessamento.DataStatus = DateTime.Now;

        //        integracaoEmProcessamento.Converte().AtualizarRegistro();
        //        IntegracaoGS.processaWSReturnLog(integracaoEmProcessamento);

        //        if (MasterClass.LogAtivado)
        //        {
        //            MasterClass.TextoLog.Append("[ControlsysWS] Erro: " + erro.Message + Environment.NewLine);
        //        }
        //        MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());
        //    }
        //}

        //[WebMethod]
        //public void AtualizaTipoTreinamento(TipoTreinamento pTipoTreinamento, int pIdIntegracao)
        //{
        //    Random identificador = new Random();
        //    //return identificador.Next();
        //}

        [WebMethod]
        public void IncluiASO(ASO pASO, int pIdIntegracao)
        {
            try
            {
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog = new System.Text.StringBuilder();
                    MasterClass.TextoLog.Append("[ControlsysWS] Iniciando procedimento IncluiASO." + Environment.NewLine);
                    MasterClass.TextoLog.Append("[ControlsysWS] IdIntegracao = " + pIdIntegracao.ToString() + Environment.NewLine);
                    //MasterClass.TextoLog.Append("[ControlsysWS] Cpf ASO = " + pASO.Cpf + Environment.NewLine);
                }

                ASO asoEmProcessamento = pASO;

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Registrando Início da Integração" + Environment.NewLine);
                }

                Integracao integracaoEmProcessamento = IntegracaoGS.ProcessaIntegracao("ASO", pIdIntegracao);

                BackgroundWorker processaItem = new BackgroundWorker();

                processaItem.DoWork += new DoWorkEventHandler(processaAsoInclui_DoWork);

                MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());

                MasterClass.TextoLog = new System.Text.StringBuilder();

                object[] Argumentos = { asoEmProcessamento, integracaoEmProcessamento };

                processaItem.RunWorkerAsync(Argumentos);

            }
            catch (Exception erro)
            {
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Erro: " + erro.Message + Environment.NewLine);
                }
                MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());
            }

            //return integracaoEmProcessamento.Codigo;
        }

        void processaAsoInclui_DoWork(object sender, DoWorkEventArgs e)
        {
            object[] argumentos = (Object[])e.Argument;

            ASO asoEmProcessamento = (ASO)argumentos[0];
            Integracao integracaoEmProcessamento = (Integracao)argumentos[1];

            try
            {
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Iniciando processamento de ASO" + Environment.NewLine);
                }

                if (!asoEmProcessamento.ValidaAso())
                {
                    throw MasterClass.UltimoErro;
                }
                
                asoEmProcessamento.InsereAso(integracaoEmProcessamento);

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Registrando Fim da Integração.ID:" + integracaoEmProcessamento.Codigo + " - Status: " + integracaoEmProcessamento.Status);
                }

                IntegracaoGS.processaWSReturnLog(integracaoEmProcessamento);
            }
            catch (Exception erro)
            {

                integracaoEmProcessamento.Status = "ERRO";
                integracaoEmProcessamento.Mensagem += "[AsoInclui] " + erro.Message;
                integracaoEmProcessamento.DataStatus = DateTime.Now;

                integracaoEmProcessamento.Converte().AtualizarRegistro();
                IntegracaoGS.processaWSReturnLog(integracaoEmProcessamento);

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Erro: " + erro.Message + Environment.NewLine);
                }
                MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());
            }
        }

        [WebMethod]
        public void IncluiMarcacaoExame(MarcacaoExame pMarcacaoExame, int pIdIntegracao)
        {
            try
            {
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog = new System.Text.StringBuilder();
                    MasterClass.TextoLog.Append("[ControlsysWS] Iniciando procedimento IncluiColaborador." + Environment.NewLine);
                    MasterClass.TextoLog.Append("[ControlsysWS] IdIntegracao = " + pIdIntegracao.ToString() + Environment.NewLine);
                    //MasterClass.TextoLog.Append("[ControlsysWS] Cpf Marcação Exame = " + pMarcacaoExame.Cpf + Environment.NewLine);
                }

                MarcacaoExame marcacaoExameEmProcessamento = pMarcacaoExame;

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Registrando Início da Integração" + Environment.NewLine);
                }

                Integracao integracaoEmProcessamento = IntegracaoGS.ProcessaIntegracao("MarcacaoExame", pIdIntegracao);

                BackgroundWorker processaItem = new BackgroundWorker();

                processaItem.DoWork += new DoWorkEventHandler(processaMarcacaoExameInclui_DoWork);

                MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());

                MasterClass.TextoLog = new System.Text.StringBuilder();

                object[] Argumentos = { marcacaoExameEmProcessamento, integracaoEmProcessamento };

                processaItem.RunWorkerAsync(Argumentos);

                //return integracaoEmProcessamento.Codigo;
            }
            catch (Exception erro)
            {
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Erro: " + erro.Message + Environment.NewLine);
                }

                MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());
            }

        }

        void processaMarcacaoExameInclui_DoWork(object sender, DoWorkEventArgs e)
        {
            object[] argumentos = (Object[])e.Argument;

            MarcacaoExame marcacaoExameEmProcessamento = (MarcacaoExame)argumentos[0];
            Integracao integracaoEmProcessamento = (Integracao)argumentos[1];


            try
            {
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Iniciando processamento de MARCAÇÃO DE EXAME" + Environment.NewLine);
                }

                if (!marcacaoExameEmProcessamento.ValidaMarcacaoExame())
                {
                    throw MasterClass.UltimoErro;
                }
                
                marcacaoExameEmProcessamento.InsereMarcacaoExame(integracaoEmProcessamento);

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Iniciando processamento de MARCAÇÃO DE EXAME" + Environment.NewLine);
                }

                IntegracaoGS.processaWSReturnLog(integracaoEmProcessamento);
            }
            catch (Exception erro)
            {
                integracaoEmProcessamento.Status = "ERRO";
                integracaoEmProcessamento.Mensagem += "[MarcacaoExameInclui] " + erro.Message;
                integracaoEmProcessamento.DataStatus = DateTime.Now;

                integracaoEmProcessamento.Converte().AtualizarRegistro();
                IntegracaoGS.processaWSReturnLog(integracaoEmProcessamento);

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Erro: " + erro.Message + Environment.NewLine);
                }
                MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());
            }
        }

        //[WebMethod]
        //public void AtualizaMarcacaoExame(MarcacaoExame pMarcacaoExame, int pIdIntegracao)
        //{
        //    Random identificador = new Random();
        //    return identificador.Next();
        //}

        [WebMethod]
        public void IncluiAfastamento(Afastamento pAfastamento, int pIdIntegracao)
        {
            try
            {
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog = new System.Text.StringBuilder();
                    MasterClass.TextoLog.Append("[ControlsysWS] Iniciando procedimento IncluiAfastamento." + Environment.NewLine);
                    MasterClass.TextoLog.Append("[ControlsysWS] IdIntegracao = " + pIdIntegracao.ToString() + Environment.NewLine);
                    //MasterClass.TextoLog.Append("[ControlsysWS] Cpf Afastamento = " + pAfastamento.Cpf + Environment.NewLine);
                }

                Afastamento afastamentoEmProcessamento = pAfastamento;

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Registrando Início da Integração" + Environment.NewLine);
                }

                Integracao integracaoEmProcessamento = IntegracaoGS.ProcessaIntegracao("Afastamento", pIdIntegracao);

                BackgroundWorker processaItem = new BackgroundWorker();

                processaItem.DoWork += new DoWorkEventHandler(processaAfastamentoInclui_DoWork);

                MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());

                MasterClass.TextoLog = new System.Text.StringBuilder();

                object[] Argumentos = { afastamentoEmProcessamento, integracaoEmProcessamento };

                processaItem.RunWorkerAsync(Argumentos);

                //return integracaoEmProcessamento.Codigo;
            }
            catch (Exception erro)
            {
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Erro: " + erro.Message + Environment.NewLine);
                }

                MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());
            }
        }

        void processaAfastamentoInclui_DoWork(object sender, DoWorkEventArgs e)
        {
            object[] argumentos = (Object[])e.Argument;

            Afastamento afastamentoEmProcessamento = (Afastamento)argumentos[0];
            Integracao integracaoEmProcessamento = (Integracao)argumentos[1];

            try
            {
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Iniciando processamento de AFASTAMENTO " + Environment.NewLine);
                }

                if (!afastamentoEmProcessamento.ValidaAfastamento())
                {
                    throw MasterClass.UltimoErro;
                }

                afastamentoEmProcessamento.InsereAfastamento(integracaoEmProcessamento);

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Registrando Fim da Integração.ID:" + integracaoEmProcessamento.Codigo + " - Status: " + integracaoEmProcessamento.Status);
                }

                IntegracaoGS.processaWSReturnLog(integracaoEmProcessamento);
            }
            catch (Exception erro)
            {
                integracaoEmProcessamento.Status = "ERRO";
                integracaoEmProcessamento.Mensagem += "[AfastamentoInclui] " + erro.Message;
                integracaoEmProcessamento.DataStatus = DateTime.Now;

                integracaoEmProcessamento.Converte().AtualizarRegistro();
                IntegracaoGS.processaWSReturnLog(integracaoEmProcessamento);

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Erro: " + erro.Message + Environment.NewLine);
                }
                MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());
            }
        }

        [WebMethod]
        public void IncluiFerias(Ferias pFerias, int pIdIntegracao)
        {
            try
            {
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog = new System.Text.StringBuilder();
                    MasterClass.TextoLog.Append("[ControlsysWS] Iniciando procedimento IncluiFerias." + Environment.NewLine);
                    MasterClass.TextoLog.Append("[ControlsysWS] IdIntegracao = " + pIdIntegracao.ToString() + Environment.NewLine);
                    //MasterClass.TextoLog.Append("[ControlsysWS] Cpf Férias = " + pFerias.Cpf + Environment.NewLine);
                }

                Ferias feriasEmProcessamento = pFerias;

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Registrando Início da Integração" + Environment.NewLine);
                }

                Integracao integracaoEmProcessamento = IntegracaoGS.ProcessaIntegracao("Ferias", pIdIntegracao);

                BackgroundWorker processaItem = new BackgroundWorker();

                processaItem.DoWork += new DoWorkEventHandler(processaFeriasInclui_DoWork);

                MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());

                MasterClass.TextoLog = new System.Text.StringBuilder();


                object[] Argumentos = { feriasEmProcessamento, integracaoEmProcessamento };

                processaItem.RunWorkerAsync(Argumentos);

                //return integracaoEmProcessamento.Codigo;
            }
            catch (Exception erro)
            {
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Erro: " + erro.Message + Environment.NewLine);
                }
            }
        }

        void processaFeriasInclui_DoWork(object sender, DoWorkEventArgs e)
        {

            object[] argumentos = (Object[])e.Argument;

            Ferias feriasEmProcessamento = (Ferias)argumentos[0];
            Integracao integracaoEmProcessamento = (Integracao)argumentos[1];           

            try
            {
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Iniciando processamento de FERIAS" + Environment.NewLine);
                }

                if (!feriasEmProcessamento.ValidaFerias())
                {
                    throw MasterClass.UltimoErro;
                }                

                feriasEmProcessamento.InsereFerias(integracaoEmProcessamento);

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Registrando Fim da Integração.ID:" + integracaoEmProcessamento.Codigo + " - Status: " + integracaoEmProcessamento.Status);
                }

                IntegracaoGS.processaWSReturnLog(integracaoEmProcessamento);
            }
            catch (Exception erro)
            {

                integracaoEmProcessamento.Status = "ERRO";
                integracaoEmProcessamento.Mensagem += "[FeriasInclui] " + erro.Message;
                integracaoEmProcessamento.DataStatus = DateTime.Now;

                integracaoEmProcessamento.Converte().AtualizarRegistro();
                IntegracaoGS.processaWSReturnLog(integracaoEmProcessamento);

                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append("[ControlsysWS] Erro: " + erro.Message + Environment.NewLine);
                }
                MasterClass.GravaLogErro(MasterClass.TextoLog.ToString());
            }
        }

        [WebMethod]
        public Integracao ConsultaIntegracao(Int32 pCodigoIntegracao)
        {

            return Integracao.Obtem(pCodigoIntegracao);

        }

        [WebMethod]
        public List<Integracao> ListaIntegracoes(string pStatus)
        {

            return Integracao.Obtem(pStatus);

        }
        
        /*
        #region Métodos de Teste
       
        //[WebMethod]
        //public string TestaIncluiColaborador(
        //    string pApelido, string pCep, string pCidade_Naturalidade, string pCnh_Numero, string pCnh_Tipo, string pCnh_Data_Validade, string pCnpj_Empresa, int pCodigo, string pTelefone1, 
        //    string pTelefone2, string pTelefone3, string pCpf, string pCtps_Data_Emissao, string pCtps_Estado, int pCtps_Numero, string pCtps_Serie, string pData_Admissao, string pEmail,
        //    string pEndereco_Bairro, string pEndereco_Cidade, string pEndereco_Complemento, string pEndereco_Estado, string pEndereco_Logradouro, string pEndereco_Numero, 
        //    string pEndereco_Pais, string pEscolaridade, string pEstado_Civil, string pData_Expedicao_RNE, string pMensalista, string pNome, string pNome_Mae, string pNome_Pai, string pCRNR,
        //    string pObservacao, string pNumero_Passaporte, int pPis, string pRegistro_Profissional, string pData_Emissao_Rg, string pEstado_Emissao_RG, string pNumero_RG, string pRNE,
        //    decimal pSalario, string pSexo, string pStatus_Ocupacao, string pTipo_CRNR, string pTipo_Sanguineo, int pNumero_Titulo_Eleitor, int pSecao_Titulo_Eleitor, int pZona_Titulo_Eleitor,
        //    string pData_Emissao_Visto, string pData_Validade_Visto
        //    )
        //{
        //    Colaborador teste = new Colaborador();
        //    teste.Apelido = pApelido;
        //    teste.Cep = pCep;
        //    teste.CidadeNaturalidade = pCidade_Naturalidade;
        //    teste.CnhNumero = pCnh_Numero;
        //    teste.CnhTipo = pCnh_Tipo;
        //    teste.CnhValidade = pCnh_Data_Validade;
        //    teste.CnpjEmpresa = pCnpj_Empresa;
        //    //teste.Codigo = ;
        //    teste.ContatoTelefone01 = pTelefone1;
        //    teste.ContatoTelefone02 = pTelefone2;
        //    teste.ContatoTelefone03 = pTelefone3;
        //    teste.Cpf = pCpf;
        //    teste.CtpsEmissao = pCtps_Data_Emissao;
        //    teste.CtpsEstado = pCtps_Estado;
        //    teste.CtpsNumero = pCtps_Numero;
        //    teste.CtpsSerie = pCtps_Serie;
        //    teste.DataAdmissao = pData_Admissao;
        //    teste.Email = "contato@email.com";
        //    teste.EnderecoBairro = "BAIRRO TESTE";
        //    teste.EnderecoCidade = "VITORIA";
        //    teste.EnderecoComplemento = "COMPLEMENTO";
        //    teste.EnderecoEstado = "ES";
        //    teste.EnderecoLogradouro = "LOGRADOURO TESTE";
        //    teste.EnderecoNumero = "0";
        //    teste.EnderecoPais = "BRASIL";
        //    teste.Escolaridade = "Teste";
        //    teste.EstadoCivil = "Teste";
        //    teste.ExpedicaoRne = DateTime.Now.ToString();
        //    teste.Mensalista = "MENSALISTA";
        //    teste.NauralidadePais = "BRASIL";
        //    teste.Nome = "Nome";
        //    teste.NomeMae = "Mãe";
        //    teste.NomePai = "Pai";
        //    teste.NumeroCrNr = "0000";
        //    teste.Observacao = "Observacao";
        //    teste.PassaporteNumero = "000000";
        //    teste.Pis = 0;
        //    teste.RegistroProfissional = "000000";
        //    teste.RgEmissao = DateTime.Now.ToString();
        //    teste.RgEstadoEmissor = "ES";
        //    teste.RgNumero = "0000000";
        //    teste.RgOrgaoEmissor = "ssp";
        //    teste.Rne = String.Empty;
        //    teste.Salario = 1234.56M;
        //    teste.Sexo = "Masculino";
        //    teste.StatusOcupacao = "teste";
        //    teste.TipoCrNr = "TipoCrNr";
        //    teste.TipoSanguineo = "Opos";
        //    teste.TituloEleitorNumero = 0000000;
        //    teste.TituloEleitorSecao = 123;
        //    teste.TituloEleitorZona = 99;
        //    teste.VistoEmissao = DateTime.Now.ToString();
        //    teste.VistoValidade = DateTime.Now.ToString();

        //    return IncluiColaborador(teste).ToString();

        //}

        [WebMethod]
        public void TestaIncluiColaborador()            
        {
            Colaborador teste = new Colaborador();
            teste.Apelido = string.Empty;
            teste.Cep = "00000-000";
            teste.CidadeNaturalidade = "Naturalidade";
            teste.CnhNumero = "0000000000";
            teste.CnhTipo = "XX";
            teste.CnhValidade = DateTime.Now.ToUniversalTime().ToString("s");//"2015-07-31T15:30:21Z";
            teste.CnpjEmpresa = "000000000001";
            //teste.Codigo = 0;
            teste.ContatoTelefone01 = "1111-1111";
            teste.ContatoTelefone02 = "2222-2222";
            teste.ContatoTelefone03 = "3333-3333";
            teste.Cpf = "00000000000";
            teste.CtpsEmissao = "2015-07-31T15:30:21Z";
            teste.CtpsEstado = "ES";
            teste.CtpsNumero = " 0" ;
            teste.CtpsSerie = "X";
            teste.DataAdmissao = "2015-07-31T15:30:21Z";
            teste.Email = "contato@email.com";
            teste.EnderecoBairro = "BAIRRO TESTE";
            teste.EnderecoCidade = "Vitória";            
            teste.EnderecoComplemento = "COMPLEMENTO";
            teste.EnderecoEstado = "ES";
            teste.EnderecoLogradouro = "RUA TESTE";
            teste.EnderecoNumero = "0";
            teste.EnderecoPais = "BRASIL";
            teste.Escolaridade = "Teste";
            teste.EstadoCivil = "Teste";
            teste.ExpedicaoRne = DateTime.Now.ToString();
            teste.Mensalista = "MENSALISTA";
            teste.NauralidadePais = "BRASIL";
            teste.Nome = "Nome";
            teste.NomeMae = "Mãe";
            teste.NomePai = "Pai";
            teste.NumeroCrNr = "0000";
            teste.Observacao = "Observacao";
            teste.PassaporteNumero = "000000";
            teste.Pis = "0" ;
            teste.RegistroProfissional = "000000";
            teste.RgEmissao = DateTime.Now.ToString();
            teste.RgEstadoEmissor = "ES";
            teste.RgNumero = "0000000";
            teste.RgOrgaoEmissor = "ssp";
            teste.Rne = String.Empty;
            teste.Salario = null;
            teste.Sexo = "Masculino";
            teste.StatusOcupacao = "teste";
            teste.TipoCrNr = "TipoCrNr";
            teste.TipoSanguineo = "Opos";
            teste.TituloEleitorNumero = "0000000" ;
            teste.TituloEleitorSecao = " 123" ;
            teste.TituloEleitorZona = " 99";
            teste.VistoEmissao = DateTime.Now.ToString();
            teste.VistoValidade = DateTime.Now.ToString();
            teste.NumeroUnidadeOrganizacional = "10";
            teste.NomeUnidadeOrganizacional = "Unidade Organizacional";
            teste.NascimentoUf = "ES";
            teste.DataNascimento = "1990-09-30T20:09:02";
            teste.CodigoPosicao = "99";
            teste.NomePosicao = "Teste";
            teste.PhtCodigo = "13" ;
            teste.PhtValidadeInicial = "2015-01-01T09:00:00";
            teste.PhtValidadeFinal = "2015-12-31T09:00:00";
            teste.TreinamentoCodigoSap = "1";
            teste.TreinamentoValidade = "2016-07-01T15:00:00";

            teste.NascimentoUf = "ES";
            teste.DataNascimento = "1998-07-31T15:30:21Z";
            teste.NumeroUnidadeOrganizacional = "99";
            teste.NomeUnidadeOrganizacional = "Teste";
            teste.CodigoPosicao = "99";
            teste.NomePosicao = "Teste";
            teste.CentroCustoCodigo = "1";
            teste.PhtCodigo = "13";
            teste.PhtValidadeInicial = "2015-01-01T00:00:01";
            teste.PhtValidadeFinal = "2015-12-31T23:59:99";          
            teste.TreinamentoValidade = "2014-10-10T12:00:00";
            
            IncluiColaborador(teste, 1);

        }

        [WebMethod]
        public void TestaCentroCusto()
        {
            CentroCusto teste = new CentroCusto();
            
            teste.Descricao = "Descrição Centro de Custo";
            teste.Nome = "Nome CC";
            teste.Numero = "1";

            IncluiCentroCusto(teste, 2);

        }

        [WebMethod]
        public void TestaAtualizaCentroCusto()
        {
            CentroCusto teste = new CentroCusto();
            //teste.Codigo = 10;
            teste.Descricao = "Descrição Modificada Centro de Custo";
            teste.Nome = "Nome CC";
            teste.Numero = "1";

            AtualizaCentroCusto(teste, 3);

        }

        [WebMethod]
        public void TestaElementoPep()
        {
            ElementoPep teste = new ElementoPep();
            
            teste.Descricao = "Descrição";
            teste.Nome = "Nome";
            teste.Numero = "99";

            IncluiElementoPep(teste,4);

        }

        [WebMethod]
        public void TestaAtualizaElementoPep()
        {
            ElementoPep teste = new ElementoPep();
            //teste.Codigo = 11;
            teste.Descricao = "Elemento Pep Alterado";
            teste.Nome = "PEP";
            teste.Numero = "99";

            AtualizaElementoPep(teste, 5);

        }

        [WebMethod]
        public void TestaFornecedor()
        {
            Fornecedor resposta = new Fornecedor();

            resposta.Ativo = 1;
            resposta.Cnpj = "000000000002";            
            //resposta.Email = "teste@email.com";
            resposta.InscricaoEstadual = "000000";
            resposta.InscricaoMunicipal = "00000";
            //resposta.NomeFantasia = "Fantasia";
            resposta.RazaoSocial = "Teste";
            resposta.Telefone = "00000000";

            IncluiFornecedor(resposta, 6);
        }

        [WebMethod]
        public void TestaPedidoCompra()
        {
            PedidoCompra resposta = new PedidoCompra();

            //resposta.Cnae = "0000";
            //resposta.CodigoEmpresa = "EMP0000001";
            resposta.CnpjFornecedor = "000000000002";
            resposta.Nome = "Nome";
            resposta.Numero = "0001";            
            //resposta.Observacao = "Teste";
            resposta.Status = "1";
            resposta.DataInicio = DateTime.Now.ToString();
            resposta.DataFim = DateTime.Now.AddDays(30).ToString();
            resposta.NumeroSetor = "13";
            //resposta.TipoSetor = "CentroCusto";

            


            IncluiPedidoCompra(resposta, 7);

        }



        //[WebMethod]
        //public string TestaPosicao()
        //{
        //    Posicao resposta = new Posicao();

        //    resposta.Codigo = 0;
        //    resposta.DataFim = DateTime.Now.AddDays(10);
        //    resposta.DataInicio = DateTime.Now;
        //    resposta.Descricao = "Teste";

        //    return IncluiPosicao(resposta).ToString();

        //}

        [WebMethod]
        public void TestaAfastamento()
        {
            Afastamento resposta = new Afastamento();

            resposta.Cpf = "11122233344";
            resposta.DataInicio = DateTime.Now.AddDays(1).ToString();
            resposta.DataRetorno = DateTime.Now.AddDays(10).ToString();


            IncluiAfastamento(resposta,8);
        }

        [WebMethod]
        public void TestaAso()
        {
            ASO resposta = new ASO();

            resposta.Cpf = "11122233344";
            resposta.TipoAtendimento = 2;
            resposta.Validade = DateTime.Now.AddYears(1).ToString();


            IncluiASO(resposta,9);
        }

        [WebMethod]
        public void TestaMarcacaoExame()
        {
            MarcacaoExame resposta = new MarcacaoExame();

            resposta.Cpf = "11122233344";
            resposta.TipoAtendimento = 2;
            resposta.DataMarcacao = DateTime.Now.AddMonths(1).ToString();


           IncluiMarcacaoExame(resposta,10);
        }

        [WebMethod]
        public void TestaFerias()
        {
            Ferias resposta = new Ferias();

            resposta.Cpf = "11122233344";
            resposta.DataInicio = DateTime.Now.AddDays(1).ToString();
            resposta.DataRetorno = DateTime.Now.AddDays(10).ToString();


            IncluiFerias(resposta,11);
        }

        //[WebMethod]
        //public void TestaPht()
        //{
        //    PHT resposta = new PHT();

        //    resposta.Ativo = 1;
        //    resposta.Codigo = 1;
        //    resposta.Escala = 0;
        //    resposta.Feriado = 0;
        //    resposta.Nome = "Turno 1";
        //    resposta.HoraFimDia1 = String.Empty;
        //    resposta.HoraFimDia2 = "17:00";
        //    resposta.HoraFimDia3 = "17:00";
        //    resposta.HoraFimDia4 = "17:00";
        //    resposta.HoraFimDia5 = String.Empty;
        //    resposta.HoraFimDia6 = String.Empty;
        //    resposta.HoraFimDia7 = String.Empty;

        //    resposta.HoraInicioDia1 = String.Empty;
        //    resposta.HoraInicioDia2 = "8:00";
        //    resposta.HoraInicioDia3= "8:00";
        //    resposta.HoraInicioDia4 = "8:00";
        //    resposta.HoraInicioDia5 = String.Empty;
        //    resposta.HoraInicioDia6 = String.Empty;
        //    resposta.HoraInicioDia7 = String.Empty;

        //    resposta.IntervaloDia1 = Int32.MinValue;
        //    resposta.IntervaloDia2 = 1;
        //    resposta.IntervaloDia3 = 1;
        //    resposta.IntervaloDia4 = 1;
        //    resposta.IntervaloDia5 = Int32.MinValue;
        //    resposta.IntervaloDia6 = Int32.MinValue;
        //    resposta.IntervaloDia7 = Int32.MinValue;

        //    resposta.NumeroSemana = 1;

        //    IncluiPHT(resposta,12);




        //}
        

        [WebMethod]
        public void TestaFornecedorAtualiza()
        {
            Fornecedor resposta = new Fornecedor();

            resposta.Ativo = 1;
            resposta.Cnpj = "000000000002";            
            //resposta.Email = "teste@email.com";
            resposta.InscricaoEstadual = "000001";
            resposta.InscricaoMunicipal = "00001";
            //resposta.NomeFantasia = "Fantasia Alterado";
            resposta.RazaoSocial = "Razão Social Alterado";
            resposta.Telefone = "1234-5678";

            AtualizaFornecedor(resposta,13);
        }



        [WebMethod]
        public void TestaPedidoCompraAtualiza()
        {
            PedidoCompra resposta = new PedidoCompra();

            //resposta.Cnae = "0000";
            //resposta.Codigo = 8;
            //resposta.CodigoEmpresa = "EMP0000001" ;
            resposta.CnpjFornecedor = "000000000002";
            resposta.Nome = "Nome Alterado";
            resposta.Numero = "0001";            
            //resposta.Observacao = "Teste de Atualização";
            resposta.Status = "Ativo";
            resposta.DataInicio = DateTime.Now.AddDays(15).ToString();
            resposta.DataFim = DateTime.Now.AddDays(45).ToString();
            resposta.NumeroSetor = "1";
            //resposta.TipoSetor = "CentroCusto";


            AtualizaPedidoCompra(resposta,14);

        }

        //[WebMethod]
        //public void TestaPhtAtualiza()
        //{
        //    PHT resposta = new PHT();

        //    resposta.Ativo = 1;
        //    resposta.Codigo = 13;
        //    resposta.Escala = 0;
        //    resposta.Feriado = 0;
        //    resposta.Nome = "Turno 1";
        //    resposta.HoraFimDia1 = String.Empty;
        //    resposta.HoraFimDia2 = "18:00";
        //    resposta.HoraFimDia3 = "18:00";
        //    resposta.HoraFimDia4 = "18:00";
        //    resposta.HoraFimDia5 = "18:00";
        //    resposta.HoraFimDia6 = "18:00";
        //    resposta.HoraFimDia7 = String.Empty;

        //    resposta.HoraInicioDia1 = String.Empty;
        //    resposta.HoraInicioDia2 = "9:00";
        //    resposta.HoraInicioDia3 = "9:00";
        //    resposta.HoraInicioDia4 = "9:00";
        //    resposta.HoraInicioDia5 = "9:00";
        //    resposta.HoraInicioDia6 = "9:00";
        //    resposta.HoraInicioDia7 = String.Empty;

        //    resposta.IntervaloDia1 = Int32.MinValue;
        //    resposta.IntervaloDia2 = 1;
        //    resposta.IntervaloDia3 = 1;
        //    resposta.IntervaloDia4 = 1;
        //    resposta.IntervaloDia5 = 1;
        //    resposta.IntervaloDia6 = 1;
        //    resposta.IntervaloDia7 = Int32.MinValue;

        //    resposta.NumeroSemana = 1;

        //    AtualizaPHT(resposta,15);




        //}

        [WebMethod]
        public void TestaIncluiColaboradorAtualiza()
        {
            Colaborador teste = new Colaborador();
            teste.Apelido = string.Empty;
            teste.Cep = "00000-000";
            teste.CidadeNaturalidade = "Naturalidade";
            teste.CnhNumero = "0000000000";
            teste.CnhTipo = "XX";
            teste.CnhValidade = DateTime.Now.ToUniversalTime().ToString("s");//"2015-07-31T15:30:21Z";
            teste.CnpjEmpresa = "000000000000";
            //teste.Codigo = 0;
            teste.ContatoTelefone01 = "1111-1111";
            teste.ContatoTelefone02 = "2222-2222";
            teste.ContatoTelefone03 = "3333-3333";
            teste.Cpf = "11122233344";
            teste.CtpsEmissao = "2015-07-31T15:30:21Z";
            teste.CtpsEstado = "ES";
            teste.CtpsNumero = " 0" ;
            teste.CtpsSerie = "X";
            teste.DataAdmissao = "2015-07-31T15:30:21Z";
            teste.Email = "contato@email.com";
            teste.EnderecoBairro = "BAIRRO TESTE";
            teste.EnderecoCidade = "Vitória";
            teste.EnderecoComplemento = "COMPLEMENTO";
            teste.EnderecoEstado = "ES";
            teste.EnderecoLogradouro = "rua TESTE";
            teste.EnderecoNumero = "0";
            teste.EnderecoPais = "BRASIL";
            teste.Escolaridade = "Teste";
            teste.EstadoCivil = "Teste";
            teste.ExpedicaoRne = "2015-07-31T15:30:21Z";
            teste.Mensalista = "MENSALISTA";
            teste.NauralidadePais = "BRASIL";
            teste.Nome = "Fulano da Silva Pereira Júnior";
            teste.NomeMae = "Mãe";
            teste.NomePai = "Pai";
            teste.NumeroCrNr = "0000";
            teste.Observacao = "Observacao";
            teste.PassaporteNumero = "000000";
            teste.Pis = "0";
            teste.RegistroProfissional = "000000";
            teste.RgEmissao = "2015-07-31T15:30:21Z";
            teste.RgEstadoEmissor = "ES";
            teste.RgNumero = "0000000";
            teste.RgOrgaoEmissor = "ssp";
            teste.Rne = String.Empty;
            teste.Salario = null;
            teste.Sexo = "Masculino";
            teste.StatusOcupacao = "teste";
            teste.TipoCrNr = "TipoCrNr";
            teste.TipoSanguineo = "Opos";
            teste.TituloEleitorNumero = "0000000";
            teste.TituloEleitorSecao = "123";
            teste.TituloEleitorZona = "99";
            teste.VistoEmissao = "2015-07-31T15:30:21Z";
            teste.VistoValidade = "2015-07-31T15:30:21Z";

            teste.NascimentoUf = "ES";
            teste.DataNascimento = "1998-07-31T15:30:21Z";
            teste.NumeroUnidadeOrganizacional = "99";
            teste.NomeUnidadeOrganizacional = "Teste";
            teste.CodigoPosicao = "99";
            teste.NomePosicao = "Teste";
            teste.CentroCustoCodigo = "1";
            teste.PhtCodigo = "1";
            teste.PhtValidadeInicial = "2015-01-01T00:00:01";
            teste.PhtValidadeFinal = "2015-12-31T23:59:99";
            teste.TreinamentoCodigoSap = "99";
            teste.TreinamentoValidade = "2014-10-10T12:00:00";
            

            AtualizaColaborador(teste,16);

        }

        [WebMethod]
        public void TestaAtualizaColaborador()
        {
            Colaborador teste = new Colaborador();
            teste.Apelido = string.Empty;
            teste.Cep = "00000-000";
            teste.CidadeNaturalidade = "Naturalidade";
            teste.CnhNumero = "0000000000";
            teste.CnhTipo = "XX";
            teste.CnhValidade = DateTime.Now.ToUniversalTime().ToString("s");//"2015-07-31T15:30:21Z";
            teste.CnpjEmpresa = "000000000001";
            //teste.Codigo = 0;
            teste.ContatoTelefone01 = "1111-1111";
            teste.ContatoTelefone02 = "2222-2222";
            teste.ContatoTelefone03 = "3333-3333";
            teste.Cpf = "11122233344";
            teste.CtpsEmissao = "2015-07-31T15:30:21Z";
            teste.CtpsEstado = "ES";
            teste.CtpsNumero = "0";
            teste.CtpsSerie = "X";
            teste.DataAdmissao = "2015-07-31T15:30:21Z";
            teste.Email = "contato@email.com";
            teste.EnderecoBairro = "BAIRRO TESTE";
            teste.EnderecoCidade = "Vitória";
            teste.EnderecoComplemento = "COMPLEMENTO";
            teste.EnderecoEstado = "ES";
            teste.EnderecoLogradouro = "rua TESTE";
            teste.EnderecoNumero = "0";
            teste.EnderecoPais = "BRASIL";
            teste.Escolaridade = "Teste";
            teste.EstadoCivil = "Teste";
            teste.ExpedicaoRne = "2015-07-31T15:30:21Z";
            teste.Mensalista = "MENSALISTA";
            teste.NauralidadePais = "BRASIL";
            teste.Nome = "Fulano da Silva Pereira Júnior";
            teste.NomeMae = "Mãe";
            teste.NomePai = "Pai";
            teste.NumeroCrNr = "0000";
            teste.Observacao = "Observacao";
            teste.PassaporteNumero = "000000";
            teste.Pis = "0";
            teste.RegistroProfissional = "000000";
            teste.RgEmissao = "2015-07-31T15:30:21Z";
            teste.RgEstadoEmissor = "ES";
            teste.RgNumero = "0000000";
            teste.RgOrgaoEmissor = "ssp";
            teste.Rne = String.Empty;
            teste.Salario = null;
            teste.Sexo = "Masculino";
            teste.StatusOcupacao = "4";
            teste.TipoCrNr = "TipoCrNr";
            teste.TipoSanguineo = "Opos";
            teste.TituloEleitorNumero = "0000000";
            teste.TituloEleitorSecao = "123";
            teste.TituloEleitorZona = "99";
            teste.VistoEmissao = "2015-07-31T15:30:21Z";
            teste.VistoValidade = "2015-07-31T15:30:21Z";

            teste.NascimentoUf = "ES";
            teste.DataNascimento = "1998-07-31T15:30:21Z";
            teste.NumeroUnidadeOrganizacional = "99";
            teste.NomeUnidadeOrganizacional = "Teste";
            teste.CodigoPosicao = "99";
            teste.NomePosicao = "Teste";
            teste.CentroCustoCodigo = "1";
            teste.PhtCodigo = "1" ;
            teste.PhtValidadeInicial = "2015-01-01T00:00:01";
            teste.PhtValidadeFinal = "2015-12-31T23:59:99";
            teste.TreinamentoCodigoSap = "99";
            teste.TreinamentoValidade = "2014-10-10T12:00:00";


            AtualizaColaborador(teste,17);

        }

        [WebMethod]
        public void TestaIncluiFerias()
        {
            Ferias teste = new Ferias();

            teste.Cpf = "11122233344";
            teste.DataInicio = DateTime.Now.AddMonths(1).ToString();
            teste.DataRetorno = DateTime.Now.AddMonths(1).AddDays(15).ToString();

            IncluiFerias(teste,18);

        }

        [WebMethod]
        public String TestaEstado(string Sigla)
        {
            return EstadoGS.Obtem(Sigla).Nome;
        }

        #endregion
        */
      
    }
}
