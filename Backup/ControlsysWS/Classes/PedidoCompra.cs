﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace br.com.globalsys.controlsys.webservice
{
    public class PedidoCompra
    {

        //Código de Projeto e Código de Pedido Pai vem da tabela Parâmetro


        #region Atributos

        //private int _Codigo;
        //private string _Cnae;
        private string _CnpjFornecedor;
        private string _Numero;
        private string _Nome;
        //private string _Observacao;
        private string _Status;

        private string _DataInicio;
        private string _DataFim;
        private string _NumeroSteor;
        private string _TipoSetor;



        #endregion

        #region Propriedades

        //public Int32 Codigo
        //{
        //    get { return _Codigo; }
        //    set { _Codigo = value; }
        //}

        //public String Cnae
        //{
        //    get { return _Cnae; }
        //    set { _Cnae = value; }
        //}

        public String CnpjFornecedor
        {
            get { return _CnpjFornecedor; }
            set { _CnpjFornecedor = value; }
        }

        public String Numero
        {
            get { return _Numero; }
            set { _Numero = value; }
         }

        public String Nome
        {
            get { return _Nome; }
            set { _Nome = value; }
        }

        //public String Observacao
        //{
        //    get { return _Observacao; }
        //    set { _Observacao = value; }
        //}

        public String Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        public String DataInicio
        {
            get { return _DataInicio; }
            set { _DataInicio = value; }
        }

        public String DataFim
        {
            get { return _DataFim; }
            set { _DataFim = value; }
        }

        public String NumeroSetor
        {
            get { return _NumeroSteor; }
            set { _NumeroSteor = value; }
        }

        //public String TipoCentroCusto
        //{
        //    get { return _TipoSetor; }
        //    set { _TipoSetor = value; }
        //}
            
        #endregion

        #region Métodos

        public Boolean ValidaPedidoCompra()
        {
            bool resposta = true;
            string Mensagem = "Campo(s) obrigatório(s) não informado(s): ";
            MasterClass.UltimoErro = new Exception();

            if (!MasterClass.ValidaString(this.CnpjFornecedor))
            {
                resposta = false;
                Mensagem += "CnpjFornecedor, ";
            }

            if (!MasterClass.ValidaString(this.Numero))
            {
                resposta = false;
                Mensagem += "Numero, ";
            }

            if (!MasterClass.ValidaString(this.Nome))
            {
                resposta = false;
                Mensagem += "Nome, ";
            }

            if (!MasterClass.ValidaString(this.Status))
            {
                resposta = false;
                Mensagem += "Status, ";
            }

            if (!MasterClass.ValidaString(this.NumeroSetor))
            {
                resposta = false;
                Mensagem += "NumeroSetor, ";
            }

            //Valida se os dados informados estão dentre os valores esperados            
            if (resposta)
            {
                Mensagem = "Valor(es) inválido(s) para o(s) campo(s) ";

                if ((this.Status != "Ativo") && (this.Status != "Inativo"))
                {
                    resposta = false;
                    Mensagem += "Ativo, ";
                }

                if (!MasterClass.ValidaDataNaoObrigatoria(this.DataInicio))
                {
                    resposta = false;
                    Mensagem += "DataInicio, ";
                }

                if (!MasterClass.ValidaDataNaoObrigatoria(this.DataFim))
                {
                    resposta = false;
                    Mensagem += "DataFim, ";
                }

            }

            if (!resposta)
            {
                Mensagem = Mensagem.Remove(Mensagem.Length - 2);
                MasterClass.UltimoErro = new Exception(Mensagem);
            }

            return resposta;

        }

        private PedidoGS ExtraiPedidoGS(EmpresaGS empresa)
        {
            PedidoGS pedido = new PedidoGS();
            ParametroGS ParametroProjeto = ParametroGS.Obtem("CodigoProjetoPadrao");
            
            //pedido.Cnae = this.Cnae;
            //pedido.Codigo = this.Codigo;
            
            pedido.CodigoEmpresa = empresa.Codigo;
            pedido.CodigoPedidoPai = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["CodigoPedidoPai"]);
            pedido.CodigoProjeto = Convert.ToInt32(ParametroProjeto.Valor);
            pedido.DataRegistro = DateTime.Now;
            pedido.DataStatus = DateTime.Now;
            pedido.Nome = this.Nome;
            pedido.Numero = this.Numero;
            //pedido.Observacao = this.Observacao;
            pedido.Status = this.Status;


            return pedido;

        }

        private PrazoGS ExtraiPrazoGS(PedidoGS pedido)
        {
            PrazoGS prazo = new PrazoGS();

            prazo.DataInicio = MasterClass.StringParaData(this.DataInicio);
            prazo.DataFim = MasterClass.StringParaData(this.DataFim);
            prazo.Registro = DateTime.Now;
            prazo.Ativo = this.Status=="Ativo";
            prazo.CodigoPedido = pedido.Codigo;
            prazo.Observacao = this.Status;


            return prazo;

        }

        private SetorPedidoGS ExtraiSetorPedidoGS(int pCodigoPedido)
        {
            SetorPedidoGS resposta = new SetorPedidoGS();
            SetorCustoGS setor = SetorCustoGS.Obtem(this.NumeroSetor);
                
            resposta.Ativo = true;
            resposta.CodigoPedido = pCodigoPedido;
            resposta.CodigoSetor = setor.Codigo;            
            resposta.DataDesativacao = DateTime.MinValue;
            resposta.DataRegistro = DateTime.Now;          


            return resposta;

        }


        public bool InserePedidoCompra(Integracao pIntegracao)
        {
            try
            {
                pIntegracao.TipoIntegracao = "PedidoCompra";
                pIntegracao.Status = "PROCESSANDO";
                pIntegracao.DataStatus = DateTime.Now;
                pIntegracao.Mensagem = String.Empty;

                pIntegracao.Converte().AtualizarRegistro();

                MasterClass item = new MasterClass();

                EmpresaGS empresa = EmpresaGS.Obtem(this.CnpjFornecedor);

                item = this.ExtraiPedidoGS(empresa);

                int CodigoPedido = item.InserirRegistroRetornaID();

                if (CodigoPedido < 1)
                {
                    throw new Exception("[PEDIDOCOMPRA].[INSEREPEDIDOCOMPRA] Registro não inserido. " + MasterClass.UltimoErro.Message);
                }

                PedidoGS pedido = PedidoGS.Obtem(this.Numero);

                PrazoGS prazo = ExtraiPrazoGS(pedido);

                if (prazo.DataInicio != DateTime.MinValue)                
                {
                    if (prazo.InserirRegistro() < 1)
                    {
                        throw new Exception("[PEDIDOCOMPRA].[INSEREPEDIDOCOMPRA] Registro não inserido. " + MasterClass.UltimoErro.Message);
                    }
                }

                SetorPedidoGS setor = ExtraiSetorPedidoGS(CodigoPedido);

                if (setor.InserirRegistro() < 1)
                {
                    throw new Exception("[PEDIDOCOMPRA].[INSEREPEDIDOCOMPRA] Registro não inserido. " + MasterClass.UltimoErro.Message);
                }


                pIntegracao.DataStatus = DateTime.Now;
                pIntegracao.Mensagem += "Pedido de Compra processado com sucesso. ";
                pIntegracao.Status = "SUCESSO";


                pIntegracao.Converte().AtualizarRegistro();

                return true;
            }
            catch (Exception erro)
            {

                pIntegracao.Status = "ERRO";
                pIntegracao.Mensagem += "Falha ao processar o registro. " + erro.Message;
                pIntegracao.DataStatus = DateTime.Now;

                pIntegracao.Converte().AtualizarRegistro();


                return false;
            }
        }

        public bool AtualizaPedidoCompra(Integracao pIntegracao)
        {
            try
            {
                pIntegracao.TipoIntegracao = "PedidoCompra";
                pIntegracao.Status = "PROCESSANDO";
                pIntegracao.DataStatus = DateTime.Now;
                pIntegracao.Mensagem = String.Empty;

                pIntegracao.Converte().AtualizarRegistro();

                MasterClass item = new MasterClass();

                EmpresaGS empresa = EmpresaGS.Obtem(this.CnpjFornecedor);
                SetorCustoGS setor = SetorCustoGS.Obtem(this.NumeroSetor);

                item = this.ExtraiPedidoGS(empresa);

                PedidoGS pedido = PedidoGS.Obtem(this.Numero);

                ((PedidoGS)item).Codigo = pedido.Codigo;


                if (item.AtualizarRegistro() < 1)
                {
                    throw new Exception("[PEDIDOCOMPRA].[ATUALIZAPEDIDOCOMPRA] Registro não atualizado. " + MasterClass.UltimoErro.Message);
                }

                PrazoGS prazo = ExtraiPrazoGS(pedido);

                if (PrazoGS.ExistePrazo(pedido.Codigo))
                {
                    prazo.Codigo = PrazoGS.Obtem(pedido.Codigo).Codigo;
                    if (prazo.AtualizarRegistro() < 1)
                    {
                        throw new Exception("[PEDIDOCOMPRA].[ATUALIZAPEDIDOCOMPRA] Registro não atualizado. " + MasterClass.UltimoErro.Message);
                    }
                }
                else
                {
                    if (prazo.InserirRegistro() < 1)
                    {
                        throw new Exception("[PEDIDOCOMPRA].[ATUALIZAPEDIDOCOMPRA] Registro não atualizado. (Falha ao criar PRAZO para o Pedido " + pedido.Codigo.ToString() + ":" + MasterClass.UltimoErro.Message);
                    }
                }

                SetorPedidoGS s = ExtraiSetorPedidoGS(pedido.Codigo);

                if (SetorPedidoGS.ExisteSetorPedido(pedido.Codigo))
                {
                    s.Codigo = SetorPedidoGS.Obtem(pedido.Codigo).Codigo;

                    if (s.AtualizarRegistro() < 1)
                    {
                        throw new Exception("[PEDIDOCOMPRA].[ATUALIZAPEDIDOCOMPRA] Registro não atualizado. " + MasterClass.UltimoErro.Message);
                    }
                }
                else
                {
                    if (s.InserirRegistro() < 1)
                    {
                        throw new Exception("[PEDIDOCOMPRA].[ATUALIZAPEDIDOCOMPRA] Registro não inserido. (Falha ao atribuir SETOR para o Pedido " + pedido.Codigo.ToString() + ":" + MasterClass.UltimoErro.Message);
                    }
                }

                {
                    pIntegracao.DataStatus = DateTime.Now;
                    pIntegracao.Mensagem += "Pedido de Compra processado com sucesso. ";
                    pIntegracao.Status = "SUCESSO";
                }

                pIntegracao.Converte().AtualizarRegistro();

                return true;
            }
            catch (Exception erro)
            {

                pIntegracao.Status = "ERRO";
                pIntegracao.Mensagem += "Falha ao processar o registro. " + erro.Message;
                pIntegracao.DataStatus = DateTime.Now;

                pIntegracao.Converte().AtualizarRegistro();


                return false;
            }
        }

        #endregion

    }
}
