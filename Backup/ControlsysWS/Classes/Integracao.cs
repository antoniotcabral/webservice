﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace br.com.globalsys.controlsys.webservice
{
    
    public class Integracao
    {

        #region Atributos

        private int _Codigo;
        private DateTime _Ocorrencia;
        private string _Status;
        private string _Mensagem;
        private DateTime _DataStatus;
        private string _TipoIntegracao;

        #endregion

        #region Propriedades

        
        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }
        
        public DateTime Ocorrencia
        {
            get { return _Ocorrencia; }
            set { _Ocorrencia = value; }
        }
                
        public String Status
        {
            get { return _Status; }
            set { _Status = value; }
        }
        
        public String Mensagem
        {
            get { return _Mensagem; }
            set { _Mensagem = value; }
        }
        
        public DateTime DataStatus
        {
            get { return _DataStatus; }
            set { _DataStatus = value; }
        }
        
        public String TipoIntegracao
        {
            get { return _TipoIntegracao; }
            set { _TipoIntegracao = value; }
        }

        #endregion

        #region Métodos


        public IntegracaoGS Converte()
        {
            IntegracaoGS resposta = new IntegracaoGS();

            try
            {
                resposta.Codigo = this.Codigo;
                resposta.DataStatus = this.DataStatus;
                resposta.Mensagem = this.Mensagem;
                resposta.Ocorrencia = this.Ocorrencia;
                resposta.Status = this.Status;
                resposta.TipoIntegracao = this.TipoIntegracao;
            }
            catch (Exception erro)
            { }

            return resposta;
        }

        public static Integracao Obtem(int pIdIntegracao)
        {
            bool Achou = false;
            IntegracaoGS item = new IntegracaoGS();

            foreach (IntegracaoGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " CD_INTEGRACAO = " + pIdIntegracao))
            {
                Achou = true;
                item = obj;
                break;
            }

            if (Achou)
                return item.Converte();
            else
                throw new Exception("[INTEGRACAO].[OBTEM] Registro de empresa não encontrado (" + pIdIntegracao + ")");
        }

        public static List<Integracao> Obtem(string pStatus)
        {
            List<Integracao> resposta = new List<Integracao>();            
            IntegracaoGS item = new IntegracaoGS();

            foreach (IntegracaoGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " TX_STATUS = '" + pStatus + "'"))
            {                
                resposta.Add(obj.Converte());                
            }

            return resposta;
            
        }

        #endregion

    }
}
