﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace br.com.globalsys.controlsys.webservice
{
    public class PHT
    {

        #region Atributos

        private int _Codigo;
        private string _Nome;
        private int _Escala;
        private int _Feriado;
        private int _Ativo;
        private int _NumeroSemana;

        private string _HoraInicioDia1;
        private string _HoraFimDia1;
        private int _IntervaloDia1;

        private string _HoraInicioDia2;
        private string _HoraFimDia2;
        private int _IntervaloDia2;

        private string _HoraInicioDia3;
        private string _HoraFimDia3;
        private int _IntervaloDia3;

        private string _HoraInicioDia4;
        private string _HoraFimDia4;
        private int _IntervaloDia4;

        private string _HoraInicioDia5;
        private string _HoraFimDia5;
        private int _IntervaloDia5;

        private string _HoraInicioDia6;
        private string _HoraFimDia6;
        private int _IntervaloDia6;

        private string _HoraInicioDia7;
        private string _HoraFimDia7;
        private int _IntervaloDia7;


        #endregion

        #region Propriedades

        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        public String Nome
        {
            get { return _Nome; }
            set { _Nome = value; }
        }

        public Int32 Escala
        {
            get { return _Escala; }
            set { _Escala = value; }
        }

        public Int32 Feriado
        {
            get { return _Feriado; }
            set { _Feriado = value; }
        }

        public Int32 Ativo
        {
            get { return _Ativo; }
            set { _Ativo = value; }
        }

        public Int32 NumeroSemana
        {
            get { return _NumeroSemana; }
            set { _NumeroSemana = value; }
        }

        public String HoraInicioDia1
        {
            get { return _HoraInicioDia1; }
            set { _HoraInicioDia1 = value; }
        }

        public String HoraFimDia1
        {
            get { return _HoraFimDia1; }
            set { _HoraFimDia1 = value; }
        }

        public int IntervaloDia1
        {
            get { return _IntervaloDia1; }
            set { _IntervaloDia1 = value; }
        }

        public String HoraInicioDia2
        {
            get { return _HoraInicioDia2; }
            set { _HoraInicioDia2 = value; }
        }

        public String HoraFimDia2
        {
            get { return _HoraFimDia2; }
            set { _HoraFimDia2 = value; }
        }

        public int IntervaloDia2
        {
            get { return _IntervaloDia2; }
            set { _IntervaloDia2 = value; }
        }

        public String HoraInicioDia3
        {
            get { return _HoraInicioDia3; }
            set { _HoraInicioDia3 = value; }
        }

        public String HoraFimDia3
        {
            get { return _HoraFimDia3; }
            set { _HoraFimDia3 = value; }
        }

        public int IntervaloDia3
        {
            get { return _IntervaloDia3; }
            set { _IntervaloDia3 = value; }
        }

        public String HoraInicioDia4
        {
            get { return _HoraInicioDia4; }
            set { _HoraInicioDia4 = value; }
        }

        public String HoraFimDia4
        {
            get { return _HoraFimDia4; }
            set { _HoraFimDia4 = value; }
        }

        public int IntervaloDia4
        {
            get { return _IntervaloDia4; }
            set { _IntervaloDia4 = value; }
        }

        public String HoraInicioDia5
        {
            get { return _HoraInicioDia5; }
            set { _HoraInicioDia5 = value; }
        }

        public String HoraFimDia5
        {
            get { return _HoraFimDia5; }
            set { _HoraFimDia5 = value; }
        }

        public int IntervaloDia5
        {
            get { return _IntervaloDia5; }
            set { _IntervaloDia5 = value; }
        }

        public String HoraInicioDia6
        {
            get { return _HoraInicioDia6; }
            set { _HoraInicioDia6 = value; }
        }

        public String HoraFimDia6
        {
            get { return _HoraFimDia6; }
            set { _HoraFimDia6 = value; }
        }

        public int IntervaloDia6
        {
            get { return _IntervaloDia6; }
            set { _IntervaloDia6 = value; }
        }

        public String HoraInicioDia7
        {
            get { return _HoraInicioDia7; }
            set { _HoraInicioDia7 = value; }
        }

        public String HoraFimDia7
        {
            get { return _HoraFimDia7; }
            set { _HoraFimDia7 = value; }
        }

        public int IntervaloDia7
        {
            get { return _IntervaloDia7; }
            set { _IntervaloDia7 = value; }
        }


        //SCOPE_IDENTITY

        //hORA_TURNO
        //BL24H = false
        //nu_seq  = nulo
        //Se hora vir nulo, não insere


        #endregion

        #region Métodos

        private TurnoGS ExtraiTurno()
        {
            TurnoGS resposta = new TurnoGS();
            //PessoaFisicaGS pessoa = PessoaFisicaGS.Obtem(this.Cpf);
            //PapelGS papel = PapelGS.Obtem(pessoa.Codigo);

            resposta.Ativo = this.Ativo == 1;
            resposta.Codigo = this.Codigo;
            resposta.DataRegistro = DateTime.Now;
            resposta.Escala = this.Escala ==1;
            resposta.Feriado = this.Feriado ==1;
            resposta.Nome = this.Nome;
            
            return resposta;
        }

        private List<HoraTurnoGS> ExtraiHoras(TurnoGS pTurno)
        {
            List<HoraTurnoGS> resposta = new List<HoraTurnoGS>();
            HoraTurnoGS item = new HoraTurnoGS();

            item.Ativo = this.Ativo==1;
            item.Codigo = this.Codigo;
            item.CodigoTurno = pTurno.Codigo;
            item.DataRegistro = DateTime.Now;


            
            item.Sequencia = Int32.MinValue;
            item.Trabalha = true;
            item.VinteQuatroHoras = this.Escala ==1;

            if (this.HoraInicioDia1 != String.Empty)
            {
                item.HoraInicio = this.HoraInicioDia1;
                item.HoraFim = this.HoraFimDia1;
                item.NumeroDiaSemana = 1;

                resposta.Add(item);
            }

            if (this.HoraInicioDia2 != String.Empty)
            {
                item.HoraInicio = this.HoraInicioDia2;
                item.HoraFim = this.HoraFimDia2;
                item.NumeroDiaSemana = 2;

                resposta.Add(item);
            }

            if (this.HoraInicioDia3 != String.Empty)
            {
                item.HoraInicio = this.HoraInicioDia3;
                item.HoraFim = this.HoraFimDia3;
                item.NumeroDiaSemana = 3;

                resposta.Add(item);
            }

            if (this.HoraInicioDia4 != String.Empty)
            {
                item.HoraInicio = this.HoraInicioDia4;
                item.HoraFim = this.HoraFimDia4;
                item.NumeroDiaSemana = 4;

                resposta.Add(item);
            }

            if (this.HoraInicioDia5 != String.Empty)
            {
                item.HoraInicio = this.HoraInicioDia5;
                item.HoraFim = this.HoraFimDia5;
                item.NumeroDiaSemana = 5;

                resposta.Add(item);
            }

            if (this.HoraInicioDia6 != String.Empty)
            {
                item.HoraInicio = this.HoraInicioDia6;
                item.HoraFim = this.HoraFimDia6;
                item.NumeroDiaSemana = 6;

                resposta.Add(item);
            }

            if (this.HoraInicioDia7 != String.Empty)
            {
                item.HoraInicio = this.HoraInicioDia7;
                item.HoraFim = this.HoraFimDia7;
                item.NumeroDiaSemana = 7;

                resposta.Add(item);
            }

            return resposta;
        }

        private GrupoTrabalhoGS ExtraiGrupoTrabalho()
        {
            GrupoTrabalhoGS resposta = new GrupoTrabalhoGS();

            resposta.Ativo = this.Ativo == 1;
            resposta.Codigo = this.Codigo;
            resposta.DataRegistro = DateTime.Now;
            resposta.Nome = this.Nome;

            return resposta;

        }

        private GrupoTrabalhoTurnoGS ExtraiGrupoTrabalhoTurno(TurnoGS pTurno, GrupoTrabalhoGS pGrupo)
        {
            GrupoTrabalhoTurnoGS resposta = new GrupoTrabalhoTurnoGS();

            resposta.Ativo = this.Ativo == 1;
            //resposta.Codigo = this.Codigo;
            resposta.DataInicio = DateTime.Now;
            resposta.DataRegistro = DateTime.Now;
            resposta.CodigoTurno = pTurno.Codigo;
            resposta.CodigoGrupoTrabalho = pGrupo.Codigo;
            

            return resposta;
        }

        

        public bool InserePht(Integracao pIntegracao)
        {
            try
            {


                pIntegracao.TipoIntegracao = "PHT";
                pIntegracao.Status = "PROCESSANDO";
                pIntegracao.DataStatus = DateTime.Now;
                pIntegracao.Mensagem = String.Empty;

                pIntegracao.Converte().AtualizarRegistro();

                MasterClass item = new MasterClass();

                item = this.ExtraiTurno();

                int CodigoTurno = item.InserirRegistroRetornaID();

                if (CodigoTurno < 1)
                {
                    throw new Exception("[PHT].[INSEREPHT] Falha ao processar Turno: Registro não inserido. " + MasterClass.UltimoErro.Message);
                }

                TurnoGS turno = (TurnoGS)item;

                turno = TurnoGS.Obtem(CodigoTurno);

                List<HoraTurnoGS> horas = this.ExtraiHoras(turno);

                foreach (HoraTurnoGS hora in horas)
                {
                    if (hora.InserirRegistro() < 1)
                    {
                        throw new Exception("[PHT].[INSEREPHT] Falha ao processar HoraTurno: Registro não inserido. " + MasterClass.UltimoErro.Message);
                    }
                }

                item = this.ExtraiGrupoTrabalho();

                CodigoTurno = item.InserirRegistroRetornaID();

                if (CodigoTurno < 1)
                {
                    throw new Exception("[PHT].[INSEREPHT] Falha ao processar Grupo de Trabalho: Registro não inserido. " + MasterClass.UltimoErro.Message);
                }

                GrupoTrabalhoGS grupo = (GrupoTrabalhoGS)item;

                grupo = GrupoTrabalhoGS.Obtem(CodigoTurno.ToString());

                item = this.ExtraiGrupoTrabalhoTurno(turno, grupo);

                if (item.InserirRegistro() < 1)
                {
                    throw new Exception("[PHT].[INSEREPHT] Falha ao processar Turno de Grupo de Trabalho: Registro não inserido. " + MasterClass.UltimoErro.Message);
                }

                pIntegracao.DataStatus = DateTime.Now;
                pIntegracao.Mensagem += "Plano de Horário de Trabalho processada com sucesso. ";
                pIntegracao.Status = "SUCESSO";


                pIntegracao.Converte().AtualizarRegistro();

                return true;
            }
            catch (Exception erro)
            {

                pIntegracao.Status = "ERRO";
                pIntegracao.Mensagem += "Falha ao processar o registro. " + erro.Message;
                pIntegracao.DataStatus = DateTime.Now;

                pIntegracao.Converte().AtualizarRegistro();


                return false;
            }
        }

        public bool AtualizaPht(Integracao pIntegracao)
        {
            try
            {


                pIntegracao.TipoIntegracao = "PHT";
                pIntegracao.Status = "PROCESSANDO";
                pIntegracao.DataStatus = DateTime.Now;
                pIntegracao.Mensagem = String.Empty;

                pIntegracao.Converte().AtualizarRegistro();

                MasterClass item = new MasterClass();

                item = this.ExtraiTurno();

                if (item.AtualizarRegistro() < 1)
                {
                    throw new Exception("[PHT].[ATUALIZAPHT] Falha ao processar Turno: Registro não inserido. " + MasterClass.UltimoErro.Message);
                }

                TurnoGS turno = (TurnoGS)item;

                turno = TurnoGS.Obtem(turno.Codigo);

                if (HoraTurnoGS.RemoveHorasDoTurno(turno.Codigo))
                {

                    List<HoraTurnoGS> horas = this.ExtraiHoras(turno);

                    foreach (HoraTurnoGS hora in horas)
                    {
                        if (hora.InserirRegistro() < 1)
                        {
                            throw new Exception("[PHT].[ATUALIZAPHT] Falha ao processar HoraTurno: Registro não inserido. " + MasterClass.UltimoErro.Message);
                        }
                    }
                }

                GrupoTrabalhoGS grupo = GrupoTrabalhoGS.ObtemPorTurno(turno.Codigo);

                item = this.ExtraiGrupoTrabalho();

                ((GrupoTrabalhoGS)item).Codigo = grupo.Codigo;


                if (item.AtualizarRegistro() < 1)
                {
                    throw new Exception("[PHT].[ATUALIZAPHT] Falha ao processar Grupo de Trabalho: Registro não inserido. " + MasterClass.UltimoErro.Message);
                }

                GrupoTrabalhoTurnoGS grupoTrabalho = GrupoTrabalhoTurnoGS.Obtem(grupo.Codigo, turno.Codigo);

                //grupo = GrupoTrabalhoGS.Obtem(grupo.DataRegistro);

                item = this.ExtraiGrupoTrabalhoTurno(turno, grupo);

                ((GrupoTrabalhoTurnoGS)item).Codigo = grupoTrabalho.Codigo;               
                

                if (item.AtualizarRegistro() < 1)
                {
                    throw new Exception("[PHT].[ATUALIZAPHT] Falha ao processar Turno de Grupo de Trabalho: Registro não inserido. " + MasterClass.UltimoErro.Message);
                }

                pIntegracao.DataStatus = DateTime.Now;
                pIntegracao.Mensagem += "Plano de Horário de Trabalho processada com sucesso. ";
                pIntegracao.Status = "SUCESSO";


                pIntegracao.Converte().AtualizarRegistro();

                return true;
            }
            catch (Exception erro)
            {

                pIntegracao.Status = "ERRO";
                pIntegracao.Mensagem += "Falha ao processar o registro. " + erro.Message;
                pIntegracao.DataStatus = DateTime.Now;

                pIntegracao.Converte().AtualizarRegistro();


                return false;
            }
        }

        #endregion

    }
}
