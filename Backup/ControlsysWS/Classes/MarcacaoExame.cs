﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace br.com.globalsys.controlsys.webservice

{
    [Serializable]
    public class MarcacaoExame
    {

        #region Atributos

        private string _Cpf;
        private int _TipoAtendimento;
        private string _DataMarcacao;
        private string _Compareceu;
        private string _TipoRegistro;
        
        #endregion

        #region Propriedades
        
       [XmlElementAttribute(IsNullable=false)]
        public String Cpf
        {
            get { return _Cpf; }
            set { _Cpf = value; }
        }
        
        public Int32 TipoAtendimento
        {
            get { return _TipoAtendimento; }
            set { _TipoAtendimento = value; }
        }

        public String DataMarcacao
        {
            get { return _DataMarcacao; }
            set { _DataMarcacao = value; }
        }

        public String Compareceu
        {
            get { return _Compareceu; }
            set { _Compareceu = value; }
        }

        private String TipoRegistro
        {
            get { return _TipoRegistro; }
            set { _TipoRegistro = value; }
        }

        #endregion

        #region Métodos

        public Boolean ValidaMarcacaoExame()
        {
            bool resposta = true;
            string Mensagem = "Campo(s) obrigatório(s) não informado(s): ";
            MasterClass.UltimoErro = new Exception();

            if (!MasterClass.ValidaString(this.Cpf))
            {
                resposta = false;
                Mensagem += "Cpf, ";
            }

            if (!MasterClass.ValidaString(this.DataMarcacao))
            {
                resposta = false;
                Mensagem += "DataMarcacao, ";
            }



            if (resposta)
            {
                Mensagem = "Valor(es) inválido(s) para o(s) campo(s): ";

                HashSet<int> ValoresTipoAtendimento = new HashSet<int>();
                ValoresTipoAtendimento.Add(1);
                ValoresTipoAtendimento.Add(2);
                ValoresTipoAtendimento.Add(4);
                ValoresTipoAtendimento.Add(5);

                if (!ValoresTipoAtendimento.Contains(this.TipoAtendimento))
                {
                    resposta = false;
                    Mensagem += "TipoAtendimento, ";
                }

                if (!MasterClass.ValidaData(this.DataMarcacao))
                {
                    resposta = false;
                    Mensagem += "DataMarcacao, ";
                }

                if (MasterClass.ValidaString(this.Compareceu))
                {
                    HashSet<String> ValoresCompareceu = new HashSet<string>();
                    ValoresCompareceu.Add("1");
                    ValoresCompareceu.Add("2");

                    if (!ValoresCompareceu.Contains(this.Compareceu))
                    {
                        resposta = false;
                        Mensagem += "Compareceu, ";
                    }
                }

            }

            if (!resposta)
            {
                Mensagem = Mensagem.Remove(Mensagem.Length - 2);
                MasterClass.UltimoErro = new Exception(Mensagem);
            }

            return resposta;

        }

        private AsoGS ExtraiAso()
        {
            AsoGS resposta = new AsoGS();
            PessoaFisicaGS pessoa = PessoaFisicaGS.Obtem(this.Cpf);
            PapelGS papel = PapelGS.Obtem(this.Cpf);

            resposta.CodigoPapel = papel.Codigo;
            resposta.DataRealizacao = MasterClass.StringParaData(this.DataMarcacao);
            resposta.DataValidade = MasterClass.StringParaData(this.DataMarcacao);
            resposta.TipoRegistro = "Exame";
            resposta.Compareceu = this.Compareceu == "1";
            resposta.TipoAtendimento = this.TipoAtendimento;

          

            return resposta;
        }

        public bool InsereMarcacaoExame(Integracao pIntegracao)
        {
            try
            {


                pIntegracao.TipoIntegracao = "MarcacaoExame";
                pIntegracao.Status = "PROCESSANDO";
                pIntegracao.DataStatus = DateTime.Now;
                pIntegracao.Mensagem = String.Empty;

                pIntegracao.Converte().AtualizarRegistro();

                MasterClass item = new MasterClass();

                item = this.ExtraiAso();

                if (item.InserirRegistro() < 1)
                {
                    throw new Exception("[MARCACAOEXAME].[INSEREMARCACAOEXAME] Registro não inserido. " + MasterClass.UltimoErro.Message);
                }
                else
                {
                    pIntegracao.DataStatus = DateTime.Now;
                    pIntegracao.Mensagem += "Marcação de Exame processada com sucesso. ";
                    pIntegracao.Status = "SUCESSO";
                }

                pIntegracao.Converte().AtualizarRegistro();

                return true;
            }
            catch (Exception erro)
            {

                pIntegracao.Status = "ERRO";
                pIntegracao.Mensagem += "Falha ao processar o registro. " + erro.Message;
                pIntegracao.DataStatus = DateTime.Now;

                pIntegracao.Converte().AtualizarRegistro();


                return false;
            }
        }

        #endregion

    }
}
