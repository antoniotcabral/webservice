﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace br.com.globalsys.controlsys.webservice
{
    public class UnidadeOrganizacional
    {
        #region Atributos

        private int _Codigo;
        private string _Nome;
        private string _Numero;
        private string _DataInicial;
        private string _DataFim;

        #endregion

        #region Propriedades

        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        public String Nome
        {
            get { return _Nome; }
            set { _Nome = value; }
        }

        public String Numero
        {
            get { return _Numero; }
            set { _Numero = value; }
        }

        public String DataInicio
        {
            get { return _DataInicial; }
            set { _DataInicial = value; }
        }

        public String DataFiml
        {
            get { return _DataFim; }
            set { _DataFim = value; }
        }


        #endregion

    }
}
