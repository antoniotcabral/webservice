﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace br.com.globalsys.controlsys.webservice
{
    public class TipoTreinamento
    {

        #region Atributos

        private int _Codigo;
        private string _Nome;
        private string _Descricao;
        private int _ValidadeDias;
        private int _Ativo;       
        
        #endregion

        #region Propriedades

        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        public String Nome
        {
            get { return _Nome; }
            set { _Nome = value; }
        }

        public String Descricao
        {
            get { return _Descricao; }
            set { _Descricao = value; }
        }

        public Int32 ValidadeDias
        {
            get { return _ValidadeDias; }
            set { _ValidadeDias = value; }
        }

        public Int32 Ativo
        {
            get { return _Ativo; }
            set { _Ativo = value; }
        }

        #endregion


        #region Métodos

        private TreinamentoGS ExtraiTreinamento()
        {
            TreinamentoGS resposta = new TreinamentoGS();

            resposta.Ativo = this.Ativo==1;
            resposta.Codigo = this.Codigo;
            //resposta.DataDesativacao = this.
            resposta.DataRegistro = DateTime.Now;
            resposta.Descricao = this.Descricao;
            resposta.Dias = this.ValidadeDias;
            resposta.Nome = this.Nome;

            return resposta;

        }


        public bool InsereTipoTreinamento(Integracao pIntegracao)
        {
            try
            {


                pIntegracao.TipoIntegracao = "TipoTreinamento";
                pIntegracao.Status = "PROCESSANDO";
                pIntegracao.DataStatus = DateTime.Now;
                pIntegracao.Mensagem = String.Empty;

                pIntegracao.Converte().AtualizarRegistro();

                MasterClass item = new MasterClass();

                item = this.ExtraiTreinamento();

                if (item.InserirRegistro() < 1)
                {
                    throw new Exception("[TIPOTREINAMENTO].[INSERETIPOTREINAMENTO] Registro não inserido. " + MasterClass.UltimoErro.Message);
                }
                else
                {
                    pIntegracao.DataStatus = DateTime.Now;
                    pIntegracao.Mensagem += "Tipo de Treinamento processado com sucesso. ";
                    pIntegracao.Status = "SUCESSO";
                }

                pIntegracao.Converte().AtualizarRegistro();

                return true;
            }
            catch (Exception erro)
            {

                pIntegracao.Status = "ERRO";
                pIntegracao.Mensagem += "Falha ao processar o registro. " + erro.Message;
                pIntegracao.DataStatus = DateTime.Now;

                pIntegracao.Converte().AtualizarRegistro();


                return false;
            }
        }
        
        #endregion

    }
}
