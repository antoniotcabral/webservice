﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace br.com.globalsys.controlsys.webservice
{
    public class Colaborador
    {

        #region Atributos

        //private int _Codigo;
        private string _Nome; //ok2
        private string _Apelido; //ok2
        private string _Email;//ok2
        private string _EnderecoComplemento;//ok2
        private string _CnpjEmpresa;//ok2
        private string _Mensalista;//ok2
        private string _Observacao;//ok
        private string _RegistroProfissional;//ok
        private string _EnderecoCep;//ok
        private string _EnderecoLogradouro; //ok
        private string _EnderecoNumero;//ok
        private string _EnderecoBairro;//ok
        private string _EnderecoCidade;//ok
        private string _EnderecoEstado;//ok
        private string _EnderecoPais;//ok
        private string _Cpf;//ok
        private string _NaturalidadeCidade;//ok
        private string _Passaporte;//ok
        private string _RgNumero;//ok
        private string _RgOrgaoEmissor;//ok
        private string _RgEstadoEmissor;//ok
        private string _NomeMae;//ok
        private string _NomePai;//ok
        private string _TipoSanguineo;//ok
        private string _Sexo;//ok
        private string _Rne;//ok
        private string _EstadoCivil;//ok
        private string _Escolaridade;//ok
        private string _NumeroCrNr;//ok
        private string _TipoCrNr;//ok
        private string _CnhNumero;//ok
        private string _CnhTipo;//ok                
        private string _CtpsSerie;//ok
        private string _CtpsEstado;//ok
        private string _NauralidadePais;//ok
        private string _ContatoTelefone01;//ok
        private string _ContatoTelefone02;//ok
        private string _ContatoTelefone03;//ok
        private string _StatusOcupacao;//ok
        private string _Municipio; 
        private string _Nacionalidade;
   

        private string _DataAdmissao;//ok2
        private string _RgEmissao;//ok
        private string _VistoEmissao;//ok
        private string _VistoValidade;//ok
        private string _ExpedicaoRne;//ok
        private string _CnhValidade;//ok
        private string _CtpsEmissao;//ok

        
        private string _Salario;//ok

        private string _Pis;//ok

        private string _TituloEleitorNumero;//ok
        private string _TituloEleitorZona;//ok
        private string _TituloEleitorSecao;//ok
        private string _CtpsNumero;//ok

        private string _NaturalidadeUf;//ok
        private string _DataNascimento;//ok
        private string _NumeroUnidadeOrganizacional;
        private string _NomeUnidadeOrganizacional;
        private string _CodigoPosicao;
        private string _NomePosicao;
        private string _CentroCustoCodigo;
        private string _PhtCodigo;
        private string _PhtValidadeInicial;
        private string _PhtValidadeFinal;
        //private string _FeriasValidadeInicial;
        //private string _FeriasValidadeFinal;
        private string _TreinamentoCodigoSap;
        private string _TreinamentoValidade;

        //private string _DescricaoCentroCusto;
        //private string _TipoCentroCusto;

        private string _CodigoSap;
        private string _CodigoGestorPonto;
        private string _CodigoSuperiorImediato;
        #endregion

        #region Propriedades

        //public Int32 Codigo
        //{
        //    get { return _Codigo; }
        //    set { _Codigo = value; }
        //}
        
        public String Nome
        {
            get { return _Nome; }
            set { _Nome = value; }
        }

        public String Apelido
        {
            get { return _Apelido; }
            set { _Apelido = value; }
        }

        public String Email
        {
            get { return _Email; }
            set { _Email = value; }
        }
        public String EnderecoComplemento
        {
            get { return _EnderecoComplemento; }
            set { _EnderecoComplemento = value; }
        }

        public String CnpjEmpresa
        {
            get { return _CnpjEmpresa; }
            set { _CnpjEmpresa = value; }
        }


        public String Mensalista
        {
            get { return _Mensalista; }
            set { _Mensalista = value; }
        }

        public String Observacao
        {
            get { return _Observacao; }
            set { _Observacao = value; }
        }

        public String RegistroProfissional
        {
            get { return _RegistroProfissional; }
            set { _RegistroProfissional = value; }
        }

        public String Cep
        {
            get { return _EnderecoCep; }
            set { _EnderecoCep = value; }
        }

        public String EnderecoLogradouro
        {
            get { return _EnderecoLogradouro.Trim(); }
            set { _EnderecoLogradouro = value; }
        }

        public String EnderecoNumero
        {
            get { return _EnderecoNumero; }
            set { _EnderecoNumero = value; }
        }

        public String EnderecoBairro
        {
            get { return _EnderecoBairro; }
            set { _EnderecoBairro = value; }
        }

        public String EnderecoCidade
        {
            get { return _EnderecoCidade; }
            set { _EnderecoCidade = value; }
        }

        public String EnderecoEstado
        {
            get { return _EnderecoEstado; }
            set { _EnderecoEstado = value; }
        }

        public String EnderecoPais
        {
            get { return _EnderecoPais; }
            set { _EnderecoPais = value; }
        }

        public String Cpf
        {
            get { return _Cpf; }
            set { _Cpf = value; }
        }

        public String CidadeNaturalidade
        {
            get { return _NaturalidadeCidade; }
            set { _NaturalidadeCidade = value; }
        }

        public String PassaporteNumero
        {
            get { return _Passaporte; }
            set { _Passaporte = value; }
        }

        public String RgNumero
        {
            get { return _RgNumero; }
            set { _RgNumero = value; }
        }

        public String RgOrgaoEmissor
        {
            get { return _RgOrgaoEmissor; }
            set { _RgOrgaoEmissor = value; }
        }

        public String RgEstadoEmissor
        {
            get { return _RgEstadoEmissor; }
            set { _RgEstadoEmissor = value; }
        }


        public String NomeMae
        {
            get { return _NomeMae; }
            set { _NomeMae = value; }
        }

        public String NomePai
        {
            get { return _NomePai; }
            set { _NomePai =  value; }
        }

        public String TipoSanguineo
        {
            get { return _TipoSanguineo; }
            set { _TipoSanguineo =  value; }
        }

        public String Sexo
        {
            get { return _Sexo; }
            set { _Sexo = value; }
        }

        public String Rne
        {
            get { return _Rne; }
            set { _Rne = value; }
        }

        public String EstadoCivil
        {
            get { return _EstadoCivil; }
            set { _EstadoCivil = value; }
        }

        public String Escolaridade
        {
            get { return _Escolaridade; }
            set { _Escolaridade = value; }
        }

        public String NumeroCrNr
        {
            get { return _NumeroCrNr; }
            set { _NumeroCrNr = value; }
        }
        
        public String TipoCrNr
        {
            get { return _TipoCrNr; }
            set { _TipoCrNr = value; }
        }

        public String CnhNumero
        {
            get { return _CnhNumero; }
            set { _CnhNumero = value; }
        }

        public String CnhTipo
        {
            get { return _CnhTipo; }
            set { _CnhTipo = value; }
        }

        public String TituloEleitorNumero
        {
            get { return _TituloEleitorNumero; }
            set { _TituloEleitorNumero = value; }
        }

        public String TituloEleitorZona
        {
            get { return _TituloEleitorZona; }
            set { _TituloEleitorZona = value; }
        }

        public String TituloEleitorSecao
        {
            get { return _TituloEleitorSecao; }
            set { _TituloEleitorSecao = value; }
        }

        public String CtpsNumero
        {
            get { return _CtpsNumero; }
            set { _CtpsNumero = value; }
        }

        public String CtpsSerie
        {
            get { return _CtpsSerie; }
            set { _CtpsSerie = value; }
        }

        public String CtpsEstado
        {
            get { return _CtpsEstado; }
            set { _CtpsEstado = value; }
        }

        public String NauralidadePais
        {
            get { return _NauralidadePais; }
            set { _NauralidadePais = value; }
        }

        public String ContatoTelefone01
        {
            get { return _ContatoTelefone01; }
            set { _ContatoTelefone01 = value; }
        }

        public String ContatoTelefone02
        {
            get { return _ContatoTelefone02; }
            set { _ContatoTelefone02 = value; }
        }

        public String ContatoTelefone03
        {
            get { return _ContatoTelefone03; }
            set { _ContatoTelefone03 = value; }
        }

        public String StatusOcupacao
        {
            get { return _StatusOcupacao; }
            set { _StatusOcupacao = value; }
        }


        public String DataAdmissao
        {
            get { return _DataAdmissao; }
            set { _DataAdmissao = value; }
        }
        
        public String RgEmissao
        {
            get { return _RgEmissao; }
            set { _RgEmissao = value; }
        }

        public String VistoEmissao
        {
            get { return _VistoEmissao; }
            set { _VistoEmissao = value; }
        }

        public String VistoValidade
        {
            get { return _VistoValidade; }
            set { _VistoValidade = value; }
        }

        public String ExpedicaoRne
        {
            get { return _ExpedicaoRne; }
            set { _ExpedicaoRne = value; }
        }

        public String CnhValidade
        {
            get { return _CnhValidade; }
            set { _CnhValidade = value; }
        }

        public String CtpsEmissao
        {
            get { return _CtpsEmissao; }
            set { _CtpsEmissao = value; }
        }

        public String Salario
        {
            get { return _Salario; }
            set { _Salario = value; }
        }

        public String Pis
        {
            get { return _Pis; }
            set { _Pis = value; }
        }

        public String NascimentoUf
        {
            get { return _NaturalidadeUf; }
            set { _NaturalidadeUf = value; }
        }

        public String DataNascimento
        {
            get { return _DataNascimento; }
            set { _DataNascimento = value; }
        }

        public String NumeroUnidadeOrganizacional
        {
            get { return _NumeroUnidadeOrganizacional; }
            set { _NumeroUnidadeOrganizacional = value; }
        }

        public String NomeUnidadeOrganizacional
        {
            get { return _NomeUnidadeOrganizacional; }
            set { _NomeUnidadeOrganizacional = value; }
        }

        public String CodigoPosicao
        {
            get { return _CodigoPosicao; }
            set { _CodigoPosicao = value; }
        }

        public String NomePosicao
        {
            get { return _NomePosicao; }
            set { _NomePosicao = value; }
        }

        public String CentroCustoCodigo
        {
            get { return _CentroCustoCodigo==null?null:_CentroCustoCodigo.Trim(); }
            set { _CentroCustoCodigo = value; }
        }

        public String PhtCodigo
        {
            get { return _PhtCodigo; }
            set { _PhtCodigo = value; }
        }

        public string PhtValidadeInicial
        {
            get { return _PhtValidadeInicial; }
            set { _PhtValidadeInicial = value; }
        }

        public string PhtValidadeFinal
        {
            get { return _PhtValidadeFinal; }
            set { _PhtValidadeFinal = value; }
        }

        public String CodigoSap
        {
            get { return _CodigoSap; }
            set { _CodigoSap = value; }
        }

        public String CodigoGestorPonto
        {
            get { return _CodigoGestorPonto; }
            set { _CodigoGestorPonto = value; }
        }

        public String CodigoSuperiorImediato
        {
            get { return _CodigoSuperiorImediato; }
            set { _CodigoSuperiorImediato = value; }
        }

        //private string FeriasValidadeInicial
        //{
        //    get { return _FeriasValidadeInicial; }
        //    set { _FeriasValidadeInicial = value; }
        //}

        //private string FeriasValidadeFinal
        //{
        //    get { return _FeriasValidadeFinal; }
        //    set { _FeriasValidadeFinal = value; }
        //}

        public String TreinamentoCodigoSap
        {
            get { return _TreinamentoCodigoSap; }
            set { _TreinamentoCodigoSap = value; }
        }

        public string TreinamentoValidade
        {
            get { return _TreinamentoValidade; }
            set { _TreinamentoValidade = value; }
        }

        //public String DescricaoCentroCusto
        //{
        //    get { return _DescricaoCentroCusto; }
        //    set { _DescricaoCentroCusto = value; }
        //}

        //public String TipoCentroCusto
        //{
        //    get { return _TipoCentroCusto; }
        //    set { _TipoCentroCusto = value; }
        //}
        



        #endregion

        #region Constantes

        private const string TipoTelefone1 = "Residencial";
        private const string TipoTelefone2 = "Emergencial";
        private const string TipoTelefone3 = "Celular";

        #endregion

        #region Métodos

        public Boolean ValidaColaborador()
        {
            bool resposta = true;
            string Mensagem = "Campo(s) obrigatório(s) não informado(s): ";
            MasterClass.UltimoErro = new Exception();

            if (!MasterClass.ValidaString(this.Cpf))
            {
                resposta = false;
                Mensagem += "Cpf, ";
            }

            if (!MasterClass.ValidaString(this.Nome))
            {
                resposta = false;
                Mensagem += "Nome, ";
            }

            if (!MasterClass.ValidaString(this.CnpjEmpresa))
            {
                resposta = false;
                Mensagem += "CnpjEmpresa, ";
            }

            if (!MasterClass.ValidaString(this.Cep))
            {
                resposta = false;
                Mensagem += "EnderecoCep, ";
            }

            if (!MasterClass.ValidaString(this.EnderecoLogradouro))
            {
                resposta = false;
                Mensagem += "EndeerecoLogradouro, ";
            }

            if (!MasterClass.ValidaString(this.EnderecoNumero))
            {
                resposta = false;
                Mensagem += "EnderecoNumero, ";
            }

            if (!MasterClass.ValidaString(this.EnderecoBairro))
            {
                resposta = false;
                Mensagem += "EnderecoBairro, ";
            }

            if (!MasterClass.ValidaString(this.EnderecoCidade))
            {
                resposta = false;
                Mensagem += "EnderecoCidade, ";
            }

            if (!MasterClass.ValidaString(this.EnderecoEstado))
            {
                resposta = false;
                Mensagem += "EnderecoEstado, ";
            }

            if (!MasterClass.ValidaString(this.EnderecoPais))
            {
                resposta = false;
                Mensagem += "EnderecoPais, ";
            }

            if (!MasterClass.ValidaString(this.Sexo))
            {
                resposta = false;
                Mensagem += "Sexo, ";
            }

            if (!MasterClass.ValidaString(this.StatusOcupacao))
            {
                resposta = false;
                Mensagem += "StatusOcupacao, ";
            }

            if (!MasterClass.ValidaString(this.DataAdmissao))
            {
                resposta = false;
                Mensagem += "DataAdmissao, ";
            }

            if (!MasterClass.ValidaString(this.NumeroUnidadeOrganizacional))
            {
                resposta = false;
                Mensagem += "NumeroUnidadeOrganizacional, ";
            }

            if (!MasterClass.ValidaString(this.NomeUnidadeOrganizacional))
            {
                resposta = false;
                Mensagem += "NomeUnidadeOrganizacional, ";
            }

            if (!MasterClass.ValidaString(this.CodigoPosicao))
            {
                resposta = false;
                Mensagem += "CodigoPosicao, ";
            }

            if (!MasterClass.ValidaString(this.NomePosicao))
            {
                resposta = false;
                Mensagem += "NomePosicao, ";
            }

            if (!MasterClass.ValidaString(this.CentroCustoCodigo))
            {
                resposta = false;
                Mensagem += "CentroCustoCodigo, ";
            }

            if (!MasterClass.ValidaString(this.PhtCodigo))
            {
                resposta = false;
                Mensagem += "PhtCodigo, ";
            }

            if (!MasterClass.ValidaString(this.PhtValidadeFinal))
            {
                resposta = false;
                Mensagem += "PhtValidadeFinal, ";
            }

            if (!MasterClass.ValidaString(this.PhtValidadeInicial))
            {
                resposta = false;
                Mensagem += "PhtValidadeInicial, ";
            }

            //Valida se os dados informados estão dentre os valores esperados            
            if (resposta)
            {
                Mensagem = "Valor(es) inválido(s) para o(s) campo(s): ";

                if (!MasterClass.ValidaData(this.DataAdmissao))
                {
                    resposta = false;
                    Mensagem += "DataAdmissao, ";
                }

                if (!MasterClass.ValidaData(this.PhtValidadeFinal))
                {
                    resposta = false;
                    Mensagem += "PhtValidadeFinal, ";
                }

                if (!MasterClass.ValidaData(this.PhtValidadeInicial))
                {
                    resposta = false;
                    Mensagem += "PhtValidadeInicial, ";
                }

             

                //Validando campos não obrigatórios
                if(!MasterClass.ValidaDataNaoObrigatoria(this.RgEmissao))
                {
                    resposta = false;
                    Mensagem += "RgEmissao, ";
                }

                if(!MasterClass.ValidaDataNaoObrigatoria(this.VistoEmissao))
                {
                    resposta = false;
                    Mensagem += "VistoEmissao, ";
                }

                if(!MasterClass.ValidaDataNaoObrigatoria(this.VistoValidade))
                {
                    resposta = false;
                    Mensagem += "VistoValidade, ";
                }

                if(!MasterClass.ValidaDataNaoObrigatoria(this.ExpedicaoRne))
                {
                    resposta = false;
                    Mensagem += "ExpedicaoRne, ";
                }

                if (!MasterClass.ValidaDataNaoObrigatoria(this.CnhValidade))
                {
                    resposta = false;
                    Mensagem += "CnhValidade, ";
                }

                if (!MasterClass.ValidaDataNaoObrigatoria(this.CtpsEmissao))
                {
                    resposta = false;
                    Mensagem += "CtpsEmissao, ";
                }

                if (!MasterClass.ValidaDataNaoObrigatoria(this.DataNascimento))
                {
                    resposta = false;
                    Mensagem += "DataNascimento, ";
                }

                if (!MasterClass.ValidaDataNaoObrigatoria(this.TreinamentoValidade))
                {
                    resposta = false;
                    Mensagem += "TreinamentoValidade, ";
                }

                if (MasterClass.ValidaString(this.TituloEleitorNumero))
                {
                    if (!MasterClass.EhInteiro(this.TituloEleitorNumero))
                    {
                        resposta = false;
                        Mensagem += "TituloEleitorNumero, ";
                    }

                    if (this.TituloEleitorNumero.Trim().Length > 12)
                    {
                        resposta = false;
                        Mensagem += "TituloEleitorNumero, ";
                    }
                }


                if (MasterClass.ValidaString(this.TipoSanguineo))
                {
                    HashSet<String> ValoresTipoSanguineo = new HashSet<string>();
                    ValoresTipoSanguineo.Add("Opos");
                    ValoresTipoSanguineo.Add("Apos");
                    ValoresTipoSanguineo.Add("Bpos");
                    ValoresTipoSanguineo.Add("ABpos");
                    ValoresTipoSanguineo.Add("Oneg");
                    ValoresTipoSanguineo.Add("Aneg");
                    ValoresTipoSanguineo.Add("Bneg");
                    ValoresTipoSanguineo.Add("ABneg");

                    if (!ValoresTipoSanguineo.Contains(this.TipoSanguineo))
                    {
                        resposta = false;
                        Mensagem += "TipoSanguineo, ";
                    }

                    if(MasterClass.ValidaString(this.Sexo))
                    {
                        HashSet<String> ValoresSexo = new HashSet<string>();
                        ValoresSexo.Add("Masculino");
                        ValoresSexo.Add("Feminino");

                        if (!ValoresSexo.Contains(this.Sexo))
                        {
                            resposta = false;
                            Mensagem += "Sexo, ";
                        }

                    }
                }

                    if (MasterClass.ValidaString(this.StatusOcupacao))
                    {
                        HashSet<String> ValoresStatus = new HashSet<string>();
                        ValoresStatus.Add("0");
                        ValoresStatus.Add("1");
                        ValoresStatus.Add("3");
                        ValoresStatus.Add("4");

                        if (!ValoresStatus.Contains(this.StatusOcupacao))
                        {
                            resposta = false;
                            Mensagem += "StatusOcupacao, ";
                        }

                    }

                    if (MasterClass.ValidaString(this.Escolaridade))
                    {
                        HashSet<String> ValoresEscolaridade = new HashSet<string>();
                        ValoresEscolaridade.Add("Analfabeto");
                        ValoresEscolaridade.Add("Fund. até 5ª Inc.");
                        ValoresEscolaridade.Add("Fund. até 5ª Comp.");
                        ValoresEscolaridade.Add("Fund. 5ª a 9ª Inc.");
                        ValoresEscolaridade.Add("Fund. Completo");
                        ValoresEscolaridade.Add("Médio Completo");
                        ValoresEscolaridade.Add("Médio Incompleto");
                        ValoresEscolaridade.Add("Superior Incompleto");
                        ValoresEscolaridade.Add("Superior Completo");
                        ValoresEscolaridade.Add("Mestrado");
                        ValoresEscolaridade.Add("Doutorado");
                        ValoresEscolaridade.Add("Pós-Graduação");

                        if (!ValoresEscolaridade.Contains(this.Escolaridade))
                        {
                            resposta = false;
                            Mensagem += "Escolaridade, ";
                        }
                    }

                           

            }

            if (resposta)
            {
                if (MasterClass.ValidaString(this.CentroCustoCodigo))
                {
                    try
                    {
                        SetorCustoGS.Obtem(this.CentroCustoCodigo);
                    }
                    catch (Exception erro)
                    {
                        resposta = false;
                        Mensagem = "ERRO: Centro de Custo não encontrado.  ";
                    }
                }
            }


            if (!resposta)
            {
                Mensagem = Mensagem.Remove(Mensagem.Length - 2);
                MasterClass.UltimoErro = new Exception(Mensagem);
            }

            return resposta;
        
        }

        private PessoaFisicaGS ExtraiPessoaFisicaGS()
        {
            PessoaFisicaGS resposta = new PessoaFisicaGS();
            PessoaFisicaGS pessoa = PessoaFisicaGS.Obtem(this.Cpf);

            if (pessoa == null)
                return pessoa;

            //Obrigatórios
            resposta.Cpf = this.Cpf;
            resposta.Sexo = this.Sexo;

            
            //Opcionais

            if (this.CtpsEstado != String.Empty && this.CtpsEstado != null)
                resposta.CarteiraTrabalhoCodigoUf = EstadoGS.Obtem(this.CtpsEstado).Codigo;
            else
                resposta.CarteiraTrabalhoCodigoUf = pessoa.CarteiraTrabalhoCodigoUf == 0 ? Int32.MinValue : pessoa.CarteiraTrabalhoCodigoUf;

            if (this.RgEstadoEmissor != String.Empty && this.RgEstadoEmissor != null)
                resposta.RgCodigoUf = EstadoGS.Obtem(this.RgEstadoEmissor).Codigo;
            else
                resposta.RgCodigoUf = pessoa.RgCodigoUf == 0 ? Int32.MinValue : pessoa.RgCodigoUf;

            if (this.NascimentoUf != String.Empty && this.NascimentoUf != null)
            {
                int CodigoEstado = EstadoGS.Obtem(this.NascimentoUf).Codigo;
                resposta.CodigoNaturalidadeCidade = CidadeGS.Obtem(this.CidadeNaturalidade, CodigoEstado).Codigo;
            }
            else
                resposta.CodigoNaturalidadeCidade = pessoa.CodigoNaturalidadeCidade == 0 ? Int32.MinValue : pessoa.CodigoNaturalidadeCidade;

            try
            {
                DateTime dataCtps = pessoa.CarteiraTrabalhoEmissao;

                resposta.CarteiraTrabalhoEmissao = MasterClass.StringParaData(this.CtpsEmissao);//this.CtpsEmissao == String.Empty ? dataCtps : Convert.ToDateTime(this.CtpsEmissao);
            }
            catch (Exception erro)
            {
                throw new Exception("[COLABORADOR].[ExtraiPessoaAgendamentoGS] Falha ao converter o campo Data Emissão CTPS.(" + this.CtpsEmissao + ")" + erro.Message);
            }

            resposta.CarteiraTrabalhoNumero = this.CtpsNumero ==String.Empty?pessoa.CarteiraTrabalhoNumero: Convert.ToDecimal(this.CtpsNumero);
            resposta.CarteiraTrabalhoSerie = this.CtpsSerie;
            resposta.Cnh = this.CnhNumero;
            resposta.CnhTipo = this.CnhTipo;
            resposta.CnhValidade = MasterClass.StringParaData(this.CnhValidade);
            resposta.Codigo = pessoa.Codigo;
            resposta.CodigoMunicipio = pessoa.CodigoMunicipio == 0 ? Int32.MinValue : pessoa.CodigoMunicipio;
            
            
            resposta.CrNrNumero = this.NumeroCrNr;
            resposta.CrTipo = this.TipoCrNr;
            resposta.DataNascimento = MasterClass.StringParaData(this.DataNascimento);
            if (MasterClass.ValidaString(this.Escolaridade))
            {
                resposta.Escolaridade = ExtraiEscolaridade();
            }
            else 
            {
                resposta.Escolaridade = pessoa.Escolaridade;
            }

            resposta.EstadoCivil = ExtraiEstadoCivil();
            resposta.Nacionalidade = this.NauralidadePais;
            resposta.NomeMae = this.NomeMae;
            resposta.NomePai = this.NomePai;
            resposta.Passaporte = this.PassaporteNumero;
            
            resposta.RgDataEmissao = MasterClass.StringParaData(this.RgEmissao);
            resposta.RgNumero = this.RgNumero;
            resposta.RgOrgao = this.RgOrgaoEmissor;
            resposta.Rne = this.Rne;
            resposta.RneDataExpedicao = MasterClass.StringParaData(this.ExpedicaoRne);
           
            resposta.TipoSanguineo = this.TipoSanguineo;
            resposta.TituloEleitorNumero = Convert.ToDecimal(this.TituloEleitorNumero);
            resposta.TituloEleitorSecao = Convert.ToDecimal(this.TituloEleitorSecao);
            resposta.TituloEleitorZona = Convert.ToDecimal(this.TituloEleitorZona);
            resposta.VistoData = MasterClass.StringParaData(this.VistoEmissao);
            resposta.VistoDataValidade = MasterClass.StringParaData(this.VistoValidade);
            
            return resposta;
        }

        private EmpregadoSapGS ExtraiEmpregadoSap()
        {
            EmpregadoSapGS resposta = new EmpregadoSapGS();

            resposta.Codigo = this.CodigoSap==null?Int32.MinValue:Convert.ToInt32(this.CodigoSap);
            resposta.CodigoGestorPonto = this.CodigoGestorPonto == null || this.CodigoGestorPonto == String.Empty ? Int32.MinValue : Convert.ToInt32(this.CodigoGestorPonto);
            resposta.CodigoSuperiorImediato = this.CodigoSuperiorImediato == null || this.CodigoSuperiorImediato == String.Empty ? Int32.MinValue : Convert.ToInt32(this.CodigoSuperiorImediato);

            

            if (resposta.Codigo != Int32.MinValue)            
            {

                #region Garante a integridade referencial

                EmpregadoSapGS novo;

                if ((!EmpregadoSapGS.Existe(resposta.CodigoGestorPonto)) && (resposta.CodigoGestorPonto != Int32.MinValue))
                {
                    novo = new EmpregadoSapGS();
                    novo.Codigo = resposta.CodigoGestorPonto;
                    novo.CodigoGestorPonto = Int32.MinValue;
                    novo.CodigoSuperiorImediato = Int32.MinValue;
                    if (novo.InserirRegistro() == -1)
                    {
                        throw new Exception("Falha ao gravar dados de Empregado SAP. " + MasterClass.UltimoErro.Message);
                    }
                }

                if ((!EmpregadoSapGS.Existe(resposta.CodigoSuperiorImediato)) && (resposta.CodigoSuperiorImediato != Int32.MinValue))
                {
                    novo = new EmpregadoSapGS();
                    novo.Codigo = resposta.CodigoSuperiorImediato;
                    novo.CodigoGestorPonto = Int32.MinValue;
                    novo.CodigoSuperiorImediato = Int32.MinValue;
                    if (novo.InserirRegistro() == -1)
                    {
                        throw new Exception("Falha ao gravar dados de Empregado SAP. " + MasterClass.UltimoErro.Message);
                    }
                 }

                #endregion


                if (EmpregadoSapGS.Existe(resposta.Codigo))
                {
                    resposta = EmpregadoSapGS.Obtem(resposta.Codigo);
                    resposta.CodigoGestorPonto = this.CodigoGestorPonto == null||this.CodigoGestorPonto==String.Empty ? Int32.MinValue : Convert.ToInt32(this.CodigoGestorPonto);
                    resposta.CodigoSuperiorImediato = this.CodigoSuperiorImediato == null ||this.CodigoSuperiorImediato == String.Empty? Int32.MinValue : Convert.ToInt32(this.CodigoSuperiorImediato);

                    //resposta.AtualizarRegistro();
                    if (resposta.AtualizarRegistro() == -1)
                    {
                        throw new Exception("Falha ao gravar dados de Empregado SAP. " + MasterClass.UltimoErro.Message);
                    }


                }
                else
                {
                    if (resposta.InserirRegistro() == -1)
                    {
                        throw new Exception("Falha ao gravar dados de Empregado SAP. " + MasterClass.UltimoErro.Message);
                    }
                    
                }
            }
            else
            {
                resposta = new EmpregadoSapGS();
            }

            return resposta;
        }

        private bool ProcessaTelefoneGS(string pCodigoPessoa)
        {
            TelefoneGS telefone1 = new TelefoneGS();
            TelefoneGS telefone2 = new TelefoneGS();
            TelefoneGS telefone3 = new TelefoneGS();

            try
            {
                
                //Trata Telefone 1
                if (this.ContatoTelefone01 != null && this.ContatoTelefone01 != String.Empty) //Contato Telefone 1 informado
                {
                    telefone1 = TelefoneGS.Obtem(pCodigoPessoa, TipoTelefone1);

                    if (telefone1 == null)//Não existe telefone cadastrado do tipo 1
                    {
                        telefone1 = new TelefoneGS();
                        telefone1.Ativo = true;
                        telefone1.CodigoPessoa = pCodigoPessoa;
                        telefone1.DataDesativacao = DateTime.MinValue;
                        telefone1.DataRegistro = DateTime.Now;
                        telefone1.Numero = this.ContatoTelefone01;
                        telefone1.Tipo = TipoTelefone1;

                        telefone1.InserirRegistro();

                    }
                    else
                    {
                        telefone1.Numero = this.ContatoTelefone01;
                        telefone1.AtualizarRegistro();
                    }
                }

                //Trata Telefone 2
                if (this.ContatoTelefone02 != null && this.ContatoTelefone02 != String.Empty) //Contato Telefone 1 informado
                {
                    telefone2 = TelefoneGS.Obtem(pCodigoPessoa, TipoTelefone2);

                    if (telefone2 == null)//Não existe telefone cadastrado do tipo 2
                    {
                        telefone2 = new TelefoneGS();
                        telefone2.Ativo = true;
                        telefone2.CodigoPessoa = pCodigoPessoa;
                        telefone2.DataDesativacao = DateTime.MinValue;
                        telefone2.DataRegistro = DateTime.Now;
                        telefone2.Numero = this.ContatoTelefone02;
                        telefone2.Tipo = TipoTelefone2;

                        telefone2.InserirRegistro();

                    }
                    else
                    {
                        telefone2.Numero = this.ContatoTelefone02;
                        telefone2.AtualizarRegistro();
                    }
                }

                //Trata Telefone 3
                if (this.ContatoTelefone03 != null && this.ContatoTelefone03 != String.Empty) //Contato Telefone 1 informado
                {
                    telefone3 = TelefoneGS.Obtem(pCodigoPessoa, TipoTelefone3);

                    if (telefone3 == null)//Não existe telefone cadastrado do tipo 3
                    {
                        telefone3 = new TelefoneGS();
                        telefone3.Ativo = true;
                        telefone3.CodigoPessoa = pCodigoPessoa;
                        telefone3.DataDesativacao = DateTime.MinValue;
                        telefone3.DataRegistro = DateTime.Now;
                        telefone3.Numero = this.ContatoTelefone03;
                        telefone3.Tipo = TipoTelefone3;

                        telefone3.InserirRegistro();

                    }
                    else
                    {
                        telefone3.Numero = this.ContatoTelefone03;
                        telefone3.AtualizarRegistro();
                    }
                }

                return true;
            }
            catch (Exception erro)
            {
                MasterClass.UltimoErro = erro;
                return false;
            }

            
        }

        private bool ProcessaCargoGS()
        {
            CargoGS cargo = new CargoGS();
            
            try
            {

                //Trata Telefone 1
                if (this.CodigoPosicao != null && this.CodigoPosicao != String.Empty) //Codigo Posição informado
                {
                    cargo = CargoGS.Obtem(Convert.ToInt32(this.CodigoPosicao));

                    if (cargo == null)//Não existe cargo cadastrado com esse código
                    {
                        cargo = new CargoGS();
                        cargo.Ativo = true;
                        cargo.Nome = this.NomePosicao;
                        cargo.CodigoSap = this.CodigoPosicao == String.Empty ? Int32.MinValue : Convert.ToInt32(this.CodigoPosicao);
                        cargo.Descricao = String.Empty;
                        cargo.DataDesativacao = DateTime.MinValue;
                        cargo.DataRegistro = DateTime.Now;
                        //cargo.Descricao = this.NomePosicao;

                        if (cargo.InserirRegistro() == -1)
                        { 
                            throw new Exception("Falha ao inserir novo cargo (" + this.CodigoPosicao + "): " + MasterClass.UltimoErro.Message);

                        }
                    }                    
                }                

                return true;
            }
            catch (Exception erro)
            {
                MasterClass.UltimoErro = erro;
                return false;
            }

        }

        private bool ProcessaGrupoTrabalhoGS(Int32 pCodigoPapel)
        {
            GrupoTrabalhoGS grupo = new GrupoTrabalhoGS();
            GrupoTrabalhoColaboradorGS colaborador = new GrupoTrabalhoColaboradorGS();

            try
            {

                //Trata Telefone 1
                if (this.PhtCodigo != null && this.PhtCodigo != String.Empty) //Codigo Posição informado
                {
                    grupo = GrupoTrabalhoGS.ObtemPorNome(this.PhtCodigo);

                    colaborador.Ativo = true;
                    colaborador.CodigoGrupoTrabalho = grupo.Codigo;
                    colaborador.CodigoPapel = pCodigoPapel;
                    colaborador.DataDesativacao = DateTime.MinValue;
                    colaborador.DataRegistro = DateTime.Now;
                    colaborador.InserirRegistro();

                }

                return true;
            }
            catch (Exception erro)
            {
                MasterClass.UltimoErro = erro;
                return false;
            }
        }




        private string ExtraiEstadoCivil()
        {
            switch (this.EstadoCivil)
            {
                case "Solt.":
                    {
                    return "1";                    
                    }
                case "UniaoEstavel":
                    {
                        return "2";
                    }
                case "Casado":
                    {
                        return "3";                        
                    }
                case "Separ.":
                    {
                        return "4";                        
                    }
                case "Divorc":
                    {
                        return "5";
                    }
                case "Viúvo":
                    {
                        return "6";
                    }
                default:
                    {
                        return string.Empty; //throw new Exception("Valor inválido para o campo EstadoCivil (" + this.EstadoCivil + ")");
                    }
            }
        }

        private string ExtraiEscolaridade()
        {
            switch (this.Escolaridade)
            {
                case "Analfabeto":
                    {
                        return "1";
                    }
                case "Fund. até 5ª Inc.":
                    {
                        return "2";
                    }
                case "Fund. até 5ª Comp.":
                    {
                        return "3";
                    }
                case "Fund. 5ª a 9ª Inc.":
                    {
                        return "2";
                    }
                case "Fund. Completo":
                    {
                        return "3";
                    }               

                case "Médio Completo":
                    {
                        return "5";
                    }

                case "Médio Incompleto":
                    {
                        return "4";
                    }

                case "Superior Incompleto":
                    {
                        return "7";
                    }

                case "Superior Completo":
                    {
                        return "8";
                    }

                case "Mestrado":
                    {
                        return "11";
                    }

                case "Doutorado":
                    {
                        return "12";
                    }

                case "Pós-Graduação":
                    {
                        return "10";
                    }
                default:
                    {
                        throw new Exception("Valor inválido para o campo Escolaridade (" + this.Escolaridade + ")");
                    }
            }
        }

        private Int32 ProcessaEndereco()
        {
            PaisGS pais = PaisGS.Obtem(this.EnderecoPais);
            EstadoGS estado = EstadoGS.Obtem(this.EnderecoEstado, pais.Codigo);
            CidadeGS cidade = CidadeGS.Obtem(this.EnderecoCidade, estado.Codigo);
            BairroGS bairro = BairroGS.Obtem(this.EnderecoBairro, cidade.Codigo);
            LogradouroGS logradouro = new LogradouroGS();
            try
            {
                 logradouro = LogradouroGS.Obtem(this.EnderecoLogradouro, bairro.Codigo);
            }
            catch (Exception erro)
            {
                if (erro.Message == "Logradouro não encontrado!")
                {
                    logradouro = new LogradouroGS();
                    logradouro.Ativo = true;
                    if (this.Cep != String.Empty && this.Cep != null)
                    {
                        if (Cep.Length != 8)
                        {
                            throw new Exception("Formato de CEP inválido! (" + this.Cep + ")");
                        }
                        logradouro.Cep = this.Cep;//this.Cep.Replace("-","").Replace(".","").Substring(0,8);
                    }
                    else
                    {
                        logradouro.Cep = String.Empty;
                    }
                    logradouro.CodigoBairro = bairro.Codigo;
                    logradouro.DataDesativacao = DateTime.MinValue;
                    logradouro.DataRegistro = DateTime.Now;
                    logradouro.Nome = this.EnderecoLogradouro;

                    int CodigoLogradouro = logradouro.InserirRegistroRetornaID();

                    logradouro = LogradouroGS.Obtem(CodigoLogradouro);

                }
                else
                {
                    throw erro;
                }
            }
            EnderecoGS endereco = new EnderecoGS();

            endereco.Ativo = true;
            //endereco.CaixaPostal = this            
            endereco.CodigoLogradouro = logradouro.Codigo;
            endereco.Complemento = this.EnderecoComplemento;
            endereco.Numero = this.EnderecoNumero;            
            endereco.DataDesativacao = DateTime.MinValue;
            endereco.DataRegistro = DateTime.Now;
            endereco.Codigo = endereco.InserirRegistroRetornaID();

            return endereco.Codigo;
        
        }

        private PessoaGS ExtraiPessoaGS(String pCodigoPessoaFisica)
        {
            PessoaGS resposta = new PessoaGS();
            PessoaGS pessoa = PessoaGS.Obtem(pCodigoPessoaFisica);
            EnderecoGS endereco = new EnderecoGS();
            

            EmpresaGS empresa = EmpresaGS.Obtem(this.CnpjEmpresa);
            PaisGS pais = PaisGS.Obtem(this.EnderecoPais);
            EstadoGS estado = EstadoGS.Obtem(this.EnderecoEstado, pais.Codigo);
            CidadeGS cidade = CidadeGS.Obtem(this.EnderecoCidade, estado.Codigo);
            BairroGS bairro = BairroGS.Obtem(this.EnderecoBairro, cidade.Codigo);
            LogradouroGS logradouro = new LogradouroGS();

            try
            {
                logradouro = LogradouroGS.Obtem(this.EnderecoLogradouro, bairro.Codigo);
            }
            catch (Exception erro)
            {
                if (erro.Message.Contains("Logradouro não encontrado"))
                {
                    logradouro = new LogradouroGS();
                    logradouro.Ativo = true;
                    if (this.Cep != String.Empty && this.Cep != null)
                    {
                        if (Cep.Length != 8)
                        {
                            throw new Exception("Formato de CEP inválido! (" + this.Cep + ")");
                        }
                        logradouro.Cep = this.Cep;//this.Cep.Replace("-","").Replace(".","").Substring(0,8);
                    }
                    else
                    {
                        logradouro.Cep = String.Empty;
                    }
                    logradouro.CodigoBairro = bairro.Codigo;
                    logradouro.DataDesativacao = DateTime.MinValue;
                    logradouro.DataRegistro = DateTime.Now;
                    logradouro.Nome = this.EnderecoLogradouro;

                    int CodigoLogradouro = logradouro.InserirRegistroRetornaID();

                    logradouro = LogradouroGS.Obtem(CodigoLogradouro);

                }
                else
                {
                    throw erro;
                }
            }

            if (pessoa.CodigoEndereco > 0)
            {
                endereco = EnderecoGS.Obtem(pessoa.CodigoEndereco);

                if ((this.EnderecoNumero != endereco.Numero) || (endereco.CodigoLogradouro != logradouro.Codigo))
                    resposta.CodigoEndereco = ProcessaEndereco();
                else
                    resposta.CodigoEndereco = pessoa.CodigoEndereco;

            }
            else
            {
                resposta.CodigoEndereco = ProcessaEndereco();
            }


            resposta.Apelido = (this.Apelido == String.Empty)||(this.Apelido == null ) ? ExtraiApelido(this.Nome) : this.Apelido;
            resposta.Codigo = pessoa.Codigo;

            

            resposta.DataRegistro = pessoa.DataRegistro;
            resposta.Email = this.Email;
            resposta.Nome = this.Nome;            

            return resposta;

        }

        //private SetorCustoGS ExtraiCentroCusto()
        //{
        //    SetorCustoGS resposta = new SetorCustoGS();

        //    resposta.Ativo = true;
        //    //resposta.Codigo = this.CentroCustoCodigo;
        //    resposta.DataRegistro = DateTime.Now;
        //    resposta.Descricao = this.DescricaoCentroCusto;
        //    resposta.Nome = this.DescricaoCentroCusto;
        //    resposta.Numero = this.CentroCustoCodigo.ToString();
        //    resposta.Tipo = this.TipoCentroCusto; 

        //    return resposta;
        //}

        private PapelGS ExtraiPapelGS(string pCodigoPessoaFisica)
        {
            PapelGS resposta = new PapelGS();
            //PapelGS papel = PapelGS.ObtemPorCodigo(pCodigoPessoaFisica);
            PapelGS papel = PapelGS.Obtem(this.Cpf);

            resposta.Codigo = papel.Codigo;
            resposta.CodigoCargo = papel.CodigoCargo==0?Int32.MinValue:papel.CodigoCargo;
            
            this.CodigoPosicao = this.CodigoPosicao==null?string.Empty:this.CodigoPosicao;

            if(this.CodigoPosicao != string.Empty)
            {
                CargoGS cargo = CargoGS.Obtem(Convert.ToInt32(this.CodigoPosicao));
                if (cargo == null)
                {
                    throw new Exception("Cargo inexistente com o código: " + this.CodigoPosicao);
                }
                else
                {
                    resposta.CodigoCargo = cargo.Codigo;
                }
            }

            resposta.CodigoModelo = papel.CodigoModelo==0?Int32.MinValue:papel.CodigoModelo;
            resposta.CodigoPessoaFisica = pCodigoPessoaFisica;
            resposta.DataRegistro = papel.DataRegistro;
            resposta.EmAnalise = papel.EmAnalise;
            resposta.Juridico = papel.Juridico;
            resposta.Passback = papel.Passback;
            resposta.Senha = papel.Senha;
            resposta.Supervisionado = papel.Supervisionado;
            resposta.Supervisor = papel.Supervisor;
            //resposta.CodigoArea = papel.CodigoArea == 0 ? Int32.MinValue : papel.CodigoArea;
            resposta.CodigoArea = ProcessaArea().Codigo;
            resposta.CodigoSap = this.CodigoSap == null ? Int32.MinValue : Convert.ToInt32(this.CodigoSap);

            //this.NumeroUnidadeOrganizacional = this.NumeroUnidadeOrganizacional == null ? string.Empty : this.NumeroUnidadeOrganizacional;

            //if (this.NumeroUnidadeOrganizacional != string.Empty)
            //{
            //    AreaGS area = AreaGS.Obtem(this.NumeroUnidadeOrganizacional);
            //    resposta.CodigoArea = area.Codigo;
            //}
            

            return resposta;

        }

        private ColaboradorGS ExtraiColaboradorGS(string pCodigoPessoaFisica)
        {
            ColaboradorGS resposta = new ColaboradorGS();
            PapelGS papel = PapelGS.Obtem(this.Cpf);
            resposta = ColaboradorGS.Obtem(papel.Codigo);
            EmpresaGS empresa = EmpresaGS.Obtem(this.CnpjEmpresa);

            resposta.CodigoEmpresa = empresa.Codigo;
            resposta.CodigoPapel = papel.Codigo;
            //resposta.ConselhoProfissional = t

            resposta.DataAdmissao = MasterClass.StringParaData(this.DataAdmissao);
            resposta.Mensalista = this.Mensalista;
            resposta.NumeroPis = Convert.ToDecimal(this.Pis);
            resposta.Observacao = this.Observacao;
            resposta.RegistroProfissional = this.RegistroProfissional;
            resposta.Salario = this.Salario==String.Empty?Decimal.MinValue:Convert.ToDecimal(this.Salario);
            
            resposta.CodigoArea = ProcessaArea().Codigo;
            

            return resposta;

        }

        private Boolean ExtraiAlocacaoColaborador(AlocacaoColaboradorGS pAlocacao)
        {
            
            MasterClass.UltimoErro = new Exception();

            AlocacaoColaboradorGS resposta = pAlocacao;


            SetorCustoGS centroCusto = SetorCustoGS.Obtem(this.CentroCustoCodigo);
            PapelGS papel = PapelGS.Obtem(this.Cpf);

            try
            {

                if (resposta != null)
                {
                    resposta.Ativo = false;
                    resposta.DataDesativacao = DateTime.Now;

                    if (resposta.CodigoSetor == centroCusto.Codigo)
                    {
                        return true;
                    }
                    
                    if (resposta.AtualizarRegistro() == -1)
                    {
                        throw new Exception("Falha ao desativar Alocação: " + MasterClass.UltimoErro.Message);
                    }
                }

                resposta = new AlocacaoColaboradorGS();

                resposta.Ativo = true;
                resposta.CodigoPapel = papel.Codigo;

                if (MasterClass.EhInteiro(System.Configuration.ConfigurationSettings.AppSettings["PedidoPadrao"]))
                {
                    resposta.CodigoPedido = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["PedidoPadrao"]);
                }
                else
                {
                    throw new Exception("Falha ao obter o valor do parâmetro 'PedidoPadrao' do arquivo de Configuração");
                }

                resposta.CodigoSetor = centroCusto.Codigo;

                resposta.DataDesativacao = DateTime.MinValue;
                resposta.DataRegistro = DateTime.Now;

                if (resposta.InserirRegistro() == -1)
                {
                    throw new Exception("Falha ao gravar Alocação: " + MasterClass.UltimoErro.Message);
                }

                return true;

            }
            catch (Exception erro)
            {
                MasterClass.UltimoErro = erro;
                return false;
            }

        }

        private StatusPapelAgendamentoGS ExtraiStatusPapelAgendamento(PapelAgendamentoGS papel)
        {
            StatusPapelAgendamentoGS resposta = new StatusPapelAgendamentoGS();

            resposta.CodigoPapelAgendamento = papel.Codigo;
            resposta.DataRegistro = DateTime.Now;
            resposta.Status = "ContratacaoAutorizada";


            return resposta;
        }

        private StatusPapelAgendamentoGS ExtraiStatusPapelAgendamento(PapelAgendamentoGS papel, StatusPapelAgendamentoGS pStatus)
        {
            StatusPapelAgendamentoGS resposta = pStatus;

            resposta.CodigoPapelAgendamento = papel.Codigo;
            resposta.DataRegistro = pStatus.DataRegistro;
            resposta.Status = pStatus.Status;
            resposta.Observacao = this.Observacao;


            return resposta;
        }

        private AreaGS ProcessaArea()
        {
            AreaGS area = AreaGS.Obtem(this.NumeroUnidadeOrganizacional);


            if (area == null)
            {
                area = new AreaGS();
                area.Ativo = true;
                area.DataDesativacao = DateTime.MinValue;
                area.DataRegistro = DateTime.Now;
                area.Nome = this.NomeUnidadeOrganizacional;
                area.Numero = this.NumeroUnidadeOrganizacional;
                area.Codigo = area.InserirRegistroRetornaID();

                if (area.Codigo < 1)
                {
                    throw new Exception("Falha ao registrar nova Área." + this.NumeroUnidadeOrganizacional);
                }

            }
            else
            {
                area.Nome = this.NomeUnidadeOrganizacional;
                area.AtualizarRegistro();
            }

            return area;
            
        }

        private PapelLogGS ProcessaPapelLog(PapelGS papel)
        {

            PapelLogGS resposta;

            if(PapelLogGS.ExistePapelLog(papel.Codigo))
            {
                resposta = PapelLogGS.Obtem(papel.Codigo);

                if((resposta.Status == "Ferias") || (resposta.Status == "Afastamento"))
                {
                    return resposta;
                }

                if (ObtemStatus() != resposta.Status)
                {
                    resposta.DataFim = DateTime.Now;
                    resposta.AtualizarRegistro();
                }
                else
                {
                    return resposta;
                }

                
            }

            resposta = new PapelLogGS();

            resposta.CodigoPapel = papel.Codigo;
            resposta.DataRegistro = DateTime.Now;
            resposta.DataInicio = DateTime.Now;
            resposta.DataFim = DateTime.MinValue;
            
            switch(this.StatusOcupacao)
            {
                case "3":
                    {
                        resposta.Status = "Ativo";
                        break;
                    }
                case "0":
                    {
                        resposta.Status = "BaixaDefinitiva";
                        break;
                    }
                case "4":
                    {
                        resposta.Status = "Inativacao";
                        break;
                    }
                case "1":
                    {
                        resposta.Status = "Inativacao";
                        break;
                    }

            }           

            resposta.InserirRegistro();

            return resposta;
        }

        private string ObtemStatus()
        {
            String resposta = String.Empty;

            switch (this.StatusOcupacao)
            {
                case "3":
                    {
                        resposta = "Ativo";
                        break;
                    }
                case "0":
                    {
                        resposta = "BaixaDefinitiva";
                        break;
                    }
                case "4":
                    {
                        resposta = "Inativacao";
                        break;
                    }
                case "1":
                    {
                        resposta = "Inativacao";
                        break;
                    }

            }

            return resposta;
        }

        private ColaboradorGS ExtraiColaboradorGS(Int32 pCodigoPapel)
        {
            ColaboradorGS resposta = new ColaboradorGS();

            try
            {
                //PessoaFisicaGS -> Pessoa -> Papel ->

                //Obtem dados auxiliares
                EmpresaGS empresa = EmpresaGS.Obtem(this.CnpjEmpresa);
                ColaboradorGS colaborador = ColaboradorGS.Obtem(pCodigoPapel);

                resposta.CodigoEmpresa = empresa.Codigo;
                resposta.CodigoPapel = pCodigoPapel;
                resposta.ConselhoProfissional = colaborador.ConselhoProfissional;
                resposta.DataAdmissao = Convert.ToDateTime(this.DataAdmissao);
                resposta.Mensalista = this.Mensalista;
                resposta.NumeroPis = Convert.ToDecimal(this.Pis);
                resposta.Observacao = this.Observacao;
                resposta.RegistroProfissional = this.RegistroProfissional;
                resposta.Salario = this.Salario==String.Empty?Decimal.MinValue:Convert.ToDecimal(this.Salario);


            }
            catch (Exception erro)
            {
                throw new Exception("[COLABORADOR].[EXTRAICOLABORADORGS] Erro ao extrair os dados de Colaborador!" + erro.Message);
            }

            return resposta;



        }

        private ColaboradorAgendamentoGS ExtraiColaboradorAgendamentoGS(PapelAgendamentoGS pPapel)
        {
            ColaboradorAgendamentoGS resposta = new ColaboradorAgendamentoGS();

            try
            {
                //Obtem dados auxiliares
                EmpresaGS empresa = EmpresaGS.Obtem(this.CnpjEmpresa);
                TurnoGS turno = TurnoGS.Obtem(this.PhtCodigo);
                GrupoTrabalhoTurnoGS grupoturno = GrupoTrabalhoTurnoGS.Obtem(turno.Codigo);
                GrupoTrabalhoGS grupo = GrupoTrabalhoGS.Obtem(grupoturno.CodigoGrupoTrabalho.ToString());
                //SetorCustoGS centroCusto = SetorCustoGS.Obtem(this.CentroCustoCodigo);

                //GrupoTrabalhoGS grupoTrabalho = GrupoTrabalhoGS.Obtem(this.PhtCodigo.ToString());
                resposta.CodigoEmpresa = empresa.Codigo;
                //resposta.CodigoGrupoTrabalho               
                resposta.CodigoPapel = pPapel.Codigo;
                //resposta.ConselhoProfissional = thi
                //resposta.CodigoSetor = centroCusto.Codigo;
                

                try
                {
                    //resposta.DataAdmissao = this.DataAdmissao == String.Empty ? DateTime.MinValue : Convert.ToDateTime(this.DataAdmissao);
                    resposta.DataAdmissao = MasterClass.StringParaData(this.DataAdmissao);
                }
                catch (Exception erro)
                {
                    throw new Exception("[COLABORADOR].[ExtraiColaboradorAgendamentoGS] Falha ao converter o campo Data Retorno.(" + this.DataAdmissao + ")");
                }

                resposta.Mensalista = this.Mensalista;
                resposta.NumeroPis = this.Pis;
                resposta.Observacao = this.Observacao;
                resposta.RegistroProfissional = this.RegistroProfissional;
                resposta.Salario = this.Salario == String.Empty ? Decimal.MinValue : Convert.ToDecimal(this.Salario); ;
                resposta.CodigoGrupoTrabalho = grupo.Codigo;

            }
            catch (Exception erro)
            {
                throw new Exception("[COLABORADOR].[EXTRAICOLABORADORAGENDAMENTOGS] Erro ao extrair os dados de Colaborador!" + erro.Message);
            }

            return resposta;
        }

        private ColaboradorAgendamentoGS ExtraiColaboradorAgendamentoGS(PapelAgendamentoGS pPapel, ColaboradorAgendamentoGS pColaborador)
        {
            ColaboradorAgendamentoGS resposta = pColaborador;

            try
            {
                //Obtem dados auxiliares
                EmpresaGS empresa = EmpresaGS.Obtem(this.CnpjEmpresa);
                TurnoGS turno = TurnoGS.Obtem(this.PhtCodigo);
                GrupoTrabalhoTurnoGS grupoturno = GrupoTrabalhoTurnoGS.Obtem(turno.Codigo);
                GrupoTrabalhoGS grupo = GrupoTrabalhoGS.Obtem(grupoturno.CodigoGrupoTrabalho.ToString());
                //SetorCustoGS centroCusto = SetorCustoGS.Obtem(this.CentroCustoCodigo);

                //GrupoTrabalhoGS grupoTrabalho = GrupoTrabalhoGS.Obtem(this.PhtCodigo.ToString());
                resposta.CodigoEmpresa = empresa.Codigo;
                //resposta.CodigoGrupoTrabalho               
                resposta.CodigoPapel = pPapel.Codigo;
                //resposta.ConselhoProfissional = thi
                //resposta.CodigoSetor = centroCusto.Codigo;


                try
                {
                    //resposta.DataAdmissao = this.DataAdmissao == String.Empty ? DateTime.MinValue : Convert.ToDateTime(this.DataAdmissao);
                    resposta.DataAdmissao = MasterClass.StringParaData(this.DataAdmissao);
                }
                catch (Exception erro)
                {
                    throw new Exception("[COLABORADOR].[ExtraiColaboradorAgendamentoGS] Falha ao converter o campo Data Retorno.(" + this.DataAdmissao + ")");
                }

                resposta.Mensalista = this.Mensalista;
                resposta.NumeroPis = this.Pis;
                resposta.Observacao = this.Observacao;
                resposta.RegistroProfissional = this.RegistroProfissional;
                resposta.Salario = this.Salario == String.Empty ? Decimal.MinValue : Convert.ToDecimal(this.Salario); ;
                resposta.CodigoGrupoTrabalho = grupo.Codigo;

            }
            catch (Exception erro)
            {
                throw new Exception("[COLABORADOR].[EXTRAICOLABORADORAGENDAMENTOGS] Erro ao extrair os dados de Colaborador!" + erro.Message);
            }

            return resposta;
        }

        private PapelAgendamentoGS ExtraiPapelAgendamento(PessoaAgendamentoGS pPessoa)
        {
            PapelAgendamentoGS resposta = new PapelAgendamentoGS();

            try
            {
                //Obtem dados auxiliares
                //EmpresaGS empresa = EmpresaGS.Obtem(this.CnpjEmpresa);
                //GrupoTrabalhoGS grupoTrabalho = GrupoTrabalhoGS.Obtem(this.PhtCodigo.ToString());

                ProcessaCargoGS();
                CargoGS cargo = CargoGS.Obtem(Convert.ToInt32(this.CodigoPosicao));
                SetorCustoGS setor = SetorCustoGS.Obtem(this.CentroCustoCodigo);


                resposta.CodigoCargo = cargo.Codigo;
                resposta.CodigoGestor = GestorGS.ObtemPadrao().Codigo;
                resposta.CodigoPapel = Int32.MinValue;
                resposta.CodigoPessoaAgendamento = pPessoa.Codigo;
                resposta.CodigoUsuario = string.Empty;
                resposta.DataRegistro = DateTime.Now;
                resposta.CodigoArea = ProcessaArea().Codigo;
                resposta.CodigoSap = this.CodigoSap == null ? Int32.MinValue : Convert.ToInt32(this.CodigoSap);
                resposta.CodigoSetor = setor.Codigo;
                
                
            }
            catch (Exception erro)
            {
                throw new Exception("[COLABORADOR].[ExtraiPapelAgendamento] Erro ao extrair os dados de Papel!" + erro.Message);
            }

            return resposta;
        }

        private PapelAgendamentoGS ExtraiPapelAgendamento(PessoaAgendamentoGS pPessoa, PapelAgendamentoGS pPapel)
        {
            PapelAgendamentoGS resposta = pPapel;

            try
            {
                //Obtem dados auxiliares
                //EmpresaGS empresa = EmpresaGS.Obtem(this.CnpjEmpresa);
                //GrupoTrabalhoGS grupoTrabalho = GrupoTrabalhoGS.Obtem(this.PhtCodigo.ToString());

                ProcessaCargoGS();
                CargoGS cargo = CargoGS.Obtem(Convert.ToInt32(this.CodigoPosicao));
                SetorCustoGS setor = SetorCustoGS.Obtem(this.CentroCustoCodigo);


                resposta.CodigoCargo = cargo.Codigo==0?Int32.MinValue:cargo.Codigo;
                resposta.CodigoGestor = GestorGS.ObtemPadrao().Codigo;
                resposta.CodigoPapel = pPapel.CodigoPapel == 0 ? Int32.MinValue : pPapel.CodigoPapel;
                resposta.CodigoPessoaAgendamento = pPessoa.Codigo;
                //resposta.CodigoUsuario = Int32.MinValue;
                //resposta.DataRegistro = DateTime.Now;
                resposta.CodigoArea = ProcessaArea().Codigo;
                resposta.CodigoSap = this.CodigoSap == null ? Int32.MinValue : Convert.ToInt32(this.CodigoSap);
                resposta.CodigoSetor = setor.Codigo;
                


            }
            catch (Exception erro)
            {
                throw new Exception("[COLABORADOR].[ExtraiPapelAgendamento] Erro ao extrair os dados de Papel!" + erro.Message);
            }

            return resposta;
        }


        private string ExtraiApelido(String pNome)
        {
            string resposta = string.Empty;
            string[] Nomes = pNome.Split(' ');
            int PosicaoSobrenome = Nomes.GetLength(0);

            if (PosicaoSobrenome == 1)
            {
                resposta = Nomes[0];
            }
            else
            {
                resposta = Nomes[0] + " "+Nomes[PosicaoSobrenome - 1];
            }

            return resposta;
        }

        private PessoaAgendamentoGS ExtraiPessoaAgendamentoGS()
        {
            PessoaAgendamentoGS resposta = new PessoaAgendamentoGS();

            try
            {
                //Obtem dados auxiliares
                EmpresaGS empresa = EmpresaGS.Obtem(this.CnpjEmpresa);
                PaisGS pais = PaisGS.Obtem(this.EnderecoPais);
                EstadoGS estado = EstadoGS.Obtem(this.EnderecoEstado, pais.Codigo);
                CidadeGS cidade = CidadeGS.Obtem(this.EnderecoCidade, estado.Codigo);
                BairroGS bairro = BairroGS.Obtem(this.EnderecoBairro, cidade.Codigo);
                LogradouroGS logradouro = new LogradouroGS();
                

                try
                {
                    logradouro = LogradouroGS.Obtem(this.EnderecoLogradouro, bairro.Codigo);
                }
                catch (Exception erro)
                {
                    if (erro.Message.Contains("[LOGRADOURO].[OBTEM] Falha ao obter o Logradouro"))
                    {
                        logradouro = new LogradouroGS();
                        logradouro.Ativo = true;
                        if (this.Cep != String.Empty && this.Cep != null)
                        {
                            if (Cep.Length != 8)
                            {
                                throw new Exception("Formato de CEP inválido! (" + this.Cep + ")");
                            }
                            logradouro.Cep = this.Cep;//this.Cep.Replace("-","").Replace(".","").Substring(0,8);
                        }
                        else
                        {
                            logradouro.Cep = String.Empty;
                        }
                        logradouro.CodigoBairro = bairro.Codigo;
                        logradouro.DataDesativacao = DateTime.MinValue;
                        logradouro.DataRegistro = DateTime.Now;
                        logradouro.Nome = this.EnderecoLogradouro;

                        int CodigoLogradouro = logradouro.InserirRegistroRetornaID();

                        logradouro = LogradouroGS.Obtem(CodigoLogradouro);

                    }
                    else
                    {
                        throw erro;
                    }
                }

                //GrupoTrabalhoGS grupoTrabalho = GrupoTrabalhoGS.Obtem(this.PhtCodigo.ToString());

                if (this.Apelido == null)
                    this.Apelido = String.Empty;

                resposta.Apelido = this.Apelido == String.Empty ? ExtraiApelido(this.Nome) : this.Apelido;

                if (this.CtpsEstado != null)
                    resposta.CarteiraTrabalhoCodigoUf = EstadoGS.Obtem(this.CtpsEstado).Codigo;
                else
                    resposta.CarteiraTrabalhoCodigoUf = Int32.MinValue;
               


                try
                {
                    //resposta.CarteiraTrabalhoEmissao = this.CtpsEmissao == String.Empty ? DateTime.MinValue : Convert.ToDateTime(this.CtpsEmissao);
                    resposta.CarteiraTrabalhoEmissao = MasterClass.StringParaData(this.CtpsEmissao);
                }
                catch (Exception erro)
                {
                    throw new Exception("[COLABORADOR].[ExtraiPessoaAgendamentoGS] Falha ao converter o campo Data Emissão CTPS.(" + this.CtpsEmissao + ")" + erro.Message);
                }

                resposta.CarteiraTrabalhoNumero = Convert.ToDecimal(this.CtpsNumero);
                resposta.CarteiraTrabalhoSerie = this.CtpsSerie;
                resposta.Cnh = this.CnhNumero;
                resposta.CnhTipo = this.CnhTipo;

                try
                {
                    //resposta.CnhValidade = this.CnhValidade == String.Empty ? DateTime.MinValue : Convert.ToDateTime(this.CnhValidade);
                    resposta.CnhValidade = MasterClass.StringParaData(this.CnhValidade);
                }
                catch (Exception erro)
                {
                    throw new Exception("[COLABORADOR].[ExtraiPessoaAgendamentoGS] Falha ao converter o campo Data Validade CNH.(" + this.CnhValidade + ")" + erro.Message);
                }

                resposta.CodigoLogradouro = logradouro.Codigo;
                resposta.CodigoMunicipio = cidade.Codigo;
                //resposta.CodigoNaturalidadeUf = EstadoGS.Obtem(this.NascimentoUf).Codigo;
                resposta.CodigoNaturalidadeUf = CidadeGS.Obtem(this.CidadeNaturalidade).Codigo;
                resposta.Cpf = this.Cpf;
                resposta.CrNrNumero = this.NumeroCrNr;
                resposta.CrTipo = this.TipoCrNr;

                

                try
                {
                    resposta.DataNascimento = MasterClass.StringParaData(this.DataNascimento);
                }
                catch (Exception erro)
                {
                    throw new Exception("[COLABORADOR].[ExtraiPessoaAgendamentoGS] Falha ao converter o campo Data de Nascimento.(" + this.DataNascimento + ")" + erro.Message);
                }

                resposta.DataRegistro = DateTime.Now;

                resposta.Email = this.Email;
                resposta.EnderecoComplemento = this.EnderecoComplemento;
                resposta.EnderecoNumero = this.EnderecoNumero;
                if (MasterClass.ValidaString(this.Escolaridade))
                {
                    resposta.Escolaridade = ExtraiEscolaridade();
                }
                else
                {
                    resposta.Escolaridade = String.Empty;

                }
                resposta.EstadoCivil = ExtraiEstadoCivil();

                resposta.Nacionalidade = this.NauralidadePais;
                resposta.Nome = this.Nome;
                resposta.NomeMae = this.NomeMae;
                resposta.NomePai = this.NomePai;
                resposta.Passaporte = this.PassaporteNumero;
                resposta.RgCodigoUf = this.RgEstadoEmissor == null?Int32.MinValue:EstadoGS.Obtem(this.RgEstadoEmissor).Codigo;
                
                try
                {
                    //resposta.RgDataEmissao = this.RgEmissao == String.Empty ? DateTime.MinValue : Convert.ToDateTime(this.RgEmissao);
                    resposta.RgDataEmissao = MasterClass.StringParaData(this.RgEmissao);
                }
                catch (Exception erro)
                {
                    throw new Exception("[COLABORADOR].[ExtraiPessoaAgendamentoGS] Falha ao converter o campo Data de Emissão do RG.(" + this.RgEmissao + ")" + erro.Message);
                }

                resposta.RgNumero = this.RgNumero;
                resposta.RgOrgao = this.RgOrgaoEmissor;
                resposta.Rne = this.Rne;

                resposta.RneDataExpedicao = MasterClass.StringParaData(this.ExpedicaoRne);

                resposta.Sexo = this.Sexo;
                resposta.TelefoneCelular = this.ContatoTelefone01;
                resposta.TelefoneEmergencia = this.ContatoTelefone02;
                resposta.TelefoneResidencial = this.ContatoTelefone03;

                resposta.TipoSanguineo = this.TipoSanguineo;
                resposta.TituloEleitorNumero = Convert.ToDecimal(this.TituloEleitorNumero);
                resposta.TituloEleitorSecao = Convert.ToDecimal(this.TituloEleitorSecao);

                resposta.TituloEleitorZona = Convert.ToDecimal(this.TituloEleitorZona);

                 try
                {
                    //resposta.VistoData = this.VistoEmissao == String.Empty ? DateTime.MinValue : Convert.ToDateTime(this.VistoEmissao);
                    resposta.VistoData = MasterClass.StringParaData(this.VistoEmissao);
                }
                catch (Exception erro)
                {
                    throw new Exception("[COLABORADOR].[ExtraiPessoaAgendamentoGS] Falha ao converter o campo Data de Emissão do RG.(" + this.VistoEmissao + ")" + erro.Message);
                }

                try
                {
                    //resposta.VistoDataValidade = this.VistoValidade == String.Empty ? DateTime.MinValue : Convert.ToDateTime(this.VistoValidade);
                    resposta.VistoDataValidade = MasterClass.StringParaData(this.VistoValidade);
                }
                catch (Exception erro)
                {
                    throw new Exception("[COLABORADOR].[ExtraiPessoaAgendamentoGS] Falha ao converter o campo Data de Emissão do RG.(" + this.VistoValidade + ")" + erro.Message);
                }

               
                

            }
            catch (Exception erro)
            {
                throw new Exception("[COLABORADOR].[ExtraiPessoaAgendamentoGS] Erro ao extrair os dados de Colaborador!" + erro.Message);
            }

            return resposta;
        }

        private PessoaAgendamentoGS ExtraiPessoaAgendamentoGS(PessoaAgendamentoGS pPessoa)
        {
            PessoaAgendamentoGS resposta = pPessoa;

            try
            {
                //Obtem dados auxiliares
                EmpresaGS empresa = EmpresaGS.Obtem(this.CnpjEmpresa);
                PaisGS pais = PaisGS.Obtem(this.EnderecoPais);
                EstadoGS estado = EstadoGS.Obtem(this.EnderecoEstado, pais.Codigo);
                CidadeGS cidade = CidadeGS.Obtem(this.EnderecoCidade, estado.Codigo);
                BairroGS bairro = BairroGS.Obtem(this.EnderecoBairro, cidade.Codigo);
                LogradouroGS logradouro = new LogradouroGS();

                try
                {
                    logradouro = LogradouroGS.Obtem(this.EnderecoLogradouro, bairro.Codigo);
                }
                catch (Exception erro)
                {
                    if (erro.Message.Contains("[LOGRADOURO].[OBTEM] Falha ao obter o Logradouro"))
                    {
                        logradouro = new LogradouroGS();
                        logradouro.Ativo = true;
                        if (this.Cep != String.Empty && this.Cep != null)
                        {
                            if (Cep.Length != 8)
                            {
                                throw new Exception("Formato de CEP inválido! (" + this.Cep + ")");
                            }
                            logradouro.Cep = this.Cep;//this.Cep.Replace("-","").Replace(".","").Substring(0,8);
                        }
                        else
                        {
                            logradouro.Cep = String.Empty;
                        }
                        logradouro.CodigoBairro = bairro.Codigo;
                        logradouro.DataDesativacao = DateTime.MinValue;
                        logradouro.DataRegistro = DateTime.Now;
                        logradouro.Nome = this.EnderecoLogradouro;

                        int CodigoLogradouro = logradouro.InserirRegistroRetornaID();

                        logradouro = LogradouroGS.Obtem(CodigoLogradouro);

                    }
                    else
                    {
                        throw erro;
                    }
                }

                //GrupoTrabalhoGS grupoTrabalho = GrupoTrabalhoGS.Obtem(this.PhtCodigo.ToString());

                if (this.Apelido == null)
                    this.Apelido = String.Empty;

                resposta.Apelido = this.Apelido == String.Empty ? ExtraiApelido(this.Nome) : this.Apelido;

                if (this.CtpsEstado != null)
                    resposta.CarteiraTrabalhoCodigoUf = EstadoGS.Obtem(this.CtpsEstado).Codigo;
                else
                    resposta.CarteiraTrabalhoCodigoUf = Int32.MinValue;



                try
                {
                    //resposta.CarteiraTrabalhoEmissao = this.CtpsEmissao == String.Empty ? DateTime.MinValue : Convert.ToDateTime(this.CtpsEmissao);
                    resposta.CarteiraTrabalhoEmissao = MasterClass.StringParaData(this.CtpsEmissao);
                }
                catch (Exception erro)
                {
                    throw new Exception("[COLABORADOR].[ExtraiPessoaAgendamentoGS] Falha ao converter o campo Data Emissão CTPS.(" + this.CtpsEmissao + ")" + erro.Message);
                }

                resposta.CarteiraTrabalhoNumero = Convert.ToDecimal(this.CtpsNumero);
                resposta.CarteiraTrabalhoSerie = this.CtpsSerie;
                resposta.Cnh = this.CnhNumero;
                resposta.CnhTipo = this.CnhTipo;

                try
                {
                    //resposta.CnhValidade = this.CnhValidade == String.Empty ? DateTime.MinValue : Convert.ToDateTime(this.CnhValidade);
                    resposta.CnhValidade = MasterClass.StringParaData(this.CnhValidade);
                }
                catch (Exception erro)
                {
                    throw new Exception("[COLABORADOR].[ExtraiPessoaAgendamentoGS] Falha ao converter o campo Data Validade CNH.(" + this.CnhValidade + ")" + erro.Message);
                }

                resposta.CodigoLogradouro = logradouro.Codigo;
                resposta.CodigoMunicipio = cidade.Codigo;
                //resposta.CodigoNaturalidadeUf = EstadoGS.Obtem(this.NascimentoUf).Codigo;
                resposta.CodigoNaturalidadeUf = CidadeGS.Obtem(this.CidadeNaturalidade).Codigo;
                resposta.Cpf = this.Cpf;
                resposta.CrNrNumero = this.NumeroCrNr;
                resposta.CrTipo = this.TipoCrNr;



                try
                {
                    resposta.DataNascimento = MasterClass.StringParaData(this.DataNascimento);
                }
                catch (Exception erro)
                {
                    throw new Exception("[COLABORADOR].[ExtraiPessoaAgendamentoGS] Falha ao converter o campo Data de Nascimento.(" + this.DataNascimento + ")" + erro.Message);
                }

                resposta.DataRegistro = DateTime.Now;

                resposta.Email = this.Email;
                resposta.EnderecoComplemento = this.EnderecoComplemento;
                resposta.EnderecoNumero = this.EnderecoNumero;
                resposta.Escolaridade = ExtraiEscolaridade();
                resposta.EstadoCivil = ExtraiEstadoCivil();

                resposta.Nacionalidade = this.NauralidadePais;
                resposta.Nome = this.Nome;
                resposta.NomeMae = this.NomeMae;
                resposta.NomePai = this.NomePai;
                resposta.Passaporte = this.PassaporteNumero;
                resposta.RgCodigoUf = this.RgEstadoEmissor == null ? Int32.MinValue : EstadoGS.Obtem(this.RgEstadoEmissor).Codigo;

                try
                {
                    //resposta.RgDataEmissao = this.RgEmissao == String.Empty ? DateTime.MinValue : Convert.ToDateTime(this.RgEmissao);
                    resposta.RgDataEmissao = MasterClass.StringParaData(this.RgEmissao);
                }
                catch (Exception erro)
                {
                    throw new Exception("[COLABORADOR].[ExtraiPessoaAgendamentoGS] Falha ao converter o campo Data de Emissão do RG.(" + this.RgEmissao + ")" + erro.Message);
                }

                resposta.RgNumero = this.RgNumero;
                resposta.RgOrgao = this.RgOrgaoEmissor;
                resposta.Rne = this.Rne;

                resposta.RneDataExpedicao = MasterClass.StringParaData(this.ExpedicaoRne);

                resposta.Sexo = this.Sexo;
                resposta.TelefoneCelular = this.ContatoTelefone03;
                resposta.TelefoneEmergencia = this.ContatoTelefone02;
                resposta.TelefoneResidencial = this.ContatoTelefone01;

                resposta.TipoSanguineo = this.TipoSanguineo;
                resposta.TituloEleitorNumero = Convert.ToDecimal(this.TituloEleitorNumero);
                resposta.TituloEleitorSecao = Convert.ToDecimal(this.TituloEleitorSecao);

                resposta.TituloEleitorZona = Convert.ToDecimal(this.TituloEleitorZona);

                try
                {
                    //resposta.VistoData = this.VistoEmissao == String.Empty ? DateTime.MinValue : Convert.ToDateTime(this.VistoEmissao);
                    resposta.VistoData = MasterClass.StringParaData(this.VistoEmissao);
                }
                catch (Exception erro)
                {
                    throw new Exception("[COLABORADOR].[ExtraiPessoaAgendamentoGS] Falha ao converter o campo Data de Emissão do RG.(" + this.VistoEmissao + ")" + erro.Message);
                }

                try
                {
                    //resposta.VistoDataValidade = this.VistoValidade == String.Empty ? DateTime.MinValue : Convert.ToDateTime(this.VistoValidade);
                    resposta.VistoDataValidade = MasterClass.StringParaData(this.VistoValidade);
                }
                catch (Exception erro)
                {
                    throw new Exception("[COLABORADOR].[ExtraiPessoaAgendamentoGS] Falha ao converter o campo Data de Emissão do RG.(" + this.VistoValidade + ")" + erro.Message);
                }




            }
            catch (Exception erro)
            {
                throw new Exception("[COLABORADOR].[ExtraiPessoaAgendamentoGS] Erro ao extrair os dados de Colaborador!" + erro.Message);
            }

            return resposta;
        }



        public bool InsereColaborador(Integracao pIntegracao)
        {
            try
            {
                

                pIntegracao.TipoIntegracao = "Colaborador";
                pIntegracao.Status = "PROCESSANDO";
                pIntegracao.DataStatus = DateTime.Now;
                pIntegracao.Mensagem = String.Empty;

                pIntegracao.Converte().AtualizarRegistro();

                //pessoa, papel, colaborador

                MasterClass item = new MasterClass();

                EmpregadoSapGS empregado = ExtraiEmpregadoSap();

                item = this.ExtraiPessoaAgendamentoGS();

                Int32 CodigoPessoa = item.InserirRegistroRetornaID();

                if (CodigoPessoa == -1)
                {
                    throw new Exception("[COLABORADOR].[INSERECOLABORADOR] Registro não inserido." + MasterClass.UltimoErro.Message);
                }
                
                String cpfPessoa = ((PessoaAgendamentoGS)item).Cpf;

                PessoaAgendamentoGS pessoa = PessoaAgendamentoGS.Obtem(CodigoPessoa);

                item = this.ExtraiPapelAgendamento(pessoa);

                Int32 CodigoPapel = item.InserirRegistroRetornaID();

                if (CodigoPapel == -1)
                {
                    throw new Exception("[COLABORADOR].[INSERECOLABORADOR] Registro não inserido." + MasterClass.UltimoErro.Message);
                }

                PapelAgendamentoGS papel = PapelAgendamentoGS.Obtem(CodigoPapel);

                item = ExtraiColaboradorAgendamentoGS(papel);

                Int32 CodigoColaborador = item.InserirRegistro();

                if (CodigoColaborador == -1)
                {
                    throw new Exception("[COLABORADOR].[INSERECOLABORADOR] Registro não inserido." + MasterClass.UltimoErro.Message);
                }


                StatusPapelAgendamentoGS status = ExtraiStatusPapelAgendamento(papel);

                if (status.InserirRegistro() == -1)
                {
                    throw new Exception("[COLABORADOR].[INSERECOLABORADOR] Registro não inserido." + MasterClass.UltimoErro.Message);
                }

                if (this.TreinamentoCodigoSap != String.Empty && this.TreinamentoCodigoSap != null)
                {
                    TreinamentoGS treinamento = TreinamentoGS.Obtem(this.TreinamentoCodigoSap);
                  
                    TreinamentoPapelAgendamentoGS novoTreinamento = new TreinamentoPapelAgendamentoGS();
                  
                    novoTreinamento.CodigoPapelAgendamento = papel.Codigo;
                    novoTreinamento.CodigoTreinamento = treinamento.Codigo;                   

                    novoTreinamento.DataRealizacao = MasterClass.StringParaData(this.TreinamentoValidade);
                    novoTreinamento.Ativo = true;
                    novoTreinamento.DataDesativacao = DateTime.MinValue;
                    novoTreinamento.DataRegistro = DateTime.Now;
                    novoTreinamento.DataValidade = DateTime.MinValue;

                    if (novoTreinamento.InserirRegistro() == -1)
                    {
                        throw new Exception("[COLABORADOR].[INSERECOLABORADOR] Registro não inserido." + MasterClass.UltimoErro.Message);
                    }
                    
                    //}

                    //ProcessaPapelLog(papel);
                }
                


                    pIntegracao.DataStatus = DateTime.Now;
                    pIntegracao.Mensagem += "Colaborador processado com sucesso. ";
                    pIntegracao.Status = "SUCESSO";
                

                pIntegracao.Converte().AtualizarRegistro();

                return true;
            }
            catch (Exception erro)
            {

                pIntegracao.Status = "ERRO";
                pIntegracao.Mensagem += "Falha ao processar o registro. " + erro.Message;
                pIntegracao.DataStatus = DateTime.Now;

                pIntegracao.Converte().AtualizarRegistro();


                return false;
            }
        }

        public bool AtualizaColaborador(Integracao pIntegracao)
        {
            try
            {


                pIntegracao.TipoIntegracao = "Colaborador";
                pIntegracao.Status = "PROCESSANDO";
                pIntegracao.DataStatus = DateTime.Now;
                pIntegracao.Mensagem = String.Empty;

                pIntegracao.Converte().AtualizarRegistro();

                //pessoa, papel, colaborador

                MasterClass item = new MasterClass();

                EmpregadoSapGS empregado = ExtraiEmpregadoSap();

                if (!ProcessaCargoGS())
                {
                    throw new Exception("[COLABORADOR].[ATUALIZACOLABORADOR] Registro não atualizado." + MasterClass.UltimoErro.Message);
                }

                item = this.ExtraiPessoaFisicaGS();


                //Caso o Colaborador tenha sido recentemente cadastrado, não constará na base de dados consolidada, fazendo que o retorno seja null.
                //Para esses casos, processar apenas os treinamentos
                if (item == null)
                {

                    StatusPapelAgendamentoGS status = StatusPapelAgendamentoGS.Obtem(this.Cpf);

                    if (status != null)
                    {
                        if (status.Status == "ContratacaoAutorizada")
                        {
                            ProcessaAtualizacaoPreCadastro();
                        }
                    }

                    if (this.TreinamentoCodigoSap != String.Empty && this.TreinamentoCodigoSap != null)
                    {
                        TreinamentoGS treinamento = TreinamentoGS.Obtem(this.TreinamentoCodigoSap);
                        PessoaAgendamentoGS pessoaFisicaAgendamento = PessoaAgendamentoGS.Obtem(this.Cpf);
                        PapelAgendamentoGS papelAgendamento = PapelAgendamentoGS.ObtemPorCodPessoa(pessoaFisicaAgendamento.Codigo);


                        TreinamentoPapelAgendamentoGS novoTreinamento = new TreinamentoPapelAgendamentoGS();
                        //novoTreinamento.Codigo = treinamento.Codigo;
                        novoTreinamento.CodigoPapelAgendamento = papelAgendamento.Codigo;
                        novoTreinamento.CodigoTreinamento = treinamento.Codigo;

                        novoTreinamento.DataRealizacao = MasterClass.StringParaData(this.TreinamentoValidade);
                        novoTreinamento.Ativo = true;
                        novoTreinamento.DataDesativacao = DateTime.MinValue;
                        novoTreinamento.DataRegistro = DateTime.Now;
                        novoTreinamento.DataValidade = DateTime.MinValue;

                        if (novoTreinamento.InserirRegistro() == -1)
                        {
                            throw new Exception("[COLABORADOR].[ATUALIZACOLABORADOR] Registro não atualizado." + MasterClass.UltimoErro.Message);
                        }
                    }
                    //else
                    //{
                    //    throw new Exception("[COLABORADOR].[ATUALIZACOLABORADOR] Cadastro de Treinamento não localizado: " + this.TreinamentoCodigoSap);
                    //}

                    pIntegracao.DataStatus = DateTime.Now;
                    pIntegracao.Mensagem += "Colaborador processado com sucesso. ";
                    pIntegracao.Status = "SUCESSO";


                    pIntegracao.Converte().AtualizarRegistro();


                    return true; ;

                }

                PessoaFisicaGS pessoa = (PessoaFisicaGS)item;

                if (item.AtualizarRegistro() == -1)
                {
                    throw new Exception("[COLABORADOR].[ATUALIZACOLABORADOR] Registro não atualizado." + MasterClass.UltimoErro.Message);
                }

                item = this.ExtraiPessoaGS(((PessoaFisicaGS)item).Codigo);

                if (item.AtualizarRegistro() == -1)
                {
                    throw new Exception("[COLABORADOR].[ATUALIZACOLABORADOR] Registro não atualizado." + MasterClass.UltimoErro.Message);
                }

                item = ExtraiPapelGS(((PessoaGS)item).Codigo);

                if (item.AtualizarRegistro() == -1)
                {
                    throw new Exception("[COLABORADOR].[ATUALIZACOLABORADOR] Registro não atualizado." + MasterClass.UltimoErro.Message);
                }


                if (!ProcessaTelefoneGS(pessoa.Codigo))
                {
                    throw new Exception("[COLABORADOR].[ATUALIZACOLABORADOR] Registro não atualizado." + MasterClass.UltimoErro.Message);
                }




                PapelGS papel = PapelGS.Obtem(this.Cpf);

                if (!ProcessaGrupoTrabalhoGS(papel.Codigo))
                {
                    throw new Exception("[COLABORADOR].[ATUALIZACOLABORADOR] Registro não atualizado." + MasterClass.UltimoErro.Message);
                }

                item = ExtraiColaboradorGS(pessoa.Codigo);

                if (item.AtualizarRegistro() == -1)
                {
                    throw new Exception("[COLABORADOR].[ATUALIZACOLABORADOR] Registro não atualizado." + MasterClass.UltimoErro.Message);
                }

                AlocacaoColaboradorGS alocacao = AlocacaoColaboradorGS.Obtem(papel.Codigo);


                if(!ExtraiAlocacaoColaborador(alocacao))
                {
                    throw new Exception("[COLABORADOR].[ATUALIZACOLABORADOR] Registro não atualizado." + MasterClass.UltimoErro.Message);
                }


                if (this.TreinamentoCodigoSap != String.Empty && this.TreinamentoCodigoSap != null)
                {
                    TreinamentoGS treinamento = TreinamentoGS.Obtem(this.TreinamentoCodigoSap);
                    PessoaFisicaGS pessoaFisica = PessoaFisicaGS.Obtem(this.Cpf);

                    //O cadastro do colaborador pode não ter sido efetivado ainda, para esses casos, pesquisar nas tabelas de Agendamento
                    if (pessoaFisica == null)
                    {
                        PessoaAgendamentoGS pessoaFisicaAgendamento = PessoaAgendamentoGS.Obtem(this.Cpf);
                        PapelAgendamentoGS papelAgendamento = PapelAgendamentoGS.ObtemPorCodPessoa(pessoaFisicaAgendamento.Codigo);

                        TreinamentoPapelAgendamentoGS novoTreinamento = new TreinamentoPapelAgendamentoGS();
                        //novoTreinamento.Codigo = treinamento.Codigo;
                        novoTreinamento.CodigoPapelAgendamento = papelAgendamento.Codigo;
                        novoTreinamento.CodigoTreinamento = treinamento.Codigo;

                        novoTreinamento.DataRealizacao = MasterClass.StringParaData(this.TreinamentoValidade);
                        novoTreinamento.Ativo = true;
                        novoTreinamento.DataDesativacao = DateTime.MinValue;
                        novoTreinamento.DataRegistro = DateTime.Now;
                        novoTreinamento.DataValidade = DateTime.MinValue;

                        if (novoTreinamento.InserirRegistro() == -1)
                        {
                            throw new Exception("[COLABORADOR].[ATUALIZACOLABORADOR] Registro não atualizado." + MasterClass.UltimoErro.Message);
                        }

                    }
                    else
                    {

                        TreinamentoPessoaGS treinamentoPessoa = new TreinamentoPessoaGS();

                        if (TreinamentoPessoaGS.ExisteTreinamento(pessoaFisica.Codigo, treinamento.Codigo))
                        {
                            treinamentoPessoa = TreinamentoPessoaGS.Obtem(pessoaFisica.Codigo, treinamento.Codigo);
                            treinamentoPessoa.Realizado = MasterClass.StringParaData(this.TreinamentoValidade);
                            if (treinamentoPessoa.AtualizarRegistro() == -1)
                            {
                                throw new Exception("[COLABORADOR].[ATUALIZACOLABORADOR] Registro não atualizado." + MasterClass.UltimoErro.Message);
                            }
                        }
                        else
                        {
                            TreinamentoPessoaGS novoTreinamento = new TreinamentoPessoaGS();
                            //PessoaAgendamentoGS pessoaFisicaAgendamento = PessoaAgendamentoGS.Obtem(this.Cpf);
                            //PapelAgendamentoGS papelAgendamento = PapelAgendamentoGS.ObtemPorCodPessoa(pessoaFisicaAgendamento.Codigo);

                            //novoTreinamento.Codigo = treinamento.Codigo;
                            novoTreinamento.CodigoPapel = papel.Codigo;
                            novoTreinamento.CodigoTreinamento = treinamento.Codigo;
                            novoTreinamento.CodigoPessoaFisica = pessoa.Codigo;

                            novoTreinamento.Realizado = MasterClass.StringParaData(this.TreinamentoValidade);
                            novoTreinamento.Ativo = true;
                            novoTreinamento.Desativacao = DateTime.MinValue;
                            novoTreinamento.Registro = DateTime.Now;
                            novoTreinamento.Validade = novoTreinamento.Realizado.AddDays(treinamento.Dias);


                            if (novoTreinamento.InserirRegistro() == -1)
                            {
                                throw new Exception("[COLABORADOR].[ATUALIZACOLABORADOR] Registro não atualizado." + MasterClass.UltimoErro.Message);
                            }
                        }
                    }
                }

                ProcessaPapelLog(papel);



                pIntegracao.DataStatus = DateTime.Now;
                pIntegracao.Mensagem += "Colaborador processado com sucesso. ";
                pIntegracao.Status = "SUCESSO";


                pIntegracao.Converte().AtualizarRegistro();

                return true;
            }
            catch (Exception erro)
            {

                pIntegracao.Status = "ERRO";
                pIntegracao.Mensagem += "Falha ao processar o registro. " + erro.Message;
                pIntegracao.DataStatus = DateTime.Now;

                pIntegracao.Converte().AtualizarRegistro();


                return false;
            }
        }

        private void ProcessaAtualizacaoPreCadastro()
        {
            try
            {
                if (PessoaAgendamentoGS.Existe(this.Cpf))
                {
                    #region PessoaAgendamento

                    PessoaAgendamentoGS pessoa = PessoaAgendamentoGS.Obtem(this.Cpf);
                    pessoa = ExtraiPessoaAgendamentoGS(pessoa);

                    if (pessoa.AtualizarRegistro() == -1)
                    {
                        throw new Exception("Falha ao processar atualização de PessoaAgendamento: " + MasterClass.UltimoErro.Message);
                    }

                    #endregion

                    #region PapelAgendamento


                    PapelAgendamentoGS papel = PapelAgendamentoGS.ObtemPorCodPessoa(pessoa.Codigo);
                    papel = ExtraiPapelAgendamento(pessoa, papel);                   

                    if (papel.AtualizarRegistro() == -1)
                    {
                        throw new Exception("Falha ao processar atualização de PapelAgendamento: " + MasterClass.UltimoErro.Message);
                    }

                    #endregion

                    #region ColaboradorAgendamento

                    ColaboradorAgendamentoGS colaborador = ColaboradorAgendamentoGS.Obtem(papel.Codigo);
                    colaborador = ExtraiColaboradorAgendamentoGS(papel, colaborador);

                    if (colaborador.AtualizarRegistro() == -1)
                    {
                        throw new Exception("Falha ao processar atualização de ColaboradorAgendamento: " + MasterClass.UltimoErro.Message);
                    }
                    #endregion

                    #region StatusPapelAgendamento
                    //Para este item, pode não ser necessário o processamento de atualização (informação fixa)
                    //StatusPapelAgendamentoGS status = StatusPapelAgendamentoGS.Obtem(papel.Codigo);
                    //status = ExtraiStatusPapelAgendamento(papel, status);

                    //if (status.AtualizarRegistro() == -1)
                    //{
                    //    throw new Exception("Falha ao processar atualização de StatusAgendamento: " + MasterClass.UltimoErro.Message);
                    //}

                    

                    #endregion

                }
                else
                {
                    throw new Exception("Registro de Pré-Cadastro inexistente para o Cpf: " + this.Cpf);
                }
            }
            catch (Exception erro)
            {
                throw new Exception("Falha ao processar atualização de Pré-Cadastro (PapelAgendamento): " + erro.Message);
            }


        }

        #endregion


    }
}
