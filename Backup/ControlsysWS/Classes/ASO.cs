﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace br.com.globalsys.controlsys.webservice
{
    //Théo está alinhando com o Vanderson
    //Criar coluna Descrição (olhar DOC)
    //Criar coluna TIPO
    public class ASO
    {

        #region Atributos

        private string _Cpf;
        private int _TipoAtendimento;
        private string _Validade;

        private string _TipoRegistro;

        #endregion

        #region Propriedades

        public String Cpf
        {
            get { return _Cpf; }
            set { _Cpf = value; }
        }

        public Int32 TipoAtendimento
        {
            get { return _TipoAtendimento; }
            set { _TipoAtendimento = value; }
        }

        public String Validade
        {
            get { return _Validade; }
            set { _Validade = value; }
        }

        private String TipoRegistro
        {
            get { return _TipoRegistro; }
            set { _TipoRegistro = value; }
        }

        #endregion

        #region Métodos

        public Boolean ValidaAso()
        {
            bool resposta = true;
            string Mensagem = "Campo(s) obrigatório(s) não informado(s): ";
            MasterClass.UltimoErro = new Exception();

            if (!MasterClass.ValidaString(this.Cpf))
            {
                resposta = false;
                Mensagem += "Cpf, ";
            }            

            if (!MasterClass.ValidaString(this.Validade))
            {
                resposta = false;
                Mensagem += "Validade, ";
            }

           

            if (resposta)
            {
                Mensagem = "Valor(es) inválido(s) para o(s) campo(s): ";

                HashSet<int> ValoresTipoAtendimento = new HashSet<int>();
                ValoresTipoAtendimento.Add(1);
                ValoresTipoAtendimento.Add(2);
                ValoresTipoAtendimento.Add(4);
                ValoresTipoAtendimento.Add(5);

                if (!ValoresTipoAtendimento.Contains(this.TipoAtendimento))
                {
                    resposta = false;
                    Mensagem += "TipoAtendimento, ";
                }

                if (!MasterClass.ValidaData(this.Validade))
                {
                    resposta = false;
                    Mensagem += "Validade, ";
                }             

            }

            if (!resposta)
            {
                Mensagem = Mensagem.Remove(Mensagem.Length - 2);
                MasterClass.UltimoErro = new Exception(Mensagem);
            }

            return resposta;

        }

        private AsoGS ExtraiAso()
        {
            AsoGS resposta = new AsoGS();
            PessoaFisicaGS pessoa = PessoaFisicaGS.Obtem(this.Cpf);
            PapelGS papel = PapelGS.Obtem(this.Cpf);

            resposta.CodigoPapel = papel.Codigo;
            resposta.DataValidade = MasterClass.StringParaData(this.Validade);
            resposta.DataRealizacao = MasterClass.StringParaData(this.Validade).AddYears(-1);
            resposta.TipoRegistro = "ASO";
            resposta.TipoAtendimento = this.TipoAtendimento;
            resposta.Compareceu = true;
            //resposta.Codigo

            

            //try
            //{
            //    resposta.DataInicio = this.DataInicio == String.Empty ? DateTime.MinValue : Convert.ToDateTime(this.DataInicio);
            //}
            //catch (Exception)
            //{
            //    throw new Exception("[ASO].[EXTRAIASO] Falha ao converter o campo Data Início.(" + this.DataRetorno + ")");
            //}

            //resposta.DataValidade = this.Validade;
            //resposta.DataRealizacao = this.Validade;

            return resposta;
        }

        public bool InsereAso(Integracao pIntegracao)
        {
            try
            {


                pIntegracao.TipoIntegracao = "ASO";
                pIntegracao.Status = "PROCESSANDO";
                pIntegracao.DataStatus = DateTime.Now;
                pIntegracao.Mensagem = String.Empty;

                pIntegracao.Converte().AtualizarRegistro();

                MasterClass item = new MasterClass();

                item = this.ExtraiAso();

                if (item.InserirRegistro() < 1)
                {
                    throw new Exception("[ASO].[INSEREASO] Registro não inserido. " + MasterClass.UltimoErro.Message);
                }
                else
                {
                    pIntegracao.DataStatus = DateTime.Now;
                    pIntegracao.Mensagem += "ASO processado com sucesso. ";
                    pIntegracao.Status = "SUCESSO";
                }

                pIntegracao.Converte().AtualizarRegistro();

                return true;
            }
            catch (Exception erro)
            {

                pIntegracao.Status = "ERRO";
                pIntegracao.Mensagem += "Falha ao processar o registro. " + erro.Message;
                pIntegracao.DataStatus = DateTime.Now;

                pIntegracao.Converte().AtualizarRegistro();


                return false;
            }
        }

        #endregion


    }
}
