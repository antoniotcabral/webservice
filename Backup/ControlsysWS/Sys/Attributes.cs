﻿using System;
using System.Reflection;
//using Oracle.DataAccess.Client;
//using System.Data.SqlClient;
//using System.Data.OleDb;
using System.Data;

namespace br.com.globalsys.controlsys.webservice
{
    /// <summary>
    /// Classe que vincula a Propriedade com o nome do campo na tabela
    /// </summary>
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public class CampoNaTabela:System.Attribute
    {
        public string NomeDoCampo;
        public DbType TipoDoCampo;
        public bool EhChave = false;
        public string TagXml;
        public bool EhAuto=false;

        public CampoNaTabela(){}

        public CampoNaTabela(string nome, DbType tipo, string tag)
        {
            NomeDoCampo = nome;
            TipoDoCampo = tipo;
            TagXml = tag;
                       
        }

        public CampoNaTabela(string nome, DbType tipo)
        {
            NomeDoCampo = nome;
            TipoDoCampo = tipo;
            EhAuto = false;
            
        }

        public CampoNaTabela(string nome, DbType tipo, bool ehChave)
        {
            NomeDoCampo = nome;
            TipoDoCampo = tipo;
            EhChave = ehChave;
        }

        public CampoNaTabela(string nome, DbType tipo, bool ehChave, string tag)
        {
            NomeDoCampo = nome;
            TipoDoCampo = tipo;
            EhChave = ehChave;
            TagXml = tag;
        }


        public CampoNaTabela(string nome, DbType tipo, bool ehChave, bool ehAuto)
        {
            NomeDoCampo = nome;
            TipoDoCampo = tipo;
            EhChave = ehChave;
            EhAuto = ehAuto;
        }

        public CampoNaTabela(string nome, DbType tipo, bool ehChave, string tag, bool ehAuto)
        {
            NomeDoCampo = nome;
            TipoDoCampo = tipo;
            EhChave = ehChave;
            TagXml = tag;
            EhAuto = ehAuto;
        }

    }

    /// <summary>
    /// Classe que vincula a Classe de Dados com a Tabela no Banco de Dados
    /// </summary>
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public  class DadosDaTabela : System.Attribute
    {
        public string Proprietario;
        public string Nome;
        public String NomeEntidade;
        public String TagXml;

        public DadosDaTabela() { }

        public DadosDaTabela(string nome)
        {            
            Nome = nome;
        }

        public DadosDaTabela(string nome, string tag)
        {
            Nome = nome;
            TagXml = tag;
        }

        public DadosDaTabela(string nome, string entidade, string tag)
        {
            Nome = nome;
            NomeEntidade = entidade;
            TagXml = tag;
        }

    }
    
    /// <summary>
    /// Classe que vincula a Classe de Negócio com a Classe de Dados
    /// </summary>
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public  class ClasseDados : System.Attribute
    {
        public string NomeClasseDados;

        public ClasseDados() { }

        public ClasseDados(string nome)
        {
            NomeClasseDados = nome;
        }

    }

    /// <summary>
    /// Classe que vincula a Propriedade da Classe de Negócio com a Propriedade da Classe de Dados
    /// </summary>
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public  class NomePropriedadeNegocio : System.Attribute
    {
        public string PropriedadeNegocio;

        public NomePropriedadeNegocio(){}

        public NomePropriedadeNegocio(string nome)
        {
            PropriedadeNegocio = nome;
        }

    }


}
