﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace br.com.globalsys.controlsys.webservice
{
    public class Ferias
    {
        //Status = ""afastamento"
        #region Atributos

        private string _Cpf;
        private string _DataInicio;
        private string _DataRetorno;

        #endregion

        #region Propriedades

        public String Cpf
        {
            get { return _Cpf; }
            set { _Cpf = value; }
        }

        public String DataInicio
        {
            get { return _DataInicio; }
            set { _DataInicio = value; }
        }

        public String DataRetorno
        {
            get { return _DataRetorno; }
            set { _DataRetorno = value; }
        }

        #endregion

        #region Métodos

        public Boolean ValidaFerias()
        {
            bool resposta = true;
            string Mensagem = "Campo(s) obrigatório(s) não informado(s): ";
            MasterClass.UltimoErro = new Exception();

            if (!MasterClass.ValidaString(this.Cpf))
            {
                resposta = false;
                Mensagem += "Cpf, ";
            }

            if (!MasterClass.ValidaString(this.DataInicio))
            {
                resposta = false;
                Mensagem += "DataInicio, ";
            }

            if (!MasterClass.ValidaString(this.DataRetorno))
            {
                resposta = false;
                Mensagem += "DataRetorno, ";
            }

            if (resposta)
            {
                Mensagem = "Valor(es) inválido(s) para o(s) campo(s): ";

                if (!MasterClass.ValidaData(this.DataInicio))
                {
                    resposta = false;
                    Mensagem += "DataInicio, ";
                }

                if (!MasterClass.ValidaData(this.DataRetorno))
                {
                    resposta = false;
                    Mensagem += "DataRetorno, ";
                }              

            }

            if (!resposta)
            {
                Mensagem = Mensagem.Remove(Mensagem.Length - 2);
                MasterClass.UltimoErro = new Exception(Mensagem);
            }

            return resposta;

        }

        private PapelLogGS ExtraiPapelLogGS()
        {
            PapelLogGS resposta = new PapelLogGS();
            //PessoaFisicaGS pessoa = PessoaFisicaGS.Obtem(this.Cpf);
            PapelGS papel = PapelGS.Obtem(this.Cpf);
            //PapelLogGS ativo = PapelLogGS.ObtemAtivo(papel.Codigo);
            resposta.DataInicio = MasterClass.StringParaData(this.DataInicio);
            resposta.DataFim = MasterClass.StringParaData(this.DataRetorno);

            resposta.CodigoPapel = papel.Codigo;
            
            resposta.DataRegistro = DateTime.Now;

            resposta.Motivo = "FÉRIAS";
            resposta.Status = "Ferias";

            return resposta;
        }

        public bool InsereFerias(Integracao pIntegracao)
        {
            try
            {


                pIntegracao.TipoIntegracao = "Ferias";
                pIntegracao.Status = "PROCESSANDO";
                pIntegracao.DataStatus = DateTime.Now;
                pIntegracao.Mensagem = String.Empty;

                pIntegracao.Converte().AtualizarRegistro();

                MasterClass item = new MasterClass();

                item = this.ExtraiPapelLogGS();

                if (item.InserirRegistro() < 1)
                {
                    throw new Exception("[FERIAS].[INSEREFERIAS] Registro não inserido. " + MasterClass.UltimoErro.Message);
                }
                else
                {
                    pIntegracao.DataStatus = DateTime.Now;
                    pIntegracao.Mensagem += "Férias processada com sucesso. ";
                    pIntegracao.Status = "SUCESSO";
                }

                pIntegracao.Converte().AtualizarRegistro();

                return true;
            }
            catch (Exception erro)
            {

                pIntegracao.Status = "ERRO";
                pIntegracao.Mensagem += "Falha ao processar o registro. " + erro.Message;
                pIntegracao.DataStatus = DateTime.Now;

                pIntegracao.Converte().AtualizarRegistro();


                return false;
            }
        }

        #endregion

    }
}
