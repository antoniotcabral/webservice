﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using br.com.globalsys.controlsys.webservice;

namespace br.com.globalsys.controlsys.webservice
{
    public class MonitorIntegracao
    {

        #region Atributos

        private int _Codigo;
        private int _IntegracaoSAP;
        private string _XML;
        private bool _Integrado;
        private DateTime _DataIntegracao;
        private DateTime _DataRegistro;

        #endregion

        #region Propriedades


        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        public Int32 IntegracaoSAP
        {
            get { return _IntegracaoSAP; }
            set { _IntegracaoSAP = value; }
        }

        public String XML
        {
            get { return _XML; }
            set { _XML = value; }
        }

        public bool Integrado
        {
            get { return _Integrado; }
            set { _Integrado = value; }
        }

        public DateTime DataIntegracao
        {
            get { return _DataIntegracao; }
            set { _DataIntegracao = value; }
        }

        public DateTime DataRegistro
        {
            get { return _DataRegistro; }
            set { _DataRegistro = value; }
        }

        #endregion

        #region Métodos


        public MonitorIntegracaoGS Converte()
        {
            MonitorIntegracaoGS resposta = new MonitorIntegracaoGS();

            try
            {
                resposta.Codigo = this.Codigo;
                resposta.IntegracaoSAP = this.IntegracaoSAP;
                resposta.XML = this.XML;
                resposta.Integrado = this.Integrado;
                resposta.DataIntegracao = this._DataIntegracao;
                resposta.DataRegistro = this.DataRegistro;
            }
            catch (Exception erro)
            { }

            return resposta;
        }

        public static MonitorIntegracao Obtem(int pIdMonitorIntegracao)
        {
            bool Achou = false;
            MonitorIntegracaoGS item = new MonitorIntegracaoGS();

            foreach (MonitorIntegracaoGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " CD_MONITORINTEGRACAO = " + pIdMonitorIntegracao))
            {
                Achou = true;
                item = obj;
                break;
            }

            if (Achou)
                return item.Converte();
            else
                throw new Exception("[MONITORINTEGRACAO].[OBTEM] Registro de monitor integração não encontrado (" + pIdMonitorIntegracao + ")");
        }

        public static List<MonitorIntegracao> Obtem(string pIntegrado)
        {
            List<MonitorIntegracao> resposta = new List<MonitorIntegracao>();
            MonitorIntegracaoGS item = new MonitorIntegracaoGS();

            foreach (MonitorIntegracaoGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " BL_INTEGRADO = '" + pIntegrado + "'"))
            {
                resposta.Add(obj.Converte());
            }

            return resposta;
        }

        public bool InsereMonitorIntegracao(Integracao pIntegracao)
        {
            try
            {                
                //MonitorIntegracao
                MonitorIntegracaoGS monitorIntegracaoGS = new MonitorIntegracaoGS();                 
                monitorIntegracaoGS.IntegracaoSAP = _IntegracaoSAP;
                monitorIntegracaoGS.XML = _XML;
                monitorIntegracaoGS.Integrado = _Integrado;
                monitorIntegracaoGS.DataIntegracao = _DataIntegracao;
                monitorIntegracaoGS.DataRegistro = _DataRegistro;

                Int32 CodigoPessoa = monitorIntegracaoGS.InserirRegistroRetornaID();
                
                return true;
            }
            catch (Exception erro)
            {

                pIntegracao.Status = "ERRO";
                pIntegracao.Mensagem += "Falha ao processar o registro. " + erro.Message;
                pIntegracao.DataStatus = DateTime.Now;

                pIntegracao.Converte().AtualizarRegistro();

                return false;
            }
        }

        #endregion

    }
}
