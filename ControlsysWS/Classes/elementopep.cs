﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace br.com.globalsys.controlsys.webservice
{
    public class ElementoPep
    {
          #region Atributos

        //private int _Codigo;
        private string _Nome;
        private string _Numero;
        private string _Descricao;
        private string _Ativo;
        private string _CPF;
        private string _TX_ID_RESPONSAVEL_SAP;
        #endregion

        #region Propriedades

        //public Int32 Codigo
        //{
        //    get { return _Codigo; }
        //    set { _Codigo = value; }
        //}

        public String Nome
        {
            get { return _Nome; }
            set { _Nome = value; }
        }

        public String Numero
        {
            get { return _Numero; }
            set { _Numero = value; }
        }

        public String Descricao
        {
            get { return _Descricao; }
            set { _Descricao = value; }
        }

        public String Ativo
        {
            get { return _Ativo; }
            set { _Ativo = value; }
        }
        public String CPF
        {
            get { return _CPF; }
            set { _CPF = value; }
        }

        public String TX_ID_RESPONSAVEL_SAP
        {
            get { return _TX_ID_RESPONSAVEL_SAP; }
            set { _TX_ID_RESPONSAVEL_SAP = value; }
        }
        #endregion

        #region Métodos

        public Boolean ValidaElementoPep()
        {
            bool resposta = true;
            string Mensagem = "Campo(s) obrigatório(s) não informado(s): ";
            MasterClass.UltimoErro = new Exception();

            if (!MasterClass.ValidaString(this.Nome))
            {
                resposta = false;
                Mensagem += "Nome, ";
            }

            if (!MasterClass.ValidaString(this.Numero))
            {
                resposta = false;
                Mensagem += "Numero, ";
            }
            /* Chamado 9778 - CH13680 - solicitado permitir nulo.
            if (!MasterClass.ValidaString(this.Descricao))
            {
                resposta = false;
                Mensagem += "Descricao, ";
            }
            */

            if (!MasterClass.ValidaString(this.Ativo))
            {
                resposta = false;
                Mensagem += "Ativo, ";
            }

            //Valida se os dados informados estão dentre os valores esperados            
            if (resposta)
            {
                Mensagem = "Valor(es) inválido(s) para o(s) campo(s) ";

                if ((this.Ativo != "1") && (this.Ativo != "0"))
                {
                    resposta = false;
                    Mensagem += "Ativo, ";
                }

            }
           

            if (!resposta)
            {
                Mensagem = Mensagem.Remove(Mensagem.Length - 2);
                MasterClass.UltimoErro = new Exception(Mensagem);
            }

            return resposta;

        }

        private SetorCustoGS ExtraiSetorCusto()
        {
            SetorCustoGS resposta = new SetorCustoGS();
            try
            {
                resposta.Ativo = this.Ativo == "1";
                //resposta.Codigo = this.Codigo;
                //resposta.DataDesativacao
                resposta.DataRegistro = DateTime.Now;
                resposta.Descricao = this.Descricao;
                resposta.Nome = this.Nome;
                resposta.Numero = this.Numero;
                resposta.Tipo = "PEP";

                resposta.TX_ID_RESPONSAVEL_SAP = this.TX_ID_RESPONSAVEL_SAP;
                resposta.CD_PAPEL_RESPONSAVEL = !string.IsNullOrEmpty(this.CPF) ? SetorCustoGS.ObterPapel(this.CPF).ToString() : null;

                if (resposta.CD_PAPEL_RESPONSAVEL == "-1")
                    throw new Exception(string.Format("Não foi possível encontrar o papel com o ducumento informado. [DOCUMENTO={0}]", this.CPF));
            }
            catch (Exception erro)
            {
                throw new Exception("[ELEMENTOPEP].[EXTRAISETORCUSTO] Erro ao extrair os dados de Setor de Custo!" + erro.Message);
            }

            return resposta;
        }

        private SetorCustoGS ExtraiSetorCusto(SetorCustoGS pElementoPep)
        {
            SetorCustoGS resposta = pElementoPep;
            try
            {
                resposta.Ativo = this.Ativo == "1";
                resposta.DataRegistro = pElementoPep.DataRegistro;
                resposta.Descricao = this.Descricao;
                resposta.Nome = this.Nome;
                resposta.TX_ID_RESPONSAVEL_SAP = this.TX_ID_RESPONSAVEL_SAP;
                resposta.CD_PAPEL_RESPONSAVEL = !string.IsNullOrEmpty(this.CPF) ? SetorCustoGS.ObterPapel(this.CPF).ToString() : null;

                if (resposta.CD_PAPEL_RESPONSAVEL == "-1")
                    throw new Exception(string.Format("Não foi possível encontrar o papel com o ducumento informado. [DOCUMENTO={0}]", this.CPF));
            }
            catch (Exception erro)
            {
                throw new Exception("[CENTROCUSTO].[EXTRAISETORCUSTO] Erro ao extrair os dados de Setor de Custo!" + erro.Message);
            }

            return resposta;
        }

        public bool InsereElementoPep(Integracao pIntegracao)
        {
            try
            {
                

                pIntegracao.TipoIntegracao = "ElementoPep";
                pIntegracao.Status = "PROCESSANDO";
                pIntegracao.DataStatus = DateTime.Now;
                pIntegracao.Mensagem = String.Empty;

                if (SetorCustoGS.Existe(this.Numero))
                {
                    pIntegracao.DataStatus = DateTime.Now;
                    pIntegracao.Mensagem += "Registro duplicado para o número: " + this.Numero;                    
                    pIntegracao.Status = "ERRO";

                    pIntegracao.Converte().AtualizarRegistro();

                    return false;
                }

                pIntegracao.Converte().AtualizarRegistro();

                SetorCustoGS item = this.ExtraiSetorCusto();

                if (item.InserirRegistro() < 1)
                {
                    throw new Exception("[ELEMENTOPEP].[INSEREELEMENTOPEP] Registro não inserido." + MasterClass.UltimoErro.Message);
                }
                else
                {
                    pIntegracao.DataStatus = DateTime.Now;
                    pIntegracao.Mensagem += "Elemento Pep processado com sucesso. ";
                    pIntegracao.Status = "SUCESSO";
                }

                pIntegracao.Converte().AtualizarRegistro();

                return true;
            }
            catch (Exception erro)
            {

                pIntegracao.Status = "ERRO";
                pIntegracao.Mensagem += "Falha ao processar o registro. " + erro.Message;
                pIntegracao.DataStatus = DateTime.Now;

                pIntegracao.Converte().AtualizarRegistro();


                return false;
            }
        }

        public bool AtualizaElementoPep(Integracao pIntegracao)
        {
            try
            {


                pIntegracao.TipoIntegracao = "ElementoPep";
                pIntegracao.Status = "PROCESSANDO";
                pIntegracao.DataStatus = DateTime.Now;
                pIntegracao.Mensagem = String.Empty;

                pIntegracao.Converte().AtualizarRegistro();

                if (!SetorCustoGS.Existe(this.Numero))
                {
                    pIntegracao.DataStatus = DateTime.Now;
                    pIntegracao.Mensagem += "Registro inexistente para atualização: " + this.Numero;
                    pIntegracao.Status = "ERRO";

                    pIntegracao.Converte().AtualizarRegistro();

                    return false;
                }

                SetorCustoGS item = SetorCustoGS.Obtem(this.Numero);
                    item = this.ExtraiSetorCusto(item);

                if (item.AtualizarRegistro() < 1)
                {
                    throw new Exception("[ELEMENTOPEP].[ATUALIZAELEMENTOPEP] Registro não atualizado." + MasterClass.UltimoErro.Message);
                }
                else
                {
                    pIntegracao.DataStatus = DateTime.Now;
                    pIntegracao.Mensagem += "Elemento Pep processado com sucesso. ";
                    pIntegracao.Status = "SUCESSO";
                }

                pIntegracao.Converte().AtualizarRegistro();

                return true;
            }
            catch (Exception erro)
            {

                pIntegracao.Status = "ERRO";
                pIntegracao.Mensagem += "Falha ao processar o registro. " + erro.Message;
                pIntegracao.DataStatus = DateTime.Now;

                pIntegracao.Converte().AtualizarRegistro();


                return false;
            }
        }

        #endregion

    }
}
