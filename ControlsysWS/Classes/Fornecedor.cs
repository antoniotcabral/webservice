﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace br.com.globalsys.controlsys.webservice
{
    public class Fornecedor
    {

        string vCodigoPessoa = string.Empty;

        #region Atributos

        //private int _Codigo;
        private string _RazaoSocial;
        //private string _NomeFantasia;
        private string _Telefone;
        //private string _Email;
        private string _Cnpj;
        private string _InscricaoEstadual;
        private string _InscricaoMunicipal;
        private int _Ativo;
        private string _CodigoEmpresadoSAP;

        #endregion

        #region Propriedades

        //public Int32 Codigo
        //{
        //    get { return _Codigo; }
        //    set { _Codigo = value; }
        //}

        public String RazaoSocial
        {
            get { return _RazaoSocial; }
            set { _RazaoSocial = value; }
        }

        //public String NomeFantasia
        //{
        //    get { return _NomeFantasia; }
        //    set { _NomeFantasia = value; }
        //}

        public String Telefone
        {
            get { return _Telefone; }
            set { _Telefone = value; }
        }

        //public String Email
        //{
        //    get { return _Email; }
        //    set { _Email = value; }
        //}

        public String Cnpj
        {
            get { return _Cnpj; }
            set { _Cnpj = value; }
        }

        public String InscricaoEstadual
        {
            get { return _InscricaoEstadual; }
            set { _InscricaoEstadual = value; }
        }

        public String InscricaoMunicipal
        {
            get { return _InscricaoMunicipal; }
            set { _InscricaoMunicipal = value; }
        }

        public Int32 Ativo
        {
            get { return _Ativo; }
            set { _Ativo = value; }
        }

        public String CodigoEmpresadoSAP
        {
            get { return _CodigoEmpresadoSAP; }
            set { _CodigoEmpresadoSAP = value; }
        }
        #endregion


        #region Métodos

        public Boolean ValidaFornecedor()
        {
            bool resposta = true;
            string Mensagem = "Campo(s) obrigatório(s) não informado(s): ";
            MasterClass.UltimoErro = new Exception();

            if (!MasterClass.ValidaString(this.RazaoSocial))
            {
                resposta = false;
                Mensagem += "RazaoSocial, ";
            }

            if (this.Ativo <0 || this.Ativo >1)
            {
                resposta = false;
                Mensagem += "Ativo, ";
            }           

            if (!resposta)
            {
                Mensagem = Mensagem.Remove(Mensagem.Length - 2);
                MasterClass.UltimoErro = new Exception(Mensagem);
            }

            return resposta;

        }

        private string ExtraiNomeFantasia(String pNome)
        {
            string resposta = string.Empty;
            string[] Nomes = pNome.Split(' ');            
            resposta = Nomes[0];
            
            return resposta;
        }

        private EmpresaGS ExtraiEmpresa(Int32 ativo)
        {
            EmpresaGS resposta = new EmpresaGS();
            resposta.Ativo = Convert.ToBoolean(ativo);
            resposta.Cnpj = this.Cnpj;
            resposta.Codigo = vCodigoPessoa;
            //resposta.DataDesativacao
            resposta.InscricaoEstadual = this.InscricaoEstadual;
            resposta.InscricaoMunicipal = this.InscricaoMunicipal;
            resposta.CalcularFTE = true;
            resposta.CodigoEmpresadoSAP = !String.IsNullOrEmpty(this.CodigoEmpresadoSAP) ? Convert.ToInt32(this.CodigoEmpresadoSAP) : Int32.MinValue;

            return resposta;

        }

        private EmpresaGS ExtraiEmpresaInserir(Int32 ativo)
        {
            EmpresaGS resposta = new EmpresaGS();
            resposta.Ativo = Convert.ToBoolean(ativo);
            resposta.Cnpj = this.Cnpj;
            resposta.Codigo = vCodigoPessoa;
            //resposta.DataDesativacao
            resposta.InscricaoEstadual = this.InscricaoEstadual;
            resposta.InscricaoMunicipal = this.InscricaoMunicipal;
            resposta.CalcularFTE = true;
            resposta.ExibirRelFTE = true;
            resposta.BloqColabSemPedido = true;
            resposta.CodigoEmpresadoSAP = !String.IsNullOrEmpty(this.CodigoEmpresadoSAP)? Convert.ToInt32(this.CodigoEmpresadoSAP): Int32.MinValue;

            return resposta;

        }

        public PessoaGS ExtraiPessoaGS(int opcao = 0)
        {
            PessoaGS resposta = new PessoaGS();
            //// 0 "Zero" quando inserir;
            resposta.Apelido = opcao == 0 ? ExtraiNomeFantasia(this.RazaoSocial) : PessoaGS.ObtemNomeFantasia(vCodigoPessoa).Apelido;
            resposta.Codigo = vCodigoPessoa;
            resposta.DataRegistro = DateTime.Now;
            resposta.Nome = this.RazaoSocial;
            //resposta.Email = this.Email;
            //resposta.CodigoEndereco = Int32.MinValue;
            
            return resposta;

        }

        private FornecedorGS ExtraiFornecedorGS()
        {
            FornecedorGS resposta = new FornecedorGS();

            resposta.Ativo = this.Ativo==1;
            resposta.Cnpj = this.Cnpj;
            resposta.Contato = string.Empty;
            resposta.DataDesativacao = DateTime.MinValue;
            resposta.DataRegistro = DateTime.Now;           
            resposta.NomeFantasia = this.ExtraiNomeFantasia(this.RazaoSocial);
            resposta.RazaoSocial = this.RazaoSocial;
            try
            {                
                resposta.Telefone = (this.Telefone==null || this.Telefone ==string.Empty)?Int32.MinValue:Convert.ToInt64(this.Telefone);
            }
            catch(Exception erro)
            {
                throw new Exception("Número de Telefone informado inválido - Apenas números." + erro.Message);
            }


            return resposta;
        }

        public bool InsereFornecedor(Integracao pIntegracao)
        {
            try
            {
                pIntegracao.TipoIntegracao = "Fornecedor";
                pIntegracao.Status = "PROCESSANDO";
                pIntegracao.DataStatus = DateTime.Now;
                pIntegracao.Mensagem = String.Empty;

                pIntegracao.Converte().AtualizarRegistro();

                MasterClass item = new MasterClass();

                if (EmpresaGS.Existe(this.Cnpj))
                { throw new Exception("Falha ao inserir Fornecedor: Este Fornecedor já existe na base de dados(" + this.Cnpj + ")"); }

                //if(FornecedorGS.Existe(this.Cnpj))
                //{
                //    throw new Exception("Falha ao inserir Fornecedor: Este Fornecedor já existe na base de dados(" + this.Cnpj + ")");
                //}

                vCodigoPessoa = PessoaGS.ProximoCodigoPessoaJuridica();
                
                item = this.ExtraiPessoaGS();

                if (item.InserirRegistro() < 1)
                {
                    if (MasterClass.UltimoErro.Message.Contains("Violation of PRIMARY KEY"))
                    {
                        while (MasterClass.UltimoErro.Message.Contains("Violation of PRIMARY KEY"))
                        {
                            MasterClass.UltimoErro = new Exception();
                            ((PessoaGS)item).Codigo = PessoaGS.ProximoCodigoPessoaJuridica();
                            item.InserirRegistro();
                        }
                    }
                    else
                    {
                        throw new Exception("[FORNECEDOR].[INSEREFORNECEDOR] Registro não inserido. Falha ao extrair Pessoa de Fornecedor." + MasterClass.UltimoErro.Message);
                    }
                }


                item = this.ExtraiEmpresaInserir(1);

                if (item.InserirRegistro() < 1)
                {
                    throw new Exception("[FORNECEDOR].[INSEREFORNECEDOR] Registro não inserido. Falha ao extrair Empresa de Fornecedor." + MasterClass.UltimoErro.Message);
                }

                item = this.ExtraiFornecedorGS();

                if (item.InserirRegistro() < 1)
                {
                    throw new Exception("[FORNECEDOR].[INSEREFORNECEDOR] Registro não inserido. Falha ao extrair Fornecedor." + MasterClass.UltimoErro.Message);
                }              


                pIntegracao.DataStatus = DateTime.Now;
                pIntegracao.Mensagem += "Fornecedor processado com sucesso. ";
                pIntegracao.Status = "SUCESSO";


                pIntegracao.Converte().AtualizarRegistro();

                return true;
            }
            catch (Exception erro)
            {

                pIntegracao.Status = "ERRO";
                pIntegracao.Mensagem += "Falha ao processar o registro. " + erro.Message;
                pIntegracao.DataStatus = DateTime.Now;

                pIntegracao.Converte().AtualizarRegistro();


                return false;
            }
        }

        public bool AtualizaFornecedor(Integracao pIntegracao)
        {
            try
            {
                pIntegracao.TipoIntegracao = "Fornecedor";
                pIntegracao.Status = "PROCESSANDO";
                pIntegracao.DataStatus = DateTime.Now;
                pIntegracao.Mensagem = String.Empty;

                pIntegracao.Converte().AtualizarRegistro();

                MasterClass item = new MasterClass();

                ////if (!FornecedorGS.Existe(this.Cnpj))
                ////{
                ////    throw new Exception("Falha ao atualizar Fornecedor: Este Fornecedor não existe na base de dados(" + this.Cnpj + ")");
                ////}
                if (!EmpresaGS.Existe(this.Cnpj))
                { throw new Exception("Falha ao atualizar Fornecedor: Este Fornecedor não existe na base de dados(" + this.Cnpj + ")"); }

                item = EmpresaGS.Obtem(this.Cnpj);
                vCodigoPessoa = ((EmpresaGS)item).Codigo;
                ////Para atender o item 39 do chamado 6307, caso a empresa já esteja inativa no controlsys, não pode ser ativada pelo WS.
                Int32 ativoTemp = ((EmpresaGS)item).Ativo == false ? 0: _Ativo ;

                //item = this.ExtraiPessoaGS(1);
                //if (item.AtualizarRegistro() < 1)
                //{
                //    throw new Exception("[FORNECEDOR].[ATUALIZAFORNECEDOR] Registro não atualizado. Falha ao extrair Pessoa de Fornecedor." + MasterClass.UltimoErro.Message);
                //}

                //item = this.ExtraiEmpresa(ativoTemp);
                //if (item.AtualizarRegistro() < 1)
                //{
                //    throw new Exception("[FORNECEDOR].[ATUALIZAFORNECEDOR] Registro não atualizado. Falha ao extrair Empresa de Fornecedor." + MasterClass.UltimoErro.Message);
                //}

                item = this.ExtraiFornecedorGS();

                 ((FornecedorGS)item).Codigo =  FornecedorGS.Obtem(this.Cnpj).Codigo;

                if (item.AtualizarRegistro() < 1)
                {
                    throw new Exception("[FORNECEDOR].[ATUALIZAFORNECEDOR] Registro não atualizado. Falha ao extrair Detalhes Fornecedor." + MasterClass.UltimoErro.Message);
                }    

                
                {
                    pIntegracao.DataStatus = DateTime.Now;
                    pIntegracao.Mensagem += "Fornecedor processado com sucesso. ";
                    pIntegracao.Status = "SUCESSO";
                }

                pIntegracao.Converte().AtualizarRegistro();

                return true;
            }
            catch (Exception erro)
            {

                pIntegracao.Status = "ERRO";
                pIntegracao.Mensagem += "Falha ao processar o registro. " + erro.Message;
                pIntegracao.DataStatus = DateTime.Now;

                pIntegracao.Converte().AtualizarRegistro();


                return false;
            }
        }


        #endregion

    }
}
