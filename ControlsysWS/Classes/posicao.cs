﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace br.com.globalsys.controlsys.webservice
{
    public class Posicao
    {

        #region Atributos

        private int _Codigo;
        private string _Descricao;
        private string _DataInicio;
        private string _DataFim;

        #endregion

        #region Propriedades

        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        public String Descricao
        {
            get { return _Descricao; }
            set { _Descricao = value; }
        }

        public String DataInicio
        {
            get { return _DataInicio; }
            set { _DataInicio = value; }
         }

        public String DataFim
        {
            get { return _DataFim; }
            set { _DataFim = value; }
        }


        #endregion

        #region Métodos

        private CargoGS ExtraiCargoGS()
        {
            CargoGS resposta = new CargoGS();

            resposta.Ativo = true;
            //resposta.Codigo
            //resposta.DataDesativacao
            resposta.DataRegistro = DateTime.Now;
            resposta.Descricao = this.Descricao;
            resposta.Nome = this.Descricao;




            return resposta;
        }

        public bool InserePosicao(Integracao pIntegracao)
        {
            try
            {
                pIntegracao.TipoIntegracao = "Posicao";
                pIntegracao.Status = "PROCESSANDO";
                pIntegracao.DataStatus = DateTime.Now;
                pIntegracao.Mensagem = String.Empty;

                pIntegracao.Converte().AtualizarRegistro();

                MasterClass item = new MasterClass();

                item = this.ExtraiCargoGS();

                if (item.InserirRegistro() < 1)
                {
                    throw new Exception("[POSICAO].[INSEREPOSICAO] Registro não inserido." + MasterClass.UltimoErro.Message);
                }              
                else
                {
                    pIntegracao.DataStatus = DateTime.Now;
                    pIntegracao.Mensagem += "Posição processada com sucesso. ";
                    pIntegracao.Status = "SUCESSO";
                }

                pIntegracao.Converte().AtualizarRegistro();

                return true;
            }
            catch (Exception erro)
            {

                pIntegracao.Status = "ERRO";
                pIntegracao.Mensagem += "Falha ao processar o registro. " + erro.Message;
                pIntegracao.DataStatus = DateTime.Now;

                pIntegracao.Converte().AtualizarRegistro();


                return false;
            }
        }

        #endregion

    }
}
