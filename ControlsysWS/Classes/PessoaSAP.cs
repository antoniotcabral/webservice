﻿using br.com.globalsys.controlsys.webservice;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace ControlsysWS.Classes
{
    public class PessoaSAP
    {
        #region Atributos

        //private int _Codigo;
        private string _Nome; //ok2
        private string _Apelido; //ok2
        private string _Email;//ok2
                              //        private string _EnderecoComplemento;//ok2
        private string _CnpjEmpresa;//ok2
                                    //        private string _Mensalista;//ok2
                                    //        private string _Observacao;//ok
                                    //        private string _RegistroProfissional;//ok
                                    //        private string _EnderecoCep;//ok
                                    //        private string _EnderecoLogradouro; //ok
                                    //        private string _EnderecoNumero;//ok
                                    //        private string _EnderecoBairro;//ok
                                    //        private string _EnderecoCidade;//ok
                                    //        private string _EnderecoEstado;//ok
                                    //        private string _EnderecoPais;//ok
        private string _Cpf;//ok
                            //        private string _NaturalidadeCidade;//ok
        private string _Passaporte;//ok
                                   //        private string _RgNumero;//ok
                                   //        private string _RgOrgaoEmissor;//ok
                                   //        private string _RgEstadoEmissor;//ok
                                   //        private string _NomeMae;//ok
                                   //        private string _NomePai;//ok
                                   //        private string _TipoSanguineo;//ok
        private string _Sexo;
        //        private string _Rne;//ok
        //        private string _EstadoCivil;//ok
        //        private string _Escolaridade;//ok
        //        private string _NumeroCrNr;//ok
        //        private string _TipoCrNr;//ok
        private string _CnhNumero;//ok
        private string _CnhTipo;//ok                
                                //        private string _CtpsSerie;//ok
                                //        private string _CtpsEstado;//ok
                                //        private string _NauralidadePais;//ok
                                //        private string _ContatoTelefone01;//ok
                                //        private string _ContatoTelefone02;//ok
                                //        private string _ContatoTelefone03;//ok
        private string _StatusOcupacao;//ok
                                       //        private string _Municipio; 
                                       //        private string _Nacionalidade;


        private string _DataAdmissao;
        //        private string _RgEmissao;//ok
        //        private string _VistoEmissao;//ok
        //        private string _VistoValidade;//ok
        //        private string _ExpedicaoRne;//ok
        private string _CnhValidade;//ok
                                    //        private string _CtpsEmissao;//ok


        //        private string _Salario;//ok

        private string _Pis;//ok

        //        private string _TituloEleitorNumero;//ok
        //        private string _TituloEleitorZona;//ok
        //        private string _TituloEleitorSecao;//ok
        //        private string _CtpsNumero;//ok

        //private string _NaturalidadeUf;//ok
        //        private string _DataNascimento;//ok
        private string _NumeroUnidadeOrganizacional;
        private string _NomeUnidadeOrganizacional;
        private string _CodigoPosicao;
        private string _NomePosicao;
        private string _CargoContratista;
        private string _NomeCargo;
        private string _CentroCustoCodigo;
        //        private string _PhtCodigo;
        //        private string _PhtValidadeInicial;
        //        private string _PhtValidadeFinal;
        //        --//private string _FeriasValidadeInicial;
        //        --//private string _FeriasValidadeFinal;
        //        private string _TreinamentoCodigoSap;
        //        private string _TreinamentoValidade;

        private string _GrupoPessoa;

        private string _IdMotivo;
        private string _DescMotivo;
        private string _Processado;
        private string _TipoAcesso;

        private string _CodigoFornecedor;
        private string _NomeFornecedor;

        //private string _DescricaoCentroCusto;
        //private string _TipoCentroCusto;

        private string _CodigoSap;
        private string _CodigoGestorPonto;
        private string _CodigoSuperiorImediato;
        private string _SubContratista;
        private string _CnpjSubContratrista;

        #endregion

        #region Propriedades

        //public Int32 Codigo
        //{
        //    get { return _Codigo; }
        //    set { _Codigo = value; }
        //}

        public String Nome
        {
            get { return _Nome; }
            set { _Nome = value; }
        }

        public String Apelido
        {
            get { return _Apelido; }
            set { _Apelido = value; }
        }

        public String Email
        {
            get { return _Email; }
            set { _Email = value; }
        }
        //public String EnderecoComplemento
        //{
        //    get { return _EnderecoComplemento; }
        //    set { _EnderecoComplemento = value; }
        //}

        public String CnpjEmpresa
        {
            get { return _CnpjEmpresa; }
            set { _CnpjEmpresa = value; }
        }

        public String SubContratista
        {
            get { return _SubContratista; }
            set { _SubContratista = value; }
        }

        public String CnpjSubContratrista
        {
            get { return _CnpjSubContratrista; }
            set { _CnpjSubContratrista = value; }
        }

        //public String Mensalista
        //{
        //    get { return _Mensalista; }
        //    set { _Mensalista = value; }
        //}

        //public String Observacao
        //{
        //    get { return _Observacao; }
        //    set { _Observacao = value; }
        //}

        //public String RegistroProfissional
        //{
        //    get { return _RegistroProfissional; }
        //    set { _RegistroProfissional = value; }
        //}

        //public String Cep
        //{
        //    get { return _EnderecoCep; }
        //    set { _EnderecoCep = value; }
        //}

        //public String EnderecoLogradouro
        //{
        //    get { return _EnderecoLogradouro.Trim(); }
        //    set { _EnderecoLogradouro = value; }
        //}

        //public String EnderecoNumero
        //{
        //    get { return _EnderecoNumero; }
        //    set { _EnderecoNumero = value; }
        //}

        //public String EnderecoBairro
        //{
        //    get { return _EnderecoBairro; }
        //    set { _EnderecoBairro = value; }
        //}

        //public String EnderecoCidade
        //{
        //    get { return _EnderecoCidade; }
        //    set { _EnderecoCidade = value; }
        //}

        //public String EnderecoEstado
        //{
        //    get { return _EnderecoEstado; }
        //    set { _EnderecoEstado = value; }
        //}

        //public String EnderecoPais
        //{
        //    get { return _EnderecoPais; }
        //    set { _EnderecoPais = value; }
        //}

        public String Cpf
        {
            get { return _Cpf; }
            set { _Cpf = value; }
        }

        //public String CidadeNaturalidade
        //{
        //    get { return _NaturalidadeCidade; }
        //    set { _NaturalidadeCidade = value; }
        //}

        public String PassaporteNumero
        {
            get { return _Passaporte; }
            set { _Passaporte = value; }
        }

        //public String RgNumero
        //{
        //    get { return _RgNumero; }
        //    set { _RgNumero = value; }
        //}

        //public String RgOrgaoEmissor
        //{
        //    get { return _RgOrgaoEmissor; }
        //    set { _RgOrgaoEmissor = value; }
        //}

        //public String RgEstadoEmissor
        //{
        //    get { return _RgEstadoEmissor; }
        //    set { _RgEstadoEmissor = value; }
        //}


        //public String NomeMae
        //{
        //    get { return _NomeMae; }
        //    set { _NomeMae = value; }
        //}

        //public String NomePai
        //{
        //    get { return _NomePai; }
        //    set { _NomePai =  value; }
        //}

        //public String TipoSanguineo
        //{
        //    get { return _TipoSanguineo; }
        //    set { _TipoSanguineo =  value; }
        //}

        public String Sexo
        {
            get { return _Sexo; }
            set { _Sexo = value; }
        }

        //public String Rne
        //{
        //    get { return _Rne; }
        //    set { _Rne = value; }
        //}

        //public String EstadoCivil
        //{
        //    get { return _EstadoCivil; }
        //    set { _EstadoCivil = value; }
        //}

        //public String Escolaridade
        //{
        //    get { return _Escolaridade; }
        //    set { _Escolaridade = value; }
        //}

        //public String NumeroCrNr
        //{
        //    get { return _NumeroCrNr; }
        //    set { _NumeroCrNr = value; }
        //}

        //public String TipoCrNr
        //{
        //    get { return _TipoCrNr; }
        //    set { _TipoCrNr = value; }
        //}

        public String CnhNumero
        {
            get { return _CnhNumero; }
            set { _CnhNumero = value; }
        }

        public String CnhTipo
        {
            get { return _CnhTipo; }
            set { _CnhTipo = value; }
        }

        //public String TituloEleitorNumero
        //{
        //    get { return _TituloEleitorNumero; }
        //    set { _TituloEleitorNumero = value; }
        //}

        //public String TituloEleitorZona
        //{
        //    get { return _TituloEleitorZona; }
        //    set { _TituloEleitorZona = value; }
        //}

        //public String TituloEleitorSecao
        //{
        //    get { return _TituloEleitorSecao; }
        //    set { _TituloEleitorSecao = value; }
        //}

        //public String CtpsNumero
        //{
        //    get { return _CtpsNumero; }
        //    set { _CtpsNumero = value; }
        //}

        //public String CtpsSerie
        //{
        //    get { return _CtpsSerie; }
        //    set { _CtpsSerie = value; }
        //}

        //public String CtpsEstado
        //{
        //    get { return _CtpsEstado; }
        //    set { _CtpsEstado = value; }
        //}

        //public String NauralidadePais
        //{
        //    get { return _NauralidadePais; }
        //    set { _NauralidadePais = value; }
        //}

        //public String ContatoTelefone01
        //{
        //    get { return _ContatoTelefone01; }
        //    set { _ContatoTelefone01 = value; }
        //}

        //public String ContatoTelefone02
        //{
        //    get { return _ContatoTelefone02; }
        //    set { _ContatoTelefone02 = value; }
        //}

        //public String ContatoTelefone03
        //{
        //    get { return _ContatoTelefone03; }
        //    set { _ContatoTelefone03 = value; }
        //}

        public String StatusOcupacao
        {
            get { return _StatusOcupacao; }
            set { _StatusOcupacao = value; }
        }


        public String DataAdmissao
        {
            get { return _DataAdmissao; }
            set { _DataAdmissao = value; }
        }

        //public String RgEmissao
        //{
        //    get { return _RgEmissao; }
        //    set { _RgEmissao = value; }
        //}

        //public String VistoEmissao
        //{
        //    get { return _VistoEmissao; }
        //    set { _VistoEmissao = value; }
        //}

        //public String VistoValidade
        //{
        //    get { return _VistoValidade; }
        //    set { _VistoValidade = value; }
        //}

        //public String ExpedicaoRne
        //{
        //    get { return _ExpedicaoRne; }
        //    set { _ExpedicaoRne = value; }
        //}

        public String CnhValidade
        {
            get { return _CnhValidade; }
            set { _CnhValidade = value; }
        }

        //public String CtpsEmissao
        //{
        //    get { return _CtpsEmissao; }
        //    set { _CtpsEmissao = value; }
        //}

        //public String Salario
        //{
        //    get { return _Salario; }
        //    set { _Salario = value; }
        //}

        public String Pis
        {
            get { return _Pis; }
            set { _Pis = value; }
        }

        //public String NascimentoUf
        //{
        //    get { return _NaturalidadeUf; }
        //    set { _NaturalidadeUf = value; }
        //}

        //public String DataNascimento
        //{
        //    get { return _DataNascimento; }
        //    set { _DataNascimento = value; }
        //}

        public String NumeroUnidadeOrganizacional
        {
            get { return _NumeroUnidadeOrganizacional; }
            set { _NumeroUnidadeOrganizacional = value; }
        }

        public String NomeUnidadeOrganizacional
        {
            get { return _NomeUnidadeOrganizacional; }
            set { _NomeUnidadeOrganizacional = value; }
        }

        public String CodigoPosicao
        {
            get { return _CodigoPosicao; }
            set { _CodigoPosicao = value; }
        }

        public String NomePosicao
        {
            get { return _NomePosicao; }
            set { _NomePosicao = value; }
        }

        public String CargoContratista
        {
            get { return _CargoContratista; }
            set { _CargoContratista = value; }
        }

        public String NomeCargo
        {
            get { return _NomeCargo; }
            set { _NomeCargo = value; }
        }

        public String CentroCustoCodigo
        {
            get { return _CentroCustoCodigo == null ? null : _CentroCustoCodigo.Trim(); }
            set { _CentroCustoCodigo = value; }
        }

        //public String PhtCodigo
        //{
        //    get { return _PhtCodigo; }
        //    set { _PhtCodigo = value; }
        //}

        //public string PhtValidadeInicial
        //{
        //    get { return _PhtValidadeInicial; }
        //    set { _PhtValidadeInicial = value; }
        //}

        //public string PhtValidadeFinal
        //{
        //    get { return _PhtValidadeFinal; }
        //    set { _PhtValidadeFinal = value; }
        //}

        public string GrupoPessoa
        {
            get { return _GrupoPessoa; }
            set { _GrupoPessoa = value; }
        }

        public string IdMotivo
        {
            get { return _IdMotivo; }
            set { _IdMotivo = value; }
        }

        public string DescMotivo
        {
            get { return _DescMotivo; }
            set { _DescMotivo = value; }
        }

        public string Processado
        {
            get { return _Processado; }
            set { _Processado = value; }
        }

        public string TipoAcesso
        {
            get { return _TipoAcesso; }
            set { _TipoAcesso = value; }
        }

        public string CodigoFornecedor
        {
            get { return _CodigoFornecedor; }
            set { _CodigoFornecedor = value; }
        }

        public string NomeFornecedor
        {
            get { return _NomeFornecedor; }
            set { _NomeFornecedor = value; }
        }

        public String CodigoSap
        {
            get { return _CodigoSap; }
            set { _CodigoSap = value; }
        }

        public String CodigoGestorPonto
        {
            get { return _CodigoGestorPonto; }
            set { _CodigoGestorPonto = value; }
        }

        public String CodigoSuperiorImediato
        {
            get { return _CodigoSuperiorImediato; }
            set { _CodigoSuperiorImediato = value; }
        }

        //private string FeriasValidadeInicial
        //{
        //    get { return _FeriasValidadeInicial; }
        //    set { _FeriasValidadeInicial = value; }
        //}

        //private string FeriasValidadeFinal
        //{
        //    get { return _FeriasValidadeFinal; }
        //    set { _FeriasValidadeFinal = value; }
        //}

        //public String TreinamentoCodigoSap
        //{
        //    get { return _TreinamentoCodigoSap; }
        //    set { _TreinamentoCodigoSap = value; }
        //}

        //public string TreinamentoValidade
        //{
        //    get { return _TreinamentoValidade; }
        //    set { _TreinamentoValidade = value; }
        //}

        //public String DescricaoCentroCusto
        //{
        //    get { return _DescricaoCentroCusto; }
        //    set { _DescricaoCentroCusto = value; }
        //}

        //public String TipoCentroCusto
        //{
        //    get { return _TipoCentroCusto; }
        //    set { _TipoCentroCusto = value; }
        //}




        #endregion

        #region Constantes

        //private const string TipoTelefone1 = "Residencial";
        //private const string TipoTelefone2 = "Emergencial";
        //private const string TipoTelefone3 = "Celular";

        #endregion

        #region Métodos

        public Boolean ValidaColaborador()
        {
            bool resposta = true;
            string Mensagem = "Campo(s) obrigatório(s) não informado(s): ";
            MasterClass.UltimoErro = new Exception();

            if (!((MasterClass.ValidaString(this.Cpf)) || (MasterClass.ValidaString(this.PassaporteNumero))))
            {
                resposta = false;
                Mensagem += "Cpf ou Passaporte, ";
            }

            this.Cpf = this.Cpf != null ? this.Cpf.Replace(".", "").Replace("/", "").Replace("-", "") : null;

            if (!MasterClass.ValidaString(this.Nome))
            {
                resposta = false;
                Mensagem += "Nome, ";
            }

            if (!MasterClass.ValidaString(this.Apelido))
            {
                resposta = false;
                Mensagem += "Apelido, ";
            }

            if (!MasterClass.ValidaString(this.StatusOcupacao))
            {
                resposta = false;
                Mensagem += "StatusOcupacao, ";
            }

            if (!MasterClass.ValidaString(this.CodigoSap))
            {
                resposta = false;
                Mensagem += "CodigoSap, ";
            }

            if (!MasterClass.ValidaString(this.IdMotivo))
            {
                resposta = false;
                Mensagem += "IdMotivo, ";
            }

            if (!MasterClass.ValidaString(this.DescMotivo))
            {
                resposta = false;
                Mensagem += "DescMotivo, ";
            }

            if (!MasterClass.ValidaString(this.Processado))
            {
                resposta = false;
                Mensagem += "Processado, ";
            }

            if (!MasterClass.ValidaString(this.TipoAcesso))
            {
                resposta = false;
                Mensagem += "TipoAcesso , ";
            }

            // Valida a quantidade de caracteres para os campos

            if (this.Cpf != null && (!(this.Cpf.Length == 0 || this.Cpf.Length == 11)))
            {
                resposta = false;
                Mensagem += "O números de caracteres do Cpf é diferente de 11, ";
            }
            if (this.Pis != null && this.Pis.Length > 12)
            {
                resposta = false;
                Mensagem += "O números de caracteres do PIS não pode ser maior que 12, ";
            }
            if (this.CnhNumero != null && this.CnhNumero.Length > 13)
            {
                resposta = false;
                Mensagem += "O números de caracteres do CNH não pode ser maior que 13, ";
            }

            //Valida se os dados informados estão dentre os valores esperados            
            if (resposta)
            {
                Mensagem = "Valor(es) inválido(s) para o(s) campo(s): ";

                if (MasterClass.ValidaString(this.CnhTipo))
                {
                    HashSet<String> ValoresTipoSanguineo = new HashSet<string>();
                    ValoresTipoSanguineo.Add("A");
                    ValoresTipoSanguineo.Add("B");
                    ValoresTipoSanguineo.Add("C");
                    ValoresTipoSanguineo.Add("D");
                    ValoresTipoSanguineo.Add("E");
                    ValoresTipoSanguineo.Add("AB");
                    ValoresTipoSanguineo.Add("AC");
                    ValoresTipoSanguineo.Add("AD");
                    ValoresTipoSanguineo.Add("AE");

                    if (!ValoresTipoSanguineo.Contains(this.CnhTipo))
                    {
                        resposta = false;
                        Mensagem += "CnhTipo, ";
                    }
                }

                if (MasterClass.ValidaString(this.StatusOcupacao))
                {
                    HashSet<String> ValoresStatus = new HashSet<string>();
                    ValoresStatus.Add("0");
                    ValoresStatus.Add("1");

                    if (!ValoresStatus.Contains(this.StatusOcupacao))
                    {
                        resposta = false;
                        Mensagem += "StatusOcupacao, ";
                    }

                }

                if (MasterClass.ValidaString(this.GrupoPessoa))
                {
                    HashSet<String> ValoresStatus = new HashSet<string>();
                    ValoresStatus.Add("1");
                    ValoresStatus.Add("2");

                    if (!ValoresStatus.Contains(this.GrupoPessoa))
                    {
                        resposta = false;
                        Mensagem += "GrupoPessoa, ";
                    }

                }


                if (!MasterClass.ValidaDataNaoObrigatoria(this.Processado))
                {
                    resposta = false;
                    Mensagem += "Processado, ";
                }

                if (MasterClass.ValidaString(this.TipoAcesso))
                {
                    HashSet<String> ValoresStatus = new HashSet<string>();
                    ValoresStatus.Add("P");
                    ValoresStatus.Add("V");
                    ValoresStatus.Add("S");

                    if (!ValoresStatus.Contains(this.TipoAcesso))
                    {
                        resposta = false;
                        Mensagem += "Processado, ";
                    }

                }

            }

            if (resposta)
            {
                if (MasterClass.ValidaString(this.CentroCustoCodigo))
                {
                    try
                    {
                        SetorCustoGS.Obtem(this.CentroCustoCodigo);
                    }
                    catch (Exception erro)
                    {
                        resposta = false;
                        Mensagem = "ERRO: Centro de Custo não encontrado.  ";
                    }
                }
            }

            if (resposta)
            {
                if (MasterClass.ValidaString(this.CodigoFornecedor))
                {
                    try
                    {
                        EmpresaGS.ObtemPorCodigoEmpresadoSAP(this.CodigoFornecedor);
                    }
                    catch (Exception erro)
                    {
                        resposta = false;
                        Mensagem = "ERRO: Fornecedor não encontrado. Código: " + this.CodigoFornecedor + " - " + this.NomeFornecedor;
                    }
                }
                else
                {
                    try
                    {
                        if (this.GrupoPessoa == "1")
                        {
                            EmpresaGS.Obtem(ConfigurationManager.AppSettings["CnpjParameto"]);
                        }
                        else
                        {
                            throw new Exception("[FORNECEDOR].[OBTEM] Registro de empresa não encontrado (" + CodigoFornecedor + ")");
                        }
                    }
                    catch (Exception erro)
                    {
                        resposta = false;
                        Mensagem = "ERRO: Fornecedor não encontrado. Código: " + this.CodigoFornecedor + " - " + this.NomeFornecedor;
                    }
                }
            }

            if (!resposta)
            {
                Mensagem = Mensagem.Remove(Mensagem.Length - 2);
                MasterClass.UltimoErro = new Exception(Mensagem);
            }

            return resposta;

        }

        private PessoaFisicaGS ExtraiPessoaFisicaGS()
        {
            PessoaFisicaGS resposta = new PessoaFisicaGS();
            PessoaFisicaGS pessoa = new PessoaFisicaGS();

            resposta = pessoa;


            if (!String.IsNullOrEmpty(this.Cpf))
            {
                pessoa = PessoaFisicaGS.Obtem(this.Cpf);
            }
            else
            {
                pessoa = PessoaFisicaGS.ObtemPassporte(this.PassaporteNumero);
            }

            if ((!String.IsNullOrEmpty(pessoa.Cpf)) && (pessoa.Cpf != this.Cpf))
            {
                throw new Exception("[PessoaSAP].[ATUALIZACOLABORADOR-1] Registro não inserido. O CPF atual: " + pessoa.Cpf + " não pode ser alterado para: " + this.Cpf + ".");
            }

            //Obrigatórios
            resposta.Cpf = this.Cpf != null ? this.Cpf : pessoa.Cpf;

            resposta.Cnh = this.CnhNumero != null ? this.CnhNumero : pessoa.Cnh;
            resposta.CnhTipo = this.CnhTipo != null ? this.CnhTipo : pessoa.CnhTipo;
            resposta.CnhValidade = this.CnhValidade != null ? MasterClass.StringParaData(this.CnhValidade) : pessoa.CnhValidade;
            resposta.Codigo = pessoa.Codigo;
            resposta.Passaporte = this.PassaporteNumero != null ? this.PassaporteNumero : pessoa.Passaporte;
            resposta.Sexo = this.Sexo != null ? this.Sexo : pessoa.Sexo;

            return resposta;
        }


        private PessoaFisicaGS ExtraiPessoaFisicaGS_Inserir(PessoaGS pPessoa)
        {
            PessoaFisicaGS resposta = new PessoaFisicaGS();

            //Obrigatórios
            resposta.Cpf = this.Cpf;

            resposta.Cnh = this.CnhNumero;
            resposta.CnhTipo = this.CnhTipo;
            resposta.CnhValidade = MasterClass.StringParaData(this.CnhValidade);
            resposta.Codigo = pPessoa.Codigo;

            resposta.Passaporte = this.PassaporteNumero;
            resposta.Sexo = this.Sexo;

            return resposta;
        }

        private EmpregadoSapGS ExtraiEmpregadoSap()
        {
            EmpregadoSapGS resposta = new EmpregadoSapGS();

            resposta.Codigo = this.CodigoSap == null ? Int32.MinValue : Convert.ToInt32(this.CodigoSap);
            resposta.CodigoGestorPonto = this.CodigoGestorPonto == null || this.CodigoGestorPonto == String.Empty ? Int32.MinValue : Convert.ToInt32(this.CodigoGestorPonto);
            resposta.CodigoSuperiorImediato = this.CodigoSuperiorImediato == null || this.CodigoSuperiorImediato == String.Empty ? Int32.MinValue : Convert.ToInt32(this.CodigoSuperiorImediato);



            if (resposta.Codigo != Int32.MinValue)
            {

                #region Garante a integridade referencial

                EmpregadoSapGS novo;

                if ((!EmpregadoSapGS.Existe(resposta.CodigoGestorPonto)) && (resposta.CodigoGestorPonto != Int32.MinValue))
                {
                    novo = new EmpregadoSapGS();
                    novo.Codigo = resposta.CodigoGestorPonto;
                    novo.CodigoGestorPonto = Int32.MinValue;
                    novo.CodigoSuperiorImediato = Int32.MinValue;
                    if (novo.InserirRegistro() == -1)
                    {
                        throw new Exception("Falha ao gravar dados de Empregado SAP. " + MasterClass.UltimoErro.Message);
                    }
                }

                if ((!EmpregadoSapGS.Existe(resposta.CodigoSuperiorImediato)) && (resposta.CodigoSuperiorImediato != Int32.MinValue))
                {
                    novo = new EmpregadoSapGS();
                    novo.Codigo = resposta.CodigoSuperiorImediato;
                    novo.CodigoGestorPonto = Int32.MinValue;
                    novo.CodigoSuperiorImediato = Int32.MinValue;
                    if (novo.InserirRegistro() == -1)
                    {
                        throw new Exception("Falha ao gravar dados de Empregado SAP. " + MasterClass.UltimoErro.Message);
                    }
                }

                #endregion


                if (EmpregadoSapGS.Existe(resposta.Codigo))
                {
                    resposta = EmpregadoSapGS.Obtem(resposta.Codigo);
                    resposta.CodigoGestorPonto = this.CodigoGestorPonto == null || this.CodigoGestorPonto == String.Empty ? Int32.MinValue : Convert.ToInt32(this.CodigoGestorPonto);
                    resposta.CodigoSuperiorImediato = this.CodigoSuperiorImediato == null || this.CodigoSuperiorImediato == String.Empty ? Int32.MinValue : Convert.ToInt32(this.CodigoSuperiorImediato);

                    //resposta.AtualizarRegistro();
                    if (resposta.AtualizarRegistro() == -1)
                    {
                        throw new Exception("Falha ao gravar dados de Empregado SAP. " + MasterClass.UltimoErro.Message);
                    }


                }
                else
                {
                    if (resposta.InserirRegistro() == -1)
                    {
                        throw new Exception("Falha ao gravar dados de Empregado SAP. " + MasterClass.UltimoErro.Message);
                    }

                }
            }
            else
            {
                resposta = new EmpregadoSapGS();
            }

            return resposta;
        }

        private bool ProcessaCargoGS()
        {
            CargoGS cargo = new CargoGS();

            try
            {
                // Se CodigoPosicao estiver nulo, informa um valor de cargo default se existir no banco de dados o CD_CARGO_SAP = 999999999.
                if (String.IsNullOrEmpty(this.CodigoPosicao) && String.IsNullOrEmpty(this.CargoContratista))
                {
                    cargo = CargoGS.Obtem(Convert.ToInt32(999999999));
                    this.CodigoPosicao = cargo != null ? Convert.ToString(cargo.CodigoSap) : null;
                    this.NomePosicao = cargo != null ? cargo.Nome : null;
                }

                //Trata Telefone 1
                if (this.CodigoPosicao != null && this.CodigoPosicao != String.Empty) //Codigo Posição informado
                {
                    cargo = CargoGS.Obtem(Convert.ToInt32(this.CodigoPosicao));

                    if (cargo == null)//Não existe cargo cadastrado com esse código
                    {
                        cargo = new CargoGS();
                        cargo.Ativo = true;
                        cargo.Nome = this.NomePosicao;
                        cargo.CodigoSap = this.CodigoPosicao == String.Empty ? Int32.MinValue : Convert.ToInt32(this.CodigoPosicao);
                        cargo.Descricao = String.Empty;
                        cargo.DataDesativacao = DateTime.MinValue;
                        cargo.DataRegistro = DateTime.Now;
                        //cargo.Descricao = this.NomePosicao;

                        if (cargo.InserirRegistro() == -1)
                        {
                            throw new Exception("Falha ao inserir novo cargo (" + this.CodigoPosicao + "): " + MasterClass.UltimoErro.Message);

                        }
                    }
                }

                else if (this.CargoContratista != null && this.CargoContratista != String.Empty)
                {
                    cargo = CargoGS.Obtem(Convert.ToInt32(this.CargoContratista));

                    if (cargo == null)//Não existe cargo cadastrado com esse código
                    {
                        cargo = new CargoGS();
                        cargo.Ativo = true;
                        cargo.Nome = this.NomeCargo;
                        cargo.CodigoSap = this.CargoContratista == String.Empty ? Int32.MinValue : Convert.ToInt32(this.CargoContratista);
                        cargo.Descricao = String.Empty;
                        cargo.DataDesativacao = DateTime.MinValue;
                        cargo.DataRegistro = DateTime.Now;
                        //cargo.Descricao = this.NomePosicao;

                        if (cargo.InserirRegistro() == -1)
                        {
                            throw new Exception("Falha ao inserir novo cargo (" + this.CargoContratista + "): " + MasterClass.UltimoErro.Message);

                        }
                    }
                }

                return true;
            }
            catch (Exception erro)
            {
                MasterClass.UltimoErro = erro;
                return false;
            }

        }

        public static bool InativaRegistrosGrupoTrabColab(Int32 pCodigoPapel)
        {
            try
            {
                Boolean resposta = true;
                ColaboradorGS col = new ColaboradorGS();

                String cmd = "UPDATE GRUPO_TRAB_COLAB SET BL_ATIVO = 0, DT_DESATIVACAO = GETDATE() WHERE BL_ATIVO = 1 AND CD_PAPEL = " + pCodigoPapel;

                resposta = col.ExecutaComando(cmd);
                if (!resposta)
                {
                    MasterClass.UltimoErro = new Exception("Falha ao inativar registros de Grupo de Trabalho Colaborador");
                }


                return resposta;
            }
            catch (Exception erro)
            {
                MasterClass.UltimoErro = erro;
                return false;
            }
        }


        private PessoaGS ExtraiPessoaGS(String pCodigoPessoaFisica)
        {
            PessoaGS resposta = new PessoaGS();
            PessoaGS pessoa = PessoaGS.Obtem(pCodigoPessoaFisica);

            resposta = pessoa;

            EmpresaGS empresa = EmpresaGS.ObtemPorCodigoEmpresadoSAP(this.CodigoFornecedor);

            resposta.Nome = this.Nome != null ? this.Nome : pessoa.Nome;

            resposta.Apelido = (this.Apelido == String.Empty) || (this.Apelido == null) ? ExtraiApelido(this.Nome) : this.Apelido;

            resposta.Codigo = pessoa.Codigo;

            resposta.DataRegistro = pessoa.DataRegistro;
            resposta.Email = this.Email != null ? this.Email : pessoa.Email;

            return resposta;

        }

        private PessoaGS ExtraiPessoa_InserirGS()
        {
            // -- Início - Valida para não deixar duplicar cpf ou passaporte.
            String CpfExiste = PessoaFisicaGS.Obtem(this.Cpf)?.Cpf;
            if ((!String.IsNullOrEmpty(this.Cpf)) && (!String.IsNullOrEmpty(CpfExiste)))
            {
                throw new Exception("[PessoaSAP].[ATUALIZACOLABORADOR-2] Registro não inserido. O CPF: " + CpfExiste + " ja existe na base de dados. ");
            }
            String PassaportExiste = PessoaFisicaGS.ObtemPassporte(this.PassaporteNumero)?.Passaporte;
            if ((!String.IsNullOrEmpty(this.PassaporteNumero)) && (!String.IsNullOrEmpty(PassaportExiste)))
            {
                throw new Exception("[PessoaSAP].[ATUALIZACOLABORADOR-3] Registro não inserido. O Passaporte : " + PassaportExiste + " ja existe na base de dados. ");
            }
            // -- Fim - Valida para não deixar duplicar cpf ou passaporte.

            PessoaGS resposta = new PessoaGS();

            EmpresaGS empresa = EmpresaGS.ObtemPorCodigoEmpresadoSAP(this.CodigoFornecedor);

            resposta.Apelido = (this.Apelido == String.Empty) || (this.Apelido == null) ? ExtraiApelido(this.Nome) : this.Apelido;
            resposta.Codigo = PessoaGS.ProximoCodigoPessoaFisica();

            resposta.DataRegistro = DateTime.Now;
            resposta.Email = this.Email;
            resposta.Nome = this.Nome;

            return resposta;

        }

        private PapelGS ExtraiPapelGS(string pCodigoPessoaFisica)
        {
            PapelGS resposta = new PapelGS();
            PapelGS papel = PapelGS.ObtemCpfPassaporte(this.Cpf, this.PassaporteNumero);

            resposta = papel;

            resposta.Codigo = papel.Codigo;
            resposta.CodigoCargo = papel.CodigoCargo == 0 ? Int32.MinValue : papel.CodigoCargo;

            this.CodigoPosicao = this.CodigoPosicao == null ? string.Empty : this.CodigoPosicao;
            this.CargoContratista = this.CargoContratista == null ? string.Empty : this.CargoContratista;

            if (this.CodigoPosicao != string.Empty)
            {
                CargoGS cargo = CargoGS.Obtem(Convert.ToInt32(this.CodigoPosicao));
                if (cargo == null)
                {
                    throw new Exception("Cargo inexistente com o código alt: " + this.CodigoPosicao);
                }
                else
                {
                    resposta.CodigoCargo = cargo.Codigo;
                }
            }

            else if (this.CargoContratista != string.Empty)
            {
                CargoGS cargo = CargoGS.Obtem(Convert.ToInt32(this.CargoContratista));
                if (cargo == null)
                {
                    throw new Exception("Cargo inexistente com o código alt: " + this.CargoContratista);
                }
                else
                {
                    resposta.CodigoCargo = cargo.Codigo;
                }
            }

            resposta.CodigoPessoaFisica = pCodigoPessoaFisica;
            resposta.DataRegistro = papel.DataRegistro;
            resposta.Supervisionado = papel.Supervisionado;
            resposta.Supervisor = papel.Supervisor;
            resposta.CodigoArea = this.NumeroUnidadeOrganizacional == null || this.NumeroUnidadeOrganizacional.Trim() == "" ? Int32.MinValue : ProcessaArea().Codigo;

            resposta.CodigoSap = this.CodigoSap == null ? Int32.MinValue : Convert.ToInt32(this.CodigoSap);

            return resposta;

        }

        private PapelGS ExtraiPapelGS_Inserir(string pCodigoPessoaFisica)
        {
            PapelGS resposta = new PapelGS();
            //PapelGS papel = PapelGS.ObtemPorCodigo(pCodigoPessoaFisica);
            //PapelGS papel = PapelGS.Obtem(this.Cpf);

            resposta.Codigo = 0;
            //resposta.CodigoCargo = papel.CodigoCargo == 0 ? Int32.MinValue : papel.CodigoCargo;

            this.CodigoPosicao = this.CodigoPosicao == null ? string.Empty : this.CodigoPosicao;
            this.CargoContratista = this.CargoContratista == null ? string.Empty : this.CargoContratista;

            if (this.CodigoPosicao != string.Empty)
            {
                CargoGS cargo = CargoGS.Obtem(Convert.ToInt32(this.CodigoPosicao));
                if (cargo == null)
                {
                    throw new Exception("Cargo inexistente com o código Inserir: " + this.CodigoPosicao);
                }
                else
                {
                    resposta.CodigoCargo = cargo.Codigo;
                }
            }
            else if (this.CargoContratista != string.Empty)
            {
                CargoGS cargo = CargoGS.Obtem(Convert.ToInt32(this.CargoContratista));
                if (cargo == null)
                {
                    throw new Exception("Cargo inexistente com o código Inserir: " + this.CargoContratista);
                }
                else
                {
                    resposta.CodigoCargo = cargo.Codigo;
                }

            }

            resposta.CodigoPessoaFisica = pCodigoPessoaFisica;
            resposta.DataRegistro = DateTime.Now;
            resposta.EmAnalise = false;
            resposta.Juridico = false;
            resposta.Passback = false;
            resposta.Supervisionado = false;
            resposta.Supervisor = false;
            resposta.CodigoArea = this.NumeroUnidadeOrganizacional == null || this.NumeroUnidadeOrganizacional.Trim() == "" ? Int32.MinValue : ProcessaArea().Codigo;
            resposta.CodigoSap = this.CodigoSap == null ? Int32.MinValue : Convert.ToInt32(this.CodigoSap);

            return resposta;

        }


        private ColaboradorGS ExtraiColaboradorGS(string pCodigoPessoaFisica)
        {
            ColaboradorGS resposta = new ColaboradorGS();
            PapelGS papel = PapelGS.ObtemCpfPassaporte(this.Cpf, this.PassaporteNumero);
            resposta = ColaboradorGS.Obtem(papel.Codigo);
            EmpresaGS empresa = EmpresaGS.ObtemPorCodigoEmpresadoSAP(this.CodigoFornecedor);

            resposta.CodigoEmpresa = empresa.Codigo;
            resposta.CodigoPapel = papel.Codigo;
            if (this.Pis != null && this.Pis != "")
                resposta.NumeroPis = Convert.ToDecimal(this.Pis);
            resposta.CodigoArea = this.NumeroUnidadeOrganizacional == null || this.NumeroUnidadeOrganizacional.Trim() == "" ? Int32.MinValue : ProcessaArea().Codigo;

            if (this.GrupoPessoa != null)
                resposta.GrupoPessoa = Convert.ToInt32(this.GrupoPessoa);

            if (this.IdMotivo != null)
                resposta.IdMotivo = this.IdMotivo;
            if (this.DescMotivo != null)
                resposta.DescMotivo = this.DescMotivo;
            if (this.Processado != null)
                resposta.Processado = Convert.ToDateTime(this.Processado);
            if (this.TipoAcesso != null)
                resposta.TipoAcesso = this.TipoAcesso;

            resposta.CnpjSubContratrista = this.CnpjSubContratrista;
            resposta.SubContratista = this.SubContratista;
            resposta.DataAdmissao = Convert.ToDateTime(this.DataAdmissao);

            return resposta;

        }

        private ColaboradorGS ExtraiColaboradorGS_Inserir(PapelGS pPapel)
        {
            ColaboradorGS resposta = new ColaboradorGS();
            EmpresaGS empresa = EmpresaGS.ObtemPorCodigoEmpresadoSAP(this.CodigoFornecedor);

            resposta.CodigoEmpresa = empresa.Codigo;
            resposta.CodigoPapel = pPapel.Codigo;
            resposta.NumeroPis = this.Pis == null || this.Pis == "" ? Int32.MinValue : Convert.ToDecimal(this.Pis);
            resposta.CodigoArea = this.NumeroUnidadeOrganizacional == null || this.NumeroUnidadeOrganizacional.Trim() == "" ? Int32.MinValue : ProcessaArea().Codigo;
            resposta.SubContratista = this.SubContratista == null || this.SubContratista == "" ? this.SubContratista : "";
            resposta.CnpjSubContratrista = this.CnpjSubContratrista == null || this.CnpjSubContratrista == "" ? this.CnpjSubContratrista : "";
            resposta.DataAdmissao = Convert.ToDateTime(this.DataAdmissao);
            resposta.GrupoPessoa = Convert.ToInt32(this.GrupoPessoa);
            resposta.AcessoPorto = 0;

            resposta.IdMotivo = this.IdMotivo;
            resposta.DescMotivo = this.DescMotivo;
            resposta.Processado = Convert.ToDateTime(this.Processado);
            resposta.TipoAcesso = this.TipoAcesso;
            resposta.MaoDeObra = "Direta";

            return resposta;
        }


        private Boolean ExtraiAlocacaoColaborador(AlocacaoColaboradorGS pAlocacao)
        {

            MasterClass.UltimoErro = new Exception();

            AlocacaoColaboradorGS resposta = pAlocacao;


            SetorCustoGS centroCusto = SetorCustoGS.Obtem(this.CentroCustoCodigo);
            PapelGS papel = PapelGS.ObtemCpfPassaporte(this.Cpf, this.PassaporteNumero);

            try
            {

                if (resposta != null)
                {
                    resposta.Ativo = false;
                    resposta.DataDesativacao = DateTime.Now;

                    if (resposta.CodigoSetor == centroCusto.Codigo)
                    {
                        return true;
                    }

                    if (resposta.AtualizarRegistro() == -1)
                    {
                        throw new Exception("Falha ao desativar Alocação: " + MasterClass.UltimoErro.Message);
                    }
                }

                resposta = new AlocacaoColaboradorGS();

                resposta.Ativo = true;
                resposta.CodigoPapel = papel.Codigo;

                if (MasterClass.EhInteiro(System.Configuration.ConfigurationSettings.AppSettings["PedidoPadrao"]))
                {
                    resposta.CodigoPedido = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["PedidoPadrao"]);
                }
                else
                {
                    throw new Exception("Falha ao obter o valor do parâmetro 'PedidoPadrao' do arquivo de Configuração");
                }

                resposta.CodigoSetor = centroCusto.Codigo;

                resposta.DataDesativacao = DateTime.MinValue;
                resposta.DataRegistro = DateTime.Now;

                if (resposta.InserirRegistro() == -1)
                {
                    throw new Exception("Falha ao gravar Alocação: " + MasterClass.UltimoErro.Message);
                }

                return true;

            }
            catch (Exception erro)
            {
                MasterClass.UltimoErro = erro;
                return false;
            }

        }

        private AreaGS ProcessaArea()
        {
            AreaGS area = AreaGS.Obtem(this.NumeroUnidadeOrganizacional);

            if (this.NumeroUnidadeOrganizacional != null && this.NumeroUnidadeOrganizacional != String.Empty)
            {
                if (area == null)
                {
                    area = new AreaGS();
                    area.Ativo = true;
                    area.DataDesativacao = DateTime.MinValue;
                    area.DataRegistro = DateTime.Now;
                    area.Nome = this.NomeUnidadeOrganizacional;
                    area.Numero = this.NumeroUnidadeOrganizacional;
                    area.Codigo = area.InserirRegistroRetornaID();

                    if (area.Codigo < 1)
                    {
                        throw new Exception("Falha ao registrar nova Área." + this.NumeroUnidadeOrganizacional);
                    }

                }
                else
                {
                    area.Nome = this.NomeUnidadeOrganizacional;
                    area.AtualizarRegistro();
                }
            }

            return area;

        }

        private PapelLogGS ProcessaPapelLog(PapelGS papel)
        {

            PapelLogGS resposta;
            List<string> motivos = new List<string> { "1", "2", "3", "4", "5", "7", "9", "10", "11", "14", "15", "21" };

            if (PapelLogGS.ExistePapelLog(papel.Codigo))
            {
                resposta = PapelLogGS.Obtem(papel.Codigo);

                if ((resposta.Status == "Ferias") || (resposta.Status == "Afastamento"))
                {
                    return resposta;
                }

                if (ObtemStatus() != resposta.Status)
                {
                    resposta.DataFim = DateTime.Now;
                    resposta.AtualizarRegistro();
                }
                else
                {
                    return resposta;
                }


            }

            resposta = new PapelLogGS();

            resposta.CodigoPapel = papel.Codigo;
            resposta.DataRegistro = DateTime.Now;
            resposta.DataInicio = DateTime.Now;
            resposta.DataFim = DateTime.MinValue;
            resposta.Motivo = this.DescMotivo;

            var existe = motivos.Where(m => m == this.IdMotivo).FirstOrDefault();

            if (existe != null)
            {
                resposta.Status = "Inativacao";
            }
            else
            {
                resposta.Status = "Ativo";
            }



            resposta.InserirRegistro();

            return resposta;
        }

        private string ObtemStatus()
        {
            String resposta = String.Empty;

            switch (this.StatusOcupacao)
            {
                case "1":
                    {
                        resposta = "Ativo";
                        break;
                    }
                case "0":
                    {
                        resposta = "Inativacao";
                        break;
                    }

            }

            return resposta;
        }

        private ColaboradorGS ExtraiColaboradorGS(Int32 pCodigoPapel)
        {
            ColaboradorGS resposta = new ColaboradorGS();

            try
            {
                //PessoaFisicaGS -> Pessoa -> Papel ->

                //Obtem dados auxiliares
                EmpresaGS empresa = EmpresaGS.Obtem(this.CnpjEmpresa);
                ColaboradorGS colaborador = ColaboradorGS.Obtem(pCodigoPapel);

                resposta.CodigoEmpresa = empresa.Codigo;
                resposta.CodigoPapel = pCodigoPapel;
                resposta.NumeroPis = Convert.ToDecimal(this.Pis);

                resposta.GrupoPessoa = Convert.ToInt32(this.GrupoPessoa);

            }
            catch (Exception erro)
            {
                throw new Exception("[PessoaSAP].[EXTRAICOLABORADORGS] Erro ao extrair os dados de Colaborador!" + erro.Message);
            }

            return resposta;
        }

        private string ExtraiApelido(String pNome)
        {
            string resposta = string.Empty;
            string[] Nomes = pNome.Split(' ');
            int PosicaoSobrenome = Nomes.GetLength(0);

            if (PosicaoSobrenome == 1)
            {
                resposta = Nomes[0];
            }
            else
            {
                resposta = Nomes[0] + " " + Nomes[PosicaoSobrenome - 1];
            }

            return resposta;
        }

        public bool InsereColaborador(Integracao pIntegracao)
        {
            try
            {
                pIntegracao.TipoIntegracao = "PessoaSAP";
                pIntegracao.Status = "PROCESSANDO";
                pIntegracao.DataStatus = DateTime.Now;
                pIntegracao.Mensagem = String.Empty;

                pIntegracao.Converte().AtualizarRegistro();

                //pessoa, papel, colaborador

                MasterClass item = new MasterClass();

                EmpregadoSapGS empregado = ExtraiEmpregadoSap();

                if (!ProcessaCargoGS())
                {
                    throw new Exception("[PessoaSAP].[ATUALIZACOLABORADOR-4] Registro não atualizado." + MasterClass.UltimoErro.Message);
                }

                PessoaFisicaGS pessoaFisicaExiste = new PessoaFisicaGS();

                if (!String.IsNullOrEmpty(this.Cpf))
                {
                    pessoaFisicaExiste = this.Cpf != "" ? PessoaFisicaGS.Obtem(this.Cpf) : null;
                }
                else
                {
                    pessoaFisicaExiste = this.PassaporteNumero != "" ? PessoaFisicaGS.ObtemPassporte(this.PassaporteNumero) : null;
                }

                if (pessoaFisicaExiste == null)
                {
                    //-Novo---------------------------------
                    item = this.ExtraiPessoa_InserirGS();

                    Int32 CodigoPessoax = item.InserirRegistro();

                    if (CodigoPessoax == -1)
                    {
                        throw new Exception("[PessoaSAP].[INSERECOLABORADOR] Registro não inserido." + MasterClass.UltimoErro.Message);
                    }

                    //-Novo--------------------------------- 
                    item = this.ExtraiPessoaFisicaGS_Inserir(((PessoaGS)item));

                    pessoaFisicaExiste = (PessoaFisicaGS)item;

                    Int32 CodigoPessoaFisica = item.InserirRegistro();

                    if (CodigoPessoaFisica == -1)
                    {
                        throw new Exception("[PessoaSAP].[INSERECOLABORADOR] Registro não inserido." + MasterClass.UltimoErro.Message);
                    }

                }
                else
                {
                    ///- inicio - Para impedir a duplicação do cpf e passaporte.
                    //PapelGS papelGS = PapelGS.ObtemCpfPassaporte(this.Cpf, this.PassaporteNumero);
                    //String codigoEmpregadoSAP = Convert.ToString(papelGS.CodigoSap);
                    //if ((((!String.IsNullOrEmpty(this.Cpf)) && pessoaFisicaExiste.Cpf == this.Cpf) ||
                    //    ((!String.IsNullOrEmpty(this.PassaporteNumero)) && (pessoaFisicaExiste.Passaporte == this.PassaporteNumero))) &&
                    //    (this.CodigoSap != codigoEmpregadoSAP))
                    //{
                    //    throw new Exception("[PessoaSAP].[ATUALIZACOLABORADOR] Registro não inserido. Já existe uma pessoa com o CPF ou Passaporte informado para o código empregado SAP: " + codigoEmpregadoSAP + " ou  código papel nulo. ");
                    //}
                    ///- fim - Para impedir a duplicação do cpf e passaporte.

                    item = this.ExtraiPessoaFisicaGS();

                    pessoaFisicaExiste = (PessoaFisicaGS)item;

                    if (item.AtualizarRegistro() == -1)
                    {
                        throw new Exception("[PessoaSAP].[ATUALIZACOLABORADOR-5] Registro não atualizado." + MasterClass.UltimoErro.Message);
                    }
                    //-----------------------------------------
                    item = this.ExtraiPessoaGS(((PessoaFisicaGS)item).Codigo);

                    if (item.AtualizarRegistro() == -1)
                    {
                        throw new Exception("[PessoaSAP].[ATUALIZACOLABORADOR-5] Registro não atualizado." + MasterClass.UltimoErro.Message);
                    }
                }

                //-Novo---------------------------------                                
                item = this.ExtraiPapelGS_Inserir(pessoaFisicaExiste.Codigo);

                Int32 CodigoPapel = item.InserirRegistroRetornaID();

                if (CodigoPapel == -1)
                {
                    throw new Exception("[PessoaSAP].[INSERECOLABORADOR] Registro não inserido." + MasterClass.UltimoErro.Message);
                }

                PapelGS papel = PapelGS.ObtemPorCodigoPapel(CodigoPapel);

                //-Novo---------------------------------                
                item = this.ExtraiColaboradorGS_Inserir(papel);

                Int32 CodigoColaborador = item.InserirRegistro();

                if (CodigoColaborador == -1)
                {
                    throw new Exception("[PessoaSAP].[INSERECOLABORADOR] Registro não inserido." + MasterClass.UltimoErro.Message);
                }

                //-Novo---------------------------------                                
                item = this.ProcessaPapelLog(papel);

                //-Novo---------------------------------

                AlocacaoColaboradorGS alocacao = AlocacaoColaboradorGS.Obtem(papel.Codigo);


                if ((!String.IsNullOrEmpty(this.CentroCustoCodigo)) && (!ExtraiAlocacaoColaborador(alocacao)))
                {
                    throw new Exception("[PessoaSAP].[ATUALIZACOLABORADOR-6] Registro não atualizado." + MasterClass.UltimoErro.Message);
                }

                pIntegracao.DataStatus = DateTime.Now;
                pIntegracao.Mensagem += this.CodigoSap + "-" + "Colaborador processado com sucesso. ";
                pIntegracao.Status = "SUCESSO";

                pIntegracao.Converte().AtualizarRegistro();

                return true;
            }
            catch (Exception erro)
            {

                pIntegracao.Status = "ERRO";
                pIntegracao.Mensagem += this.CodigoSap + "-" + "Falha ao processar o registro. " + erro.Message;
                pIntegracao.DataStatus = DateTime.Now;

                pIntegracao.Converte().AtualizarRegistro();


                return false;
            }
        }

        public bool AtualizaColaborador(Integracao pIntegracao)
        {
            //MasterClass item = new MasterClass();

            //item.conexao.ConnectionString = ConfigurationSettings.AppSettings["ConnectionString"];

            //if (item.conexao.State != System.Data.ConnectionState.Open)
            //    item.conexao.Open();

            //SqlTransaction transaction = item.conexao.BeginTransaction("Transacao");

            try
            {
                pIntegracao.TipoIntegracao = "PessoaSAP";
                pIntegracao.Status = "PROCESSANDO";
                pIntegracao.DataStatus = DateTime.Now;
                pIntegracao.Mensagem = String.Empty;

                pIntegracao.Converte().AtualizarRegistro();

                //pessoa, papel, colaborador

                MasterClass item = new MasterClass();

                EmpregadoSapGS empregado = ExtraiEmpregadoSap();

                if (!ProcessaCargoGS())
                {
                    throw new Exception("[PessoaSAP].[ATUALIZACOLABORADOR-7] Registro não atualizado." + MasterClass.UltimoErro.Message);
                }

                //--------------------------------
                item = this.ExtraiPessoaFisicaGS();

                PessoaFisicaGS pessoa = (PessoaFisicaGS)item;

                ///- inicio - Para impedir a duplicação do cpf e passaporte.                
                //Int32 codigoEmpregadoSAP = PapelGS.ObtemCpfPassaporte(this.Cpf, this.PassaporteNumero).CodigoSap;
                //if ((((!String.IsNullOrEmpty(this.Cpf)) && pessoa.Cpf == this.Cpf) ||
                //    ((!String.IsNullOrEmpty(this.PassaporteNumero)) && (pessoa.Passaporte == this.PassaporteNumero))) &&
                //    (Convert.ToInt32(this.CodigoSap) != codigoEmpregadoSAP))
                //{
                //    throw new Exception("[PessoaSAP].[ATUALIZACOLABORADOR] Registro não inserido. Já existe uma pessoa com o CPF ou Passaporte informado para o código empregado SAP: " + codigoEmpregadoSAP + " ou  código papel nulo. ");
                //}
                ///- fim - Para impedir a duplicação do cpf e passaporte.

                if (item.AtualizarRegistro() == -1)
                {
                    throw new Exception("[PessoaSAP].[ATUALIZACOLABORADOR-8] Registro não atualizado." + MasterClass.UltimoErro.Message);
                }
                //--------------------------------
                item = this.ExtraiPessoaGS(((PessoaFisicaGS)item).Codigo);

                if (item.AtualizarRegistro() == -1)
                {
                    throw new Exception("[PessoaSAP].[ATUALIZACOLABORADOR-9] Registro não atualizado." + MasterClass.UltimoErro.Message);
                }
                //--------------------------------
                item = ExtraiPapelGS(((PessoaGS)item).Codigo);

                if (item.AtualizarRegistro() == -1)
                {
                    throw new Exception("[PessoaSAP].[ATUALIZACOLABORADOR-10] Registro não atualizado." + MasterClass.UltimoErro.Message);
                }

                //--------------------------------
                PapelGS papel = PapelGS.ObtemCpfPassaporte(this.Cpf, this.PassaporteNumero);

                //if (!ProcessaGrupoTrabalhoGS(papel.Codigo))
                //{
                //    throw new Exception("[PessoaSAP].[ATUALIZACOLABORADOR-11] Registro não atualizado." + MasterClass.UltimoErro.Message);
                //}
                //--------------------------------
                item = ExtraiColaboradorGS(pessoa.Codigo);

                if (item.AtualizarRegistro() == -1)
                {
                    throw new Exception("[PessoaSAP].[ATUALIZACOLABORADOR-12] Registro não atualizado." + MasterClass.UltimoErro.Message);
                }
                //--------------------------------
                AlocacaoColaboradorGS alocacao = AlocacaoColaboradorGS.Obtem(papel.Codigo);

                if ((!String.IsNullOrEmpty(this.CentroCustoCodigo)) && (!ExtraiAlocacaoColaborador(alocacao)))
                {
                    throw new Exception("[PessoaSAP].[ATUALIZACOLABORADOR-13] Registro não atualizado." + MasterClass.UltimoErro.Message);
                }

                ProcessaPapelLog(papel);

                pIntegracao.DataStatus = DateTime.Now;
                pIntegracao.Mensagem += this.CodigoSap + "-" + "Colaborador processado com sucesso. ";
                pIntegracao.Status = "SUCESSO";

                pIntegracao.Converte().AtualizarRegistro();

                return true;
            }
            catch (Exception erro)
            {

                pIntegracao.Status = "ERRO";
                pIntegracao.Mensagem += this.CodigoSap + "-" + "Falha ao processar o registro. " + erro.Message;
                pIntegracao.DataStatus = DateTime.Now;

                pIntegracao.Converte().AtualizarRegistro();


                return false;
            }
        }

        #endregion

    }
}
