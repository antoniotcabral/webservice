﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Web;
using System.Data.Common;
using System.Data;
using System.Reflection;
using System.Configuration;
using System.IO;
using System.Net;
using System.Diagnostics;
using System.Net.Mail;
using System.Data.SqlClient;
using System.Text;
//using System.ServiceModel;
//using System.ServiceModel.Channels;
//using System.ServiceModel.Description;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    public class MasterClass
    {
        #region Variáveis de Configuração da Conexão com o Banco de Dados

        protected static string localBancoPortal = ConfigurationSettings.AppSettings["ConnectionString"];
        protected static string localBancoIntegracao = ConfigurationSettings.AppSettings["ConnectionString"];
        public static string webservice = ConfigurationSettings.AppSettings["EnderecoWebservice"];
        
        protected static string localBanco = ConfigurationSettings.AppSettings["ConnectionString"];

        public static string UsuarioWS   = ConfigurationSettings.AppSettings["usuarioWS"];
        public static string SenhaWS  = ConfigurationSettings.AppSettings["senhaWS"];
        
        public SqlConnection conexao = new SqlConnection();
        protected DbCommand comando = new SqlCommand();
        protected DbDataReader acesso;

        protected string condicaoGeral = "";
        protected string condicaoParcial = string.Empty;

        protected string prefixoParametro = "@";

        public static Exception UltimoErro;

        public static Boolean LogEmArquivo = true;

        public static StringBuilder TextoLog = new StringBuilder();

        #endregion


        #region Variáveis de Configurações do Aplicativo

        protected static String nomeArquivoLog = "IntegraControlSys.log";
        protected static String AppPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
        
        public static String StatusDaOperacao = String.Empty;

        public static ArrayList ItensSincronizados;

        public static ArrayList ErrosEncontrados = new ArrayList();

        public static Boolean LogAtivado = (ConfigurationSettings.AppSettings["GravaLog"] == "true");
            

        #endregion

        public MasterClass()
            : base()
        {

        }

        
        public static ArrayList ClassesDoSistema()
        {
            
            ArrayList minhasClasses = new ArrayList();
            System.Reflection.Assembly a = System.Reflection.Assembly.GetExecutingAssembly();

            try
            {
                foreach (Type t in a.GetTypes())
                {
                    //Se a classe possuir o AtributoDeClasse
                    foreach (Attribute att in t.GetCustomAttributes(false))
                    {
                        if (att is DadosDaTabela)
                        {
                            minhasClasses.Add(t);
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
            }

            return minhasClasses;
        }


        #region Procedimentos de Acesso ao Banco de Dados

        public virtual DbDataReader ExecutaComandoReader(string cmm)
        {
            try
            {

                conexao.ConnectionString = localBanco;

                if (conexao.State != System.Data.ConnectionState.Open)
                    conexao.Open();

                comando = new SqlCommand(cmm, (SqlConnection)(SqlConnection)conexao);

                acesso = comando.ExecuteReader();

            }
            catch (Exception ex)
            {
                UltimoErro = ex;
            }
            return acesso;

        }

        public virtual DbDataReader ExecutaComandoDbReader(string cmd)
        {
            try
            {
                conexao.ConnectionString = localBanco;

                if (conexao.State != System.Data.ConnectionState.Open)
                    conexao.Open();

                comando = new SqlCommand(cmd, (SqlConnection)conexao);

                return  comando.ExecuteReader();

            }
            catch (Exception ex)
            {
                UltimoErro = ex;
                return null;
            }

        }

        public virtual Object ExecutaEscalar(string cmd)
        {
            try
            {
                conexao.ConnectionString = localBanco;

                if (conexao.State != System.Data.ConnectionState.Open)
                    conexao.Open();

                comando = new SqlCommand(cmd, (SqlConnection)conexao);

                return comando.ExecuteScalar();

            }
            catch (Exception ex)
            {
                UltimoErro = ex;
                return null;
            }

        }
        
        public virtual bool ExecutaComando(string cmd)
        {
            try
            {
                conexao.ConnectionString = localBanco;

                if (conexao.State != System.Data.ConnectionState.Open)
                    conexao.Open();

                comando = new SqlCommand(cmd, (SqlConnection) (SqlConnection) conexao);

                comando.ExecuteReader();

                conexao.Close();

                return true;
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
                return false;
            }

        }

        public virtual bool ExecutaComandoNoQuery(string cmd)
        {
            try
            {
                conexao.ConnectionString = localBanco;

                if (conexao.State != System.Data.ConnectionState.Open)
                    conexao.Open();

                comando = new SqlCommand(cmd, (SqlConnection)(SqlConnection)conexao);

                comando.ExecuteNonQuery();

                conexao.Close();

                return true;
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
                return false;
            }

        }

        public virtual int ExecutaComandoContarRegistros(string cmd)
        {
            try
            {
                conexao.ConnectionString = localBanco;

                if (conexao.State != System.Data.ConnectionState.Open)
                    conexao.Open();

                comando = new SqlCommand(cmd, (SqlConnection)(SqlConnection)conexao);

                int registros = Convert.ToInt32(comando.ExecuteScalar());

                conexao.Close();

                return registros;
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
                return -1;
            }

        }

        public virtual DbDataReader LerTodosRegistros()        
        {
            try
            {
                conexao.ConnectionString = localBanco;

                if (conexao.State != System.Data.ConnectionState.Open)
                    conexao.Open();

                comando = new SqlCommand(PreparaComandoSelect(), (SqlConnection)(SqlConnection)conexao);

                acesso = comando.ExecuteReader();

                conexao.Close();
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
            }
            return acesso;

        }

        

        public virtual DbDataReader LerRegistros(List<DbParameter> filtro, string strCondicao, DbConnection cnx)
        {
            try
            {

                comando = new SqlCommand(PreparaComandoSelect(), (SqlConnection)cnx);

                if (filtro.Count > 0)
                {
                    comando.CommandText += strCondicao;

                    foreach (DbParameter par in filtro)
                    {
                        comando.Parameters.Add(par);
                    }
                }
                else
                {
                    if (strCondicao != string.Empty)
                    {
                        comando.CommandText += " WHERE " + strCondicao;
                    }
                }

                acesso = comando.ExecuteReader();
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
            }
            

            return acesso;
        }

        public virtual DbDataReader LerRegistrosFiltradosOrdenados(List<DbParameter> filtro, string strCondicao, DbConnection cnx, string campoOrdem)
        {
            try
            {

                comando = new SqlCommand(PreparaComandoSelect(), (SqlConnection)cnx);

                if (filtro.Count > 0)
                {
                    comando.CommandText += strCondicao;

                    foreach (DbParameter par in filtro)
                    {
                        comando.Parameters.Add(par);
                    }
                }
                else
                {
                    if (strCondicao != string.Empty)
                    {
                        comando.CommandText += " WHERE " + strCondicao;
                    }
                }

                if (campoOrdem != String.Empty)
                {
                    comando.CommandText += " ORDER BY " + campoOrdem;
                }

                acesso = comando.ExecuteReader();
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
            }


            return acesso;
        }


        public virtual DbDataReader LerRegistrosOrdenados(List<DbParameter> filtro, string strCondicao, DbConnection cnx, string CampoOrdem )
        {
            try
            {
                comando = new SqlCommand(PreparaComandoSelect(), (SqlConnection)cnx);

                if (filtro.Count > 0)
                {
                    comando.CommandText += strCondicao;

                    foreach (DbParameter par in filtro)
                    {
                        comando.Parameters.Add(par);
                    }
                }
                else
                {
                    if (strCondicao != string.Empty)
                    {
                        comando.CommandText += " WHERE " + strCondicao;
                    }
                }

                if (CampoOrdem != String.Empty)
                {
                    comando.CommandText += " ORDER BY " + CampoOrdem;
                }



                acesso = comando.ExecuteReader();

            }
            catch (Exception ex)
            {
                UltimoErro = ex;
            }

            return acesso;
        }

        public virtual DbDataReader LerRegistrosCustom( string strComando, DbConnection cnx)
        {
            try
            {

                comando = new SqlCommand(strComando, (SqlConnection)cnx);                

                acesso = comando.ExecuteReader();
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
            }


            return acesso;
        }

        public virtual int ApagarRegistros(List<DbParameter> filtro, string strCondicao)
        {
            int removidos = 0;

            try
            {
                conexao.ConnectionString = localBanco;

                if (conexao.State != System.Data.ConnectionState.Open)
                    conexao.Open();

                comando = new SqlCommand(PreparaComandoDelete(), (SqlConnection)conexao);

                comando.CommandText += strCondicao;

                foreach (DbParameter par in filtro)
                {
                    comando.Parameters.Add(par);
                }

                removidos = comando.ExecuteNonQuery();

                conexao.Close();
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
            }

            return removidos;
        }

        public virtual int UltimoCodigo()
        {
            conexao.ConnectionString = localBanco;

            if (conexao.State != System.Data.ConnectionState.Open)
                conexao.Open();

            comando = new SqlCommand("SELECT SCOPE_IDENTITY()", (SqlConnection)conexao);

            object ultimoCodigo = (int)comando.ExecuteReader()[0];

            return 1;
        }

        public virtual int InserirRegistro()
        {
            try
            {
                conexao.ConnectionString = localBanco;

                if (conexao.State != System.Data.ConnectionState.Open)
                    conexao.Open();

                comando = new SqlCommand(PreparaComandoInsert(), (SqlConnection) conexao);

                foreach (PropertyInfo prop in this.GetType().GetProperties())
                {
                    foreach (CampoNaTabela campo in prop.GetCustomAttributes(false))
                    {

                        if (campo.EhAuto)
                            break;

                        Debug.Print(campo.NomeDoCampo);
                        if (prop.GetValue(this, null) != null)
                        {
                            Debug.Print(prop.GetValue(this, null).ToString());
                        }
                        else
                        {
                            Debug.Print("NULL");
                        }

                        SqlParameter parametro;

                        if (prop.GetValue(this, null) != null)
                        {
                            string valor = prop.GetValue(this, null).ToString().Trim();

                            if (prop.PropertyType.Name == "DateTime")
                            {
                                DateTime data = (DateTime)prop.GetValue(this, null);
                                if(data.Year < 1930)
                                {
                                    valor = string.Empty;
                                }
                            }

                            if (prop.PropertyType.Name == "Int32")
                            {
                                Int32 valorInteiro = (Int32)prop.GetValue(this, null);
                                if (valorInteiro == Int32.MinValue)
                                {
                                    valor = string.Empty;
                                }
                            }

                            if (valor != string.Empty)
                            {
                                //valor = valor.Replace(",", ".");
                                parametro = new SqlParameter("p" + campo.NomeDoCampo, valor);
                                parametro.DbType = campo.TipoDoCampo;
                            }
                            else
                            {
                                parametro = new SqlParameter("p" + campo.NomeDoCampo, DBNull.Value);
                                parametro.DbType = campo.TipoDoCampo;

                            }

                            comando.Parameters.Add(parametro);
                        }
                        else
                        {
                            if (prop.GetValue(this, null) == null)
                            {
                                parametro = new SqlParameter("p" + campo.NomeDoCampo, DBNull.Value);
                                parametro.DbType = campo.TipoDoCampo;
                            }
                            else
                            {
                                parametro = new SqlParameter("p" + campo.NomeDoCampo, prop.GetValue(this, null));
                                parametro.DbType = campo.TipoDoCampo;
                            }

                            comando.Parameters.Add(parametro);
                        }
                    }
                }


                int inseridos = comando.ExecuteNonQuery();               

                conexao.Close();

                return inseridos;
            }
            catch(Exception ex)
            {
                UltimoErro = ex;
                return -1;
                //throw ex;
            }
            
        }

        public virtual int InserirRegistroRetornaID()
        {
            try
            {
                conexao.ConnectionString = localBanco;

                if (conexao.State != System.Data.ConnectionState.Open)
                    conexao.Open();

                comando = new SqlCommand(PreparaComandoInsert(), (SqlConnection)conexao);

                foreach (PropertyInfo prop in this.GetType().GetProperties())
                {
                    foreach (CampoNaTabela campo in prop.GetCustomAttributes(false))
                    {

                        if (campo.EhAuto)
                            break;

                        Debug.Print(campo.NomeDoCampo);
                        if (prop.GetValue(this, null) != null)
                        {
                            Debug.Print(prop.GetValue(this, null).ToString());
                        }
                        else
                        {
                            Debug.Print("NULL");
                        }

                        SqlParameter parametro;

                        if (prop.GetValue(this, null) != null)
                        {
                            string valor = prop.GetValue(this, null).ToString().Trim();

                            if (prop.PropertyType.Name == "DateTime")
                            {
                                DateTime data = (DateTime)prop.GetValue(this, null);
                                if (data.Year < 1930)
                                {
                                    valor = string.Empty;
                                }
                            }

                            if (prop.PropertyType.Name == "Int32")
                            {
                                Int32 valorInteiro = (Int32)prop.GetValue(this, null);
                                if (valorInteiro == Int32.MinValue)
                                {
                                    valor = string.Empty;
                                }
                            }

                            //if (prop.PropertyType.Name == "Decimal")
                            //{
                            //    Decimal valorDecimal = (Decimal)prop.GetValue(this, null);
                            //    if (valorDecimal == Decimal.MinValue)
                            //    {
                            //        valor = string.Empty;
                            //    }
                            //    valor = valor.Replace(",", ".");
                            //}

                            if (valor != string.Empty)
                            {
                                //valor = valor.Replace(",", ".");
                                parametro = new SqlParameter("p" + campo.NomeDoCampo, valor);
                                parametro.DbType = campo.TipoDoCampo;
                            }
                            else
                            {
                                parametro = new SqlParameter("p" + campo.NomeDoCampo, DBNull.Value);
                                parametro.DbType = campo.TipoDoCampo;

                            }

                            comando.Parameters.Add(parametro);
                        }
                        else
                        {
                            if (prop.GetValue(this, null) == null)
                            {
                                parametro = new SqlParameter("p" + campo.NomeDoCampo, DBNull.Value);
                                parametro.DbType = campo.TipoDoCampo;
                            }
                            else
                            {
                                parametro = new SqlParameter("p" + campo.NomeDoCampo, prop.GetValue(this, null));
                                parametro.DbType = campo.TipoDoCampo;
                            }

                            comando.Parameters.Add(parametro);
                        }
                    }
                }

                comando.CommandText += ";SELECT CAST(scope_identity() AS int)";


                int inseridos = (int) comando.ExecuteScalar();

                conexao.Close();

                return inseridos;
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
                return -1;
                //throw ex;
            }

        }

        public virtual int AtualizarRegistro()
        {
            try
            {
                conexao.ConnectionString = localBanco;

                if (conexao.State != System.Data.ConnectionState.Open)
                    conexao.Open();

                comando = new SqlCommand(PreparaComandoUpdate(), (SqlConnection)conexao);

                foreach (PropertyInfo prop in this.GetType().GetProperties())
                {
                    foreach (CampoNaTabela campo in prop.GetCustomAttributes(false))
                    {
                        if (!(campo.EhChave))
                            if (campo.EhAuto)
                                break;

                        //if (campo.EhAuto)
                        //    break;

                        Debug.Print(campo.NomeDoCampo);
                        
                        if (prop.GetValue(this, null) != null)
                        {
                            Debug.Print(prop.GetValue(this, null).ToString());
                        }
                        else
                        {
                            Debug.Print("NULL");
                        }

                        SqlParameter parametro;

                        if (prop.GetValue(this, null) != null)
                        {
                            string valor = prop.GetValue(this, null).ToString().Trim();

                            if (prop.PropertyType.Name == "DateTime")
                            {
                                DateTime data = (DateTime)prop.GetValue(this, null);
                                if (data.Year < 1930)
                                {
                                    valor = string.Empty;
                                }
                            }

                            if (prop.PropertyType.Name == "Int32")
                            {
                                Int32 valorInteiro = (Int32)prop.GetValue(this, null);
                                if (valorInteiro == Int32.MinValue)
                                {
                                    valor = string.Empty;
                                }
                            }

                            if (valor != string.Empty)
                            {
                                //valor = valor.Replace(",", ".");
                                parametro = new SqlParameter("p" + campo.NomeDoCampo, valor);
                                parametro.DbType = campo.TipoDoCampo;
                            }
                            else
                            {
                                parametro = new SqlParameter("p" + campo.NomeDoCampo, DBNull.Value);
                                parametro.DbType = campo.TipoDoCampo;

                            }

                            comando.Parameters.Add(parametro);
                        }
                        else
                        {
                            if (prop.GetValue(this, null) == null)
                            {
                                parametro = new SqlParameter("p" + campo.NomeDoCampo, DBNull.Value);
                                parametro.DbType = campo.TipoDoCampo;
                            }
                            else
                            {
                                parametro = new SqlParameter("p" + campo.NomeDoCampo, prop.GetValue(this, null));
                                parametro.DbType = campo.TipoDoCampo;
                            }

                            comando.Parameters.Add(parametro);
                        }
                    }
                }


                int inseridos = comando.ExecuteNonQuery();

                conexao.Close();

                return inseridos;
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
                //throw ex;
                return -1;
            }

        }

        

        public virtual int AtualizarRegistro(string pCustomWhere)
        {
            try
            {
                conexao.ConnectionString = localBanco;

                if (conexao.State != System.Data.ConnectionState.Open)
                    conexao.Open();

                comando = new SqlCommand(PreparaComandoUpdate(pCustomWhere), (SqlConnection)conexao);

                foreach (PropertyInfo prop in this.GetType().GetProperties())
                {
                    foreach (CampoNaTabela campo in prop.GetCustomAttributes(false))
                    {

                        //if (campo.EhAuto)
                        //    break;

                        Debug.Print(campo.NomeDoCampo);

                        if (prop.GetValue(this, null) != null)
                        {
                            Debug.Print(prop.GetValue(this, null).ToString());
                        }
                        else
                        {
                            Debug.Print("NULL");
                        }

                        SqlParameter parametro;

                        if (prop.GetValue(this, null) != null)
                        {
                            string valor = prop.GetValue(this, null).ToString().Trim();

                            if (prop.PropertyType.Name == "DateTime")
                            {
                                DateTime data = (DateTime)prop.GetValue(this, null);
                                if (data.Year < 1930)
                                {
                                    valor = string.Empty;
                                }
                            }

                            if (prop.PropertyType.Name == "Int32")
                            {
                                Int32 valorInteiro = (Int32)prop.GetValue(this, null);
                                if (valorInteiro == Int32.MinValue)
                                {
                                    valor = string.Empty;
                                }
                            }

                            if (valor != string.Empty)
                            {
                                //valor = valor.Replace(",", ".");
                                parametro = new SqlParameter("p" + campo.NomeDoCampo, valor);
                                parametro.DbType = campo.TipoDoCampo;
                            }
                            else
                            {
                                parametro = new SqlParameter("p" + campo.NomeDoCampo, DBNull.Value);
                                parametro.DbType = campo.TipoDoCampo;

                            }

                            if (!campo.EhChave)
                                comando.Parameters.Add(parametro);
                        }
                        else
                        {
                            if (prop.GetValue(this, null) == null)
                            {
                                parametro = new SqlParameter("p" + campo.NomeDoCampo, DBNull.Value);
                                parametro.DbType = campo.TipoDoCampo;
                            }
                            else
                            {
                                parametro = new SqlParameter("p" + campo.NomeDoCampo, prop.GetValue(this, null));
                                parametro.DbType = campo.TipoDoCampo;
                            }

                            //Já que o Where é customizado, não precisa dos parâmetros chave primária
                            //if (!campo.EhChave) 
                                comando.Parameters.Add(parametro);
                        }
                    }
                }


                int inseridos = comando.ExecuteNonQuery();

                conexao.Close();

                return inseridos;
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
                //throw ex;
                return -1;
            }

        }

        /*
        public virtual int AtualizarRegistro()
        {
            int atualizados = 0;
            try
            {
                conexao.ConnectionString = localBanco;

                if (conexao.State != System.Data.ConnectionState.Open)
                    conexao.Open();

                comando = PreparaComandoUpdateV2();

                comando.Connection = conexao;

                atualizados = comando.ExecuteNonQuery();

                conexao.Close();
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
            }
            return atualizados;
        }

        public virtual int AtualizarRegistro(List<DbParameter> itens)
        {
            int atualizados = 0;

            try
            {
                conexao.ConnectionString = localBanco;

                if (conexao.State != System.Data.ConnectionState.Open)
                    conexao.Open();

                comando = new SqlCommand(PreparaComandoUpdate(), (SqlConnection)conexao);

                foreach (DbParameter par in itens)
                {
                    comando.Parameters.Add(par);
                }

                 atualizados = comando.ExecuteNonQuery();

                conexao.Close();
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
            }

            return atualizados;
        }
        */
      
        public static List<MasterClass> ReaderToList(DbDataReader reader, Type tipoDado)
        {

            List<MasterClass> ListadeDados = new List<MasterClass>();
            object classeDado;

            int lendo = 0;

            try
            {
                while (reader.Read())
                {
                    classeDado = Activator.CreateInstance(tipoDado);

                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        foreach (PropertyInfo prop in tipoDado.GetProperties())
                        {
                            foreach (CampoNaTabela campo in prop.GetCustomAttributes(false))
                            {
                                String nCampo = campo.NomeDoCampo.Replace("[", "");
                                nCampo = nCampo.Replace("]", "");                                

                                if (nCampo.ToUpper() == reader.GetName(i).ToUpper())
                                {
                                    Debug.Print("Campo: " + nCampo);

                                    try
                                    {
                                        Debug.Print(reader.GetValue(i).ToString());
                                    }
                                    catch (Exception)
                                    {
                                        Debug.Print("NULO");
                                    }
                                    if (reader.GetValue(i) != DBNull.Value)
                                    {
                                        prop.SetValue(classeDado, Convert.ChangeType(reader.GetValue(i), prop.PropertyType), null);
                                    }
                                    else
                                    {
                                        prop.SetValue(classeDado, null, null);
                                    }
                                    break;
                                }
                            }
                            //break;
                        }

                    }

                    lendo++;

                    MasterClass.StatusDaOperacao = " Lendo registro " + lendo.ToString();

                    //System.Windows.Forms.Application.DoEvents();

                    ListadeDados.Add((MasterClass)classeDado);

                }
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
            }

            return ListadeDados;
        }

        public static DataTable ReaderToDataTable(DbDataReader reader, Type tipoDado)
        {
            DataTable ListadeDados = new DataTable();
            object classeDado;

            try
            {
                //Adiciona as colunas no DataTable
                foreach (PropertyInfo prop in tipoDado.GetProperties())
                {
                    foreach (CampoNaTabela campo in prop.GetCustomAttributes(false))
                    {
                        if (!ListadeDados.Columns.Contains(campo.NomeDoCampo))
                        {
                            ListadeDados.Columns.Add(prop.Name.ToUpper());
                        }
                    }
                }

                while (reader.Read())
                {
                    classeDado = Activator.CreateInstance(tipoDado);
                    DataRow novaLinha = ListadeDados.NewRow();

                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        foreach (PropertyInfo prop in tipoDado.GetProperties())
                        {
                            foreach (CampoNaTabela campo in prop.GetCustomAttributes(false))
                            {

                                if (campo.NomeDoCampo == reader.GetName(i))
                                {
                                    if (reader.GetValue(i) != DBNull.Value)
                                    {
                                        //prop.SetValue(classeDado, Convert.ChangeType(reader.GetValue(i), prop.PropertyType), null);
                                        novaLinha[prop.Name] = reader.GetValue(i);
                                    }
                                    break;
                                }
                            }
                            //break;
                        }

                    }

                    ListadeDados.Rows.Add(novaLinha);

                }
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
            }

            return ListadeDados;
        }

        private string PreparaComandoSelect()
        {
            Type tipo = this.GetType();
            string comandoSelect = " SELECT ";
            string tabela = string.Empty;

            try
            {

                #region Obtem o nome da Tabela

                tabela = NomeTabela(tipo);
                
                #endregion


                #region Obtem a lista de campos e as associa ao comando

                foreach (PropertyInfo propInfo in tipo.GetProperties())
                {
                    foreach (Attribute att in propInfo.GetCustomAttributes(false))
                    {
                        if (att is CampoNaTabela)
                        {
                            //  if (((CampoNaTabela)att).NomeDoCampo.Contains("AS"))
                            //{
                            //    comandoSelect += ((CampoNaTabela)att).NomeDoCampo + ", ";
                            // }
                            // else
                            // {
                            comandoSelect += tabela + "." + ((CampoNaTabela)att).NomeDoCampo + ", ";
                            //}
                            break;
                        }
                    }
                }

                if (comandoSelect == " SELECT ")
                    throw new Exception("Associação de Nomes de Campos não definidas na Classe de Dados - " + tipo.FullName);
                else
                    comandoSelect = comandoSelect.Substring(0, comandoSelect.Length - 2) + " FROM " + tabela;

                #endregion

                //comandoSelect += " WHERE 0=0 ";
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
            }
            return comandoSelect;

        }

        public static string NomeTabela(Type tipo)
        {
            string tabela = "";
            foreach (Attribute att in tipo.GetCustomAttributes(false))
            {
                if (att is DadosDaTabela)
                {
                    tabela = ((DadosDaTabela)att).Nome;
                    break;
                }
            }

            if (tabela == string.Empty)
                throw new Exception("Associação de Tabela não definida na Classe de Dados - " + tipo.FullName);

            return tabela;
        }

        private string PreparaComandoInsert()
        {
            Type tipo = this.GetType();
            string comandoInsert = " INSERT INTO ";
            string montaParametros = string.Empty;
            string tabela = string.Empty;

            try
            {
                #region Obtem o nome da Tabela

                foreach (Attribute att in tipo.GetCustomAttributes(false))
                {
                    if (att is DadosDaTabela)
                    {
                        tabela = ((DadosDaTabela)att).Nome;
                        break;
                    }
                }

                if (tabela == string.Empty)
                    throw new Exception("Associação de Tabela não definida na Classe de Dados - " + tipo.FullName);
                else
                    comandoInsert += tabela + "(";




                #endregion


                #region Obtem a lista de campos e as associa ao comando

                foreach (PropertyInfo propInfo in tipo.GetProperties())
                {
                    foreach (Attribute att in propInfo.GetCustomAttributes(false))
                    {
                        if (att is CampoNaTabela)
                        {
                            if (((CampoNaTabela)att).EhAuto)
                                break;

                            comandoInsert += ((CampoNaTabela)att).NomeDoCampo + ", ";
                            montaParametros += prefixoParametro + "p" + ((CampoNaTabela)att).NomeDoCampo + ", ";
                            break;
                        }
                    }
                }

                if (montaParametros == string.Empty)
                    throw new Exception("Associação de Nomes de Campos não definidas na Classe de Dados - " + tipo.FullName);
                else
                    comandoInsert = comandoInsert.Substring(0, comandoInsert.Length - 2) + ") VALUES (" + montaParametros.Substring(0, montaParametros.Length - 2) + ")";

                #endregion
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
            }

            return comandoInsert;
        }

        private string PreparaComandoDelete()
        {
            Type tipo = this.GetType();
            string comandoDelete = " DELETE FROM ";
            string montaParametros = string.Empty;
            string tabela = string.Empty;

            try
            {
                #region Obtem o nome da Tabela

                foreach (Attribute att in tipo.GetCustomAttributes(false))
                {
                    if (att is DadosDaTabela)
                    {
                        tabela = ((DadosDaTabela)att).Nome;
                        break;
                    }
                }

                if (tabela == string.Empty)
                    throw new Exception("Associação de Tabela não definida na Classe de Dados - " + tipo.FullName);
                else
                    comandoDelete += tabela + " WHERE ";

                #endregion
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
            }
            return comandoDelete;
        }

        public string PreparaComandoUpdate()
        {
            Type tipo = this.GetType();
            string comandoUpdate = " UPDATE ";
            string montaParametros = string.Empty;
            string tabela = string.Empty;

            try
            {

                #region Obtem o nome da Tabela

                foreach (Attribute att in tipo.GetCustomAttributes(false))
                {
                    if (att is DadosDaTabela)
                    {
                        tabela = ((DadosDaTabela)att).Nome;
                        break;
                    }
                }

                if (tabela == string.Empty)
                    throw new Exception("Associação de Tabela não definida na Classe de Dados - " + tipo.FullName);
                else
                    comandoUpdate += tabela + " SET ";




                #endregion
                
                #region Obtem a lista de campos e as associa ao comando

                foreach (PropertyInfo propInfo in tipo.GetProperties())
                {
                    foreach (Attribute att in propInfo.GetCustomAttributes(false))
                    {
                        if (att is CampoNaTabela)
                        {
                            //if (!((CampoNaTabela)att).EhAuto)
                            //{
                                if (!((CampoNaTabela)att).EhChave)
                                {
                                    if (((CampoNaTabela)att).EhAuto)
                                        break;

                                    comandoUpdate += ((CampoNaTabela)att).NomeDoCampo + " = ";
                                    comandoUpdate += prefixoParametro + "p" + ((CampoNaTabela)att).NomeDoCampo + ", ";
                                }
                                else
                                {
                                    if (montaParametros != string.Empty)
                                    {
                                        montaParametros += " AND ";
                                    }
                                    montaParametros += ((CampoNaTabela)att).NomeDoCampo + " = ";
                                    montaParametros += prefixoParametro + "p" + ((CampoNaTabela)att).NomeDoCampo;
                                }
                                break;
                            //}
                        }
                    }
                }

                if (comandoUpdate == " UPDATE " + tabela + " SET ")
                    throw new Exception("Associação de Nomes de Campos não definidas na Classe de Dados - " + tipo.FullName);
                else
                    comandoUpdate = comandoUpdate.Substring(0, comandoUpdate.Length - 2) + " WHERE " + montaParametros;//montaParametros.Substring(0, montaParametros.Length - 5) ;

                #endregion

            }
            catch (Exception ex)
            {
                UltimoErro = ex;
            }

            return comandoUpdate;
        }

        public string PreparaComandoUpdate(string pCustomWhere)
        {
            Type tipo = this.GetType();
            string comandoUpdate = " UPDATE ";
            string montaParametros = string.Empty;
            string tabela = string.Empty;

            try
            {

                #region Obtem o nome da Tabela

                foreach (Attribute att in tipo.GetCustomAttributes(false))
                {
                    if (att is DadosDaTabela)
                    {
                        tabela = ((DadosDaTabela)att).Nome;
                        break;
                    }
                }

                if (tabela == string.Empty)
                    throw new Exception("Associação de Tabela não definida na Classe de Dados - " + tipo.FullName);
                else
                    comandoUpdate += tabela + " SET ";




                #endregion

                #region Obtem a lista de campos e as associa ao comando

                foreach (PropertyInfo propInfo in tipo.GetProperties())
                {
                    foreach (Attribute att in propInfo.GetCustomAttributes(false))
                    {
                        if (att is CampoNaTabela)
                        {
                            if (!((CampoNaTabela)att).EhAuto)
                            {
                                if (!((CampoNaTabela)att).EhChave)
                                {
                                    comandoUpdate += ((CampoNaTabela)att).NomeDoCampo + " = ";
                                    comandoUpdate += prefixoParametro + "p" + ((CampoNaTabela)att).NomeDoCampo + ", ";
                                }
                                //else
                                //{
                                //    if (montaParametros != string.Empty)
                                //    {
                                //        montaParametros += " AND ";
                                //    }
                                //    montaParametros += ((CampoNaTabela)att).NomeDoCampo + " = ";
                                //    montaParametros += prefixoParametro + "p" + ((CampoNaTabela)att).NomeDoCampo;
                                //}
                                break;
                            }
                        }
                    }
                }

                if (comandoUpdate == " UPDATE " + tabela + " SET ")
                    throw new Exception("Associação de Nomes de Campos não definidas na Classe de Dados - " + tipo.FullName);
                else
                    comandoUpdate = comandoUpdate.Substring(0, comandoUpdate.Length - 2) + pCustomWhere;// " WHERE " + montaParametros;//montaParametros.Substring(0, montaParametros.Length - 5) ;

                #endregion

            }
            catch (Exception ex)
            {
                UltimoErro = ex;
            }

            return comandoUpdate;
        }

        private DbCommand PreparaComandoUpdateV2()
        {
            DbCommand UpdateCmd = new SqlCommand();
            DbParameter par;

            Type tipo = this.GetType();
            string comandoUpdate = " UPDATE ";
            string montaParametros = string.Empty;
            string tabela = string.Empty;

            try
            {
                #region Obtem o nome da Tabela

                foreach (Attribute att in tipo.GetCustomAttributes(false))
                {
                    if (att is DadosDaTabela)
                    {
                        tabela = ((DadosDaTabela)att).Nome;
                        break;
                    }
                }

                if (tabela == string.Empty)
                    throw new Exception("Associação de Tabela não definida na Classe de Dados - " + tipo.FullName);
                else
                    comandoUpdate += tabela + " SET ";




                #endregion

                /// Separação para pegar o SET primeiro, depois o WHERE, por causa de erro (provavelmente) causado pela ordem dos parâmetros no comando

                #region Obtem a lista de campos e as associa ao comando (ATRIBUIÇÃO)

                foreach (PropertyInfo propInfo in tipo.GetProperties())
                {
                    foreach (Attribute att in propInfo.GetCustomAttributes(false))
                    {
                        if (att is CampoNaTabela)
                        {
                            if (!((CampoNaTabela)att).EhChave)
                            {
                                comandoUpdate += ((CampoNaTabela)att).NomeDoCampo + " = ";
                                comandoUpdate += prefixoParametro + "p" + ((CampoNaTabela)att).NomeDoCampo + ", ";

                                par = new SqlParameter(prefixoParametro + "p" + ((CampoNaTabela)att).NomeDoCampo, propInfo.GetValue(this, null));

                                UpdateCmd.Parameters.Add(par);

                            }
                            /*
                        else
                        {
                            if (montaParametros != string.Empty)
                            {
                                montaParametros += " AND ";
                            }
                            montaParametros += ((CampoNaTabela)att).NomeDoCampo + " = ";
                            montaParametros += ":p" + ((CampoNaTabela)att).NomeDoCampo;

                            par = new SqlParameter(":p" + ((CampoNaTabela)att).NomeDoCampo, propInfo.GetValue(this, null));

                            UpdateCmd.Parameters.Add(par);

                        }
                             **/
                            break;
                        }
                    }
                }
                #endregion


                #region Obtem a lista de campos e as associa ao comando (RESTRIÇÃO)

                foreach (PropertyInfo propInfo in tipo.GetProperties())
                {
                    foreach (Attribute att in propInfo.GetCustomAttributes(false))
                    {
                        if (att is CampoNaTabela)
                        {
                            if (((CampoNaTabela)att).EhChave)
                            {
                                if (montaParametros != string.Empty)
                                {
                                    montaParametros += " AND ";
                                }
                                montaParametros += ((CampoNaTabela)att).NomeDoCampo + " = ";
                                montaParametros += prefixoParametro + "p" + ((CampoNaTabela)att).NomeDoCampo;

                                par = new SqlParameter(prefixoParametro + "p" + ((CampoNaTabela)att).NomeDoCampo, propInfo.GetValue(this, null));

                                UpdateCmd.Parameters.Add(par);

                            }

                            break;
                        }
                    }
                }
                #endregion



                #region Composição final do comando


                if (comandoUpdate == " UPDATE " + tabela + " SET ")
                    throw new Exception("Associação de Nomes de Campos não definidas na Classe de Dados - " + tipo.FullName);
                else
                    comandoUpdate = comandoUpdate.Substring(0, comandoUpdate.Length - 2) + " WHERE " + montaParametros;//montaParametros.Substring(0, montaParametros.Length - 5) ;

                #endregion

                UpdateCmd.CommandText = comandoUpdate;
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
            }

            return UpdateCmd;
        }

        public virtual DbDataReader ObtemDR(Type tipo)
        {
            DbDataReader listaRetorno = null;

            try
            {
                conexao.Open();

                listaRetorno = LerRegistros(new List<DbParameter>(), string.Empty, (SqlConnection)this.conexao);

                conexao.Close();
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
            }

            return listaRetorno;
        }

        public virtual List<MasterClass> ObtemRegistros(Type tipo)
        {

            List<MasterClass> listaRetorno =  new List<MasterClass>();

            try
            {
                conexao.Open();

                listaRetorno = ReaderToList(LerRegistros(new List<DbParameter>(), string.Empty, this.conexao), tipo);

                conexao.Close();
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
            }

            return listaRetorno;

        }

        public virtual List<MasterClass> ObtemRegistrosCustom(Type tipo, String comando)
        {

            List<MasterClass> listaRetorno = new List<MasterClass>();

            try
            {
                conexao.ConnectionString = localBanco;

                conexao.Open();

                listaRetorno = ReaderToList(LerRegistrosCustom(comando, this.conexao), tipo);

                conexao.Close();
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
            }

            return listaRetorno;

        }

        public virtual List<MasterClass> ObtemRegistrosFiltrados(Type tipo, string condicao)
        {

            List<MasterClass> listaRetorno = new List<MasterClass>();
            try
            {
                conexao.ConnectionString = localBanco;

                conexao.Open();

                listaRetorno = ReaderToList(LerRegistros(new List<DbParameter>(), condicao, this.conexao), tipo);

                conexao.Close();
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
            }

            return listaRetorno;

        }

        public virtual List<MasterClass> ObtemRegistrosFiltradosOrdenados(Type tipo, string condicao, string campoOrdem)
        {

            List<MasterClass> listaRetorno = new List<MasterClass>();
            try
            {
                conexao.ConnectionString = localBanco;

                conexao.Open();

                listaRetorno = ReaderToList(LerRegistros(new List<DbParameter>(), condicao, this.conexao), tipo);

                conexao.Close();
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
            }

            return listaRetorno;

        }

        public virtual List<MasterClass> ObtemRegistrosOrdenados(Type tipo, string CampoOrdem)
        {

            List<MasterClass> listaRetorno = new List<MasterClass>();

            try
            {

                conexao.Open();

                listaRetorno = ReaderToList(LerRegistrosOrdenados(new List<DbParameter>(), string.Empty, this.conexao, CampoOrdem), tipo);

                conexao.Close();
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
            }

            return listaRetorno;

        }

      
        #endregion

     

     

        public virtual void Sincroniza()
        {
            try
            {


                //Se a classe possuir o AtributoDeClasse
                foreach (Attribute att in this.GetType().GetCustomAttributes(false))
                {
                    if (att is DadosDaTabela)
                    {
                        String msg = "Efetuando sincronismo de " + ((DadosDaTabela)att).NomeEntidade;
                        RegistraLog(msg);
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
                GravaLogWithEvent("Erro em 'MasterClass.Sincroniza': " + ex.Message);
            }
          
        }

        public static void RegistraLog(string msg)
        {
            //B2B_Monitor2.Form1.Principal.TxbLog.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - " +msg + Environment.NewLine + B2B_Monitor2.Form1.Principal.TxbLog.Text;
            
            //B2B_Monitor2.Form1.Principal.TxbLog.Text += DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            //B2B_Monitor2.Form1.Principal.TxbLog.Text += msg;
            //
            MasterClass.GravaEvento(msg, EventLogEntryType.Error);

        }

        public static void LimpaLog()
        {
            String arquivoLog = AppPath + "\\" + nomeArquivoLog;

            arquivoLog = arquivoLog.Replace("file:\\", "");

            File.Delete(arquivoLog);
        }

        public static String ObtemLog()
        {
            String arquivoLog = AppPath + "\\" + nomeArquivoLog;

            arquivoLog = arquivoLog.Replace("file:\\", "");

            String conteudo = File.ReadAllText(arquivoLog);

            return conteudo;
        }

        public static void GravaLogErro(String msg)
        {
            try
            {

                if (!LogEmArquivo)
                    return;

                String arquivoLog = AppPath + "\\" + nomeArquivoLog;

                arquivoLog = arquivoLog.Replace("file:\\", "");

                if (!File.Exists(arquivoLog))
                {
                    //File.CreateText(arquivoLog);
                    String Linha = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + Environment.NewLine;
                    Linha += msg + Environment.NewLine;
                    File.WriteAllText(arquivoLog, Linha);
                }
                else
                {

                    StreamWriter log = File.AppendText(arquivoLog);

                    log.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                    log.WriteLine(msg);

                    log.Close();
                }

            }
            catch (System.IO.IOException ioEx)
            {
                UltimoErro = ioEx;
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
                //System.Windows.Forms.MessageBox.Show("Falha na gravação do arquivo de Log.");
            }

        }

        public static void GravaLogWithEvent(String msg)
        {
            try
            {
                GravaLogErro(msg);

                String nomeFonte = "IntegradorGS";

                if (!EventLog.SourceExists(nomeFonte))
                    EventLog.CreateEventSource(nomeFonte, "Application");

                EventLog.WriteEntry(nomeFonte, msg, EventLogEntryType.Warning);

                //EnviaEmail(msg);
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
            }

        }
              
        public static void GravaLogWithEventOfType(String msg, EventLogEntryType tipo)
        {
            try
            {
                GravaLogErro(msg);

                String nomeFonte = "PedidoCompraWS";

                if (!EventLog.SourceExists(nomeFonte))
                    EventLog.CreateEventSource(nomeFonte, "Application");

                EventLog.WriteEntry(nomeFonte, msg, tipo);

                //EnviaEmail(msg);
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
            }

        }

        public static void GravaEvento(String msg, EventLogEntryType tipo)
        {
            try
            {
                // GravaLog(msg);

                String nomeFonte = "PedidoCompraWS";

                if (!EventLog.SourceExists(nomeFonte))
                    EventLog.CreateEventSource(nomeFonte, "Application");

                EventLog.WriteEntry(nomeFonte, msg, tipo);

                //EnviaEmail(msg);
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
            }

        }

        public  bool TestaConexaoBanco()
        {
            try
            {
                conexao.ConnectionString = localBanco;

                if (conexao.State != System.Data.ConnectionState.Open)
                    conexao.Open();

                if (conexao.State == System.Data.ConnectionState.Open)
                {
                    conexao.Close();
                    return true;
                }
                else
                {
                    UltimoErro = new Exception("Falha ao conectar ao Banco: Não conseguiu abrir a conexão.");
                    return false;
                }               
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
                return false;
            }
        }

        public static Stream ObterStream(string s)
        {
            MemoryStream stream = new MemoryStream();
            try
            {

                StreamWriter writer = new StreamWriter(stream);
                writer.Write(s);
                writer.Flush();
                stream.Position = 0;
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
            }
            return stream;
        }

        public static String TestarConexaoBanco(String pConnectionString)
        {
            
            DbConnection conexao = new SqlConnection();

            conexao.ConnectionString = pConnectionString;

            try
            {
               conexao.Open();
               conexao.Close();

               return "OK";
            }
            catch (Exception erro)
            {
                UltimoErro = erro;
                return erro.Message;
            }
        }

        protected void ConverteCampos(MasterClass itemDestino)
        {
            try
            {
                foreach (PropertyInfo propDestino in itemDestino.GetType().GetProperties())
                {
                    foreach (CampoNaTabela campo in propDestino.GetCustomAttributes(false))
                    {
                        PropertyInfo propOrigem = this.GetType().GetProperty(propDestino.Name);
                        propDestino.SetValue(itemDestino, propOrigem.GetValue(this, null), null);
                        try//Ignora os erros de Debug.Print
                        {
                            Debug.Print(propOrigem.Name + " = " + propOrigem.GetValue(this, null).ToString());
                            Debug.Print(propDestino.Name + " = " + propDestino.GetValue(itemDestino, null).ToString());
                        }
                        catch 
                        { 
                            Debug.Print("Erro ao imprimir item");
                        }
                    }

                }
            }
            catch (Exception erro)
            {
                MasterClass.RegistraLog(erro.Message);
            }
        }

        public static DateTime StringParaData(String pData)
        {
            DateTime resposta;           

            try
            {
                if (pData == null)
                    pData = String.Empty;

                if (pData.IndexOf("-") < 0)
                {
                    if (pData.Length == 8)
                    {
                        pData = pData.Substring(0, 4) + "-" + pData.Substring(4, 2) + "-" + pData.Substring(6, 2);
                    }
                }
                else
                {
                    resposta = Convert.ToDateTime(pData);
                }

                resposta = pData == String.Empty ? DateTime.MinValue : Convert.ToDateTime(pData);
            }
            catch (Exception erro)
            {
                throw new Exception("[MASTERCLASS].[STRINGPARADATA] Falha ao converter Data(" + pData + ")" + erro.Message);
            }

            return resposta;
        }

        public static Boolean EhInteiro(String pNumero)
        {
            try
            {
                Int64.Parse(pNumero);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static Boolean EhDecimal(String pNumero)
        {
            try
            {
                Decimal.Parse(pNumero);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool ValidaString(String pString)
        {
            if (pString == null)
            {
                return false;
            }

            if (pString.Trim() == String.Empty)
            {
                return false;
            }

            return true;
        }

        public static bool ValidaData(String pString)
        {
            if (pString == null)
            {
                return false;
            }

            if (pString.Trim() == String.Empty)
            {
                return false;
            }

            try
            {
                StringParaData(pString);
                return true;
            }
            catch
            {
                return false;
            }
            
        }

        public static bool ValidaDataNaoObrigatoria(String pString)
        {
            if (pString == null)
            {
                return true;
            }

            if (pString.Trim() == String.Empty)
            {
                return true;
            }

            try
            {
                StringParaData(pString);
                return true;
            }
            catch
            {
                return false;
            }

        }

       

        

    }

}
