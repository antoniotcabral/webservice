﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("PAPEL_LOG")]
    public class PapelLogGS:MasterClass
    {

        #region Atributos

        
        private int _Codigo;
        private int _CodigoPapel;

        private string _Status;        
        private string _Motivo;          

        private DateTime _DataInicio;
        private DateTime _DataFim;
        private DateTime _DataRegistro;
        
        


        #endregion

        #region Propriedades

        

        [CampoNaTabela("CD_PAPEL_LOG", DbType.Int32,true,true)]
        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        [CampoNaTabela("CD_PAPEL", DbType.Int32)]
        public Int32 CodigoPapel
        {
            get { return _CodigoPapel; }
            set { _CodigoPapel = value; }
        }

        [CampoNaTabela("TX_STATUS", DbType.String)]
        public String Status
        {
            get { return _Status; }
            set { _Status = value; }
        }
        

        [CampoNaTabela("TX_MOTIVO", DbType.String)]
        public String Motivo
        {
            get { return _Motivo; }
            set { _Motivo = value; }
        }

        [CampoNaTabela("DT_INICIO", DbType.DateTime)]
        public DateTime DataInicio
        {
            get { return _DataInicio; }
            set { _DataInicio = value; }
        }

        [CampoNaTabela("DT_FIM", DbType.DateTime)]
        public DateTime DataFim
        {
            get { return _DataFim; }
            set { _DataFim = value; }
        }


        [CampoNaTabela("DT_REGISTRO", DbType.DateTime)]
        public DateTime DataRegistro
        {
            get { return _DataRegistro; }
            set { _DataRegistro = value; }
        }

        


        

        #endregion


    #region Métodos

        public static PapelLogGS Obtem(Int32 pCodigoPapel)
        {
            bool Achou = false;
            PapelLogGS item = new PapelLogGS();

            foreach (PapelLogGS obj in item.ObtemRegistrosFiltradosOrdenados(item.GetType(), " CD_PAPEL = '" + pCodigoPapel + "' AND DT_FIM IS NULL ", "DT_REGISTRO"))
            {
                Achou = true;
                item = obj;
                //break;
            }

            if (Achou)
                return item;
            else
                throw new Exception("[PAPELLOGGS].[OBTEM] Registro de Papel Log não encontrado (CodigoPapel: " + pCodigoPapel.ToString() + " )");
        }

        public static PapelLogGS ObtemAtivo(Int32 pCodigoPapel)
        {
            bool Achou = false;
            PapelLogGS item = new PapelLogGS();

            foreach (PapelLogGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " CD_PAPEL = '" + pCodigoPapel + "' AND DT_INICIO < GETDATE() AND (DT_FIM IS NULL OR DT_FIM > GETDATE()) AND TX_STATUS = 'Ativo' "))
            {
                Achou = true;
                item = obj;
                break;
            }

            if (Achou)
                return item;
            else
                throw new Exception("[PAPELLOGGS].[OBTEM] Registro de Papel Log não encontrado (CodigoPapel: " + pCodigoPapel.ToString() + " )");
        }

        public static Boolean ExistePapelLog(Int32 pCodigoPapel)
        {
           
            PapelLogGS item = new PapelLogGS();

            foreach (PapelLogGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " CD_PAPEL = '" + pCodigoPapel + "' AND DT_FIM IS NULL "))
            {
                return true;
            }

            return false;
        }

        public static bool InativaRegistros(Int32 pCodigoPapel)
        {
            try
            {
                Boolean resposta = true;
                PapelLogGS papel = new PapelLogGS();

                String cmd = "UPDATE PAPEL_LOG SET DT_FIM = GETDATE() WHERE CD_PAPEL = " + pCodigoPapel;

                resposta = papel.ExecutaComando(cmd);
                if (!resposta)
                {
                    MasterClass.UltimoErro = new Exception("Falha ao inativar registros de Papel Log");
                }


                return resposta;
            }
            catch (Exception erro)
            {
                MasterClass.UltimoErro = erro;
                return false;
            }
        }


    #endregion



    }
}
