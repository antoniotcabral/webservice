﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("CARGO")]
    public class CargoGS:MasterClass
    {

        #region Atributos

        
        private int _Codigo;

        private string _Nome;        
        private string _Descricao;

        private bool _Ativo;

        private DateTime _DataRegistro;
        private DateTime _DataDesativacao;

        private int _CodigoSap;


        #endregion

        #region Propriedades

        

        [CampoNaTabela("CD_CARGO", DbType.Int32,true,true)]
        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        [CampoNaTabela("TX_NOME", DbType.String)]
        public String Nome
        {
            get { return _Nome; }
            set { _Nome = value; }
        }
        

        [CampoNaTabela("TX_DESCRICAO", DbType.String)]
        public String Descricao
        {
            get { return _Descricao; }
            set { _Descricao = value; }
        }

        [CampoNaTabela("BL_ATIVO", DbType.Boolean)]
        public Boolean Ativo
        {
            get { return _Ativo; }
            set { _Ativo = value; }
        }


        [CampoNaTabela("DT_REGISTRO", DbType.DateTime)]
        public DateTime DataRegistro
        {
            get { return _DataRegistro; }
            set { _DataRegistro = value; }
        }

        [CampoNaTabela("DT_DESATIVACAO", DbType.DateTime)]
        public DateTime DataDesativacao
        {
            get { return _DataDesativacao; }
            set { _DataDesativacao = value; }
        }

        [CampoNaTabela("CD_CARGO_SAP", DbType.Int32)]
        public Int32 CodigoSap
        {
            get { return _CodigoSap; }
            set { _CodigoSap = value; }
        }

        

        #endregion

        #region Métodos

        public static CargoGS Obtem(string pNomeCargo)
        {
            CargoGS item = new CargoGS();
            //List<CargoGS> itens = new List<CargoGS>();
            bool Achou = false;

            try
            {
                //Avaliar o nome como passado
                foreach (CargoGS obj in item.ObtemRegistrosFiltrados(item.GetType(), "  TX_NOME  = '" + pNomeCargo + "'"))
                {
                    item = obj;
                    Achou = true;
                    break;
                }



            }
            catch (Exception erro)
            {
                throw new Exception("[CARGO].[OBTEM] Falha ao obter o Cargo com nome  " + pNomeCargo + ":" + erro.Message);
            }

            if (Achou)
                return item;
            else
                return null;
        }

        public static CargoGS Obtem(int pCodigoSap)
        {
            CargoGS item = new CargoGS();
            //List<CargoGS> itens = new List<CargoGS>();
            bool Achou = false;

            try
            {
                //Avaliar o nome como passado
                foreach (CargoGS obj in item.ObtemRegistrosFiltrados(item.GetType(), "  CD_CARGO_SAP  = " + pCodigoSap ))
                {
                    item = obj;
                    Achou = true;
                    break;
                }



            }
            catch (Exception erro)
            {
                throw new Exception("[CARGO].[OBTEM] Falha ao obter o Cargo com código  " + pCodigoSap  + ":" + erro.Message);
            }

            if (Achou)
                return item;
            else
                return null;
        }

        #endregion


    }
}
