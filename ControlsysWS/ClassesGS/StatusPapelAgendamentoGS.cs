﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("STATUS_PAPELAGENDAMENTO")]
    public class StatusPapelAgendamentoGS:MasterClass
    {

        #region Atributos

        private int _Codigo;
        private int _CodigoPapelAgendamento;
        
        private DateTime _DataRegistro;

        private string _Observacao;
        private string _Status;

        #endregion

        #region Propriedades


        [CampoNaTabela("CD_STATUS_PAPELAGENDAMENTO", DbType.Int32, true, true)]
        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        [CampoNaTabela("CD_PAPEL_AGENDAMENTO", DbType.Int32)]
        public Int32 CodigoPapelAgendamento
        {
            get { return _CodigoPapelAgendamento; }
            set { _CodigoPapelAgendamento = value; }
        }

        [CampoNaTabela("DT_REGISTRO", DbType.DateTime)]
        public DateTime DataRegistro
        {
            get { return _DataRegistro; }
            set { _DataRegistro = value; }
        }

        [CampoNaTabela("TX_OBS", DbType.String)]
        public String Observacao
        {
            get { return _Observacao; }
            set { _Observacao = value; }
        }

        [CampoNaTabela("TX_STATUS", DbType.String)]
        public String Status
        {
            get { return _Status; }
            set { _Status = value; }
        }


        

        #endregion

        #region Métodos

        public static StatusPapelAgendamentoGS Obtem(Int32 pCodigoPapelAgendamento)
        {
            StatusPapelAgendamentoGS item = new StatusPapelAgendamentoGS();
            Boolean Achou = false;


            //Avaliar o nome como passado
            foreach (StatusPapelAgendamentoGS obj in item.ObtemRegistrosFiltradosOrdenados(item.GetType(), " CD_PAPEL_AGENDAMENTO = " + pCodigoPapelAgendamento, "DT_REGISTRO"))
            {
                Achou = true;
                item = obj;                
            }

            if (Achou)
                return item;
            else
                throw new Exception("[PAPELAGENDAMENTO].[OBTEM] Registro de Status Papel Agendamento não encontrado (" + pCodigoPapelAgendamento.ToString() + ")");
        }

        public static StatusPapelAgendamentoGS Obtem(String pCpf)
        {
            StatusPapelAgendamentoGS item = new StatusPapelAgendamentoGS();
            Boolean Achou = false;


            String condicao = "CD_PAPEL_AGENDAMENTO IN (SELECT CD_PAPEL_AGENDAMENTO FROM PAPEL_AGENDAMENTO WHERE CD_PESSOA_AGENDAMENTO IN (SELECT CD_PESSOA_AGENDAMENTO FROM PESSOA_AGENDAMENTO WHERE TX_CPF = '"+pCpf +"'))";

            //Avaliar o nome como passado
            foreach (StatusPapelAgendamentoGS obj in item.ObtemRegistrosFiltradosOrdenados(item.GetType(), condicao, "DT_REGISTRO"))
            {
                Achou = true;
                item = obj;
            }

            if (Achou)
                return item;
            else
                return null; //throw new Exception("[PAPELAGENDAMENTO].[OBTEM] Registro de Status Papel Agendamento não encontrado (Cpf:" + pCpf.ToString() + ")");
        }

        #endregion



    }
}
