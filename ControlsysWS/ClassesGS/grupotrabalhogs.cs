﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("GRUPO_TRABALHO")]
    public class GrupoTrabalhoGS:MasterClass
    {

        #region Atributos

        
        private int _Codigo;
               
        private string _Nome;
        
        private bool _Ativo;

        private DateTime _DataRegistro;
        private DateTime _DataDesativacao;

        private String _NumeroPht;


        #endregion

        #region Propriedades

        

        [CampoNaTabela("CD_GRUPO_TRABALHO", DbType.Int32,true, true)]
        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }        

        [CampoNaTabela("TX_NOME", DbType.String)]
        public String Nome
        {
            get { return _Nome; }
            set { _Nome = value; }
        }


        [CampoNaTabela("BL_ATIVO", DbType.Boolean)]
        public Boolean Ativo
        {
            get { return _Ativo; }
            set { _Ativo = value; }
        }


        [CampoNaTabela("DT_REGISTRO", DbType.DateTime)]
        public DateTime DataRegistro
        {
            get { return _DataRegistro; }
            set { _DataRegistro = value; }
        }

        [CampoNaTabela("DT_DESATIVACAO", DbType.DateTime)]
        public DateTime DataDesativacao
        {
            get { return _DataDesativacao; }
            set { _DataDesativacao = value; }
        }

        [CampoNaTabela("NU_PHT", DbType.String)]
        public String NumeroPht
        {
            get { return _NumeroPht; }
            set { _NumeroPht = value; }
        }

        

        #endregion

        #region Métodos

        public static GrupoTrabalhoGS Obtem(DateTime pData)
        {
            GrupoTrabalhoGS item = new GrupoTrabalhoGS();
            Boolean Achou = false;


            //Avaliar o nome como passado
            foreach (GrupoTrabalhoGS obj in item.ObtemRegistrosFiltrados(item.GetType(), "  CONVERT(VARCHAR, DT_REGISTRO,120) = '" + pData.ToString("yyyy-MM-dd HH:mm:ss") + "'"))
            {
                Achou = true;
                item = obj;
                break;
            }

            if (Achou)
                return item;
            else
                throw new Exception("[GRUPOTRABALHO].[OBTEM] Registro de Grupo de Trabalho não encontrado (" + pData.ToString() + ")");
        }

        public static GrupoTrabalhoGS Obtem(string pCodigo)
        {
            GrupoTrabalhoGS item = new GrupoTrabalhoGS();
            Boolean Achou = false;


            //Avaliar o nome como passado
            foreach (GrupoTrabalhoGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " CD_GRUPO_TRABALHO = " + pCodigo.ToString()))
            {
                Achou = true;
                item = obj;
                break;
            }

            if (Achou)
                return item;
            else
                throw new Exception("[GRUPOTRABALHO].[OBTEM] Registro de Grupo de Trabalho não encontrado (" + pCodigo.ToString() + ")");
        }

        public static GrupoTrabalhoGS ObtemPorNome(string pNome)
        {
            GrupoTrabalhoGS item = new GrupoTrabalhoGS();
            Boolean Achou = false;


            //Avaliar o nome como passado
            foreach (GrupoTrabalhoGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " TX_NOME = '" + pNome + "'"))
            {
                Achou = true;
                item = obj;
                break;
            }

            if (Achou)
                return item;
            else
                throw new Exception("[GRUPOTRABALHO].[OBTEM] Registro de Grupo de Trabalho não encontrado (" + pNome + ")");
        }

        public static GrupoTrabalhoGS ObtemPorTurno(Int32 pCodigoTurno)
        {
            GrupoTrabalhoGS item = new GrupoTrabalhoGS();
            Boolean Achou = false;
            GrupoTrabalhoTurnoGS grupo = new GrupoTrabalhoTurnoGS();

            foreach(GrupoTrabalhoTurnoGS g in grupo.ObtemRegistrosFiltrados(grupo.GetType()," CD_TURNO = " + pCodigoTurno))
            {
                grupo = g;
                Achou = true;
                break;
            }

            if(!Achou)
            {
                throw new Exception("[GRUPOTRABALHO].[OBTEMPORTURNO] Falha ao obter o registro de GrupoTrabalho. (" + pCodigoTurno.ToString() + ")");
            }
            
            //Avaliar o nome como passado
            foreach (GrupoTrabalhoGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " CD_GRUPO_TRABALHO = " + grupo.CodigoGrupoTrabalho))
            {
                Achou = true;
                item = obj;
                break;
            }

            if (Achou)
                return item;
            else
                throw new Exception("[GRUPOTRABALHO].[OBTEMPORTURNO] Registro de Grupo de Trabalho não encontrado (" + pCodigoTurno.ToString() + ")");
        }

        #endregion



    }
}
