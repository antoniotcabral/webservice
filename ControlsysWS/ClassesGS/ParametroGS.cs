﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("PARAMETRO")]
    public class ParametroGS:MasterClass
    {

        #region Atributos

        
        private int _Codigo;
        private string _Valor;
        private string _Nome;
        private string _Tipo;
        private string _Url;


        #endregion

        #region Propriedades

        

        [CampoNaTabela("CD_PARAMETRO", DbType.Int32,true)]
        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        [CampoNaTabela("TX_VALOR", DbType.String)]
        public String Valor
        {
            get { return _Valor; }
            set { _Valor = value; }
        }

        [CampoNaTabela("TX_NOME", DbType.String)]
        public String Nome
        {
            get { return _Nome; }
            set { _Nome = value; }
        }


        [CampoNaTabela("TX_TIPO", DbType.String)]
        public String Tipo
        {
            get { return _Tipo; }
            set { _Tipo = value; }
        }

        [CampoNaTabela("TX_URL", DbType.String)]
        public String Url
        {
            get { return _Url; }
            set { _Url = value; }
        }

        

        #endregion

        public static ParametroGS Obtem(String pNome)
        {
            bool Achou = false;
            ParametroGS item = new ParametroGS();

            foreach (ParametroGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " TX_NOME = '" + pNome + "'"))
            {
                Achou = true;
                item = obj;
                break;
            }

            if (Achou)
                return item;
            else
                throw new Exception("[PARAMETRO].[OBTEM] Registro de Parâmetro não encontrado (" + pNome + ")");
        
        }

        

    }
}
