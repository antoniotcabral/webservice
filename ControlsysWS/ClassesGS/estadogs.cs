﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("ESTADO")]
    public class EstadoGS:MasterClass
    {

        #region Atributos

        
        private int _Codigo;
        private int _CodigoPais;

        
        private string _Nome;
        private string _Sigla;
        
        private bool _Ativo;

        private DateTime _DataRegistro;
        private DateTime _DataDesativacao;


        #endregion

        #region Propriedades

        

        [CampoNaTabela("CD_ESTADO", DbType.Int32,true)]
        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        [CampoNaTabela("CD_PAIS", DbType.Int32)]
        public Int32 CodigoPais
        {
            get { return _CodigoPais; }
            set { _CodigoPais = value; }
        }

        [CampoNaTabela("TX_ESTADO", DbType.String)]
        public String Nome
        {
            get { return _Nome; }
            set { _Nome = value; }
        }

        [CampoNaTabela("TX_UF", DbType.String)]
        public String Sigla
        {
            get { return _Sigla; }
            set { _Sigla = value; }
        }


        [CampoNaTabela("BL_ATIVO", DbType.Boolean)]
        public Boolean Ativo
        {
            get { return _Ativo; }
            set { _Ativo = value; }
        }


        [CampoNaTabela("DT_REGISTRO", DbType.DateTime)]
        public DateTime DataRegistro
        {
            get { return _DataRegistro; }
            set { _DataRegistro = value; }
        }

        [CampoNaTabela("DT_DESATIVACAO", DbType.DateTime)]
        public DateTime DataDesativacao
        {
            get { return _DataDesativacao; }
            set { _DataDesativacao = value; }
        }


        

        #endregion

        #region Método

        public static EstadoGS Obtem(int pCodigo)
        {
            bool Achou = false;
            EstadoGS item = new EstadoGS();


            try
            {
                //Avaliar o nome como passado
                foreach (EstadoGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " CD_ESTADO  = " + pCodigo.ToString()))
                {
                    Achou = true;
                    item = obj;
                    break;
                }

                if (!Achou)
                {
                    throw new Exception("[ESTADO].[OBTEM] Estado inexistente(Código: " + pCodigo.ToString() + ")!");
                }

            }
            catch (Exception erro)
            {
                throw new Exception("[ESTADO].[OBTEM] Falha ao obter o Estado com o Código " + pCodigo.ToString() + " - " + erro.Message);
            }


            return item;
        }

        public static EstadoGS Obtem(string pNome, int pCodigoPais)
        {
            EstadoGS item = new EstadoGS();
            List<EstadoGS> itens = new List<EstadoGS>();

            try
            {
                //Avaliar o nome como passado
                foreach (EstadoGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " TX_UF  = '" + pNome + "' AND CD_PAIS = " + pCodigoPais))
                {
                    itens.Add(obj);
                }

                //Não encontrou Estado com o nome igual, procurando com nome parecido
                if (itens.Count == 0)
                {
                    foreach (EstadoGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " TX_NOME LIKE = '%" + pNome + "%' AND CD_PAIS = " + pCodigoPais))
                    {
                        itens.Add(obj);
                    }

                    if (itens.Count == 0)
                    {
                        throw new Exception("Estado não encontrado!");
                    }
                    else if (itens.Count > 1)
                    {
                        throw new Exception("Encontrado mais de 1 Estado com a descrição passada!");
                    }
                    else if (itens.Count == 1)
                    {
                        item = itens[0];
                    }
                }
                else if (itens.Count > 1)
                {
                    throw new Exception("Encontrado mais de 1 Estado com a descrição passada!");
                }
                else if (itens.Count == 1)
                {
                    item = itens[0];
                }

            }
            catch (Exception erro)
            {
                throw new Exception("[ESTADO].[OBTEM] Falha ao obter o Estado com a descrição " + pNome + "("+pCodigoPais.ToString()+"): " + erro.Message);
            }


            return item;
        }

        public static EstadoGS Obtem(string pSigla)
        {
            bool Achou = false;
            EstadoGS item = new EstadoGS();
            

            try
            {
                //Avaliar o nome como passado
                foreach (EstadoGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " TX_UF  = '" + pSigla + "'" ))
                {
                    Achou = true;
                    item = obj;
                    break;
                }

                if (!Achou)
                {
                    throw new Exception("[ESTADO].[OBTEM] Estado inexistente(" + pSigla + ")!"); 
                }

            }
            catch (Exception erro)
            {
                throw new Exception("[ESTADO].[OBTEM] Falha ao obter o Estado com a sigla " + pSigla + " - " + erro.Message);
            }


            return item;
        }

        #endregion



    }
}
