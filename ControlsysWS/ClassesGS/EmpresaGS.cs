﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("EMPRESA")]
    public class EmpresaGS:MasterClass
    {

        #region Atributos

        private string _Codigo;
        private string _Cnpj;
        private string _InscricaoEstadual;
        private string _InscricaoMunicipal;
        private bool _calcularFTE;
        private bool _Ativo;
        private Int64 _CodigoEmpresadoSAP;

        private DateTime _DataDesativacao;

        private bool _exibirRelFTE;
        private bool _bloqColabSemPedido;

        #endregion

        #region Propriedades

        [CampoNaTabela("CD_PESSOA", DbType.String,true,false)]
        public String Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }
        

        [CampoNaTabela("TX_CNPJ", DbType.String)]
        public String Cnpj
        {
            get { return _Cnpj; }
            set { _Cnpj = value; }
        }

        [CampoNaTabela("TX_IESTADUAL", DbType.String)]
        public String InscricaoEstadual
        {
            get { return _InscricaoEstadual; }
            set { _InscricaoEstadual = value; }
        }

        [CampoNaTabela("TX_IMUNICIPAL", DbType.String)]
        public String InscricaoMunicipal
        {
            get { return _InscricaoMunicipal; }
            set { _InscricaoMunicipal = value; }
        }

        [CampoNaTabela("BL_ATIVO", DbType.Boolean)]
        public Boolean Ativo
        {
            get { return _Ativo; }
            set { _Ativo = value; }
        }

        [CampoNaTabela("DT_DESATIVACAO", DbType.DateTime)]
        public DateTime DataDesativacao
        {
            get { return _DataDesativacao; }
            set { _DataDesativacao = value; }
        }

        [CampoNaTabela("BL_CALCULAR_FTE", DbType.Boolean)]
        public bool CalcularFTE
        {
            get { return this._calcularFTE; }
            set { this._calcularFTE = value; }
        }

        [CampoNaTabela("BL_EXIBIR_REL_FTE", DbType.Boolean)]
        public bool ExibirRelFTE
        {
            get { return this._exibirRelFTE; }
            set { this._exibirRelFTE = value; }
        }

        [CampoNaTabela("BL_BLOQ_COLAB_SEM_PEDIDO", DbType.Boolean)]
        public bool BloqColabSemPedido
        {
            get { return this._bloqColabSemPedido; }
            set { this._bloqColabSemPedido = value; }
        }

        [CampoNaTabela("CD_EMPRESA_SAP", DbType.Int64)]
        public Int64 CodigoEmpresadoSAP
        {
            get { return _CodigoEmpresadoSAP; }
            set { _CodigoEmpresadoSAP = value; }
        }
        

        #endregion

        #region Métodos

        public static EmpresaGS Obtem(String pCnpj)
        {
            bool Achou = false;
            EmpresaGS item = new EmpresaGS();

            foreach (EmpresaGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " TX_CNPJ = '" + pCnpj +"'"))
            {
                Achou = true;
                item = obj;
                break;
            }

            if(Achou)
                return item;
            else
                throw new Exception("[EMPRESA].[OBTEM] Registro de empresa não encontrado ("+pCnpj+")");
        }

        public static EmpresaGS ObtemPorCodigoEmpresadoSAP(String pCodigoEmpresadoSAP)
        {
            bool Achou = false;
            EmpresaGS item = new EmpresaGS();

            foreach (EmpresaGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " CD_EMPRESA_SAP = '" + pCodigoEmpresadoSAP + "'"))
            {
                Achou = true;
                item = obj;
                break;
            }

            if (Achou)
                return item;
            else
                throw new Exception("[EMPRESA].[OBTEM] Registro de empresa não encontrado (" + pCodigoEmpresadoSAP + ")");
        }


        public static Boolean Existe(String pCnpj)
        {
            bool Achou = false;
            EmpresaGS item = new EmpresaGS();

            foreach (EmpresaGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " TX_CNPJ = '" + pCnpj + "'"))
            {
                Achou = true;                
                break;
            }

            return Achou;
        }

        #endregion

    }
}
