﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("PAPEL")]
    public class PapelGS:MasterClass
    {

        #region Atributos

        private int _Codigo;
        private string _CodigoPessoa;
        private int _CodigoCargo;
        //private int _CodigoModelo;
        private int _CodigoArea;

        //private string _Senha;

        private bool _EmAnalise;
        private bool _Juridico;
        private bool _PassBack;

        private DateTime _DataRegistro;

        private bool _Supervisor;
        private bool _Supervisionado;

        private int _CodigoSap;



        #endregion

        #region Propriedades

        [CampoNaTabela("CD_PAPEL", DbType.Int32,true,true)]
        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        [CampoNaTabela("CD_PESSOA_FISICA", DbType.String)]
        public String CodigoPessoaFisica
        {
            get { return _CodigoPessoa; }
            set { _CodigoPessoa = value; }
        }

        [CampoNaTabela("CD_CARGO", DbType.Int32)]
        public Int32 CodigoCargo
        {
            get { return _CodigoCargo; }
            set { _CodigoCargo = value; }
        }

        [CampoNaTabela("CD_AREA", DbType.Int32)]
        public Int32 CodigoArea
        {
            get { return _CodigoArea; }
            set { _CodigoArea = value; }
        }

        //[CampoNaTabela("CD_MODELO", DbType.Int32)]
        //public Int32 CodigoModelo
        //{
        //    get { return _CodigoModelo; }
        //    set { _CodigoModelo = value; }
        //}

        //[CampoNaTabela("TX_SENHA", DbType.String)]
        //public String Senha
        //{
        //    get { return _Senha; }
        //    set { _Senha = value; }
        //}


        [CampoNaTabela("BL_SOB_ANALISE", DbType.Boolean)]
        public Boolean EmAnalise
        {
            get { return _EmAnalise; }
            set { _EmAnalise = value; }
        }

        [CampoNaTabela("BL_JURIDICO", DbType.Boolean)]
        public Boolean Juridico
        {
            get { return _Juridico; }
            set { _Juridico = value; }
        }

        [CampoNaTabela("BL_PASSBACK", DbType.Boolean)]
        public Boolean Passback
        {
            get { return _PassBack; }
            set { _PassBack = value; }
        }

        [CampoNaTabela("DT_REGISTRO", DbType.DateTime)]
        public DateTime DataRegistro
        {
            get { return _DataRegistro; }
            set { _DataRegistro = value; }
        }

        [CampoNaTabela("BL_SUPERVISOR", DbType.Boolean)]
        public Boolean Supervisor
        {
            get { return _Supervisor; }
            set { _Supervisor = value; }
        }

        [CampoNaTabela("BL_SUPERVISIONADO", DbType.Boolean)]
        public Boolean Supervisionado
        {
            get { return _Supervisionado; }
            set { _Supervisionado = value; }
        }

        [CampoNaTabela("CD_EMPREGADO_SAP", DbType.Int32)]
        public Int32 CodigoSap
        {
            get { return _CodigoSap; }
            set { _CodigoSap = value; }
        }

        #endregion


        #region Métodos
        public static PapelGS Obtem(String pCPF)
        {
            bool Achou = false;
            PapelGS item = new PapelGS();
            String Cnpj = System.Configuration.ConfigurationSettings.AppSettings["CnpjParameto"];


            /* String ComandoObtemAtivo = @"SELECT P.CD_PESSOA_FISICA, P.CD_PAPEL
                                                                                   ,P.CD_AREA 
                                                                                   ,P.CD_CARGO
                                                                                   ,P.CD_MODELO
                                                                                   ,P.TX_SENHA
                                                                                   ,P.BL_SOB_ANALISE
                                                                                   ,P.BL_JURIDICO
                                                                                   ,P.BL_PASSBACK
                                                                                   ,P.DT_REGISTRO
                                                                                   ,P.BL_SUPERVISOR
                                                                                   ,P.BL_SUPERVISIONADO
                                                                                 FROM COLABORADOR C
                                                                                 INNER JOIN PAPEL P ON P.CD_PAPEL = C.CD_PAPEL
                                                                                 INNER JOIN dbo.PESSOA_FISICA pf ON pf.CD_PESSOA_FISICA = P.CD_PESSOA_FISICA AND
                                                                                      pf.TX_CPF = '" + pCPF + @"'
                                                                                 INNER JOIN PESSOA PES ON PES.CD_PESSOA = P.CD_PESSOA_FISICA
                                                                                 INNER JOIN (
                                                                                     SELECT MAX(PL.CD_PAPEL_LOG) AS MAX_CD_PAPEL_LOG, PL.CD_PAPEL
                                                                                     FROM PAPEL_LOG PL
                                                                                     GROUP BY PL.CD_PAPEL
                                                                                 ) MAX_PL ON MAX_PL.CD_PAPEL = C.CD_PAPEL
                                                                                 INNER JOIN PAPEL_LOG PL ON PL.CD_PAPEL_LOG = MAX_PL.MAX_CD_PAPEL_LOG
                                                                                 WHERE EXISTS(SELECT MAX(C1.CD_PAPEL)
                                                                                              FROM COLABORADOR C1
                                                                                              INNER JOIN PAPEL P1 ON P1.CD_PAPEL = C1.CD_PAPEL
                                                                                              GROUP BY P1.CD_PESSOA_FISICA
                                                                                              HAVING MAX(C1.CD_PAPEL) = C.CD_PAPEL)
                                                                                     AND (PL.DT_FIM IS NULL OR PL.DT_FIM > CONVERT(DATE, GETDATE()))
                                                                                     AND (PL.DT_INICIO <= CONVERT(DATE, GETDATE()))";*/

            String ComandoObtemAtivo = @"select top 1 *
                                                                        from(
                                                                            select    pa.*, 1 prioridade 
                                                                            from      PESSOA_FISICA pf
                                                                            inner join papel pa on pf.CD_PESSOA_FISICA = pa.CD_PESSOA_FISICA
                                                                            inner join COLABORADOR co on pa.CD_PAPEL = co.CD_PAPEL
                                                                            inner join (select max(CD_PAPEL_LOG) cd_papel_log, ipl.cd_papel from PAPEL_LOG ipl group by ipl.cd_papel) max_pl on co.CD_PAPEL = max_pl.CD_PAPEL
                                                                            inner join papel_log pl on max_pl.cd_papel_log = pl.CD_PAPEL_LOG 
                                                                            where  1 = 1
                                                                              and  pf.TX_CPF =  '" + pCPF + @"'
                                                                              and  co.CD_EMPRESA = (select em.cd_pessoa from EMPRESA em where em.TX_CNPJ = '" + Cnpj + @"')
                                                                              and  pl.TX_STATUS like 'Ativo'
                                                                    union all
                                                                            select    pa.*, 2 prioridade
                                                                            from      PESSOA_FISICA pf
                                                                            inner join papel pa on pf.CD_PESSOA_FISICA = pa.CD_PESSOA_FISICA
                                                                            inner join COLABORADOR co on pa.CD_PAPEL = co.CD_PAPEL
                                                                            inner join (select max(CD_PAPEL_LOG) cd_papel_log, ipl.cd_papel from PAPEL_LOG ipl group by ipl.cd_papel) max_pl on co.CD_PAPEL = max_pl.CD_PAPEL
                                                                            inner join papel_log pl on max_pl.cd_papel_log = pl.CD_PAPEL_LOG 
                                                                            where  1 = 1
                                                                            and pf.TX_CPF =  '" + pCPF + @"'
                                                                            and co.CD_EMPRESA = (select em.cd_pessoa from EMPRESA em where em.TX_CNPJ ='" + Cnpj + @"')
                                                                            and pl.TX_STATUS in ('Ferias', 'LicencaMedica','Afastamento')
                                                                    union all
                                                                            select    pa.*, 3 prioridade
                                                                            from      PESSOA_FISICA pf
                                                                            inner join papel pa on pf.CD_PESSOA_FISICA = pa.CD_PESSOA_FISICA
                                                                            inner join COLABORADOR co on pa.CD_PAPEL = co.CD_PAPEL
                                                                            inner join (select max(CD_PAPEL_LOG) cd_papel_log, ipl.cd_papel from PAPEL_LOG ipl group by ipl.cd_papel) max_pl on co.CD_PAPEL = max_pl.CD_PAPEL
                                                                            inner join papel_log pl on max_pl.cd_papel_log = pl.CD_PAPEL_LOG 
                                                                            where  1 = 1
                                                                            and pf.TX_CPF = '" + pCPF + @"'
                                                                            and co.CD_EMPRESA = (select em.cd_pessoa from EMPRESA em where em.TX_CNPJ = '" + Cnpj + @"')
                                                                            and pl.TX_STATUS like 'Inativacao'
                                                                    ) tabela
                                                                    order by prioridade, DT_REGISTRO desc";


            foreach (PapelGS obj in item.ObtemRegistrosCustom(item.GetType(), ComandoObtemAtivo))
            {
                Achou = true;
                item = obj;
                break;
            }

            if (Achou)
                return item;
            else
                throw new Exception("[PESSOAFISICA].[OBTEM] Registro de Pessoa não encontrado (" + pCPF + ")");
        }

        public static PapelGS ObtemCpfPassaporte(String pCPF, String pPassaporte = null, bool pRetornaNulo = false)
        {
            bool Achou = false;
            PapelGS item = new PapelGS();
            String Cnpj = System.Configuration.ConfigurationSettings.AppSettings["CnpjParameto"];


            /* String ComandoObtemAtivo = @"SELECT P.CD_PESSOA_FISICA, P.CD_PAPEL
                                                                                   ,P.CD_AREA 
                                                                                   ,P.CD_CARGO
                                                                                   ,P.CD_MODELO
                                                                                   ,P.TX_SENHA
                                                                                   ,P.BL_SOB_ANALISE
                                                                                   ,P.BL_JURIDICO
                                                                                   ,P.BL_PASSBACK
                                                                                   ,P.DT_REGISTRO
                                                                                   ,P.BL_SUPERVISOR
                                                                                   ,P.BL_SUPERVISIONADO
                                                                                 FROM COLABORADOR C
                                                                                 INNER JOIN PAPEL P ON P.CD_PAPEL = C.CD_PAPEL
                                                                                 INNER JOIN dbo.PESSOA_FISICA pf ON pf.CD_PESSOA_FISICA = P.CD_PESSOA_FISICA AND
                                                                                      pf.TX_CPF = '" + pCPF + @"'
                                                                                 INNER JOIN PESSOA PES ON PES.CD_PESSOA = P.CD_PESSOA_FISICA
                                                                                 INNER JOIN (
                                                                                     SELECT MAX(PL.CD_PAPEL_LOG) AS MAX_CD_PAPEL_LOG, PL.CD_PAPEL
                                                                                     FROM PAPEL_LOG PL
                                                                                     GROUP BY PL.CD_PAPEL
                                                                                 ) MAX_PL ON MAX_PL.CD_PAPEL = C.CD_PAPEL
                                                                                 INNER JOIN PAPEL_LOG PL ON PL.CD_PAPEL_LOG = MAX_PL.MAX_CD_PAPEL_LOG
                                                                                 WHERE EXISTS(SELECT MAX(C1.CD_PAPEL)
                                                                                              FROM COLABORADOR C1
                                                                                              INNER JOIN PAPEL P1 ON P1.CD_PAPEL = C1.CD_PAPEL
                                                                                              GROUP BY P1.CD_PESSOA_FISICA
                                                                                              HAVING MAX(C1.CD_PAPEL) = C.CD_PAPEL)
                                                                                     AND (PL.DT_FIM IS NULL OR PL.DT_FIM > CONVERT(DATE, GETDATE()))
                                                                                     AND (PL.DT_INICIO <= CONVERT(DATE, GETDATE()))";*/

            String ComandoObtemAtivo = @"select top 1 *
                                                                        from(
                                                                            select    pa.*, 1 prioridade 
                                                                            from      PESSOA_FISICA pf WITH (NOLOCK)
                                                                            inner join papel pa WITH (NOLOCK) on pf.CD_PESSOA_FISICA = pa.CD_PESSOA_FISICA
                                                                            inner join COLABORADOR co WITH (NOLOCK) on pa.CD_PAPEL = co.CD_PAPEL
                                                                            inner join (select max(CD_PAPEL_LOG) cd_papel_log, ipl.cd_papel from PAPEL_LOG ipl WITH (NOLOCK) group by ipl.cd_papel) max_pl on co.CD_PAPEL = max_pl.CD_PAPEL
                                                                            inner join papel_log pl WITH (NOLOCK) on max_pl.cd_papel_log = pl.CD_PAPEL_LOG 
                                                                            where  1 = 1 ";
            if (!String.IsNullOrEmpty(pCPF))
            {
                ComandoObtemAtivo += "                                        and  ( pf.TX_CPF =  '" + pCPF + @"' ) ";
            }
            else
            {
                ComandoObtemAtivo += "                                        and  ( pf.TX_PASSAPORTE =  '" + pPassaporte + @"' And pf.TX_PASSAPORTE <> ''  And pf.TX_PASSAPORTE is Not Null) ";
            }
            //ComandoObtemAtivo += "                                          and  co.CD_EMPRESA = (select em.cd_pessoa from EMPRESA em where em.TX_CNPJ = '" + Cnpj + @"')
            ComandoObtemAtivo += @"                                           and  pl.TX_STATUS like 'Ativo'
                                                                    union all
                                                                            select    pa.*, 2 prioridade
                                                                            from      PESSOA_FISICA pf WITH (NOLOCK)
                                                                            inner join papel pa WITH (NOLOCK) on pf.CD_PESSOA_FISICA = pa.CD_PESSOA_FISICA
                                                                            inner join COLABORADOR co WITH (NOLOCK) on pa.CD_PAPEL = co.CD_PAPEL
                                                                            inner join (select max(CD_PAPEL_LOG) cd_papel_log, ipl.cd_papel from PAPEL_LOG ipl WITH (NOLOCK) group by ipl.cd_papel) max_pl on co.CD_PAPEL = max_pl.CD_PAPEL
                                                                            inner join papel_log pl WITH (NOLOCK) on max_pl.cd_papel_log = pl.CD_PAPEL_LOG 
                                                                            where  1 = 1 ";
            if (!String.IsNullOrEmpty(pCPF))
            {
                ComandoObtemAtivo += "                                        and  ( pf.TX_CPF =  '" + pCPF + @"' ) ";
            }
            else
            {
                ComandoObtemAtivo += "                                        and  ( pf.TX_PASSAPORTE =  '" + pPassaporte + @"' And pf.TX_PASSAPORTE <> ''  And pf.TX_PASSAPORTE is Not Null) ";
            }
            //ComandoObtemAtivo += "                                          and co.CD_EMPRESA = (select em.cd_pessoa from EMPRESA em where em.TX_CNPJ ='" + Cnpj + @"')
            ComandoObtemAtivo += @"                                           and pl.TX_STATUS in ('Ferias', 'LicencaMedica','Afastamento')
                                                                    union all
                                                                            select    pa.*, 3 prioridade
                                                                            from      PESSOA_FISICA pf WITH (NOLOCK)
                                                                            inner join papel pa WITH (NOLOCK) on pf.CD_PESSOA_FISICA = pa.CD_PESSOA_FISICA
                                                                            inner join COLABORADOR co WITH (NOLOCK) on pa.CD_PAPEL = co.CD_PAPEL
                                                                            inner join (select max(CD_PAPEL_LOG) cd_papel_log, ipl.cd_papel from PAPEL_LOG ipl WITH (NOLOCK) group by ipl.cd_papel) max_pl on co.CD_PAPEL = max_pl.CD_PAPEL
                                                                            inner join papel_log pl WITH (NOLOCK) on max_pl.cd_papel_log = pl.CD_PAPEL_LOG 
                                                                            where  1 = 1 ";
            if (!String.IsNullOrEmpty(pCPF))
            {
                ComandoObtemAtivo += "                                        and  ( pf.TX_CPF =  '" + pCPF + @"' ) ";
            }
            else
            {
                ComandoObtemAtivo += "                                        and  ( pf.TX_PASSAPORTE =  '" + pPassaporte + @"' And pf.TX_PASSAPORTE <> ''  And pf.TX_PASSAPORTE is Not Null) ";
            }
            //ComandoObtemAtivo += "                                          and co.CD_EMPRESA = (select em.cd_pessoa from EMPRESA em where em.TX_CNPJ = '" + Cnpj + @"')
            ComandoObtemAtivo += @"                                           and pl.TX_STATUS like 'Inativacao'
                                                                    ) tabela
                                                                    order by prioridade, DT_REGISTRO desc";


            foreach (PapelGS obj in item.ObtemRegistrosCustom(item.GetType(), ComandoObtemAtivo))
            {
                Achou = true;
                item = obj;
                break;
            }

            if (Achou)
                return item;
            else
                if (pRetornaNulo)
                    return null;
                else
                    throw new Exception("[PESSOAFISICA].[OBTEM] Registro de Pessoa não encontrado ( cpf: " + pCPF + " ou Passaporte: " + pPassaporte + ")");
        }


        public static PapelGS ObtemPorCodigo(String pCodigoPessoaFisica)
        {
            bool Achou = false;
            PapelGS item = new PapelGS();

            foreach (PapelGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " CD_PESSOA_FISICA = '" + pCodigoPessoaFisica + "'"))
            {
                Achou = true;
                item = obj;
                break;
            }

            if (Achou)
                return item;
            else
                throw new Exception("[PESSOAFISICA].[OBTEM] Não foi encontrado registro Ativo para o CodigoPessoa (" + pCodigoPessoaFisica + ")");
        }

        public static PapelGS ObtemPorCodigoPapel(Int32 pCodigoPapel)
        {
            bool Achou = false;
            PapelGS item = new PapelGS();

            foreach (PapelGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " CD_PAPEL = '" + Convert.ToString(pCodigoPapel) + "'"))
            {
                Achou = true;
                item = obj;
                break;
            }

            if (Achou)
                return item;
            else
                throw new Exception("[PAPEL].[OBTEM] Não foi encontrado registro Ativo para o CodigoPapel (" + pCodigoPapel + ")");
        }


        //

        #endregion

    }
}
