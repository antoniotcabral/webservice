﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("SETOR_CUSTO")]
    public class SetorCustoGS:MasterClass
    {

        #region Atributos

        
        private int _Codigo;

        private string _Nome;
        private string _Tipo;
        private string _Numero;
        private string _Descricao;
        private bool _Ativo;
        private string _TX_ID_RESPONSAVEL_SAP;
        private string _CD_PAPEL_RESPONSAVEL;


        private DateTime _DataRegistro;
        private DateTime _DataDesativacao;


        #endregion

        #region Propriedades

        

        [CampoNaTabela("CD_SETOR", DbType.Int32,false,true)]
        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        [CampoNaTabela("TX_NOME", DbType.String)]
        public String Nome
        {
            get { return _Nome; }
            set { _Nome = value; }
        }


        [CampoNaTabela("TX_TIPO", DbType.String)]
        public String Tipo
        {
            get { return _Tipo; }
            set { _Tipo = value; }
        }

        [CampoNaTabela("NU_SETOR", DbType.String,true)]
        public String Numero
        {
            get { return _Numero; }
            set { _Numero = value; }
        }

        [CampoNaTabela("TX_DESCRICAO", DbType.String)]
        public String Descricao
        {
            get { return _Descricao; }
            set { _Descricao = value; }
        }

        [CampoNaTabela("BL_ATIVO", DbType.Boolean)]
        public Boolean Ativo
        {
            get { return _Ativo; }
            set { _Ativo = value; }
        }


        [CampoNaTabela("DT_REGISTRO", DbType.DateTime)]
        public DateTime DataRegistro
        {
            get { return _DataRegistro; }
            set { _DataRegistro = value; }
        }

        [CampoNaTabela("DT_DESATIVACAO", DbType.DateTime)]
        public DateTime DataDesativacao
        {
            get { return _DataDesativacao; }
            set { _DataDesativacao = value; }
        }

        [CampoNaTabela("TX_ID_RESPONSAVEL_SAP", DbType.String)]
        public String TX_ID_RESPONSAVEL_SAP
        {
            get { return _TX_ID_RESPONSAVEL_SAP; }
            set { _TX_ID_RESPONSAVEL_SAP = value; }
        }

        [CampoNaTabela("CD_RESPONSAVEL", DbType.String)]
        public string CD_PAPEL_RESPONSAVEL
        {
            get { return _CD_PAPEL_RESPONSAVEL; }
            set { _CD_PAPEL_RESPONSAVEL = value; }
        }

        #endregion

        #region Métodos

        public static SetorCustoGS Obtem(string pNumero, String pTipo)
        {
            bool Achou = false;
            SetorCustoGS item = new SetorCustoGS();


            try
            {
                //Avaliar o nome como passado
                foreach (SetorCustoGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " NU_SETOR  = '" + pNumero + "' AND TX_TIPO= '" + pTipo + "'"))
                {
                    Achou = true;
                    item = obj;
                    break;
                }

                if (!Achou)
                {
                    throw new Exception("[SETORCUSTO].[OBTEM] Setor Custo inexistente(" + pTipo +" - " + pNumero + ")!");
                }

            }
            catch (Exception erro)
            {
                throw new Exception("[SETORCUSTO].[OBTEM] Falha ao obter Setor Custo(" + pTipo + " - " + pNumero + ")!" + erro.Message);
            }


            return item;
        }

        public static SetorCustoGS Obtem(string pNumero)
        {
            bool Achou = false;
            SetorCustoGS item = new SetorCustoGS();


            try
            {
                //Avaliar o nome como passado
                foreach (SetorCustoGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " NU_SETOR  = '" + pNumero + "'"))
                {
                    Achou = true;
                    item = obj;
                    break;
                }

                if (!Achou)
                {
                    throw new Exception("[SETORCUSTO].[OBTEM] Setor Custo inexistente(" + pNumero + ")!");
                }

            }
            catch (Exception erro)
            {
                throw new Exception("[SETORCUSTO].[OBTEM] Falha ao obter Setor Custo(" + pNumero + ")!" + erro.Message);
            }


            return item;
        }

        public static Boolean Existe(string pNumero)
        {
            bool Achou = false;
            SetorCustoGS item = new SetorCustoGS();


            try
            {
                
                foreach (SetorCustoGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " NU_SETOR  = '" + pNumero + "'"))
                {
                    Achou = true;                    
                    break;
                }                
            }
            catch (Exception erro)
            {
                //throw new Exception("[SETORCUSTO].[OBTEM] Falha ao obter Setor Custo(" + pNumero + ")!" + erro.Message);
                MasterClass.UltimoErro = erro;
                Achou = false;
            }


            return Achou;
        }
        
        public static int ObterPapel(string documento)
        {
            var master = new MasterClass();
            return (int)master.ExecutaEscalar(string.Format("select [dbo].[obter_papel]('{0}')", documento));
        }
        #endregion


    }
}
