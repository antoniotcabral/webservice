﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("BAIRRO")]
    public class BairroGS:MasterClass
    {

        #region Atributos

        
        private int _Codigo;
        private int _CodigoCidade;

        
        private string _Nome;
        
        private bool _Ativo;

        private DateTime _DataRegistro;
        private DateTime _DataDesativacao;


        #endregion

        #region Propriedades

        

        [CampoNaTabela("CD_BAIRRO", DbType.Int32,true,true)]
        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        [CampoNaTabela("CD_CIDADE", DbType.Int32)]
        public Int32 CodigoCidade
        {
            get { return _CodigoCidade; }
            set { _CodigoCidade = value; }
        }

        [CampoNaTabela("TX_BAIRRO", DbType.String)]
        public String Nome
        {
            get { return _Nome; }
            set { _Nome = value; }
        }


        [CampoNaTabela("BL_ATIVO", DbType.Boolean)]
        public Boolean Ativo
        {
            get { return _Ativo; }
            set { _Ativo = value; }
        }


        [CampoNaTabela("DT_REGISTRO", DbType.DateTime)]
        public DateTime DataRegistro
        {
            get { return _DataRegistro; }
            set { _DataRegistro = value; }
        }

        [CampoNaTabela("DT_DESATIVACAO", DbType.DateTime)]
        public DateTime DataDesativacao
        {
            get { return _DataDesativacao; }
            set { _DataDesativacao = value; }
        }


        

        #endregion

        #region Métodos

        public static BairroGS Obtem(string pNome, int pCodigoCidade)
        {
            BairroGS item = new BairroGS();
            List<BairroGS> itens = new List<BairroGS>();

            try
            {
                //Avaliar o nome como passado
                foreach (BairroGS obj in item.ObtemRegistrosFiltrados(item.GetType(), "  UPPER(TX_BAIRRO)  = '" + pNome.ToUpper() + "' AND CD_CIDADE = " + pCodigoCidade))
                {
                    itens.Add(obj);
                }

                //Não encontrou Estado com o nome igual, procurando com nome parecido
                if (itens.Count == 0)
                {
                    foreach (BairroGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " UPPER(TX_BAIRRO)  LIKE  '%" + pNome.ToUpper() + "%' AND CD_CIDADE = " + pCodigoCidade))
                    {
                        itens.Add(obj);
                    }

                    if (itens.Count == 0)
                    {
                        //throw new Exception("Bairro não encontrado!");

                        BairroGS bairro = new BairroGS();
                        bairro.Ativo = true;
                        bairro.CodigoCidade = pCodigoCidade;
                        bairro.DataDesativacao = DateTime.MinValue;
                        bairro.DataRegistro = DateTime.Now;
                        bairro.Nome = pNome;

                        int CodigoBairro = bairro.InserirRegistroRetornaID();
                        

                        return BairroGS.Obtem(pNome, pCodigoCidade);

                    }
                    else if (itens.Count > 1)
                    {
                        item = itens[0];
                        //throw new Exception("Encontrado mais de 1 Bairro com a descrição passada!");
                    }
                    else if (itens.Count == 1)
                    {
                        item = itens[0];
                    }
                }
                else if (itens.Count > 1)
                {
                    item = itens[0];
                    //throw new Exception("Encontrado mais de 1 Bairro com a descrição passada!");
                }
                else if (itens.Count == 1)
                {
                    item = itens[0];
                }

            }
            catch (Exception erro)
            {
                throw new Exception("[BAIRRO].[OBTEM] Falha ao obter o Bairro com a descrição " + pNome + "(" + pCodigoCidade.ToString() + "): " + erro.Message);
            }


            return item;
        }

        #endregion



    }
}
