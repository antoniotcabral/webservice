﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("HORA_TURNO")]
    public class HoraTurnoGS:MasterClass
    {

        #region Atributos

        
        private int _Codigo;
        private int _CodigoTurno;
        private int _Sequencia;
        
        private string _HoraInicio;
        private string _HoraFim;

        private bool _Trabalha;
        private bool _VinteQuatro;
        
        private bool _Ativo;

        private DateTime _DataRegistro;
        private DateTime _DataDesativacao;

        private int _NumeroDiaSemana;

        #endregion

        #region Propriedades

        

        [CampoNaTabela("CD_HORA_TURNO", DbType.Int32,true,true)]
        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        [CampoNaTabela("CD_TURNO", DbType.Int32)]
        public Int32 CodigoTurno
        {
            get { return _CodigoTurno; }
            set { _CodigoTurno = value; }
        }

        [CampoNaTabela("NU_SEQ", DbType.Int32)]
        public Int32 Sequencia
        {
            get { return _Sequencia; }
            set { _Sequencia = value; }
        }

        [CampoNaTabela("HR_INICIO", DbType.Time)]
        public String HoraInicio
        {
            get { return _HoraInicio; }
            set { _HoraInicio = value; }
        }

        [CampoNaTabela("HR_FIM", DbType.Time)]
        public String HoraFim
        {
            get { return _HoraFim; }
            set { _HoraFim = value; }
        }

        [CampoNaTabela("BL_TRABALHA", DbType.Boolean)]
        public Boolean Trabalha
        {
            get { return _Trabalha; }
            set { _Trabalha = value; }
        }

        [CampoNaTabela("BL_24H", DbType.Boolean)]
        public Boolean VinteQuatroHoras
        {
            get { return _VinteQuatro; }
            set { _VinteQuatro = value; }
        }

        [CampoNaTabela("BL_ATIVO", DbType.Boolean)]
        public Boolean Ativo
        {
            get { return _Ativo; }
            set { _Ativo = value; }
        }


        [CampoNaTabela("DT_REGISTRO", DbType.DateTime)]
        public DateTime DataRegistro
        {
            get { return _DataRegistro; }
            set { _DataRegistro = value; }
        }

        [CampoNaTabela("DT_DESATIVACAO", DbType.DateTime)]
        public DateTime DataDesativacao
        {
            get { return _DataDesativacao; }
            set { _DataDesativacao = value; }
        }

        [CampoNaTabela("NU_DIA_SEMANA", DbType.Int32)]
        public Int32 NumeroDiaSemana
        {
            get { return _NumeroDiaSemana; }
            set { _NumeroDiaSemana = value; }
        }
        

        #endregion

        #region Métodos

        public static bool RemoveHorasDoTurno(Int32 pCodigoTurno)
        {
            String cmd = "DELETE FROM HORA_TURNO WHERE CD_TURNO = " + pCodigoTurno.ToString();
            HoraTurnoGS item = new HoraTurnoGS();

            return item.ExecutaComando(cmd);


        }

        #endregion



    }
}
