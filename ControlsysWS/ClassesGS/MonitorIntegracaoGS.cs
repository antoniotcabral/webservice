﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using ControlsysWS.ReturnLog;
using System.ServiceModel;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("MONITORINTEGRACAO")]
    public class MonitorIntegracaoGS : MasterClass
    {

        #region Atributos

        private int _Codigo;
        private int _IntegracaoSAP;
        private string _XML;
        private bool _Integrado;
        private DateTime _DataIntegracao;        
        private DateTime _DataRegistro;

        #endregion

        #region Propriedades

        [CampoNaTabela("CD_MONITORINTEGRACAO", DbType.Int32, true, true)]
        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        [CampoNaTabela("ID_INTEGRACAO_SAP", DbType.Int32)]
        public Int32 IntegracaoSAP
        {
            get { return _IntegracaoSAP; }
            set { _IntegracaoSAP = value; }
        }

        [CampoNaTabela("ME_XML", DbType.String)]
        public String XML
        {
            get { return _XML; }
            set { _XML = value; }
        }

        [CampoNaTabela("BL_INTEGRADO", DbType.Boolean)]
        public Boolean Integrado
        {
            get { return _Integrado; }
            set { _Integrado = value; }
        }

        [CampoNaTabela("DT_INTEGRACAO", DbType.DateTime)]
        public DateTime DataIntegracao
        {
            get { return _DataIntegracao; }
            set { _DataIntegracao = value; }
        }

        [CampoNaTabela("DT_REGISTRO", DbType.DateTime)]
        public DateTime DataRegistro
        {
            get { return _DataRegistro; }
            set { _DataRegistro = value; }
        }

        #endregion

        #region Métodos
        
        private static int ObtemProximoCodigo()
        {
            try
            {

                MonitorIntegracaoGS integra = new MonitorIntegracaoGS();

                string cmdCodigo = "SELECT MAX(CD_MONITORINTEGRACAO)+1 FROM MONITORINTEGRACAO";

                int ProxCod = integra.ExecutaComandoContarRegistros(cmdCodigo);

                return ProxCod == -1 ? 1 : ProxCod;

            }
            catch (Exception erro)
            {
                throw new Exception("[ObtemProximoCodigo]Falha ao obter o código do monitor de integração: " + erro.Message);
            }
        }

        public static bool ExisteMonitorIntegracao(int pCodigoMonitorIntegracao)
        {
            bool Achou = false;
            MonitorIntegracaoGS item = new MonitorIntegracaoGS();

            foreach (MonitorIntegracaoGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " CD_MONITORINTEGRACAO = " + pCodigoMonitorIntegracao))
            {
                Achou = true;
                break;
            }

            return Achou;
        }

        public static MonitorIntegracaoGS Obtem(int pCodigo)
        {
            bool Achou = false;
            MonitorIntegracaoGS item = new MonitorIntegracaoGS();

            foreach (MonitorIntegracaoGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " CD_MONITORINTEGRACAO = '" + pCodigo + "'"))
            {
                Achou = true;
                item = obj;
                break;
            }

            if (Achou)
                return item;
            else
                throw new Exception("[MONITORINTEGRACAO].[OBTEM] Registro de monitor integração não encontrado (" + pCodigo + ")");
        }

        
        public static MonitorIntegracao ProcessaMonitorIntegracao(Int32 pCodigo, Int32 pIntegracaoSAP, string pXML, Boolean pIntegrado, DateTime pDataIntegracao, DateTime pDataRegisro)
        {
            try
            {
                MonitorIntegracaoGS resposta = new MonitorIntegracaoGS();

                resposta.Codigo = pCodigo;//ObtemProximoCodigo();
                resposta.IntegracaoSAP = pIntegracaoSAP;
                resposta.XML = pXML;
                resposta.Integrado = pIntegrado;
                resposta.DataIntegracao = pDataIntegracao;
                resposta.DataRegistro = pDataRegisro;                
                /*
                if (MasterClass.LogAtivado)
                {
                    MasterClass.TextoLog.Append($"[{GetCurrentMethod()}]({GetCurrentMethod(true)}) Registrando Início do Monitor de Integração no Banco de Dados." + Environment.NewLine);
                }

                if (IntegracaoGS.ExisteIntegracao(pCodigo))
                {
                    resposta.AtualizarRegistro();
                }
                else
                {
                    resposta.InserirRegistro();
                }
                */
                MonitorIntegracao monitorintegra = resposta.Converte();
                
                return monitorintegra;

            }
            catch (Exception erro)
            {
                throw new Exception($"[{GetCurrentMethod()} - {GetCurrentMethod(true)}] Falha ao criar Monitor de Integração. (Detalhe:" + erro.Message + ")");
            }
        }
        
        public MonitorIntegracao Converte()
        {
            MonitorIntegracao resposta = new MonitorIntegracao();

            resposta.Codigo = this.Codigo;
            resposta.IntegracaoSAP = this.IntegracaoSAP;
            resposta.XML = this.XML;
            resposta.Integrado = this.Integrado;
            resposta.DataIntegracao = this.DataIntegracao;
            resposta.DataRegistro = this.DataRegistro;

            return resposta;

        }
        
        [MethodImpl(MethodImplOptions.NoInlining)]
        public static string GetCurrentMethod(bool anterior = false)
        {
            StackTrace st = new StackTrace();
            StackFrame sf = st.GetFrame(anterior ? 2 : 1);

            return sf.GetMethod().Name;
        }
        
        #endregion

    }
}
