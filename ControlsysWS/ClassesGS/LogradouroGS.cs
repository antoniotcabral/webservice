﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("LOGRADOURO")]
    public class LogradouroGS:MasterClass
    {

        #region Atributos

        
        private int _Codigo;
        private int _CodigoBairro;

        
        private string _Nome;
        private string _Cep;
        
        private bool _Ativo;

        private DateTime _DataRegistro;
        private DateTime _DataDesativacao;


        #endregion

        #region Propriedades



        [CampoNaTabela("CD_LOGRADOURO", DbType.Int32, true,true)]
        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        [CampoNaTabela("CD_BAIRRO", DbType.Int32)]
        public Int32 CodigoBairro
        {
            get { return _CodigoBairro; }
            set { _CodigoBairro = value; }
        }

        [CampoNaTabela("TX_NOME", DbType.String)]
        public String Nome
        {
            get { return _Nome; }
            set { _Nome = value; }
        }


        [CampoNaTabela("NU_CEP", DbType.String)]
        public String Cep
        {
            get { return _Cep; }
            set { _Cep = value; }
        }


        [CampoNaTabela("BL_ATIVO", DbType.Boolean)]
        public Boolean Ativo
        {
            get { return _Ativo; }
            set { _Ativo = value; }
        }


        [CampoNaTabela("DT_REGISTRO", DbType.DateTime)]
        public DateTime DataRegistro
        {
            get { return _DataRegistro; }
            set { _DataRegistro = value; }
        }

        [CampoNaTabela("DT_DESATIVACAO", DbType.DateTime)]
        public DateTime DataDesativacao
        {
            get { return _DataDesativacao; }
            set { _DataDesativacao = value; }
        }


        

        #endregion

        #region Métodos

        public static LogradouroGS Obtem(string pNome, int pCodigoBairro)
        {
            LogradouroGS item = new LogradouroGS();
            List<LogradouroGS> itens = new List<LogradouroGS>();

            try
            {

                pNome = pNome.Replace("'", "''");

                //Avaliar o nome como passado
                foreach (LogradouroGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " UPPER(TX_NOME)  = '" + pNome.ToUpper() + "' AND CD_BAIRRO = " + pCodigoBairro))
                {
                    itens.Add(obj);
                }

                //Não encontrou Estado com o nome igual, procurando com nome parecido
                if (itens.Count == 0)
                {
                    foreach (LogradouroGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " UPPER(TX_NOME) LIKE  '%" + pNome.ToUpper() + "%' AND CD_BAIRRO = " + pCodigoBairro))
                    {
                        itens.Add(obj);
                    }

                    if (itens.Count == 0)
                    {
                        throw new Exception("Logradouro não encontrado!");
                    }
                    else if (itens.Count > 1)
                    {
                        item = itens[0];
                        //throw new Exception("Encontrado mais de 1 Logradouro com a descrição passada!");
                    }
                    else if (itens.Count == 1)
                    {
                        item = itens[0];
                    }
                }
                else if (itens.Count > 1)
                {
                    item = itens[0];
                    //throw new Exception("Encontrado mais de 1 Logradouro com a descrição passada!");
                }
                else if (itens.Count == 1)
                {
                    item = itens[0];
                }

            }
            catch (Exception erro)
            {
                throw new Exception("[LOGRADOURO].[OBTEM] Falha ao obter o Logradouro com a descrição " + pNome + "(" + pCodigoBairro.ToString() + "): " + erro.Message);
            }


            return item;
        }

        public static LogradouroGS Obtem(Int32 pCodigoLogradouro)
        {
            bool Achou = false;
            LogradouroGS item = new LogradouroGS();

            foreach (LogradouroGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " CD_LOGRADOURO = " + pCodigoLogradouro))
            {
                Achou = true;
                item = obj;
                break;
            }

            if (Achou)
                return item;
            else
                throw new Exception("[PESSOAFISICA].[OBTEM] Registro de Logradouro não encontrado (" + pCodigoLogradouro + ")");
        }

        #endregion



    }
}
