﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("PESSOA")]
    public class PessoaGS:MasterClass
    {

        #region Atributos

        private string _Codigo;
        //private int _CodigoEndereco;

        private string _Nome;
        private string _Apelido;
        private string _Email;

        private DateTime _DataRegistro;

        #endregion

        #region Propriedades

        [CampoNaTabela("CD_PESSOA", DbType.String,true,false)]
        public String Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        //[CampoNaTabela("CD_ENDERECO", DbType.Int32)]
        //public Int32 CodigoEndereco
        //{
        //    get { return _CodigoEndereco; }
        //    set { _CodigoEndereco = value; }
        //}

        [CampoNaTabela("TX_NOME", DbType.String)]
        public String Nome
        {
            get { return _Nome; }
            set { _Nome = value; }
        }


        [CampoNaTabela("TX_APELIDO", DbType.String)]
        public String Apelido
        {
            get { return _Apelido; }
            set { _Apelido = value; }
        }

        [CampoNaTabela("TX_EMAIL", DbType.String)]
        public String Email
        {
            get { return _Email; }
            set { _Email = value; }
        }


        [CampoNaTabela("DT_REGISTRO", DbType.DateTime)]
        public DateTime DataRegistro
        {
            get { return _DataRegistro; }
            set { _DataRegistro = value; }
        }
        #endregion

        #region Métodos

        public static string ProximoCodigoPessoaJuridica()
        { 
            int resposta = 0;
            string strResposta = string.Empty;
            PessoaGS pessoa = new PessoaGS();

            //resposta = pessoa.ExecutaComandoContarRegistros("SELECT MAX(SUBSTRING(CD_PESSOA,4,7)) FROM PESSOA WHERE CD_PESSOA LIKE 'EMP%'");

            resposta = pessoa.ExecutaComandoContarRegistros("SELECT MAX(SUBSTRING(CD_PESSOA,3,8)) FROM PESSOA WHERE CD_PESSOA LIKE 'EM%' AND CD_PESSOA NOT LIKE 'EMP%'");

            if (resposta == -1)
                resposta = 1;
            else
                resposta++;

            strResposta = resposta.ToString();

            while (strResposta.Length < 8)
            {
                strResposta = "0" + strResposta;
            }

            strResposta = "EM" + strResposta;

            return strResposta;


        }

        public static string ProximoCodigoPessoaFisica()
        {
            int resposta = 0;
            string strResposta = string.Empty;
            PessoaGS pessoa = new PessoaGS();

            //resposta = pessoa.ExecutaComandoContarRegistros("SELECT MAX(SUBSTRING(CD_PESSOA,4,7)) FROM PESSOA WHERE CD_PESSOA LIKE 'EMP%'");

            resposta = pessoa.ExecutaComandoContarRegistros("SELECT MAX(SUBSTRING(CD_PESSOA,3,8)) FROM PESSOA WHERE CD_PESSOA LIKE 'PF%' ");

            if (resposta == -1)
                resposta = 1;
            else
                resposta++;

            strResposta = resposta.ToString();

            while (strResposta.Length < 8)
            {
                strResposta = "0" + strResposta;
            }

            strResposta = "PF" + strResposta;

            return strResposta;


        }

        public static PessoaGS Obtem(String pCodigo)
        {
            bool Achou = false;
            PessoaGS item = new PessoaGS();

            foreach (PessoaGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " CD_PESSOA = '" + pCodigo + "'"))
            {
                Achou = true;
                item = obj;
                break;
            }

            if (Achou)
                return item;
            else
                throw new Exception("[PESSOAGS].[OBTEM] Registro de Pessoa não encontrado (" + pCodigo.ToString() + ")");
        }
        public static PessoaGS ObtemNomeFantasia(String pCodigo)
        {
            bool Achou = false;
            PessoaGS item = new PessoaGS();

            foreach (PessoaGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " CD_PESSOA = '" + pCodigo + "'"))
            {
                Achou = true;
                item = obj;
                break;
            }

            if (Achou)
                return item;
            else
                throw new Exception("[PESSOAGS].[OBTEM] Registro de Pessoa não encontrado (" + pCodigo.ToString() + ")");
        }


        #endregion




    }
}
