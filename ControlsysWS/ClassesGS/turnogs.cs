﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("TURNO")]
    public class TurnoGS:MasterClass
    {

        #region Atributos

        
        private int _Codigo;

        private string _Nome;        
        

        private bool _Ativo;
        private bool _Escala;
        private bool _Feriado;

        private DateTime _DataRegistro;
        private DateTime _DataDesativacao;


        #endregion

        #region Propriedades

        

        [CampoNaTabela("CD_TURNO", DbType.Int32,true,true)]
        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        [CampoNaTabela("TX_NOME", DbType.String)]
        public String Nome
        {
            get { return _Nome; }
            set { _Nome = value; }
        }
               

        [CampoNaTabela("BL_ATIVO", DbType.Boolean)]
        public Boolean Ativo
        {
            get { return _Ativo; }
            set { _Ativo = value; }
        }

        
        [CampoNaTabela("BL_FERIADO", DbType.Boolean)]
        public Boolean Feriado
        {
            get { return _Feriado; }
            set { _Feriado = value; }
        }

        [CampoNaTabela("BL_ESCALA", DbType.Boolean)]
        public Boolean Escala
        {
            get { return _Escala; }
            set { _Escala = value; }
        }


        [CampoNaTabela("DT_REGISTRO", DbType.DateTime)]
        public DateTime DataRegistro
        {
            get { return _DataRegistro; }
            set { _DataRegistro = value; }
        }

        [CampoNaTabela("DT_DESATIVACAO", DbType.DateTime)]
        public DateTime DataDesativacao
        {
            get { return _DataDesativacao; }
            set { _DataDesativacao = value; }
        }


        

        #endregion

        #region Métodos

        public static TurnoGS Obtem(DateTime pData)
        {
            bool Achou = false;
            TurnoGS item = new TurnoGS();


            foreach (TurnoGS obj in item.ObtemRegistrosFiltrados(item.GetType(), "  CONVERT(VARCHAR, DT_REGISTRO,120) = '" + pData.ToString("yyyy-MM-dd HH:mm:ss")+"'"))
            {
                Achou = true;
                item = obj;
                break;
            }

            if (Achou)
                return item;
            else
                throw new Exception("[TURNO].[OBTEM] Registro de Turno não encontrado (" + pData.ToString() + ")");
        }

        public static TurnoGS Obtem(Int32 pCodigo)
        {
            bool Achou = false;
            TurnoGS item = new TurnoGS();


            foreach (TurnoGS obj in item.ObtemRegistrosFiltrados(item.GetType(), "  CD_TURNO =" + pCodigo.ToString()))
            {
                Achou = true;
                item = obj;
                break;
            }

            if (Achou)
                return item;
            else
                throw new Exception("[TURNO].[OBTEM] Registro de Turno não encontrado (" + pCodigo.ToString() + ")");
        }

        public static TurnoGS Obtem(String pNome)
        {
            bool Achou = false;
            TurnoGS item = new TurnoGS();


            foreach (TurnoGS obj in item.ObtemRegistrosFiltrados(item.GetType(), "  TX_NOME = '" + pNome + "'"))
            {
                Achou = true;
                item = obj;
                break;
            }

            if (Achou)
                return item;
            else
                throw new Exception("[TURNO].[OBTEM] Registro de Turno não encontrado com o Nome passado (" + pNome + ")");
        }

        #endregion



    }
}
