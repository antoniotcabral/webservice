﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("FORNECEDOR")]
    public class FornecedorGS:MasterClass
    {

        #region Atributos

        private int _Codigo;
        
        private string _RazaoSocial;
        private string _NomeFantasia;
        private long _Telefone;
        private string _Email;
        private string _Cnpj;
        private string _Contato;


        private bool _Ativo;
        private DateTime _DataRegistro;
        private DateTime _DataDesativacao;

        #endregion

        #region Propriedades

        [CampoNaTabela("CD_FORNECEDOR", DbType.Int32, true, true)]
        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }



        [CampoNaTabela("TX_RAZAO_SOCIAL", DbType.String)]
        public String RazaoSocial
        {
            get { return _RazaoSocial; }
            set { _RazaoSocial = value; }
        }


        [CampoNaTabela("TX_NOME_FANTASIA", DbType.String)]
        public String NomeFantasia
        {
            get { return _NomeFantasia; }
            set { _NomeFantasia = value; }
        }

        [CampoNaTabela("NU_TELEFONE", DbType.Int64)]
        public Int64 Telefone
        {
            get { return _Telefone; }
            set { _Telefone = value; }
        }

        [CampoNaTabela("NU_CNPJ", DbType.String)]
        public String Cnpj
        {
            get { return _Cnpj; }
            set { _Cnpj = value; }
        }

        [CampoNaTabela("TX_CONTATO", DbType.String)]
        public String Contato
        {
            get { return _Contato; }
            set { _Contato = value; }
        }



        [CampoNaTabela("TX_EMAIL", DbType.String)]
        public String Email
        {
            get { return _Email; }
            set { _Email = value; }
        }


        [CampoNaTabela("BL_ATIVO", DbType.Boolean)]
        public Boolean Ativo
        {
            get { return _Ativo; }
            set { _Ativo = value; }
        }


        [CampoNaTabela("DT_REGISTRO", DbType.DateTime)]
        public DateTime DataRegistro
        {
            get { return _DataRegistro; }
            set { _DataRegistro = value; }
        }

        [CampoNaTabela("DT_DESATIVACAO", DbType.DateTime)]
        public DateTime DataDesativacao
        {
            get { return _DataDesativacao; }
            set { _DataDesativacao = value; }
        }

        #endregion

        #region Métodos

        

        public static FornecedorGS Obtem(String pCnpj)
        {
            bool Achou = false;
            FornecedorGS item = new FornecedorGS();

            foreach (FornecedorGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " NU_CNPJ = '" + pCnpj + "'"))
            {
                Achou = true;
                item = obj;
                break;
            }

            if (Achou)
                return item;
            else
                throw new Exception("[FORNECEDORGS].[OBTEM] Registro de Forncedor não encontrado (" + pCnpj.ToString() + ")");
        }



        public static Boolean Existe(String pCnpj)
        {
            bool Achou = false;
            FornecedorGS item = new FornecedorGS();

            foreach (FornecedorGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " NU_CNPJ = '" + pCnpj + "'"))
            {
                Achou = true;
                //item = obj;
                break;
            }

            return Achou;
        }


        #endregion




    }
}
