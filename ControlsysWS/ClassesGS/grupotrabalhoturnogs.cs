﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("GRUPO_TRABALHO_TURNO")]
    public class GrupoTrabalhoTurnoGS:MasterClass
    {

        #region Atributos

        
        private int _Codigo;
        private int _CodigoTurno;
        private int _CodigoGrupoTrabalho;
        private DateTime _DataInicio;

        private bool _Ativo;

        private DateTime _DataRegistro;
        private DateTime _DataDesativacao;


        #endregion

        #region Propriedades

        

        [CampoNaTabela("CD_GRUPO_TRABALHO_TURNO", DbType.Int32,true, true)]
        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        [CampoNaTabela("CD_TURNO", DbType.Int32 )]
        public Int32 CodigoTurno
        {
            get { return _CodigoTurno; }
            set { _CodigoTurno = value; }
        }

        [CampoNaTabela("CD_GRUPO_TRABALHO", DbType.Int32)]
        public Int32 CodigoGrupoTrabalho
        {
            get { return _CodigoGrupoTrabalho; }
            set { _CodigoGrupoTrabalho = value; }
        }

        [CampoNaTabela("DT_INICIO", DbType.DateTime)]
        public DateTime DataInicio
        {
            get { return _DataInicio; }
            set { _DataInicio = value; }
        }

        [CampoNaTabela("BL_ATIVO", DbType.Boolean)]
        public Boolean Ativo
        {
            get { return _Ativo; }
            set { _Ativo = value; }
        }


        [CampoNaTabela("DT_REGISTRO", DbType.DateTime)]
        public DateTime DataRegistro
        {
            get { return _DataRegistro; }
            set { _DataRegistro = value; }
        }

        [CampoNaTabela("DT_DESATIVACAO", DbType.DateTime)]
        public DateTime DataDesativacao
        {
            get { return _DataDesativacao; }
            set { _DataDesativacao = value; }
        }


        

        #endregion

        #region Métodos

        public static GrupoTrabalhoTurnoGS Obtem(Int32 pCodigoGrupo, Int32 pCodigoTurno)
        {
            GrupoTrabalhoTurnoGS item = new GrupoTrabalhoTurnoGS();
            Boolean Achou = false;


            //Avaliar o nome como passado
            foreach (GrupoTrabalhoTurnoGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " CD_GRUPO_TRABALHO = "+pCodigoGrupo.ToString()+" AND CD_TURNO = " + pCodigoTurno.ToString()))
            {
                Achou = true;
                item = obj;
                break;
            }

            if (Achou)
                return item;
            else
                throw new Exception("[GRUPOTRABALHOTURNO].[OBTEM] Registro de  Turno de Grupo de Trabalho não encontrado");
        }

        public static GrupoTrabalhoTurnoGS Obtem(Int32 pCodigoTurno)
        {
            GrupoTrabalhoTurnoGS item = new GrupoTrabalhoTurnoGS();
            Boolean Achou = false;


            //Avaliar o nome como passado
            foreach (GrupoTrabalhoTurnoGS obj in item.ObtemRegistrosFiltrados(item.GetType(), "  CD_TURNO = " + pCodigoTurno.ToString()))
            {
                Achou = true;
                item = obj;
                break;
            }

            if (Achou)
                return item;
            else
                throw new Exception("[GRUPOTRABALHOTURNO].[OBTEM] Registro de  Turno de Grupo de Trabalho não encontrado");
        }

        #endregion



    }
}
