﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace br.com.globalsys.controlsys.webservice
{
    [Serializable]
    [DadosDaTabela("ENDERECO")]
    public class EnderecoGS:MasterClass
    {

        #region Atributos

        
        private int _Codigo;
        private int _CodigoLogradouro;

        
        private string _Complemento;
        private string _Numero;
        
        private decimal _CaixaPostal;

        private bool _Ativo;

        private DateTime _DataRegistro;
        private DateTime _DataDesativacao;


        #endregion

        #region Propriedades

        

        [CampoNaTabela("CD_ENDERECO", DbType.Int32,true,true)]
        public Int32 Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        [CampoNaTabela("CD_LOGRADOURO", DbType.Int32)]
        public Int32 CodigoLogradouro
        {
            get { return _CodigoLogradouro; }
            set { _CodigoLogradouro = value; }
        }

        [CampoNaTabela("TX_COMPLEMENTO", DbType.String)]
        public String Complemento
        {
            get { return _Complemento; }
            set { _Complemento = value; }
        }


        [CampoNaTabela("NU_ENDERECO", DbType.String)]
        public String Numero
        {
            get { return _Numero; }
            set { _Numero = value; }
        }

        [CampoNaTabela("NU_POSTAL", DbType.Decimal)]
        public Decimal CaixaPostal
        {
            get { return _CaixaPostal; }
            set { _CaixaPostal = value; }
        }

        [CampoNaTabela("BL_ATIVO", DbType.Boolean)]
        public Boolean Ativo
        {
            get { return _Ativo; }
            set { _Ativo = value; }
        }


        [CampoNaTabela("DT_REGISTRO", DbType.DateTime)]
        public DateTime DataRegistro
        {
            get { return _DataRegistro; }
            set { _DataRegistro = value; }
        }

        [CampoNaTabela("DT_DESATIVACAO", DbType.DateTime)]
        public DateTime DataDesativacao
        {
            get { return _DataDesativacao; }
            set { _DataDesativacao = value; }
        }


        

        #endregion


        #region Métodos


        public static EnderecoGS Obtem(Int32 pCodigoEndereco)
        {
            bool Achou = false;
            EnderecoGS item = new EnderecoGS();

            foreach (EnderecoGS obj in item.ObtemRegistrosFiltrados(item.GetType(), " CD_ENDERECO = " + pCodigoEndereco))
            {
                Achou = true;
                item = obj;
                break;
            }

            if (Achou)
                return item;
            else
                throw new Exception("[PESSOAFISICA].[OBTEM] Registro de Logradouro não encontrado (" + pCodigoEndereco + ")");
        }

        #endregion


    }
}
