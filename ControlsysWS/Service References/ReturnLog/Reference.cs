﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ControlsysWS.ReturnLog {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://csa.com.br/CH8675/INT_fromCSys_ToERPHR_ReturnLog", ConfigurationName="ReturnLog.MI_OB_CSYS_ReturnLog")]
    public interface MI_OB_CSYS_ReturnLog {
        
        // CODEGEN: Generating message contract since the operation MI_OB_CSYS_ReturnLog is neither RPC nor document wrapped.
        [System.ServiceModel.OperationContractAttribute(IsOneWay=false, Action="http://sap.com/xi/WebService/soap1.1")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        void MI_OB_CSYS_ReturnLog(ControlsysWS.ReturnLog.MI_OB_CSYS_ReturnLog1 request);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://csa.com.br/CH8675/INT_fromCSys_ToERPHR_ReturnLog")]
    public partial class DT_CSysReturnLogReturnLog : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string idField;
        
        private string statusField;
        
        private string mensagemField;
        
        private string tX_TIPOField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public string ID {
            get {
                return this.idField;
            }
            set {
                this.idField = value;
                this.RaisePropertyChanged("ID");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=1)]
        public string Status {
            get {
                return this.statusField;
            }
            set {
                this.statusField = value;
                this.RaisePropertyChanged("Status");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=2)]
        public string Mensagem {
            get {
                return this.mensagemField;
            }
            set {
                this.mensagemField = value;
                this.RaisePropertyChanged("Mensagem");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=3)]
        public string TX_TIPO {
            get {
                return this.tX_TIPOField;
            }
            set {
                this.tX_TIPOField = value;
                this.RaisePropertyChanged("TX_TIPO");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class MI_OB_CSYS_ReturnLog1 {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://csa.com.br/CH8675/INT_fromCSys_ToERPHR_ReturnLog", Order=0)]
        [System.Xml.Serialization.XmlArrayItemAttribute("ReturnLog", Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=false)]
        public ControlsysWS.ReturnLog.DT_CSysReturnLogReturnLog[] MT_CSysReturnLog;
        
        public MI_OB_CSYS_ReturnLog1() {
        }
        
        public MI_OB_CSYS_ReturnLog1(ControlsysWS.ReturnLog.DT_CSysReturnLogReturnLog[] MT_CSysReturnLog) {
            this.MT_CSysReturnLog = MT_CSysReturnLog;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface MI_OB_CSYS_ReturnLogChannel : ControlsysWS.ReturnLog.MI_OB_CSYS_ReturnLog, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class MI_OB_CSYS_ReturnLogClient : System.ServiceModel.ClientBase<ControlsysWS.ReturnLog.MI_OB_CSYS_ReturnLog>, ControlsysWS.ReturnLog.MI_OB_CSYS_ReturnLog {
        
        public MI_OB_CSYS_ReturnLogClient() {
        }
        
        public MI_OB_CSYS_ReturnLogClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public MI_OB_CSYS_ReturnLogClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public MI_OB_CSYS_ReturnLogClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public MI_OB_CSYS_ReturnLogClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        void ControlsysWS.ReturnLog.MI_OB_CSYS_ReturnLog.MI_OB_CSYS_ReturnLog(ControlsysWS.ReturnLog.MI_OB_CSYS_ReturnLog1 request) {
            base.Channel.MI_OB_CSYS_ReturnLog(request);
        }
        
        public void MI_OB_CSYS_ReturnLog(ControlsysWS.ReturnLog.DT_CSysReturnLogReturnLog[] MT_CSysReturnLog) {
            ControlsysWS.ReturnLog.MI_OB_CSYS_ReturnLog1 inValue = new ControlsysWS.ReturnLog.MI_OB_CSYS_ReturnLog1();
            inValue.MT_CSysReturnLog = MT_CSysReturnLog;
            ((ControlsysWS.ReturnLog.MI_OB_CSYS_ReturnLog)(this)).MI_OB_CSYS_ReturnLog(inValue);
        }
    }
}
