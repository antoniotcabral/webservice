﻿using ControlsysWSMI.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using System.Web;
using System.Web.Mvc;

namespace ControlsysWSMI.Controllers
{
    public class HomeController : Controller
    {        
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
     
            return View();
        }       
    }
}
