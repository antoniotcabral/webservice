﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using System.IO;
using Hangfire;
using System.Configuration;
using ControlsysWSMI.DAO;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;

[assembly: OwinStartup(typeof(ControlsysWSMI.Startup))]

namespace ControlsysWSMI
{
    public class Startup
    {
        private static bool processing = false;        

        public void Configuration(IAppBuilder app)
        {
            GlobalConfiguration.Configuration.UseSqlServerStorage(ConfigurationManager.AppSettings["hangfiredb"], new Hangfire.SqlServer.SqlServerStorageOptions { SchemaName = "hangfire_wsmi" });

            string hangfire_IntervaloTempo = ConfigurationManager.AppSettings["hangfire_IntervaloTempo"];
            if (String.IsNullOrEmpty(hangfire_IntervaloTempo))
            {
                hangfire_IntervaloTempo = "*/5 * * * *";
            }

            app.UseHangfireServer();
            app.UseHangfireDashboard();

            RecurringJob.AddOrUpdate("Integração Controlsys Colaborador", () => Integracao_Colaborador(), hangfire_IntervaloTempo); // Minuto a minuto
        }

        public void Integracao_Colaborador()
        {
            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                try
                {
                    if (!Startup.processing)
                    {
                        Startup.processing = true;

                        string ENDPOINT_CONTROLSYS = ConfigurationManager.AppSettings["EnderecoWebservice"];

                        /**/
                        string qtdRegIntegracao = ConfigurationManager.AppSettings["qtdRegIntegracao"];
                        int qtdRegInt = 500;
                        if (!String.IsNullOrEmpty(qtdRegIntegracao))
                        {
                            if (Convert.ToInt32(qtdRegIntegracao) > 2000)
                            {
                                qtdRegInt = Convert.ToInt32(2000);
                            }
                            else
                            {
                                qtdRegInt = Convert.ToInt32(qtdRegIntegracao);
                            }
                        }
                        /**/

                        int seq = 0;
                        DateTime dataHora = DateTime.Now;
                        DateTime ultimoEnvioLogErro = DateTime.Now;
                        try
                        {
                            using (MonitorIntegracaoDAO dao = new MonitorIntegracaoDAO())
                            {
                                List<MonitorIntegracao> listaMonitorIntegracao = dao.Lista().Where(x => x.Integrado == false).ToList().OrderBy(y => y.DataRegistro).Take(qtdRegInt).ToList();

                                if (listaMonitorIntegracao != null)
                                {
                                    foreach (var item in listaMonitorIntegracao)
                                    {
                                        string x = item.XML;
                                        ControlsysWS.PessoaSAP colaborador = Newtonsoft.Json.JsonConvert.DeserializeObject<ControlsysWS.PessoaSAP>(item.XML);

                                        EndpointAddress endpointControlsys = new EndpointAddress(ENDPOINT_CONTROLSYS);

                                        ControlsysWS.ControlsysWebServiceSoapClient WS = new ControlsysWS.ControlsysWebServiceSoapClient();
                                        WS.Endpoint.Address = endpointControlsys;
                                        WS.NovidadesPessoaSAPSincrono(colaborador, item.IntegracaoSAP);

                                        item.Integrado = true;
                                        item.DataIntegracao = DateTime.Now;

                                        dao.Atualiza(item);

                                        System.GC.Collect();

                                        /*Log*/
                        /*
                        StreamWriter writer = new StreamWriter("C:\\Controlsyswsmi\\LogControlsyswsmi.txt", true);

                        seq++;
                        using (writer)
                        {
                            //if (dataHora.AddMinutes(1) <= DateTime.Now)
                            //{
                            // Escreve uma nova linha no final do arquivo
                            writer.WriteLine(String.Format("Contador: {0} Data/Hora: {1}", seq, DateTime.Now.ToString()));
                            dataHora = DateTime.Now;
                            //}                                                    
                        }
                        */
                    }
                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //System.Diagnostics.Debug.WriteLine("Log: " + Guid.NewGuid().ToString());                        
                            if (ultimoEnvioLogErro.AddMinutes(10) <= DateTime.Now)
                            {
                                using (LogErrorDAO logErrorDAO = new LogErrorDAO())
                                {
                                    LogError logError = new LogError();
                                    logError.Message = ex.Message;
                                    logError.StackTrace = ex.StackTrace;
                                    logError.Url = "controlsyswsmi\\monitorIntegracao";
                                    logError.DataRegistro = DateTime.Now;
                                    logError.UserName = System.Configuration.ConfigurationSettings.AppSettings["usuarioWSMI"];

                                    logErrorDAO.Adiciona(logError);

                                    ultimoEnvioLogErro = DateTime.Now;
                                }
                            }
                        }
                        
                    }
                }



                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    Startup.processing = false;
                }
            });

        }
    }
}
