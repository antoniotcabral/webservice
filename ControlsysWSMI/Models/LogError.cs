namespace ControlsysWSMI
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("LOG_ERROR")]
    public partial class LogError
    {
        [Key]
        public int Codigo { get; set; }

        public string Message { get; set; }

        [Required]
        public string StackTrace { get; set; }

        [Required]
        public string Url { get; set; }

        [Required]
        public string UserName { get; set; }

        public DateTime DataRegistro { get; set; }
    }
}
