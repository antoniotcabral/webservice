namespace ControlsysWSMI
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MONITORINTEGRACAO")]
    public partial class MonitorIntegracao
    {
        [Key]
        public int Codigo { get; set; }

        public int IntegracaoSAP { get; set; }

        public String XML { get; set; }

        public bool Integrado { get; set; }

        public DateTime DataIntegracao { get; set; }

        public DateTime DataRegistro { get; set; }
    }
}
