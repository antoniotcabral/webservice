﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ControlsysWSMI.DAO
{   
    public class MonitorIntegracaoDAO :IDisposable
    {
        private ContextoControlsys db = new ContextoControlsys();

        public void Adiciona(MonitorIntegracao monitorIntegracao)
        {            
            db.MonitorIntegracoes.Add(monitorIntegracao);
            db.SaveChanges();
        }

        public List<MonitorIntegracao> Lista()
        {
            return db.MonitorIntegracoes.ToList();
        }

        public MonitorIntegracao BuscarPorId(int id)
        {
            return db.MonitorIntegracoes.Find(id);
        }

        public void remover(int id)
        {
            MonitorIntegracao monitorIntegracao = db.MonitorIntegracoes.Find(id);
            db.MonitorIntegracoes.Remove(monitorIntegracao);
            db.SaveChanges();
        }

        public void Atualiza(MonitorIntegracao monitorIntegracao)
        {
            db.Entry(monitorIntegracao).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}