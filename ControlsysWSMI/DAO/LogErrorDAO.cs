﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ControlsysWSMI.DAO
{
    public class LogErrorDAO :IDisposable
    {
        private ContextoControlsys db = new ContextoControlsys();

        public void Adiciona(LogError logError)
        {
            db.LogError.Add(logError);
            db.SaveChanges();
        }

        public List<LogError> Lista()
        {
            return db.LogError.ToList();
        }

        public LogError BuscarPorId(int id)
        {
            return db.LogError.Find(id);
        }

        public void remover(int id)
        {
            LogError logError = db.LogError.Find(id);
            db.LogError.Remove(logError);
            db.SaveChanges();
        }

        public void Atualiza(LogError logError)
        {
            db.Entry(logError).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}