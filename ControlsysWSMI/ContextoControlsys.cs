namespace ControlsysWSMI
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ContextoControlsys : DbContext
    {
        public ContextoControlsys()
            : base("name=ContextoControlsys")
        {
        }
        
        public virtual DbSet<LogError> LogError { get; set; }        
        public virtual DbSet<MonitorIntegracao> MonitorIntegracoes { get; set; }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<MonitorIntegracao>()
           .Property(b => b.Codigo)
           .HasColumnName("CD_MonitorIntegracao");

            modelBuilder.Entity<MonitorIntegracao>()
           .Property(b => b.IntegracaoSAP)
           .HasColumnName("ID_Integracao_SAP");

            modelBuilder.Entity<MonitorIntegracao>()
           .Property(b => b.XML)
           .HasColumnName("ME_XML");

            modelBuilder.Entity<MonitorIntegracao>()
           .Property(b => b.Integrado)
           .HasColumnName("BL_INTEGRADO");

            modelBuilder.Entity<MonitorIntegracao>()
           .Property(b => b.DataIntegracao)
           .HasColumnName("DT_INTEGRACAO");

            modelBuilder.Entity<MonitorIntegracao>()
           .Property(b => b.DataRegistro)
           .HasColumnName("DT_REGISTRO");

            modelBuilder.Entity<LogError>()
           .Property(l => l.Codigo)
           .HasColumnName("CD_LOG_ERROR");

            modelBuilder.Entity<LogError>()
           .Property(l => l.Message)
           .IsUnicode(false)
           .HasColumnName("TX_MESSAGE");

            modelBuilder.Entity<LogError>()
           .Property(l => l.StackTrace)
           .IsUnicode(false)
           .HasColumnName("TX_STACK");

            modelBuilder.Entity<LogError>()
           .Property(l => l.Url)
           .IsUnicode(false)
           .HasColumnName("TX_URL");

            modelBuilder.Entity<LogError>()
           .Property(l => l.UserName)
           .HasColumnName("TX_USER");

            modelBuilder.Entity<LogError>()
           .Property(l => l.DataRegistro)
           .HasColumnName("DT_REGISTRO");
              
        }
    }
}
